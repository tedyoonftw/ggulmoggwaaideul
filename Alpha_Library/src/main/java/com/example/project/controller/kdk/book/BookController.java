package com.example.project.controller.kdk.book;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.controller.hjh.search.Pager;
import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.service.jm.RentService;
import com.example.project.service.jm.SearchKeyword;
import com.example.project.service.kdk.book.BookService;
import com.example.project.service.kdk.book.MemoService;

@Controller
@RequestMapping("kdk/book/book/*")
public class BookController {
	@Inject
	BookService bookService;

	@Inject
	MemoService memoService;
	
	@Inject
	RentService rentService;
	
	@Inject
	SearchKeyword keywordService;

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "1") int curPage) throws Exception {

		// 레코드 갯수 계산
		int count = bookService.countArticle();
		// 페이지 관련 설정
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<BookDTO> list = bookService.listBook(start, end); // 게시물 목록
		ModelAndView mav = new ModelAndView();
		HashMap<String, Object> map = new HashMap<>();
		map.put("list", list); // map에 자료 저장
		map.put("count", list.size());
		map.put("pager", pager); // 페이지 네비게이션을 위한 변수
		mav.setViewName("kdk/book/book_list");
		mav.addObject("map", map);
		/* mav.addObject("list", bookService.listBook(start, end)); */
		System.out.println(mav);
		return mav;
	}

	// http://localhost/project/book/book/detail/A100
	// 도서번호가 {book_code}에 전달됨
	@RequestMapping("detail")
	public ModelAndView detail(@RequestParam String book_code, ModelAndView mav, HttpSession session , @RequestParam(defaultValue="jm") String msg) {

		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			String renting = rentService.rentinfouserid(userid, book_code);
			if (renting != null) {
				mav.addObject("checkrent", renting);
			}
		}
		mav.addObject( "msg" , msg);
		mav.setViewName("kdk/book/book_detail");
		mav.addObject("userid", userid);
		mav.addObject("dto", bookService.detailBook(book_code));
		mav.addObject("list", memoService.listMemo(book_code));
		if(userid != null) {
			keywordService.insertTitleKeyword(book_code,userid);
		}
		return mav;
	}

	/* 관리자 페이지 부분 */
	@RequestMapping("book_insert.do")
	public String book_insert() {
		return "ym/book/book_insert";
	}

	/*@RequestMapping("insert.do")
	public String insert(BookDTO dto) {
		System.out.println("디티오" + dto);
		bookService.insertBook(dto);
		return "redirect:/ym/book/list.do";
	}*/

	@RequestMapping("update.do")
	public String update(@ModelAttribute BookDTO dto) throws Exception {

		if (dto != null) {
			bookService.updateBook(dto);
		}
		return "redirect:/ym/book/list.do";
	}

	@RequestMapping("delete.do")
	public String delete(String book_code) throws Exception {
		bookService.deleteBook(book_code);
		return "redirect:/ym/book/list.do";
	}

}
