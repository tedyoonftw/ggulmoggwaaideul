package com.example.project.controller.gm.email;

import java.util.Random;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project.model.gm.dto.EmailDTO;
import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.gm.email.EmailService;
import com.example.project.service.pu.mypage.MypageService;

@Controller
@RequestMapping("project/email/*")  
public class EmailController {
	
	@Inject
	EmailService emailService;
	
	@Inject
	MypageService mypageService;
	
	
	@RequestMapping("pw_send.do")
	public String pw_send(@ModelAttribute EmailDTO dto,UserDTO dto2,String userid,String email1,
			String email2,Model model) {
		
		
		userid = mypageService.pw_find(dto2);
		System.out.println("아이디이이이이이이이이이"+userid);
		if(userid != null) {
			String email = email1+"@"+email2;
			
			Random rand=new Random();
			int number1=rand.nextInt(10);
			
			String numberA = String.valueOf(number1);
			
			int number2=rand.nextInt(10);
			String numberB = String.valueOf(number2);
			
			
			int number3=rand.nextInt(10);
			String numberC = String.valueOf(number3);
			
			int number4=rand.nextInt(10);
			String numberD = String.valueOf(number4);
			
			String number=numberA+numberB+numberC+numberD;
			System.out.println("인증번호 : "+number);
			
			String senderName="운영자";
			String senderMail="homt1234@naver.com";
			String subject="인증번호를 인증해주세요.";
			String message="인증번호 ["+number+"]를 입력해 주세요.";
			/*String receiveMail=dto.getEmail();*/
			try {
				dto.setSenderName(senderName);
				dto.setSenderMail(senderMail);
				dto.setSubject(subject);
				dto.setMessage(message);
				dto.setReceiveMail(email);
				/*dto.setReceiveMail(receiveMail);*/
				System.out.println("dto : "+dto);
				emailService.sendMail(dto);
				model.addAttribute("message","이메일이 발송되었습니다.");
				model.addAttribute("number",number);
				model.addAttribute("userid",userid);
				model.addAttribute("email1",email1);
				model.addAttribute("email2",email2);
				
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("message","이메일 발송 실패...");
			}
			return "pu/mypage/pw_find";
		}else {
			model.addAttribute("message", "가입시 입력하신 회원정보를 다시 확인해주시기 바랍니다.");
			model.addAttribute("userid",userid);
			model.addAttribute("email1",email1);
			model.addAttribute("email2",email2);
			return "pu/mypage/pw_find";
		}
	}
	
	@RequestMapping("id_send.do")
	public String id_send(@ModelAttribute EmailDTO dto,UserDTO dto2,String username,String email1,
			String email2,Model model) {
		
		String userid = mypageService.id_find(dto2);
		System.out.println("아이디이이이이이이이이이"+userid);
		if(userid != null) {
			String email = email1+"@"+email2;
			
			Random rand=new Random();
			int number1=rand.nextInt(10);
			
			String numberA = String.valueOf(number1);
			
			int number2=rand.nextInt(10);
			String numberB = String.valueOf(number2);
			
			
			int number3=rand.nextInt(10);
			String numberC = String.valueOf(number3);
			
			int number4=rand.nextInt(10);
			String numberD = String.valueOf(number4);
			
			String number=numberA+numberB+numberC+numberD;
			System.out.println("인증번호 : "+number);
			
			String senderName="운영자";
			String senderMail="homt1234@naver.com";
			String subject="비밀번호를 인증해주세요.";
			String message="인증번호 ["+number+"]를 입력해 주세요.";
			/*String receiveMail=dto.getEmail();*/
			try {
				dto.setSenderName(senderName);
				dto.setSenderMail(senderMail);
				dto.setSubject(subject);
				dto.setMessage(message);
				dto.setReceiveMail(email);
				/*dto.setReceiveMail(receiveMail);*/
				System.out.println("dto : "+dto);
				emailService.sendMail(dto);
				model.addAttribute("message","이메일이 발송되었습니다.");
				model.addAttribute("number",number);
				model.addAttribute("userid",userid);
				model.addAttribute("username",username);
				model.addAttribute("email1",email1);
				model.addAttribute("email2",email2);
				
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("message","이메일 발송 실패...");
			}
			return "pu/mypage/id_find";
		}else {
			model.addAttribute("message", "가입시 입력하신 회원정보를 다시 확인해주시기 바랍니다.");
			model.addAttribute("userid",userid);
			model.addAttribute("username",username);
			model.addAttribute("email1",email1);
			model.addAttribute("email2",email2);
			return "pu/mypage/id_find";
		}
	}
	
	@RequestMapping("write.do")
	public String write(Model model,String confirm) {
		System.out.println("이메일 인증 : "+confirm);
		if(confirm != null) {
			model.addAttribute("confirm",confirm);
		}
		return "gm/email/write";
	}
	
	@RequestMapping("send.do")
	public String send(@ModelAttribute EmailDTO dto,Model model) {
		
		Random rand=new Random();
		int number1=rand.nextInt(10);
		
		String numberA = String.valueOf(number1);
		
		int number2=rand.nextInt(10);
		String numberB = String.valueOf(number2);
		
		
		int number3=rand.nextInt(10);
		String numberC = String.valueOf(number3);
		
		int number4=rand.nextInt(10);
		String numberD = String.valueOf(number4);
		
		String number=numberA+numberB+numberC+numberD;
		System.out.println("인증번호 : "+number);
		
		String senderName="운영자";
		String senderMail="homt1234@naver.com";
		String subject="이메일을 인증해주세요.";
		String htmlContent="";
		htmlContent += "<div style=\"background-color: #e4f1fe; font-size: 16px; \">";
		htmlContent += "<div align=\"center\" style=\"background-color: white; margin-left: 200px; margin-right: 200px; \">";
		htmlContent += "<img alt=\"\" src=\"http://localhost/project/images/mire.png\">";
		htmlContent += "<div align=\"left\">";
		htmlContent += "<br>안녕하세요. 미래 도서관입니다.<br>";
		htmlContent += "회원가입을 위해 이메일을 인증하려면 아래 링크를 클릭해주세요.<br><br>";
		htmlContent += "<a href='http://localhost/project/project/email/confirm.do?confirm=Y&receiveMail="+dto.getReceiveMail()+"' target='_blank' style=\"font-size: 20px; color: blue;\"><b>이메일 본인 인증하기</b></a>";
		htmlContent += "<br><br><br>";
		htmlContent += "<span style=\"font-size: 18px;\">미래재단 이사장  허준희</span><br><br>";
		htmlContent += "<img alt=\"\" src=\"http://localhost/project/images/junhee.png\" style=\"width: 160px; height: 200px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		htmlContent += "<img alt=\"\" src=\"http://localhost/project/images/esajang.jpg\" style=\"width: 360px; height: 100px;\">";
		htmlContent += "</div>";
		htmlContent += "</div>";
		htmlContent += "</div>";
		/*String receiveMail=dto.getEmail();*/
		try {
			dto.setSenderName(senderName);
			dto.setSenderMail(senderMail);
			dto.setSubject(subject);
			dto.setMessage(htmlContent);
			/*dto.setReceiveMail(receiveMail);*/
			System.out.println("dto : "+dto);
			emailService.sendMail(dto);
			model.addAttribute("message","이메일이 발송되었습니다.");
			model.addAttribute("number",number);
			model.addAttribute("receiveMail",dto.getReceiveMail());
			
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message","이메일 발송 실패...");
		}
		return "gm/email/write";
	}
	
	@RequestMapping("agree.do")
	public String agree() {
		return "gm/email/agree";
	}
	
	@RequestMapping("sendMail.do")
	public String sendMail(Model model,String receiveMail) {
		//메일 추가
		emailService.insertMail(receiveMail);
		model.addAttribute("receiveMail",receiveMail);
		return "gm/email/sendMail";
	}
	
	@RequestMapping("confirm.do")
	public String confirm(Model model,String confirm,String receiveMail) {
		
		if(confirm.equals("Y")) {
			emailService.updateMail(receiveMail);
		}
		
		return "gm/email/naver";
	}
	
	@RequestMapping("mailCheck.do")
	public String mailCheck(Model model, String receiveMail) {
		//메일인증 여부
		int count=emailService.confirm(receiveMail);
		if(count == 0) {
		model.addAttribute("confirm","N");
		} else {
		String[] email=receiveMail.split("@");
		String email1=email[0];
		String email2=email[1];
		//가입메일인지 체크
		String username=emailService.check(email1, email2);
		System.out.println("==============="+username);
			
			if(username != null) {//회원이면
				model.addAttribute("check","Y");
				return "pu/login/userlogin";
			}
		model.addAttribute("confirm","Y");
		}
		model.addAttribute("receiveMail",receiveMail);
		return "gm/email/confirm";
	}
}










