package com.example.project.controller.ym;

import javax.inject.Inject;

import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.service.ym.ChartService;



@RestController
@RequestMapping("chart/*")
public class ChartController {
	
	@Inject
	ChartService chartService;
	
	@RequestMapping("book_list.do")
	public ModelAndView chart1() {
		return new ModelAndView("chart/chart1");
	}
	
	@RequestMapping("book_list2.do")
	public ModelAndView chart2() {
		return new ModelAndView("chart/chart2");
	}
	
	@RequestMapping("book_list3.do")
	public ModelAndView chart3() {
		return new ModelAndView("chart/chart3");
	}
	
	@RequestMapping("urm_list.do")
	public ModelAndView chart4() {
		return new ModelAndView("chart/chart4");
	}
	
	@RequestMapping("book_group_list.do")
	public JSONObject book_group_list(){
		return chartService.getChartData2();
	}
	
	@RequestMapping("book_group_list2.do")
	public JSONObject book_group_list2(){
		return chartService.getChartData3();
	}
	
	@RequestMapping("book_group_list3.do")
	public JSONObject book_group_list3(){
		return chartService.getChartData4();
	}
	
	@RequestMapping("urm_list1.do")
	public JSONObject urm_list4(){
		return chartService.getChartData5();
	}
	
}
