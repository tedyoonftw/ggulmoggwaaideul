package com.example.project.controller.jm;


import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project.service.jm.InfoService;

@Controller
@RequestMapping("project/info/*")
public class infoController {
	
	@Inject
	InfoService infoService;
	

	@RequestMapping("visit.do")
	public String visit() {
	
	return "jm/info/visit";
	
	}
	
	@RequestMapping("book.do")
	public String chang() {
	
	return "jm/info/book";
	
	}
	
	@RequestMapping("cal.do")
	public String cal() {
	
	return "calendar";
	
	}
	
}
