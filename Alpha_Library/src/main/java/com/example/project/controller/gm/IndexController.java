package com.example.project.controller.gm;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.pu.login.UserService;

@Controller
@RequestMapping("project/gm/*")
public class IndexController {
	
	private static final Logger logger=LoggerFactory.getLogger(IndexController.class);
	
	@Inject
	UserService userService;
	
	@RequestMapping("insa.do")
	public String insa() {
		return "gm/insa/insa";
	}
	
	/*@RequestMapping("signup.do")
	public String signup(@ModelAttribute UserDTO dto) {
		userService.signup(dto);
		return "pu/login/signup";
	}
*/
}
