package com.example.project.controller.ysr.qna;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ysr.qna.dto.QnADTO;
import com.example.project.service.ysr.qna.Pager;
import com.example.project.service.ysr.qna.QnAService;

@Controller
@RequestMapping("qna/*")
public class QnAController {
	
	@Inject
	QnAService service;
	
	@RequestMapping("getAttach/{qno}")
	@ResponseBody //view가 아닌 데이터 자체를 리턴
	public List<String> getFile(@PathVariable int qno) {
		//System.out.println("번호:"+qno);
		List<String> f=service.getFile(qno);
		//System.out.println("리스트:"+f);
		return f;
	}
	
	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue="title") String search_option,
			@RequestParam(defaultValue="") String keyword,
			@RequestParam(defaultValue="1") int curPage) 
		throws Exception {
		
		int count=service.countArticle(search_option, keyword);
		System.out.println("갯수"+count);
		Pager pager=new Pager(count,curPage);
		System.out.println(pager);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		
		List<QnADTO> list=service.list(search_option, keyword, start, end);
		//System.out.println("목록:"+list);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager",pager);
		map.put("search_option", search_option);
		map.put("keyword",keyword);
		mav.setViewName("ysr/qna/qna_list");
		mav.addObject("map",map);
		
		return mav;
	}
	
	@RequestMapping("view.do")
	public ModelAndView view(int qno) throws Exception {
		ModelAndView mav=new ModelAndView();
		mav.setViewName("ysr/qna/qna_view");
		mav.addObject("dto",service.read(qno));
		return mav;
	}
	
	@RequestMapping("write.do")
	public String write() {
		return "ysr/qna/qna_write";
	}
	
	@RequestMapping("insert.do")
	public String insert(@ModelAttribute QnADTO dto
		, HttpSession session) throws Exception {
		String adminid=(String)session.getAttribute("adminid");
		dto.setAdminid(adminid);
		
		service.create(dto);
		return "redirect:/qna/list.do";
	}
	
	@RequestMapping("insert2.do")
	public String insert2(@ModelAttribute QnADTO dto
		, HttpSession session) throws Exception {
		String userid=(String)session.getAttribute("userid");
		dto.setUserid(userid);
		
		service.create2(dto);
		return "redirect:/qna/list.do";
	}
	
	@RequestMapping("update.do")
	public String update(QnADTO dto) throws Exception {
		if(dto != null) {
			//System.out.println("dto:"+dto);
			service.update(dto);
		}
		return "redirect:/qna/list.do";
	}
	
	@RequestMapping("delete.do")
	public String delete(int qno) throws Exception {
		service.delete(qno);
		return "redirect:/qna/list.do";
	}
}
