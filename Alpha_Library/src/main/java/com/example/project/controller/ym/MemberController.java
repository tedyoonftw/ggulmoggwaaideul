package com.example.project.controller.ym;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ym.dto.MemberDTO;
import com.example.project.service.ym.MemberService;
import com.example.project.service.ysr.qna.Pager;

@Controller
@RequestMapping("ym/membership/*")
public class MemberController {

	@Inject
	MemberService memberService;
	
	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue="1") int curPage,
			@RequestParam(defaultValue="username") String search_option,
			@RequestParam(defaultValue="") String keyword, ModelAndView mav) throws Exception {
		int count = memberService.countArticle(search_option,keyword);
		Pager pager=new Pager(count,curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();		
		
		List<MemberDTO> list = memberService.list(search_option,keyword,start,end);
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager",pager); // 페이지 네이게이션을 위한 변수
		map.put("search_option",search_option);
		map.put("keyword",keyword);
		mav.setViewName("ym/member/membership");
		mav.addObject("map",map);
		
		return mav;
	}
	
	@RequestMapping("listAll.do")
	public String list(){
			List<MemberDTO> list = memberService.listAll();
		return "ym/member/member_listall";
	}
	
	@RequestMapping("view.do")
	public ModelAndView view(ModelAndView mav, String userid) throws Exception {
		//System.out.println("아이디"+userid);
		mav.setViewName("ym/member/member_view");
		mav.addObject("dto",memberService.view(userid));
		return mav;
	}
	
	@RequestMapping("delete.do")
	public String delete(@RequestParam String userid) throws Exception {
		memberService.delete(userid);
		return "redirect:/ym/membership/list.do";
	}
	
	@RequestMapping("update.do")
	public String update(@ModelAttribute MemberDTO dto) throws Exception{
		//System.out.println("뭐여"+dto);
		if(dto!=null) {			
			memberService.update(dto);
		}
		return "redirect:/ym/membership/list.do";
	}
	
}
