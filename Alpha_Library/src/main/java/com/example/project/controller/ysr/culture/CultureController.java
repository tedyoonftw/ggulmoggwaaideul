package com.example.project.controller.ysr.culture;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.controller.ysr.upload.ImageUploadController;
import com.example.project.model.ysr.culture.dto.CultureDTO;
import com.example.project.service.ysr.culture.CultureService;
import com.example.project.service.ysr.culture.Pager;

@Controller
@RequestMapping("culture/*")
public class CultureController {

	@Inject
	CultureService cultureService;

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "title") String search_option,
			@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "1") int curPage)
			throws Exception {

		int count = cultureService.countArticle(search_option, keyword);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();
		int cno = 1;
		List<CultureDTO> list = cultureService.list(search_option, keyword, start, end);
		// System.out.println("리스트:"+list);
		ModelAndView mav = new ModelAndView();
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager", pager);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		mav.setViewName("ysr/culture/list");
		mav.addObject("map", map);
		return mav;
	}
	//문화프로그램 뷰
	@RequestMapping("view.do")
	public ModelAndView view(int cno,String username) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ysr/culture/view");
		//System.out.println("창섭이이이이이이"+username);
		mav.addObject("dto", cultureService.view(cno,username));
		System.out.println("");
		return mav;
	}
	//문화프로그램 수정 
	@RequestMapping("edit/{cno}")
	public ModelAndView edit(@PathVariable("cno") int cno,String username) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ysr/culture/edit");
		mav.addObject("dto", cultureService.view(cno,username));
		return mav;
	}

	@RequestMapping("update.do")
	public String update(@ModelAttribute CultureDTO dto) throws Exception {
		// System.out.println("얍:"+dto);
		if (dto != null) {
			cultureService.update(dto);
		}
		return "redirect:/culture/list.do";
	}

	@RequestMapping("delete.do")
	public String delete(int cno) throws Exception {
		cultureService.delete(cno);
		return "redirect:/culture/list.do";
	}

	@RequestMapping("write.do")
	public String write() {
		return "ysr/culture/write";
	}

	@RequestMapping("insert.do")
	public String insert(@ModelAttribute CultureDTO dto) throws Exception {
		
		cultureService.insert(dto);
		/*
		 * String ctn=dto.getUrl(); String[] arr=ctn.split("/"); String result="";
		 * for(String a : arr) { if(a.indexOf(".") != -1) { String[] arr2=a.split("\"");
		 * System.out.println(arr2[0]); result=arr2[0]; } }
		 * System.out.println("뭐게~~~~:"+result);
		 */
		 //System.out.println("이미지:"+dto);
		return "redirect:/culture/list.do";
	}

	private static final Logger logger = LoggerFactory.getLogger(CultureController.class);

	@RequestMapping("imageUpload.do")
	public void imageUpload(HttpServletRequest request, HttpServletResponse response, MultipartFile upload,
			CultureDTO dto) throws Exception {
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		// int seq = cultureService.insert(dto);
		// 업로드한 파일 이름
		String fileName = upload.getOriginalFilename();
		// 파일을 바이트 배열로 변환
		byte[] bytes = upload.getBytes();
		// 이미지를 업로드할 디렉토리(배포 디렉토리로 설정)
		String uploadPath = "D:\\work\\.metadata\\.plugins\\" + "org.eclipse.wst.server.core\\tmp0\\"
				+ "wtpwebapps\\project\\WEB-INF\\views\\images\\";
		OutputStream out = new FileOutputStream(new File(uploadPath + fileName));
		// 서버로 업로드
		out.write(bytes);
		// 클라이언트에 결과 표시
		String callback = request.getParameter("CKEditorFuncNum");
		// 서버=>클라이언트로 텍스트 전송
		PrintWriter printWriter = response.getWriter();
		String fileUrl = request.getContextPath() + "/images/" + fileName;
		// +"__@"+seq;
		// dto.setCno(seq);
		
		dto.setUrl(fileUrl);
		int result = cultureService.insertUrl(dto);
		
		if(result == 0) {
			printWriter.println("insert 실패");
		}
		// int success = cultureService.update(dto);
		// if(success == 0) {
		// printWriter.println("DB 업로드 실패");
		else {
		printWriter.println("<script>window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + fileUrl
				+ "','이미지가 업로드되었습니다:)')" + "</script>");
		}
		printWriter.flush();
		//System.out.println("디티오"+dto);
	}
}
