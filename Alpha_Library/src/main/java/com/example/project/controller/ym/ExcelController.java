package com.example.project.controller.ym;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.project.service.ym.ExcelService;

@Controller
public class ExcelController {
 
    @Autowired
    ExcelService excelService; 
 
    @RequestMapping("/excelDownload")
    public String excelTransform(Map<String, Object> ModelMap, HttpServletResponse response) throws Exception{
         
        response.setHeader("Content-disposition", "attachment; filename=" + "books" + ".xlsx"); //target명을 파일명으로 작성
 
        //엑셀에 작성할 리스트를 가져온다.
        List<Object> excelList= excelService.getAllObjects(); 
         
        //ExcelView(com.example.project.util.ym.ExcelView) 에 넘겨줄 값 셋팅
        ModelMap.put("excelList", excelList); 
 
        return "excelView"; //servlet-context.xml 에서 name이 excelView(com.example.project.util.ym.ExcelView)인것 호출
 
    }
 
}