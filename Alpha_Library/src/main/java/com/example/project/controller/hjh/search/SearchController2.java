package com.example.project.controller.hjh.search;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.hjh.book.dto.BookDTOj;
import com.example.project.model.jm.dto.KeywordDTO;
import com.example.project.model.jm.dto.TitleKeywordDTO;
import com.example.project.service.jm.SearchKeyword;
/*
public class SearchController2 {

	@Inject
	BookServicej bookServicej;

	@Inject
	SearchKeyword keywordService;

	/// 이거변경 0309 이거 또 변경 0310 정명
	@RequestMapping("search.do")
	public String search(HttpSession session, Model model) {

		String userid = (String) session.getAttribute("userid");
			if (userid != null) {
				List<KeywordDTO> keywords = keywordService.searchList(userid);
				model.addAttribute("keywords", keywords);
				List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
				System.out.println(titleKeyword);
				model.addAttribute("titleKeyword", titleKeyword);
				System.out.println();
			}
		return "hjh/search";
	}

	//// 이거 변경 0309 김정명 이거 또 변경 0310 정명
	@RequestMapping("list.do")
	public ModelAndView list(String search_option, String keyword, HttpSession session) {
		ModelAndView mav = new ModelAndView();
		List<BookDTOj> list = bookServicej.searchList(search_option, keyword);
		int count1 = bookServicej.count(search_option, keyword);

		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			keywordService.insertKeyword(userid, keyword);
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			mav.addObject("keywords", keywords);
			System.out.println(keywords);
		}

		switch (search_option) {

		case "title":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String title = dtoj.getTitle();
				if (title.indexOf(keyword) != -1) {

					dtoj.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}
			}

			break;

		case "author":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String author = dtoj.getTitle();
				if (author.indexOf(keyword) != -1) {

					dtoj.setTitle(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		case "publisher":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String publisher = dtoj.getTitle();
				if (publisher.indexOf(keyword) != -1) {

					dtoj.setTitle(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		case "all":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String author = dtoj.getTitle();
				String publisher = dtoj.getTitle();
				String title = dtoj.getTitle();
				if (author.indexOf(keyword) != -1 || publisher.indexOf(keyword) != -1 || title.indexOf(keyword) != -1) {
					dtoj.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					dtoj.setPublisher(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					dtoj.setAuthor(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		}

		mav.setViewName("hjh/search_list");
		Map<String, Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		map.put("list", list);
		map.put("count1", count1);
		mav.addObject("map", map);
		return mav;
	}

	@RequestMapping("search2.do")
	public String search2(HttpSession session, Model model) {

		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			model.addAttribute("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			model.addAttribute("titleKeyword", titleKeyword);
		}
		return "hjh/search2";
	}

	@RequestMapping("list2.do")
	public ModelAndView list2(@ModelAttribute BookDTOj dtoj, String order_option, String date1, String date2,
			HttpSession session) {
		List<BookDTOj> list2 = bookServicej.search2List(dtoj, order_option, date1, date2);
		int count2 = bookServicej.count2(dtoj, order_option, date1, date2);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("hjh/search2_list");
		Map<String, Object> map = new HashMap<>();
		map.put("dtoj", dtoj);
		map.put("order_option", order_option);
		map.put("date1", date1);
		map.put("date2", date2);
		map.put("list2", list2);
		map.put("count2", count2);
		mav.addObject("map", map);
		return mav;
	}

	@RequestMapping("search3.do")
	public String search3(HttpSession session, Model model) {

		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			model.addAttribute("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			System.out.println(titleKeyword);
			model.addAttribute("titleKeyword", titleKeyword);
			System.out.println();
		}
		return "hjh/search3";
	}

	@RequestMapping("list3.do")
	public ModelAndView list3(@RequestParam String group_code, String period) {
		// Calendar cal = Calendar.getInstance();
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
		long now = System.currentTimeMillis();
		// Long today = cal.getTimeInMillis();
		long realperiod = 0;
		if (period.equals("1week")) {
			realperiod = now - 1000 * 60 * 60 * 24 * 7; // 오늘-1주일 (일주일전)
		} else if (period.equals("2week")) {
			realperiod = now - 1000 * 60 * 60 * 24 * 7 * 2;
		} else if (period.equals("3week")) {
			realperiod = now - 1000 * 60 * 60 * 24 * 7 * 3;
		} else if (period.equals("1month")) {
			realperiod = now - 1000 * 60 * 60 * 24 * 30;
		}
		Date date = new Date(realperiod);
		String realrealperiod = SDF.format(date);

		List<BookDTOj> list3 = bookServicej.search3List(group_code, period, realrealperiod);
		int count3 = bookServicej.count3(group_code, period, realrealperiod);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("hjh/search3_list");
		Map<String, Object> map = new HashMap<>();
		map.put("list3", list3);
		map.put("group_code", group_code);
		map.put("period", period);
		map.put("count3", count3);
		mav.addObject("map", map);
		return mav;
	}

}
*/