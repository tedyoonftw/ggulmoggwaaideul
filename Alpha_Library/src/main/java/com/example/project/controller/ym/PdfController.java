package com.example.project.controller.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ym.dto.AdminBookDTO;
import com.example.project.service.ym.AdminBookService;
import com.example.project.service.ym.PdfService;
 
@Controller
@RequestMapping("ym/pdf/*")
public class PdfController {

	@Inject
	PdfService pdfService;
	
	@Inject
	AdminBookService adminbookService;
	
	@RequestMapping("pdf.do")
	public ModelAndView pdf() throws Exception {
		String result = pdfService.createPdf();
		return new ModelAndView("ym/book/result", "message", result);
	}
	
	@RequestMapping("downloadPDF")
	public ModelAndView downloadPDF() throws Exception {
		List<AdminBookDTO> items=adminbookService.listAll();
		return new ModelAndView("pdfView","listBooks",items);
	}

}  
