package com.example.project.controller.pu.login;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.pu.admin.dto.AdminDTO;
import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.pu.admin.AdminService;

@Controller
@RequestMapping("pu/login/admin/*")
public class AdminController {
	
	private static final Logger logger=LoggerFactory.getLogger(AdminController.class);
	
	@Inject
	AdminService adminService;
	
	@RequestMapping("login.do")
	public String login() {
		return "pu/login/userlogin";
	}
	
	
	@RequestMapping("login_check.do")
	public String login_check(UserDTO dto, HttpSession session
			,Model model) {
		String adminid = dto.getUserid();
		String adminpw = dto.getPasswd();
		AdminDTO dto1 = new AdminDTO();
		dto1.setAdminid(adminid);
		dto1.setAdminpw(adminpw);
		boolean result=adminService.loginCheck(dto1, session);
		if(result) {
			return "redirect:/";
		}else {
			model.addAttribute("message","error");
			return "pu/login/userlogin";
		}
	}
	
	
	@RequestMapping("logout.do")
	public String logout(HttpSession session, ModelAndView mav) {
		adminService.logout(session);
		return "redirect:/";
	}
}