package com.example.project.controller.pu.login;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.pu.login.UserService;

@Controller
@RequestMapping("pu/login/user/*")
public class UserController {
	
	private static final Logger logger=LoggerFactory.getLogger(UserController.class);
	
	@Inject
	UserService userService;
	
	@RequestMapping("login.do")
	public String login() {
		return "pu/login/userlogin";
	}
	
	/*@RequestMapping("login_check.do")
	public ModelAndView login_check(UserDTO dto, HttpSession session) {
		boolean result=userService.loginCheck(dto, session);
		ModelAndView mav=new ModelAndView();
		if(result) {
			mav.setViewName("index");
		}else {
			mav.setViewName("pu/login/userlogin");
			mav.addObject("message", "error");
		}
		return mav;
	}*/
	
	@RequestMapping("login_check.do")
	public String login_check(UserDTO dto, HttpSession session
			,Model model) {
		boolean result=userService.loginCheck(dto, session);
		System.out.println(result);
		if(result) {
			return "redirect:/";
		}else {
			model.addAttribute("message","error");
			return "pu/login/userlogin";
		}
	}
	
	
	/*@RequestMapping("logout.do")
	public ModelAndView logout(HttpSession session, ModelAndView mav) {
		userService.logout(session);
		mav.setViewName("pu/login/userlogin");
		mav.addObject("message", "logout");
		return mav;
	}*/
	
	@RequestMapping("logout.do")
	public String logout(HttpSession session) {
		userService.logout(session);
		return "redirect:/";
	}
	
	
	@RequestMapping("insert.do")
	public String signup(@ModelAttribute UserDTO dto) {
		userService.signup(dto);
		return "redirect:/pu/login/user/login.do";
	}
	
	@RequestMapping("signup.do")
	public String join() {
		return "pu/login/signup";
	}
	
	@RequestMapping("check_id.do")
	public String check_id(UserDTO dto,String userid,Model model) {
		String username=userService.check_id(userid);
		System.out.println("유저네임:"+username);
		if(username==null) {
			model.addAttribute("message","use");
			model.addAttribute("dto",dto);
		}else {
			model.addAttribute("message","overlap");
			model.addAttribute("dto",dto);
		}
		return "pu/login/signup";
	}

}