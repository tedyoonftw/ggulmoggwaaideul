package com.example.project.controller.pu.mypage;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.hjh.mypage.MypageServicej;
import com.example.project.service.jm.RentService;
import com.example.project.service.pu.login.UserService;
import com.example.project.service.pu.mypage.MypageService;

@Controller
@RequestMapping("pu/page/*")
public class PageController {
	
	@Inject
	RentService rentService;

	@Inject
	MypageService mypageService;
	
	@Inject
	MypageServicej mypageServicej;
	
	@Inject
	UserService userService;
	
	@RequestMapping("bye.do")
	public ModelAndView bye(ModelAndView mav,HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		mypageService.bye(userid);
		List<MypageDTOj> nowlist=mypageServicej.nowrent(userid);
		int nowrentcount = rentService.nowRentCount(userid);
		System.out.println("대출권수"+nowrentcount);
		mav.addObject("rentcount", nowrentcount); // 대출중인 권수 확인
		mav.addObject("dto",mypageService.bye(userid));
		mav.setViewName("pu/mypage/usercancel");
		return mav;
	}
	
	@Transactional
	@RequestMapping("cancel.do")
	public String cancel(String userid,HttpSession session) {
		userService.logout(session);
		mypageService.cancel(userid);
		mypageService.memo1(userid);
		mypageService.applicant(userid);
		mypageService.qna(userid);
		mypageService.readingroom(userid);
		mypageService.urm(userid);
		mypageService.hopebook(userid);
		mypageService.cart(userid);
		return "redirect:/";
	}
	
	@RequestMapping("pw.do")
	public ModelAndView pw(ModelAndView mav,HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		mypageService.passwd(userid);
		mav.setViewName("pu/mypage/pw_modify");
		mav.addObject("dto",mypageService.passwd(userid));
		return mav;
	}
	
	@RequestMapping("passwd.do")
	public String pw_modify(UserDTO dto) {
		mypageService.pw_modify(dto);
		return "redirect:/pu/page/pw.do";
	}
	
	@RequestMapping("list.do")
	public ModelAndView list(ModelAndView mav,HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		mypageService.userinfo(userid);
		mav.setViewName("pu/mypage/userinfo");
		mav.addObject("dto", mypageService.userinfo(userid));
		return mav;
	}
	
	@RequestMapping("update.do")
	public String update(UserDTO dto) {
		mypageService.info_modify(dto);
		return "redirect:/pu/page/list.do";
	}
	
	@RequestMapping("choose.do")
	public String choose() {
		return "pu/mypage/choose";
	}
	
	@RequestMapping("id_page.do")
	public String id_page() {
		return "pu/mypage/id_find";
	}
	
	@RequestMapping("pw_page.do")
	public String pw_page() {
		return "pu/mypage/pw_find";
	}
	
	//추가된 부분
	@RequestMapping("pw_re.do")
	public String pw_re(UserDTO dto) {
		mypageService.pw_modify(dto);
		return "redirect:/pu/login/user/login.do";
	}
	
	
	@RequestMapping("id_find.do")
	public String id_find(String userid,Model model) {
		model.addAttribute("userid", userid);
		return "pu/mypage/id";
	}
	
	@RequestMapping("pw_find.do")
	public String pw_find(String userid,Model model) {
		model.addAttribute("userid", userid);
		return "pu/mypage/pw_re";
	}

	
}
