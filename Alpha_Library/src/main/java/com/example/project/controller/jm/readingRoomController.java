package com.example.project.controller.jm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.jm.dto.ReadingRoomDTO;
import com.example.project.model.jm.dto.urmDTO;
import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.service.jm.ReadingService;

@Controller
@RequestMapping("/project/readingroom/*")
public class readingRoomController {

	@Inject
	ReadingService readingService;

	@RequestMapping("list.do")
	public String visit(Model model, HttpSession session) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String userid = (String) session.getAttribute("userid");

		List<ReadingRoomDTO> gigang = readingService.logoutcheck();

		

		if (gigang != null) {

			for (ReadingRoomDTO dto : gigang) {
				Date checktime = sdf.parse(dto.getEtime());
				Date today = sdf.parse(sdf.format(cal.getTimeInMillis()));
				if (today.getTime() >= checktime.getTime()) {
					readingService.logout(dto.getSeatno());
					urmDTO urmdto = new urmDTO();
					urmdto.setStime(dto.getStime());
					urmdto.setEtime(dto.getEtime());
					urmdto.setUserid(dto.getUserid());
					readingService.updatereservationinfo(urmdto);
					
				}
			}
		}

		if (userid != null) {
			String sadragon = readingService.seatedmemberlist(userid);
			model.addAttribute("search", sadragon);
		}

		List<ReadingRoomDTO> list = readingService.list();
		model.addAttribute("count", readingService.countseat());
		model.addAttribute("list", list);
		return "jm/readingroom/seat";

	}

	@RequestMapping("reservation.do/{seatno}")
	public ModelAndView reservation(@PathVariable int seatno, ModelAndView mav, HttpSession session) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		ReadingRoomDTO reading = readingService.viewseat(seatno);
		String userid = (String) session.getAttribute("userid");

		if (reading.getPrereserve().equals("Y")) {
			mav.addObject("message", "등록중 다른사람이 이용등록을 하고있습니다. 다른좌석을 선택하시거나, 잠시후 다시 시도해 주세요");
			mav.setViewName("jm/readingroom/result");

		} else {

			long currenttime = cal.getTimeInMillis();
			String starttime = sdf.format(currenttime);
			String endtime = sdf.format(currenttime + 1000 * 60 * 1);
			UserDTO user = readingService.memberinfo(userid);
			readingService.prereserve(seatno, userid);
			Map<String, Object> dto = new HashMap<>();
			dto.put("sadragon", reading.getSadragon());
			dto.put("starttime", starttime);
			dto.put("endtime", endtime);
			dto.put("userid", userid);
			dto.put("gender", user.getGender());
			dto.put("username", user.getUsername());
			dto.put("seatno", seatno);
			mav.addObject("dto", dto);
			mav.setViewName("jm/readingroom/seatinfo");

		}
		return mav;
	}

	@RequestMapping("reserve.do")
	public String reserve(Model model, String userid, int seatno, int gender, String stime, String etime,
			HttpSession session) {
		ReadingRoomDTO dto = new ReadingRoomDTO();
		String address = "";
		
		List<String> checkOtherPage = readingService.checkprereserve();

		for (int i = 0; i < checkOtherPage.size(); i++) {
			if (checkOtherPage != null && checkOtherPage.get(i).equals(userid)) {
				readingService.viewOtherPage(checkOtherPage.get(i));
			}
		}

		dto.setUserid(userid);
		dto.setSeatno(seatno);
		dto.setGender(gender);
		dto.setStime(stime);
		dto.setEtime(etime);
		urmDTO urmdto = new urmDTO();
		urmdto.setStime(stime);
		urmdto.setUserid(userid);
		int result = readingService.reservation(dto);
		readingService.reserveinfo(urmdto);
		readingService.afterreserve(seatno);
		model.addAttribute("message", "등록되었습니다.");
		address = "jm/readingroom/result";

		return address;
	}

	// 사용중인지 아닌지
	@RequestMapping("sadragoninfo.do/{seatno}")
	public String view(@PathVariable int seatno, Model model, HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		UserDTO user = readingService.memberinfo(userid);
		ReadingRoomDTO reading = readingService.viewseat(seatno);

		Map<String, Object> dto = new HashMap<>();
		dto.put("sadragon", reading.getSadragon());
		dto.put("starttime", reading.getStime());
		dto.put("endtime", reading.getEtime());
		dto.put("userid", userid);
		dto.put("gender", user.getGender());
		dto.put("username", user.getUsername());
		dto.put("seatno", seatno);
		model.addAttribute("dto", dto);

		return "jm/readingroom/seatinfo";
	}

	@RequestMapping("logout.do/{seatno}")
	public String logout(@PathVariable int seatno, Model model, HttpSession session) {
		ReadingRoomDTO reading = readingService.viewseat(seatno);
		String userid = reading.getUserid();
		String stime = reading.getStime();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		long currenttime = cal.getTimeInMillis();
		String etime = sdf.format(currenttime);
		urmDTO urmdto = new urmDTO();
		urmdto.setUserid(userid);
		urmdto.setStime(stime);
		urmdto.setEtime(etime);
		readingService.updatereservationinfo(urmdto);
		readingService.logout(seatno);
		model.addAttribute("message", "로그아웃되었습니다");
		return "jm/readingroom/result";
	}

	@RequestMapping("close.do/{seatno}")
	public String close(@PathVariable int seatno, Model model, HttpSession session) {
		readingService.afterreserve(seatno);
		model.addAttribute("message", "취소되었습니다.");
		return "jm/readingroom/result";
	}

}
