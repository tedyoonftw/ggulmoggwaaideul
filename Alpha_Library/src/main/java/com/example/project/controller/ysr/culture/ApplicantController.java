package com.example.project.controller.ysr.culture;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.model.ysr.culture.dto.ApplicantDTO;
import com.example.project.service.jm.ReadingService;
import com.example.project.service.ysr.culture.ApplicantService;

@Controller
@RequestMapping("applicant/*")
public class ApplicantController {
	
	@Inject
	ApplicantService Service;
	
	@Inject
	ReadingService readingService;
	
	@RequestMapping("list.do")
	public ModelAndView list(ModelAndView mav) throws Exception {
		List<ApplicantDTO> list=Service.list();
		mav.setViewName("hjh/mypage/applicant_list");
		mav.addObject("list", list);
		return mav;
	}
	
	@Transactional
	@RequestMapping("write.do")
	public String write(int cno,String title ,Model model,HttpSession session) throws Exception {
		String userid =  (String) session.getAttribute("userid");
		UserDTO dto = readingService.memberinfo(userid);
		//System.out.println("세련이가 엄청 귀여워요오오오오오오오오오오오 씨익"+dto.getUsername());
		String username = dto.getUsername();
		String result=Service.check(userid,cno);
		if(result==null) {
			Map <String ,Object> dto2 = new HashMap<>();
			dto2.put("username", username);
			dto2.put("title", title);
			dto2.put("cno", cno);
			dto2.put("userid", userid);
			//System.out.println("번호오오오"+cno);
			/*model.addAttribute("username", username);
		model.addAttribute("title", title);
		model.addAttribute("cno", cno);*/
			model.addAttribute("dto", dto2);
			//System.out.println("진짜 배고프니까 싸게싸게 가자"+dto2);
			return "ysr/culture/applicant_insert";
		}else {
			model.addAttribute("message","error");
			return "ysr/culture/list";
		}
		
	}
	
	@RequestMapping("insert.do")
	public String insert(@ModelAttribute ApplicantDTO dto, String username) throws Exception {
		Service.insert(dto);
		return "redirect:/culture/list.do";
	}
	
	@Transactional
	@RequestMapping("delete.do")
	public String delete(@RequestParam int ano) throws Exception {
		Service.delete(ano);
		
		//System.out.println("나와라!!!!!"+ano);
		return "redirect:/applicant/list.do";
	}
}
