package com.example.project.controller.ysr.notice;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ysr.notice.dto.NoticeDTO;
import com.example.project.service.ysr.notice.NoticeService;
import com.example.project.service.ysr.notice.Pager;

@Controller
@RequestMapping("notice/*")
public class NoticeController {
	
	@Inject
	NoticeService noticeService;
	
	@RequestMapping("list.do")
	public ModelAndView list(
		@RequestParam(defaultValue="title") String search_option,
		@RequestParam(defaultValue="") String keyword,
		@RequestParam(defaultValue="1") int curPage) 
		throws Exception {
		
		int count=noticeService.countArticle(search_option, keyword);
		Pager pager=new Pager(count,curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		
		List<NoticeDTO> list=noticeService.list(search_option, keyword, start, end);
		//System.out.println(list);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count",count);
		map.put("pager",pager);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		mav.setViewName("ysr/notice/list");
		mav.addObject("map",map);
		
		return mav;
	}
	
	@RequestMapping("view.do")
	public ModelAndView view(int nno, HttpSession session)
		throws Exception {
		noticeService.increaseViewcnt(nno,session);
		ModelAndView mav=new ModelAndView();
		mav.setViewName("ysr/notice/view");
		mav.addObject("dto",noticeService.read(nno));
		return mav;
	}
	
	@RequestMapping("write.do")
	public String write() {
		return "ysr/notice/write";
	}
	
	@RequestMapping("edit/{nno}")
	public ModelAndView edit(@PathVariable("nno") int nno) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("ysr/notice/edit");
		mav.addObject("dto", noticeService.read(nno));
		return mav;
	}
	
	@RequestMapping("update.do")
	public String update(NoticeDTO dto) throws Exception {
		if(dto!=null) {
			noticeService.update(dto);
		}
		return "redirect:/notice/list.do";
	}
	
	@RequestMapping("delete.do")
	public String delete(String fullName,int nno) throws Exception {
		noticeService.delete(fullName,nno);
		return "redirect:/notice/list.do";
	}
	
	@RequestMapping("insert.do")
	public String insert(@ModelAttribute NoticeDTO dto
		, HttpSession session) throws Exception {
		String adminid=(String)session.getAttribute("adminid");
		dto.setAdminid(adminid);
		noticeService.create(dto);
		return "redirect:/notice/list.do";
		
	}
	
	@RequestMapping("getFile/{nno}")
	@ResponseBody
	public List<String> getFile(@PathVariable int nno) {
		return noticeService.getFile(nno);
	}
}
