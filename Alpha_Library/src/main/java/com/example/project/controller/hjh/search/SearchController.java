package com.example.project.controller.hjh.search;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.hjh.book.dto.BookDTOj;
import com.example.project.model.jm.dto.KeywordDTO;
import com.example.project.model.jm.dto.TitleKeywordDTO;
import com.example.project.service.hjh.book.BookServicej;
import com.example.project.service.hjh.book.Pager;
import com.example.project.service.jm.SearchKeyword;

@Controller
@RequestMapping("/hjh/book/*")
public class SearchController {

	@Inject
	BookServicej bookServicej;
	
	@Inject
	SearchKeyword keywordService;
	
	@RequestMapping("bookwitch/{book_code}")
	public String bookwitch(@PathVariable String book_code, Model model){
		BookDTOj bookdto=bookServicej.bookinfo(book_code);
		model.addAttribute("bookdto",bookdto);
		return "hjh/search/witchimg";
	}

	@RequestMapping("search.do")
	public String search(HttpSession session, Model model) {

		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			model.addAttribute("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			model.addAttribute("titleKeyword", titleKeyword);
		}

		model.addAttribute("ingikeywords", ingikeyword);
		return "hjh/search/search";
	}

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "1") int curPage,
			@RequestParam(defaultValue = "all") String search_option, @RequestParam(defaultValue = "") String keyword,HttpSession session) {
		
		String userid = (String) session.getAttribute("userid");

		if (keyword != null && userid != null && !keyword.equals("")) {
			keywordService.insertKeyword(userid, keyword); // 최근검색어 목록에 집어넣음

		}

		if (keyword != null && !keyword.equals("")) {

			String findkey = keywordService.findkeyword(keyword); // 기존검색어에 있나 찾음
			if (findkey == null) {
				keywordService.newkeyword(keyword);
			} else if (findkey != null) {
				keywordService.viewnoplus(keyword);
			}
		}
		
		
		/////////////////////////////////////////////////////////////// 여기까지 간략검색
		/////////////////////////////////////////////////////////////// ////////////////////////////////
		
		
		int count = bookServicej.count(search_option, keyword);
		// 페이지 관련 설정
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<BookDTOj> list = bookServicej.searchList(search_option, keyword, start, end);

		switch (search_option) {

		case "title":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String title = dtoj.getTitle();
				if (title.indexOf(keyword) != -1) {

					dtoj.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}
			}

			break;

		case "author":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String author = dtoj.getAuthor();
				if (author.indexOf(keyword) != -1) {

					dtoj.setAuthor(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		case "publisher":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String publisher = dtoj.getPublisher();
				if (publisher.indexOf(keyword) != -1) {

					dtoj.setPublisher(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		case "all":

			for (int i = 0; i < list.size(); i++) {
				BookDTOj dtoj = list.get(i);
				String author = dtoj.getAuthor();
				String publisher = dtoj.getPublisher();
				String title = dtoj.getTitle();
				if (author.indexOf(keyword) != -1 || publisher.indexOf(keyword) != -1 || title.indexOf(keyword) != -1) {
					dtoj.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					dtoj.setPublisher(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					dtoj.setAuthor(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
					list.set(i, dtoj);
				}

			}
			break;

		}

		ModelAndView mav = new ModelAndView();
		////////////////////////////////////////////////////////////////////
		if (userid != null) {
			List<KeywordDTO> recentkeywords = keywordService.searchList(userid);
			mav.addObject("keywords", recentkeywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			mav.addObject("titleKeyword", titleKeyword);
		}

		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		///////////////////////////////////////////////////////// 인기검색어는 누구나 떠야하니까
		mav.setViewName("hjh/search/search_list");
		Map<String, Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		map.put("list", list);
		map.put("count1", count);
		map.put("pager", pager);
		mav.addObject("map", map);
		mav.addObject("ingikeywords", ingikeyword);
		return mav;
	}

	@RequestMapping("search2.do")
	public String search2(HttpSession session, Model model) {
		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			model.addAttribute("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			model.addAttribute("titleKeyword", titleKeyword);
		}
		model.addAttribute("ingikeywords", ingikeyword);
		return "hjh/search/search2";
	}

	@RequestMapping("list2.do")
	public ModelAndView list2(@RequestParam(defaultValue = "1") int curPage, @ModelAttribute BookDTOj dtoj,
			String order_option, String date1
, String date2, HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		
		int count = bookServicej.count2(dtoj, order_option, date1, date2);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<BookDTOj> list = bookServicej.search2List(dtoj, order_option, date1, date2, start, end);
		
			String changetitle = dtoj.getTitle();
			String changeauthor = dtoj.getAuthor();
			String changepulisher = dtoj.getPublisher();
		for (int i = 0; i < list.size(); i++) {
			dtoj = list.get(i);
			String author = dtoj.getAuthor(); // 장문 
			String publisher = dtoj.getPublisher(); // 장문
			String title = dtoj.getTitle(); //여긴 장문이니까
			
			if (author.indexOf(changeauthor) != -1) {
				dtoj.setAuthor(author.replace(changeauthor, "<font color='red;'>" + changeauthor + "</font>"));
			
			
			}  
			if (publisher.indexOf(changepulisher) != -1) {
				dtoj.setPublisher(publisher.replace(changepulisher, "<font color='red;'>" + changepulisher + "</font>"));
			
			
			}  
			if (title.indexOf(changetitle) != -1) {
				dtoj.setTitle(title.replace(changetitle, "<font color='red;'>" + changetitle + "</font>"));
			}

			list.set(i, dtoj);
		}
		
		ModelAndView mav = new ModelAndView();
		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			mav.addObject("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			mav.addObject("titleKeyword", titleKeyword);
		}
		
		mav.setViewName("hjh/search/search_list");
		Map<String, Object> map = new HashMap<>();
		map.put("dtoj", dtoj);
		map.put("order_option", order_option);
		map.put("date1", date1);
		map.put("date2", date2);
		map.put("list", list);
		map.put("count2", count);
		map.put("ingikeywords", ingikeyword);
		map.put("pager", pager);
		mav.addObject("map", map);
		return mav;
	}

	@RequestMapping("search3.do")
	public String search3(HttpSession session, Model model) {
		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		String userid = (String) session.getAttribute("userid");
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			model.addAttribute("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			model.addAttribute("titleKeyword", titleKeyword);
		}
		model.addAttribute("ingikeywords", ingikeyword);
		return "hjh/search/search3";
	}

	@RequestMapping("list3.do")
	public ModelAndView list3(@RequestParam(defaultValue = "1") int curPage, @RequestParam String group_code,
			String period ,HttpSession session) {
		String userid = (String) session.getAttribute("userid");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
		Long today = cal.getTimeInMillis();
		long realperiod = 0;
		
		if (period.equals("1week")) {
			realperiod = today - 1000 * 60 * 60 * 24 * 7; // 오늘-1주일 (일주일전)
		} else if (period.equals("2week")) {
			realperiod = today - 1000 * 60 * 60 * 24 * 7 * 2;
		} else if (period.equals("3week")) {
			realperiod = today - 1000 * 60 * 60 * 24 * 7 * 3;
		} else if (period.equals("1month")) {
			cal.set(Calendar.MONTH, Calendar.MONTH - 1);
			today = cal.getTimeInMillis();
			realperiod = today;
		}
		Date date = new Date(realperiod);
		String realrealperiod = SDF.format(date);
		System.out.println("realperiod:" + realperiod);
		System.out.println("date:" + date);
		System.out.println("realrealperiod:" + realrealperiod);

		int count = bookServicej.count3(group_code, period, realrealperiod);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();
		List<BookDTOj> list = bookServicej.search3List(group_code, period, realrealperiod, start, end);
	
		ModelAndView mav = new ModelAndView();
		List<KeywordDTO> ingikeyword = keywordService.getIngiKeyword();
		if (userid != null) {
			List<KeywordDTO> keywords = keywordService.searchList(userid);
			mav.addObject("keywords", keywords);
			List<TitleKeywordDTO> titleKeyword = keywordService.getTitleKeyword(userid);
			mav.addObject("titleKeyword", titleKeyword);
		}
		mav.setViewName("hjh/search/search_list");
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("group_code", group_code);
		map.put("ingikeywords", ingikeyword);
		map.put("period", period);
		map.put("count3", count);
		map.put("pager", pager);
		mav.addObject("map", map);
		return mav;
	}

}
