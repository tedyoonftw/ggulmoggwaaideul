package com.example.project.controller.kdk.book;



import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project.service.kdk.book.BookService;
import com.example.project.service.kdk.book.MemoService;



@Controller
@RequestMapping("kdk/book/memo/*")
public class MemoController {
	
	@Inject
	MemoService memoService;
	
	@Inject
	BookService bookService;
	
	/*@RequestMapping("list.do")
	public ModelAndView list(ModelAndView mav, String book_code) {
		List<MemoDTO> list=memoService.listMemo(book_code);
		mav.setViewName("kdk/book/book_detail");
		mav.addObject("list", list);
		return mav;
	}*/
	
	
/*	@RequestMapping("insert.do")
	public String insert(@ModelAttribute MemoDTO dto) {
		memoService.insert(dto);
		return "redirect:/kdk/book/book/list2.do";
	}*/

	
	@RequestMapping("insert.do")
	public String insert(String userid, String memo, String book_code) {
		memoService.insertMemo(userid, memo, book_code);
	/*	String book_code = dto.getBook_code();*/
		return "redirect:/kdk/book/book/detail/"+book_code;
	}
	
	/*@RequestMapping("delete.do")
	public String delete(HttpSession session, int idx, String book_code) {
		String userid = (String) session.getAttribute("userid");
		if(userid != null) {
			memoService.deleteMemo(idx);
		}
		return "redirect:/kdk/book/book/detail/"+book_code;
	}*/
	
	@RequestMapping("westpyeongsakjae.do")
	public String delete(int idx,HttpSession session, String book_code) {
		String userid = (String) session.getAttribute("userid");
		if(userid != null || session.getAttribute("adminid") != null ) {
		//레코드 삭제 처리
		memoService.deleteMemo(idx);
		}
		//삭제 완료 후 목록으로 이동
		return "redirect:/kdk/book/book/detail/"+book_code;
	}
	
	/*@RequestMapping("MemoUpdate/{idx}")
	public String update(@PathVariable int idx, @ModelAttribute MemoDTO dto, String book_code) {
		//레코드 삭제 처리
		memoService.updateMemo(dto);
		
		//삭제 완료 후 목록으로 이동
		return "redirect:/kdk/book/book/detail/"+book_code;
	}*/
	
	
	
}
