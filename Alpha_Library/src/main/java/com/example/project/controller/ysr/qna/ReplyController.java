package com.example.project.controller.ysr.qna;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ysr.qna.dto.ReplyDTO;
import com.example.project.service.ysr.qna.ReplyService;

@RestController
@RequestMapping("/reply/*")
public class ReplyController {
	
	@Inject
	ReplyService replyService;
	
	@RequestMapping("list.do")
	public ModelAndView list(int qno,ModelAndView mav) {
		List<ReplyDTO> list=replyService.list(qno);
		mav.setViewName("ysr/qna/reply_list");
		mav.addObject("list",list);
		return mav;
	}
	
	@RequestMapping("list_json.do")
	@ResponseBody
	public List<ReplyDTO> list_json(int qno) {
		return replyService.list(qno);
	}
	
	@RequestMapping("insert.do")
	public void insert(ReplyDTO dto,int qno, HttpSession session) throws Exception {
		String adminid=(String)session.getAttribute("adminid");
		dto.setReplyer(adminid);
		replyService.create(dto,qno);
		System.out.println("야:"+dto);
	}
}
