package com.example.project.controller.ym;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ym.dto.AdminBookDTO;
import com.example.project.service.hjh.book.Pager;
import com.example.project.service.ym.AdminBookService;
import com.example.project.service.ym.PdfService;

@Controller
@RequestMapping("ym/adminbook/*")
public class AdminBookController {

	@Inject
	AdminBookService bookService;

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "1") int curPage,
			@RequestParam(defaultValue = "author") String search_option, @RequestParam(defaultValue = "") String keyword,
			ModelAndView mav) throws Exception {
		int count = bookService.countArticle(search_option, keyword);
		// System.out.println("카운트"+count);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<AdminBookDTO> list = bookService.listBook(search_option, keyword, start, end);
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager", pager); // 페이지 네이게이션을 위한 변수
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		mav.setViewName("ym/book/book_list");
		mav.addObject("map", map);

		return mav;
	}


}
