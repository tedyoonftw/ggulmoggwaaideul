package com.example.project.controller.ym;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.ym.dto.AdminApplicantDTO;
import com.example.project.service.hjh.book.Pager;
import com.example.project.service.ym.AdminApplicantService;

@Controller
@RequestMapping("ym/adminapplicant/*")
public class AdminApplicantController {

	@Inject
	AdminApplicantService applicantService;

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "1") int curPage,
			@RequestParam(defaultValue = "userid") String search_option, @RequestParam(defaultValue = "") String keyword,
			ModelAndView mav, int cno) throws Exception {
		int count = applicantService.countArticle(search_option, keyword,cno);
		System.out.println("카운트"+count);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<AdminApplicantDTO> list = applicantService.listApplicant(search_option, keyword, start, end, cno);
		Map<String, Object> map = new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager", pager);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		mav.setViewName("ym/applicant/applicant_list");
		mav.addObject("map", map);

		return mav;
	}


}
