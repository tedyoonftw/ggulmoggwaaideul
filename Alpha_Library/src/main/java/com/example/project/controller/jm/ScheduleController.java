package com.example.project.controller.jm;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.jm.dto.ScheduleDTO;
import com.example.project.service.jm.ScheduleService;

@Controller
@RequestMapping("project/schedule/*")
public class ScheduleController {
	
	private static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);
	
	@Inject
	ScheduleService scheduleService;
	
	
	
	@RequestMapping("bigcalview.do")
	public String bigcalview(@RequestParam(defaultValue="MONTH") String type,Model model,HttpSession session) {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd"); 
		Date today = calendar.getTime();
		int curYear = 	calendar.get(Calendar.YEAR);
		int curMonth = 	(calendar.get(Calendar.MONTH) + 1);
		int curDay = calendar.get(Calendar.DATE);
		
		//today 정보
		
		model.addAttribute("today",today);
		System.out.println("달력 커이어 =" + curYear);
		model.addAttribute("curYear", curYear);
		model.addAttribute("curMonth", curMonth);
		model.addAttribute("curDay", curDay);
		//해당월의 1일로 캘린더 설정.
		calendar.set(Calendar.DATE, 1); 
		
		/*Date firstDayOFMonth2 = calendar.getTime();*/
		Long firstDayOFMonth = calendar.getTimeInMillis();
		
       
		model.addAttribute("firstDayOfMonth", firstDayOFMonth);
		System.out.println("firstDayOfMonth "+calendar.getTime());
		session.setAttribute("firstDayOfWeek", calendar.get(Calendar.DAY_OF_WEEK));
		session.setAttribute("lastDayOfMonth", 	calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//해당월의 마지막일로 캘린더 설정.
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		session.setAttribute("lastDayOfLastWeek", calendar.get(Calendar.DAY_OF_WEEK));
		//다음달의 1일로 설정.
		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		Long firstDayOFNextMonth = calendar.getTimeInMillis();
		String strDT2 = dayTime.format(firstDayOFNextMonth);
		Date firstDayOfNextMonth = java.sql.Date.valueOf(strDT2.substring(0, 10));
		
		model.addAttribute("firstDayOfNextMonth", firstDayOfNextMonth);
		System.out.println("firstDayOfNextMonth 이게?:"+firstDayOfNextMonth);
	    System.out.println("밀리세컨드"+firstDayOFMonth);
	    String strDT = dayTime.format(firstDayOFMonth); 
	    System.out.println("변환됐나"+strDT);
	    Date firstDayOfMonth = java.sql.Date.valueOf(strDT.substring(0, 10));
		model.addAttribute("month_query",scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		System.out.println("여기는 인포컨트롤러 :"+scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		return "jm/schedule/ViewMonth";
	}
	
	@RequestMapping("afterupdate.do/{curYear}/{curMonth}/{curDay}")
	public String afterdelete(@RequestParam(defaultValue="MONTH") String type,
			@PathVariable("curYear")int curYear, @PathVariable("curMonth")int curMonth, 
			@PathVariable("curDay")int curDay ,Model model, HttpSession session) {
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd"); 
		Date today = calendar.getTime();
		
	/*	String strType = type;*/

	/*	if(strType != null && !strType.equals("")) {
			int intYear 	= curYear;
			int intMonth 	= curMonth;
			int intDay 		= curDay;

			if(intMonth > 12) {
				intYear += 1;
				intMonth = 1;
			}
			if(intMonth < 1) {
				intYear -= 1;
				intMonth = 12;
			}

			calendar.set(intYear, intMonth-1, intDay);
		
		
		}*/

		//today 정보
		model.addAttribute("today",today);
		model.addAttribute("curYear", curYear);
		model.addAttribute("curMonth", curMonth);
		model.addAttribute("curDay", curDay);
		//해당월의 1일로 캘린더 설정.
		calendar.set(Calendar.DATE, 1); 
		Long firstDayOFMonth = calendar.getTimeInMillis();
		String strDT = dayTime.format(firstDayOFMonth); 
		Date firstDayOfMonth = java.sql.Date.valueOf(strDT.substring(0, 10));
		model.addAttribute("firstDayOfMonth", firstDayOFMonth);
		System.out.println("firstDayOfMonth "+calendar.getTime());
		session.setAttribute("firstDayOfWeek", calendar.get(Calendar.DAY_OF_WEEK));
		session.setAttribute("lastDayOfMonth", 	calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//해당월의 마지막일로 캘린더 설정.
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		session.setAttribute("lastDayOfLastWeek", calendar.get(Calendar.DAY_OF_WEEK));
		//다음달의 1일로 설정.
		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		Long firstDayOFNextMonth = calendar.getTimeInMillis();
		String strDT2 = dayTime.format(firstDayOFNextMonth);
		Date firstDayOfNextMonth = java.sql.Date.valueOf(strDT2.substring(0, 10));
		model.addAttribute("firstDayOfNextMonth", firstDayOfNextMonth);
		model.addAttribute("month_query",scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		System.out.println("뒷처리 .DO :"+scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		return "jm/schedule/ViewMonth";
	}
	
	
	@RequestMapping("bigcalplusview.do")
	public String bigcalplussview(@RequestParam String type, @RequestParam int curYear, @RequestParam int curMonth, @RequestParam int curDay ,Model model,HttpSession session) {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd"); 
		String strType = type;
		System.out.println("타입="+strType);

		if(strType != null && !strType.equals("")) {
			int intYear 	= curYear;
			int intMonth 	= curMonth;
			int intDay 		= curDay;

			if(intMonth > 12) {
				intYear += 1;
				intMonth = 1;
			}
			if(intMonth < 1) {
				intYear -= 1;
				intMonth = 12;
			}

			calendar.set(intYear, intMonth-1, intDay);
			model.addAttribute("today",today);
			model.addAttribute("curYear", intYear);
			model.addAttribute("curMonth", intMonth);
			model.addAttribute("curDay", intDay);
		}

		//today 정보
		
	
		//해당월의 1일로 캘린더 설정.
		calendar.set(Calendar.DATE, 1); 
		
		Long firstDayOFMonth = calendar.getTimeInMillis();
		String strDT = dayTime.format(firstDayOFMonth); 
		Date firstDayOfMonth = java.sql.Date.valueOf(strDT.substring(0, 10));
		model.addAttribute("firstDayOfMonth", firstDayOFMonth);
		session.setAttribute("firstDayOfWeek", calendar.get(Calendar.DAY_OF_WEEK));
		session.setAttribute("lastDayOfMonth", 	calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//해당월의 마지막일로 캘린더 설정.
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		session.setAttribute("lastDayOfLastWeek", calendar.get(Calendar.DAY_OF_WEEK));
		//다음달의 1일로 설정.
		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		Long firstDayOFNextMonth = calendar.getTimeInMillis();
		String strDT2 = dayTime.format(firstDayOFNextMonth);
		Date firstDayOfNextMonth = java.sql.Date.valueOf(strDT2.substring(0, 10));
		model.addAttribute("firstDayOfNextMonth", firstDayOfNextMonth);
		model.addAttribute("month_query",scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		return "jm/schedule/ViewMonth";
	}
	@RequestMapping("bigcalminusview.do")
	public String bigcalminusview(@RequestParam String type, @RequestParam int curYear, @RequestParam int curMonth, @RequestParam int curDay ,Model model,HttpSession session) {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd"); 
		String strType = type;
		System.out.println("타입="+strType);

		if(strType != null && !strType.equals("")) {
			int intYear 	= curYear;
			int intMonth 	= curMonth;
			int intDay 		= curDay;

			if(intMonth > 12) {
				intYear += 1;
				intMonth = 1;
			}
			if(intMonth < 1) {
				intYear -= 1;
				intMonth = 12;
			}

			calendar.set(intYear, intMonth-1, intDay);
			model.addAttribute("today",today);
			model.addAttribute("curYear", intYear);
			model.addAttribute("curMonth", intMonth);
			model.addAttribute("curDay", intDay);
		}

		//today 정보
		
	
		//해당월의 1일로 캘린더 설정.
		calendar.set(Calendar.DATE, 1); 
		
		Long firstDayOFMonth = calendar.getTimeInMillis();
		String strDT = dayTime.format(firstDayOFMonth); 
		Date firstDayOfMonth = java.sql.Date.valueOf(strDT.substring(0, 10));
		model.addAttribute("firstDayOfMonth", firstDayOFMonth);
		session.setAttribute("firstDayOfWeek", calendar.get(Calendar.DAY_OF_WEEK));
		session.setAttribute("lastDayOfMonth", 	calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//해당월의 마지막일로 캘린더 설정.
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		session.setAttribute("lastDayOfLastWeek", calendar.get(Calendar.DAY_OF_WEEK));
		//다음달의 1일로 설정.
		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		Long firstDayOFNextMonth = calendar.getTimeInMillis();
		String strDT2 = dayTime.format(firstDayOFNextMonth);
		Date firstDayOfNextMonth = java.sql.Date.valueOf(strDT2.substring(0, 10));
		
		model.addAttribute("firstDayOfNextMonth", firstDayOfNextMonth);
		System.out.println("firstDayOfNextMonth 이게?:"+firstDayOfNextMonth);
		
		model.addAttribute("month_query",scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		System.out.println("여기는 마이너스인포컨트롤러 :"+scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		return "jm/schedule/ViewMonth";
	}
	
	@RequestMapping("viewschedule.do")
	public ModelAndView viewschedule(ModelAndView mav,String type, int schedule_id) {
		mav.setViewName("jm/schedule/UpdateForm");
		ScheduleDTO dto = new ScheduleDTO();
		mav.addObject("type",type);
		dto=scheduleService.ShowSchedule(schedule_id);
		mav.addObject("dto",dto);
		return mav;
	};
	
	@RequestMapping("delete.do")
	public String delete(Model model,String type, @RequestParam int schedule_id) {
		model.addAttribute("type", "INIT");
		System.out.println("딜리트는 실행되었다 어째서 안되는거냐?");
		scheduleService.delete(schedule_id);
		return "jm/schedule/UpdateForm";
	};
	
	
	@RequestMapping("update.do")
	public String update(Model model, String type, ScheduleDTO dto) {
		scheduleService.update(dto);
		return "redirect:/project/schedule/viewschedule.do?type=SELECT&schedule_id="+dto.getSchedule_id();
	}
	
	
	@RequestMapping("schedule.do")
	public String Save(Model model) {
		String type = "INIT";
		model.addAttribute("type", type);		
		return "jm/schedule/UpdateForm";
	}
	
	@RequestMapping("insertScheduleByIndex.do")
	public String insetByIndex(int curYear, int curMonth, int curDay,Model model) {
		String type = "view";
		model.addAttribute("curYear", curYear);
		model.addAttribute("curMonth", curMonth);
		model.addAttribute("curDay", curDay);
		model.addAttribute("type", type);		
		return "jm/schedule/UpdateForm";
	}	
	
	
	@RequestMapping("insert.do")
	public String insert(Model model, int curYear, int curMonth, int curDay , int event, String schedule_subject , String schedule_content) {
		ScheduleDTO dto = new ScheduleDTO();
		Date schedule_date_time = java.sql.Date.valueOf(curYear+"-"+curMonth+"-"+curDay);
		dto.setEvent(event);
		dto.setCurDay(curDay);
		dto.setCurMonth(Integer.valueOf(curMonth));
		dto.setCurYear(Integer.valueOf(curYear));
		dto.setSchedule_subject(schedule_subject);
		dto.setSchedule_content(schedule_content);
		dto.setSchedule_date_time(schedule_date_time);
		scheduleService.insert(dto);
		return "jm/schedule/UpdateForm";
	}
	
	
}
