package com.example.project.controller.hjh.mypage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.service.hjh.mypage.MypageServicej;
import com.example.project.service.jm.RentService;

@Controller
@RequestMapping("hjh/mypage/*")
public class MypageController {
	
	@Inject
	MypageServicej mypageService;
	
	@Inject
	RentService rentService;
	
	@RequestMapping("nowrent.do")
	public ModelAndView nowlist(HttpSession session) {
		String userid=(String)session.getAttribute("userid");
		System.out.println("userid:"+userid);
		List<MypageDTOj> nowlist=mypageService.nowrent(userid);
		int nowrentcount = rentService.nowRentCount(userid);
		ModelAndView mav=new ModelAndView();
		mav.addObject("rentcount", nowrentcount); // 대출중인 권수 확인
		mav.addObject("nowlist", nowlist);
		mav.setViewName("hjh/mypage/nowrent");
		return mav;
		
	
	}
	
	@RequestMapping("beforerent.do")
	public ModelAndView beforelist(HttpSession session) {
		String userid=(String)session.getAttribute("userid");
		List<MypageDTOj> beforelist=mypageService.beforerent(userid);
		ModelAndView mav=new ModelAndView();
		mav.addObject("blist", beforelist);
		mav.setViewName("hjh/mypage/beforerent");
		return mav;
		
		
	}
	
	@RequestMapping("bookcart.do")
	public ModelAndView cartlist(HttpSession session) {
		String userid=(String)session.getAttribute("userid");
		List<MypageDTOj> cartlist=mypageService.bookcart(userid);
		ModelAndView mav=new ModelAndView();
		mav.addObject("clist", cartlist);
		mav.setViewName("hjh/mypage/bookcart");
		return mav;
	}
	

	
	@RequestMapping("wishlist.do")
	public ModelAndView wishlist(HttpSession session) {
		String userid=(String)session.getAttribute("userid");
		List<MypageDTOj> wishlist=mypageService.wishlist(userid);
		ModelAndView mav=new ModelAndView();
		mav.addObject("wlist", wishlist);
		mav.setViewName("hjh/mypage/wishlist");
		return mav;
	}
	
	@RequestMapping("admin_wishlist.do")
	public ModelAndView admin_wishlist(HttpSession session) {
		String adminid=(String)session.getAttribute("adminid");
		List<MypageDTOj> wishlist=mypageService.admin_wishlist(adminid);
		ModelAndView mav=new ModelAndView();
		mav.addObject("wlist", wishlist);
		mav.setViewName("hjh/mypage/admin_wishlist");
		return mav;
	}
	
	@RequestMapping("form.do")
	public String form(HttpSession session, Model model) {
		String userid=(String)session.getAttribute("userid");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String hpdate = sdf.format(cal.getTime()); 
		model.addAttribute("hpdate", hpdate);
		return "hjh/mypage/form";
	}
	
	//hopebook
	@RequestMapping("insert.do")
	public String write(String hpdate, String userid, String title,
			String author,String publisher,String isbn,String pubyear,
			String content) {
		 mypageService.hopebook(hpdate,userid,title,author,publisher,
				 isbn,pubyear,content);
		 return "redirect:/";
	}
	
	@RequestMapping("detail/{hpnum}")
	public ModelAndView detail(@PathVariable int hpnum,
			ModelAndView mav) {
		mav.setViewName("hjh/mypage/wish_detail");
		mav.addObject("dto", mypageService.detail(hpnum));
		return mav;
	}
	
	
	
	@RequestMapping("wishupdate.do")
	public String update(int hpnum,String userid, String title,
			String author,String publisher,String isbn,String pubyear,
			String content) {
		System.out.println("hpnum:"+hpnum);
		System.out.println("hpnum:"+userid);
		System.out.println("hpnum:"+title);
		System.out.println("hpnum:"+author);
		System.out.println("hpnum:"+publisher);
		System.out.println("hpnum:"+isbn);
		System.out.println("hpnum:"+pubyear);
		System.out.println("hpnum:"+content);
		mypageService.wishupdate(hpnum, title, author, publisher, isbn, pubyear, content);
		return "redirect:/hjh/mypage/wishlist.do";
	}
	
	@RequestMapping("wishdelete.do")
	public String delete(int hpnum) {
		mypageService.wishdelete(hpnum);
		return "redirect:/hjh/mypage/wishlist.do";
	}
	
	@RequestMapping("proceed.do")
	public String proceed(int hpnum,String userid) {
		mypageService.proceed(hpnum,userid);
		return "redirect:/hjh/mypage/admin_wishlist.do";
	}

	
}
