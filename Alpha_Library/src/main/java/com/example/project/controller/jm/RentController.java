package com.example.project.controller.jm;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.service.hjh.mypage.MypageServicej;
import com.example.project.service.jm.RentService;
import com.example.project.service.jm.SearchKeyword;
import com.example.project.service.kdk.book.BookService;

@Controller
@RequestMapping("/book/rent/*")
public class RentController {

	@Inject
	BookService bookService;

	@Inject
	RentService rentService;

	@Inject
	MypageServicej mypageService;

	@Inject
	SearchKeyword keywordService;

	// 대출 정보를 알아냄
	@RequestMapping("rentinfo/{book_code}")
	public String rentBook(@PathVariable String book_code, Model model, HttpSession session) {
		BookDTO bookdto = rentService.bookinfo(book_code);
		String userid = (String) session.getAttribute("userid");
		model.addAttribute("userid", userid);
		model.addAttribute("bookdto", bookdto);

		return "jm/rent/bookRent";
	}

	// 대출을 눌렀을때 몇권있는 지를 판단하고 3권이면 못빌리게 함
	@RequestMapping(value = "rent.do", produces = "application/text; charset=utf8")
	@ResponseBody
	@Transactional
	public String Bookrent(String userid, String book_code, String rent_date, String return_date, String group_code) {
		RentDTO dto = new RentDTO();
		dto.setBook_code(book_code);
		dto.setGroup_code(group_code);
		dto.setRent_date(java.sql.Date.valueOf(rent_date));
		dto.setReturn_date(java.sql.Date.valueOf(return_date));
		dto.setUserid(userid);
		String message = "";
		int rentcount = rentService.nowRentCount(userid);
		List<RentDTO> penaltylistbyuserid = rentService.checkdely(userid);
		System.out.println("==============대출을 하자 대출을 1" + penaltylistbyuserid);

		if (penaltylistbyuserid.isEmpty()) { // 테이블이 null 일경우가 있음 처음 빌리는 회원일 경우
			System.out.println("실행됐냐 안됐냐?! 널이니까 줬잖아");
			if (rentcount < 3) {
				rentService.updatebyrent(book_code);
				rentService.rent(dto);
				int rentcount2 = rentService.nowRentCount(userid);
				message = "대출에 성공하였습니다. 현재 대출 중인 도서권수 : (" + rentcount2 + ")권";
			} else {
				message = "대출에 실패하였습니다. 대출중인 도서를 확인해 주세요 <br>" + "현재 대출 중인 도서권수 : (" + rentcount + ")권";
			}

		} else {

			for (RentDTO checkdelay : penaltylistbyuserid) {

				System.out.println("=========대출을 하자 대출을" + checkdelay);
				if (checkdelay.getPenalty().equals("N") || checkdelay.getPenalty() == null) {
					if (rentcount < 3) {
						rentService.updatebyrent(book_code);
						rentService.rent(dto);
						int rentcount2 = rentService.nowRentCount(userid);
						message = "대출에 성공하였습니다. 현재 대출 중인 도서권수 : (" + rentcount2 + ")권";
					} else {
						message = "대출에 실패하였습니다. 대출중인 도서를 확인해 주세요 <br>" + "현재 대출 중인 도서권수 : (" + rentcount + ")권";
					}

				} else {

					message = "대출에 실패하였습니다. 연체된 도서로인해서 다음과 같은 날까지 대출이 불가합니다  " + "대출불가 날짜:"
							+ rentService.getdelaygigan(userid) +"\n 대출현황을 확인해주시길 바랍니다";
				}
			}
		}

		// 베스트 8 뽑기 책을 빌릴때마다 키워드가 입력이 됨
		String gijonbookcode = keywordService.findBestTitle(book_code);
		if (gijonbookcode == null) {

			keywordService.newBestTitle(book_code);

		} else {

			keywordService.rentnoPlus(book_code);
		}
		return message;

	}

	// 반납일을 연기
	@RequestMapping("smoke.do/{rentno}")
	public String smoke(@PathVariable int rentno, Model model) {

		MypageDTOj dtoj = mypageService.getinfo(rentno);
		Date return_date = dtoj.getReturn_date();
		rentService.smoke(rentno, return_date);
		return "redirect:/hjh/mypage/nowrent.do";
	}

	// 반납을 실행
	@RequestMapping("halfnab.do/{rent_code}")
	@Transactional
	public String returnbook(@PathVariable int rent_code, HttpSession session) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		RentDTO rentinfo = rentService.getrentinfo(rent_code);
		String book_code = rentinfo.getBook_code();
		Date return_date = java.sql.Date.valueOf(sdf.format(cal.getTimeInMillis()));
		String userid = (String) session.getAttribute("userid");

		if (userid != null) {
			// 연체를 했는지 확인해봄
			List<RentDTO> Rentinfo = rentService.RentinfoListByUserid(userid);
			for (RentDTO rentdto : Rentinfo) {

				String kiteche = rentdto.getKiteche();
				System.out.println(
						kiteche + "====================================이사람은 연체자 입니다 ===============================");

				if (kiteche.equals("Y")) { // 연체가 확인됨

					List<RentDTO> delayconfirm = rentService.checkdely(userid); // 기존에 패널티 여부를 검사
					System.out.println("==========여기까지 잘왔냐?==========" + delayconfirm);
					if (delayconfirm.isEmpty()) {
						rentService.updateDelayHalfnab(rent_code, return_date);
						rentService.updateDelayDate(rentdto.getRent_code()); // 해당 관리코드의 연체 반납일자를 업데이트
					
					} else {
						
						for (RentDTO checkpanelty : delayconfirm) {

							if (checkpanelty.getPenalty().equals("Y")) { // 있다면
								int pre_rent_code = checkpanelty.getRent_code();

								rentService.updateDelayHalfnab(rent_code, return_date); // 연체 반납일자와 반납생태를 업데이트
								rentService.updateDelayGigan(pre_rent_code, rent_code); // 현재의 렌트코드에 해당하는 패널티 기간을 업데이트
																					

							} else { // 기존에 패널티가가 없음을 확인했을떄

								rentService.updateDelayHalfnab(rent_code, return_date);
								rentService.updateDelayDate(rentdto.getRent_code()); // 해당 관리코드의 연체 반납일자를 업데이트
							}
						}
					}
				} else { //연체가 아님

					rentService.halfnab(rent_code, return_date); // 반납일자와 반납상태를 업데이트
				}
				// 책의 상태와 rent 테이블에서 의 변화는 어찌됐는 해줘야죵
				rentService.updateByReturnbook(book_code); // 책의 상태를 업데이트

			}

		}

		return "redirect:/hjh/mypage/nowrent.do";
	}

	// 연체를 했을때 유저에게 경고를 날려줌
	@RequestMapping("loadgo.do")
	public String loadgo(HttpSession session, Model model) {
		String userid = (String) session.getAttribute("userid");

		if (userid != null) {
			int delaycount = rentService.delaycount(userid);
			model.addAttribute("delaycount", delaycount);
			model.addAttribute("delay", "confrim");
		}
		return "Loadgo";
	}

}
