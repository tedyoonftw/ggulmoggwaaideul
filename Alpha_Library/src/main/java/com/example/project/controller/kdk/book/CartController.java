package com.example.project.controller.kdk.book;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project.controller.hjh.search.Pager;
import com.example.project.model.kdk.book.dto.CartDTO;
import com.example.project.service.jm.RentService;
import com.example.project.service.jm.SearchKeyword;
import com.example.project.service.kdk.book.BookService;
import com.example.project.service.kdk.book.CartService;
import com.example.project.service.kdk.book.MemoService;


@Controller
@RequestMapping("kdk/book/*")
public class CartController {

	@Inject
	CartService cartService;
	
	@Inject
	BookService bookService;

	@Inject
	MemoService memoService;
	
	@Inject
	RentService rentService;
	
	@Inject
	SearchKeyword keywordService;
	
	@RequestMapping("/cart/list.do")
	public ModelAndView list(HttpSession session, 
			@RequestParam(defaultValue="title") String search_option,
			@RequestParam(defaultValue="") String keyword,
			@RequestParam(defaultValue="1") int curPage) throws Exception {
		
		System.out.println("키워드"+keyword+
				"서치옵션"+search_option+
				"현재페이지"+curPage);
		
		
		
		//레코드 갯수 계산
		String userid = (String) session.getAttribute("userid");
				int count=
						cartService.countArticle(search_option,keyword,userid);
				//페이지 관련 설정
				Pager pager=new Pager(count,curPage);
				int start=pager.getPageBegin();
				int end=pager.getPageEnd();
		
		
		
		List<CartDTO> list = 
				cartService.listCart(search_option,keyword,start,end,userid); //게시물 목록
		
		switch (search_option) {

		  case "title":

		   for (int i = 0; i < list.size(); i++) {
		    CartDTO dto = list.get(i);
		    String title = dto.getTitle();
		    if (title.indexOf(keyword) != -1) {

		     dto.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     list.set(i, dto);
		    }
		   }

		   break;

		  case "author":

		   for (int i = 0; i < list.size(); i++) {
			CartDTO dto = list.get(i);
		    String author = dto.getAuthor();
		    if (author.indexOf(keyword) != -1) {

		     dto.setAuthor(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     list.set(i, dto);
		    }

		   }
		   break;

		  case "publisher":

		   for (int i = 0; i < list.size(); i++) {
			CartDTO dto = list.get(i);
		    String publisher = dto.getPublisher();
		    if (publisher.indexOf(keyword) != -1) {

		     dto.setPublisher(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     list.set(i, dto);
		    }

		   }
		   break;

		  case "all":

		   for (int i = 0; i < list.size(); i++) {
			CartDTO dto = list.get(i);
		    String author = dto.getAuthor();
		    String publisher = dto.getPublisher();
		    String title = dto.getTitle();
		    if (author.indexOf(keyword) != -1 || publisher.indexOf(keyword) != -1 || title.indexOf(keyword) != -1) {
		     dto.setTitle(title.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     dto.setPublisher(publisher.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     dto.setAuthor(author.replace(keyword, "<font color='red;'>" + keyword + "</font>"));
		     list.set(i, dto);
		    }

		   }
		   break;

		  }
		
		ModelAndView mav=new ModelAndView();
		HashMap<String, Object> map = new HashMap<>();
		map.put("list", list); //맵에 자료 추가
		System.out.println(list);
		map.put("count", list.size());
		map.put("pager", pager); //페이지 네비게이션을 위한 변수
		map.put("search_option", search_option);
		map.put("keyword",keyword); 
		
		mav.setViewName("kdk/book/cart_list"); //jsp 페이지 이름
		mav.addObject("map", map); //jsp에 전달할 데이터
		return mav;
	}
	
	@RequestMapping("cart/insert.do")
	public String insert(HttpSession session,@ModelAttribute CartDTO dto , Model model, @RequestParam String book_code) {
		// 세션에 userid 변수가 존재하는지 확인
			String userid = (String) session.getAttribute("userid");
			// 관심도서목록에 insert 처리 후 관심도서 목록으로 이동
			dto.setUserid(userid);
			
			String msg="";
			String page = "";
			// 장바구니에 기존 상품이 있는지 검사
			
			int count = cartService.countCart(book_code, userid);
			
			if(count == 0) {
				//없으면 insert 기존 장바구니에 존재하지 않음
				cartService.insert(dto);
				page =  "/kdk/book/cart_list";
			
			} else {
				//있으면
				msg = "alreadyexist";
				page = "redirect:detail?book_code="+book_code+"&msg="+msg;
				
			}
			
			return page;
	}
	
	
	//관심도서목록 개별 삭제
	@RequestMapping("cart/delete.do")
	public String delete(@RequestParam int [] select
											,HttpSession session) {
		if(session.getAttribute("userid") != null)
		for(int i=0; i<select.length; i++)
		{
			cartService.delete(select[i]);
		}
		return "redirect:/kdk/book/cart/list.do";
	}
	
	//관심도서목록 전체 삭제
	@RequestMapping("cart/deleteAll.do")
	public String deleteAll(HttpSession session) {
		//세션변수 조회(로그인 여부 검사)
		String userid = (String) session.getAttribute("userid");
		if(userid != null) { //로그인한 상태이면
			//장바구니를 비우고
			cartService.deleteAll(userid);
		}
		//관심도서 목록으로 이동
		return "redirect:/kdk/book/cart/list.do";
	}
	
	@RequestMapping("cart/detail")
	public ModelAndView detail(
			
	String book_code, ModelAndView mav, HttpSession session, String msg) 
	
	
	{

		String userid = (String) session.getAttribute("userid");
		
		if (userid != null) {
			String renting = rentService.rentinfouserid(userid, book_code);
			if (renting != null) {
				mav.addObject("checkrent", renting);
			}
		}
		mav.setViewName("kdk/book/book_detail");
		mav.addObject("msg",msg);
		mav.addObject("userid", userid);
		mav.addObject("dto", bookService.detailBook(book_code));
		mav.addObject("list", memoService.listMemo(book_code));
		if(userid != null) {
			keywordService.insertTitleKeyword(book_code,userid);
		}
		return mav;
	}
	
	
	
}
