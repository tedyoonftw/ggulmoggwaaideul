package com.example.project.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import ch.qos.logback.core.net.SyslogOutputStream;

public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("인텁셉터와 왔다");
		HttpSession session = request.getSession();
		if (session.getAttribute("userid") == null) {
			response.sendRedirect(request.getContextPath()+"/pu/login/user/login.do?message=nologin");
			return false; //메인으로 넘어가지 않음
		} else {
			return true;
		}
	}

}
