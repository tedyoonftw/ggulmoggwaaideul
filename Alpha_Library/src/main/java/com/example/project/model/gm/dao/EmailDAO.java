package com.example.project.model.gm.dao;

public interface EmailDAO {
	public String check(String email1,String email2);
	public void insertMail(String email);
	public void updateMail(String email);
	public void updateMail2(String email);
	public int mailCheck(String email);
	public int mailCheck2(String email);
}
