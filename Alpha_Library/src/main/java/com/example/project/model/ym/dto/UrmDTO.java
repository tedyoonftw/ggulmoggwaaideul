package com.example.project.model.ym.dto;

public class UrmDTO {

	private String userid;
	private String username;
	private int count;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "UrmDTO [userid=" + userid + ", username=" + username + ", count=" + count + "]";
	}
	
	
	
}
