package com.example.project.model.kdk.book.dto;

import java.util.Date;

public class MemoDTO {
	private int idx;
	private String userid;
	private String memo;
	private Date post_date;
	private String book_code;		//책 레코드키
	
	//getter,setter,toString,생성자
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getPost_date() {
		return post_date;
	}
	public void setPost_date(Date post_date) {
		this.post_date = post_date;
	}
	public String getBook_code() {
		return book_code;
	}
	public void setBook_code(String book_code) {
		this.book_code = book_code;
	}
	@Override
	public String toString() {
		return "MemoDTO [idx=" + idx + ", userid=" + userid + ", memo=" + memo + ", post_date=" + post_date
				+ ", book_code=" + book_code + "]";
	}
	
	
	public MemoDTO() {
	}
	public MemoDTO(int idx, String userid, String memo, Date post_date, String book_code) {
		this.userid = userid;
		this.memo = memo;
		this.book_code = book_code;
	}
	
}
