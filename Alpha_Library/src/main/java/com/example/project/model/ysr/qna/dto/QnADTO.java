package com.example.project.model.ysr.qna.dto;

import java.util.Arrays;
import java.util.Date;

public class QnADTO {
	private int qno;
	private String title;
	private String content;
	private String adminid;
	private String userid;
	private Date regdate;
	private String username;
	private String check1;
	private String adminname;
	private int cnt;
	private String show;
	private String files[];
	
	public int getQno() {
		return qno;
	}
	public void setQno(int qno) {
		this.qno = qno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAdminid() {
		return adminid;
	}
	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCheck1() {
		return check1;
	}
	public void setCheck1(String check1) {
		this.check1 = check1;
	}
	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public String getShow() {
		return show;
	}
	public void setShow(String show) {
		this.show = show;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] files) {
		this.files = files;
	}
	
	@Override
	public String toString() {
		return "QnADTO [qno=" + qno + ", title=" + title + ", content=" + content + ", adminid=" + adminid + ", userid="
				+ userid + ", regdate=" + regdate + ", username=" + username + ", check1=" + check1 + ", adminname="
				+ adminname + ", cnt=" + cnt + ", show=" + show + ", files=" + Arrays.toString(files) + "]";
	}
	
}
