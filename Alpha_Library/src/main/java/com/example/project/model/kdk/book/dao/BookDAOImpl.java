package com.example.project.model.kdk.book.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.kdk.book.dto.BookDTO;

@Repository
public class BookDAOImpl implements BookDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<BookDTO> listBook(int start, int end) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("start", start);
		map.put("end", end);
	//mapper에는 2개 이상의 값을 전달할 수 없음(dto 또는 map 사용)
		return sqlSession.selectList("book.book_list", map);
	}

	@Override
	public BookDTO detailBook(String book_code) {
		return sqlSession.selectOne("book.book_detail", book_code);
	}

	@Override
	public void updateBook(BookDTO dto) {
		sqlSession.update("book.book_update",dto);
	}

	@Override
	public void deleteBook(String book_code) {
		sqlSession.delete("book.book_delete",book_code);
	}

	@Override
	public void insertBook(BookDTO dto) {
		sqlSession.insert("book.book_insert", dto);
	}

	@Override
	public String fileInfo(String book_code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countArticle() throws Exception {
		return sqlSession.selectOne("book.countArticle");
	}

	
}
