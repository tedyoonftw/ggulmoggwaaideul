package com.example.project.model.jm.dto;

public class urmDTO {

	private int icode;
	private String userid;
	private String stime;
	private String etime;
	public int getIcode() {
		return icode;
	}
	public void setIcode(int icode) {
		this.icode = icode;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getStime() {
		return stime;
	}
	public void setStime(String stime) {
		this.stime = stime;
	}
	public String getEtime() {
		return etime;
	}
	public void setEtime(String etime) {
		this.etime = etime;
	}
	
	@Override
	public String toString() {
		return "urmDTO [icode=" + icode + ", userid=" + userid + ", stime=" + stime + ", etime=" + etime + "]";
	}
	
	
	
}
