package com.example.project.model.ysr.culture.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ysr.culture.dto.CultureDTO;

@Repository
public class CultureDAOImpl implements CultureDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<CultureDTO> list(String search_option, String keyword, int start, int end) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
		
		return sqlSession.selectList("culture.list",map);
	}

	@Override
	public CultureDTO view(int cno,String username) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("cno", cno);
		map.put("username", username);
		return sqlSession.selectOne("culture.view",map);
	}

	@Override
	public void insert(CultureDTO dto) throws Exception {
		sqlSession.insert("culture.insert",dto);
	}

	@Override
	public void update(CultureDTO dto) throws Exception {
		sqlSession.update("culture.update",dto);
	}

	@Override
	public void delete(int cno) throws Exception {
		sqlSession.update("culture.delete",cno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("culture.countArticle",map);
	}

	@Override
	public void addFile(String fileName) {
		sqlSession.insert("culture.addFile",fileName);
	}

	@Override
	public List<String> getFile(int cno) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateFile(String fullName, int cno) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteFile(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public int insertUrl(CultureDTO dto) {
		return sqlSession.insert("culture.insertUrl", dto);
	}

}
