package com.example.project.model.ym.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.model.ym.dto.AdminApplicantDTO;
import com.example.project.model.ym.dto.AdminBookDTO;

@Repository
public class AdminApplicantDAOImpl implements AdminApplicantDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<AdminApplicantDTO> listApplicant(String search_option, String keyword, int start, int end, int cno) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
		map.put("cno", cno);
	//mapper에는 2개 이상의 값을 전달할 수 없음(dto 또는 map 사용)
		return sqlSession.selectList("adminapplicant.applicant_list", map);
	}

	@Override
	public int countArticle(String search_option, String keyword, int cno) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("cno", cno);
		return sqlSession.selectOne("adminapplicant.countArticle",map);
	}
	
}
