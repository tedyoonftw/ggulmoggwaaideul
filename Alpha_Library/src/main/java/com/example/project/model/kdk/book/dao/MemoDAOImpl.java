package com.example.project.model.kdk.book.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.kdk.book.dto.MemoDTO;

@Repository
public class MemoDAOImpl implements MemoDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<MemoDTO> listMemo(String book_code) {
		return sqlSession.selectList("book.memo_list", book_code);
	}


	@Override
	public void updateMemo(MemoDTO dto) {
		sqlSession.update("book.memo_update", dto);
	}

	@Override
	public void deleteMemo(int idx) {
		sqlSession.delete("book.memo_delete", idx);
	}

	@Override
	public void insertMemo(String userid, String memo, String book_code) {
		Map<String,Object> map = new HashMap<>();
		map.put("userid", userid);
		map.put("memo", memo);
		map.put("book_code", book_code);
		sqlSession.insert("book.memo_insert",map);
	}

}
