package com.example.project.model.ym.dto;

import java.util.Date;

public class AdminApplicantDTO {
	private int ano;
	private int cno;
	private String title;
	private String userid;
	private String username;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "AdminApplicantDTO [ano=" + ano + ", cno=" + cno + ", title=" + title + ", userid=" + userid + ", username="
				+ username + "]";
	}

	
	//getter, setter, toString
		
	
}
