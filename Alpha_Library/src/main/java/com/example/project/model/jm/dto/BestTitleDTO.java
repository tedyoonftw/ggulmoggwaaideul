package com.example.project.model.jm.dto;

public class BestTitleDTO {
int bestno; //일련번호
String book_code;
int rentno; // 빌려간 횟수
String author;
String title;
String imgsrc;


public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getImgsrc() {
	return imgsrc;
}
public void setImgsrc(String imgsrc) {
	this.imgsrc = imgsrc;
}
public int getBestno() {
	return bestno;
}
public void setBestno(int bestno) {
	this.bestno = bestno;
}
public String getBook_code() {
	return book_code;
}
public void setBook_code(String book_code) {
	this.book_code = book_code;
}
public int getRentno() {
	return rentno;
}
public void setRentno(int rentno) {
	this.rentno = rentno;
}
@Override
public String toString() {
	return "BestTitleDTO [bestno=" + bestno + ", book_code=" + book_code + ", rentno=" + rentno + ", author=" + author
			+ ", title=" + title + ", imgsrc=" + imgsrc + "]";
}

}
