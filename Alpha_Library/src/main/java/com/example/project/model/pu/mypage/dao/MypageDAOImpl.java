package com.example.project.model.pu.mypage.dao;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.pu.login.dto.UserDTO;

@Repository
public class MypageDAOImpl implements MypageDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public void pw_modify(UserDTO dto) {
		sqlSession.update("my.pw_modify", dto);
	}

	@Override
	public UserDTO userinfo(String userid) {
		return sqlSession.selectOne("my.userinfo", userid);
	}

	@Override
	public void info_modify(UserDTO dto) {
		sqlSession.update("my.info_modify", dto);
	}

	@Override
	public UserDTO passwd(String userid) {
		return sqlSession.selectOne("my.userinfo", userid);
	}

	@Override
	public UserDTO bye(String userid) {
		return sqlSession.selectOne("my.userinfo",userid);
	}

	@Override
	public void cancel(String userid) {
		//sqlSession.delete("my.byebye", userid); -회원탈퇴 수정
		sqlSession.update("my.byebye", userid);
	}
	
	
	@Override
	public String id_find(UserDTO dto2) {
		return sqlSession.selectOne("my.id_find", dto2);
	}

	@Override
	public String pw_find(UserDTO dto2) {
		return sqlSession.selectOne("my.pw_find", dto2);
	}

	@Override
	public void memo1(String userid){
		sqlSession.delete("my.memo1", userid);
	}
	
	@Override
	public void applicant(String userid) {
		sqlSession.delete("my.applicant", userid);
	}

	@Override
	public void qna(String userid) {
		sqlSession.delete("my.qna", userid);
	}

	@Override
	public void readingroom(String userid) {
		sqlSession.delete("my.readingroom", userid);
	}

	@Override
	public void urm(String userid) {
		sqlSession.delete("my.urm", userid);
	}

	@Override
	public void hopebook(String userid) {
		sqlSession.delete("my.hopebook", userid);
	}

	@Override
	public void cart(String userid) {
		sqlSession.delete("my.cart", userid);
	}

}
