package com.example.project.model.jm.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.jm.dto.ScheduleDTO;

@Repository
public class ScheduleDAOImpl implements ScheduleDAO {



	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<ScheduleDTO> month_query(Date firstDayOfMonth,Date firstDayOfNextMonth) {
		Map<String,Object> map = new HashMap<>();
		map.put("firstDayOfMonth", firstDayOfMonth);
		map.put("firstDayOfNextMonth",firstDayOfNextMonth);
		return sqlSession.selectList("schedule.index",map) ;
	}
	
	@Override
	public ScheduleDTO ShowSchedule(int schedule_id) {
		ScheduleDTO dto = new ScheduleDTO();
		dto = sqlSession.selectOne("schedule.show",schedule_id);
		return dto;
		
	}

	@Override
	public void DeleteSchedule(int schedule_id) {
		sqlSession.delete("schedule.delete",schedule_id);
	}

	@Override
	public void update(ScheduleDTO dto) {
		sqlSession.update("schedule.update", dto);
	}

	@Override
	public void insert(ScheduleDTO dto) {
		sqlSession.insert("schedule.insert",dto);
	};
	
	

}
