package com.example.project.model.ym.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("excelDao")
public class ExcelDAOImpl implements ExcelDAO{
 
    @Autowired //인젝트랑 같은기능 (자바가 아니고 , 스프링용)
    private SqlSession sqlSession;
 
    public void setSqlSession(SqlSession sqlSession){
        this.sqlSession = sqlSession;
    }
 
    @Override
    public List<Object> getBooks(){
        return sqlSession.selectList("adminbook.listAll");
    }
 
 
}