package com.example.project.model.hjh.book.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.hjh.book.dto.BookDTOj;

@Repository
public class BookDAOjImpl implements BookDAOj {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<BookDTOj> searchList(String search_option, String keyword,
			int start,int end) {
		Map<String, Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("search.search_list" , map);
	}

	@Override
	public List<BookDTOj> search2List(BookDTOj dtoj, String order_option, 
			String date1, String date2,int start,int end) {
		
		Map<String, Object> map=new HashMap<>();
		map.put("dtoj", dtoj);
		map.put("order_option", order_option);
		map.put("date1", date1);
		map.put("date2", date2);
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("search.search2_list", map);
	}

	@Override
	public List<BookDTOj> search3List(String group_code, 
			String period, String realrealperiod,int start,int end) {
		System.out.println("group_code:"+group_code);
		System.out.println("period:"+period);;
		System.out.println("realrealperiod:"+realrealperiod);
		Map<String, Object> map=new HashMap<>();
		map.put("group_code", group_code);
		map.put("period", period);
		map.put("realrealperiod", realrealperiod);
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("search.search3_list", map);
	}

	@Override
	public int count(String search_option, String keyword) {
		Map<String, Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		return sqlSession.selectOne("search.count1", map);
	}

	@Override
	public int count2(BookDTOj dtoJ, String order_option, String date1, String date2) {
		Map<String, Object> map=new HashMap<>();
		map.put("dtoj", dtoJ);
		map.put("order_option", order_option);
		map.put("date1", date1);
		map.put("date2", date2);
		return sqlSession.selectOne("search.count2", map);
	}

	@Override
	public int count3(String group_code, String period, String realrealperiod) {
		Map<String, Object> map=new HashMap<>();
		map.put("group_code", group_code);
		map.put("period", period);
		map.put("realrealperiod", realrealperiod);
		return sqlSession.selectOne("search.count3", map);
	}

	@Override
	public BookDTOj bookinfo(String book_code) {
		return sqlSession.selectOne("search.bookinfo", book_code);
	}

}
