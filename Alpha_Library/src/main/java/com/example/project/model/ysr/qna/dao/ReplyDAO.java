package com.example.project.model.ysr.qna.dao;

import java.util.List;

import com.example.project.model.ysr.qna.dto.ReplyDTO;

public interface ReplyDAO {
	
	public List<ReplyDTO> list(int qno);
	public int count(int qno);
	public void create(ReplyDTO dto);
}
