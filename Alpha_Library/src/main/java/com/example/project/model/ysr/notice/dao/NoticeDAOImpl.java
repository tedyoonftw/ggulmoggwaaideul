package com.example.project.model.ysr.notice.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ysr.notice.dto.NoticeDTO;

@Repository
public class NoticeDAOImpl implements NoticeDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<NoticeDTO> list(String search_option, String keyword, int start, int end) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
		
		return sqlSession.selectList("notice.list",map);
	}

	@Override
	public void create(NoticeDTO dto) throws Exception {
		sqlSession.insert("notice.insert",dto);
	}

	@Override
	public void update(NoticeDTO dto) throws Exception {
		sqlSession.update("notice.update",dto);
	}

	@Override
	public void delete(int nno) throws Exception {
		sqlSession.update("notice.delete",nno);
	}

	@Override
	public NoticeDTO read(int nno) throws Exception {
		return sqlSession.selectOne("notice.read",nno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("notice.countArticle",map);
	}

	@Override
	public void increaseViewcnt(int nno) throws Exception {
		sqlSession.update("notice.increaseViewcnt",nno);
	}

	@Override
	public void addFile(String fullName) {
		sqlSession.insert("notice.addFile",fullName);
	}

	@Override
	public List<String> getFile(int nno) {
		return sqlSession.selectList("notice.getFile",nno);
	}

	@Override
	public void updateFile(String fullName, int nno) {
		Map<String,Object> map=new HashMap<>();
		map.put("fullName", fullName);
		map.put("nno", nno);
		sqlSession.insert("notice.updateFile",map);
	}

	@Override
	public void deleteFile(String fullName) {
		sqlSession.delete("notice.deleteFile",fullName);
	}

}
