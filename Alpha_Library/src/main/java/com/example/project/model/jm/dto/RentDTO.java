package com.example.project.model.jm.dto;

import java.util.Date;

public class RentDTO {
private int rent_code;
private String userid;
private String group_code;
private String book_code;
private Date rent_date;
private Date return_date;
private String halfnab;
private String kiteche;
private String delay_date;
private String delay_gigan;
private String penalty;


public String getDelay_date() {
	return delay_date;
}
public void setDelay_date(String delay_date) {
	this.delay_date = delay_date;
}
public String getDelay_gigan() {
	return delay_gigan;
}
public void setDelay_gigan(String delay_gigan) {
	this.delay_gigan = delay_gigan;
}
public String getPenalty() {
	return penalty;
}
public void setPenalty(String penalty) {
	this.penalty = penalty;
}
public String getKiteche() {
	return kiteche;
}
public void setKiteche(String kiteche) {
	this.kiteche = kiteche;
}
public String getHalfnab() {
	return halfnab;
}
public void setHalfnab(String halfnab) {
	this.halfnab = halfnab;
}
public int getRent_code() {
	return rent_code;
}
public void setRent_code(int rent_code) {
	this.rent_code = rent_code;
}
public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
public String getGroup_code() {
	return group_code;
}
public void setGroup_code(String group_code) {
	this.group_code = group_code;
}
public String getBook_code() {
	return book_code;
}
public void setBook_code(String book_code) {
	this.book_code = book_code;
}
public Date getRent_date() {
	return rent_date;
}
public void setRent_date(Date rent_date) {
	this.rent_date = rent_date;
}
public Date getReturn_date() {
	return return_date;
}
public void setReturn_date(Date return_date) {
	this.return_date = return_date;
	
}
@Override
public String toString() {
	return "RentDTO [rent_code=" + rent_code + ", userid=" + userid + ", group_code=" + group_code + ", book_code="
			+ book_code + ", rent_date=" + rent_date + ", return_date=" + return_date + ", halfnab=" + halfnab
			+ ", kiteche=" + kiteche + ", delay_date=" + delay_date + ", delay_gigan=" + delay_gigan + ", penalty="
			+ penalty + "]";
}


}
