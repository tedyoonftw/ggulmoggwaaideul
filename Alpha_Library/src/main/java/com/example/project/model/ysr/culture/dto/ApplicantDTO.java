package com.example.project.model.ysr.culture.dto;

public class ApplicantDTO {
	
	private int cno;
	private int ano;
	private String userid;
	private String username;
	private String title;
	
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return "ApplicantDTO [cno=" + cno + ", ano=" + ano + ", userid=" + userid + ", username=" + username
				+ ", title=" + title + "]";
	}

}
