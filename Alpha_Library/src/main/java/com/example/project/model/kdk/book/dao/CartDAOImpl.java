package com.example.project.model.kdk.book.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.kdk.book.dto.CartDTO;

@Repository
public class CartDAOImpl implements CartDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<CartDTO> listCart(
			String search_option, String keyword,int start, int end, String userid)
															throws Exception {
		System.out.println("서치옵션"+search_option);
		System.out.println("키워드"+keyword);
		Map<String,Object> map=new HashMap<>();
		map.put("userid", userid);
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start); //맵에 자료 저장
		map.put("end", end);
// mapper에는 2개 이상의 값을 전달할 수 없음(dto 또는 map 사용)
		return sqlSession.selectList("cart.listCart", map);
	}

	@Override
	public void delete(int cart_id) {
		sqlSession.delete("cart.delete", cart_id);
	}

	@Override
	public void deleteAll(String userid) {
		sqlSession.delete("cart.deleteAll", userid);
	}

	@Override
	public void update(int cart_id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insert(CartDTO dto) {
		sqlSession.insert("cart.insert", dto);
	}

	//레코드 갯수 계산
	@Override
	public int countArticle(String search_option, String keyword,String userid) throws Exception {
		Map<String,String> map=new HashMap<>();
		map.put("userid", userid);
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("cart.countArticle",map);
	}

	@Override
	public int countCart(String book_code, String userid) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("book_code", book_code);
		map.put("userid", userid);
		return sqlSession.selectOne("cart.countCart", map);
	}

}
