package com.example.project.model.ysr.notice.dto;

import java.util.Arrays;
import java.util.Date;

public class NoticeDTO {
	
	private int nno;
	private String title;
	private String content;
	private String adminid;
	private Date regdate;
	private int viewcnt;
	private String adminname;
	private int cnt;
	private String show;
	private String[] files;
	
	public int getNno() {
		return nno;
	}
	public void setNno(int nno) {
		this.nno = nno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAdminid() {
		return adminid;
	}
	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public int getViewcnt() {
		return viewcnt;
	}
	public void setViewcnt(int viewcnt) {
		this.viewcnt = viewcnt;
	}
	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public String getShow() {
		return show;
	}
	public void setShow(String show) {
		this.show = show;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] files) {
		this.files = files;
	}
	
	@Override
	public String toString() {
		return "NoticeDTO [nno=" + nno + ", title=" + title + ", content=" + content + ", adminid=" + adminid
				+ ", regdate=" + regdate + ", viewcnt=" + viewcnt + ", adminname=" + adminname + ", cnt=" + cnt
				+ ", show=" + show + ", files=" + Arrays.toString(files) + "]";
	}
	
}
