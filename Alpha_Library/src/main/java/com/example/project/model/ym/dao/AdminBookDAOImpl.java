package com.example.project.model.ym.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.model.ym.dto.AdminBookDTO;

@Repository
public class AdminBookDAOImpl implements AdminBookDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<AdminBookDTO> listBook(String search_option, String keyword, int start, int end) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
	//mapper에는 2개 이상의 값을 전달할 수 없음(dto 또는 map 사용)
		return sqlSession.selectList("adminbook.book_list", map);
	}

	@Override
	public BookDTO detailBook(String book_code) {
		return sqlSession.selectOne("adminbook.book_detail", book_code);
	}

	@Override
	public void updateBook(BookDTO dto) {
		sqlSession.update("adminbook.book_update",dto);
	}

	@Override
	public void deleteBook(String book_code) {
		sqlSession.delete("adminbook.book_delete",book_code);
	}

	@Override
	public void insertBook(BookDTO dto) {
		sqlSession.insert("adminbook.book_insert", dto);
	}

	@Override
	public String fileInfo(String book_code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("adminbook.countArticle",map);
	}

	@Override
	public List<AdminBookDTO> listAll() throws Exception {
		return sqlSession.selectList("adminbook.listAll");
	}

	
}
