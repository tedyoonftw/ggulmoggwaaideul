package com.example.project.model.ym.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ym.dto.UrmDTO;

@Repository
public class UrmDAOImpl implements UrmDAO{

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<UrmDTO> list() {
		return sqlSession.selectList("urm.list");
	}

}
