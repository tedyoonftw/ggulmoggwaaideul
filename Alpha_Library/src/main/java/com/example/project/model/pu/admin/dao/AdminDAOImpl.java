package com.example.project.model.pu.admin.dao;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.pu.admin.dto.AdminDTO;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public AdminDTO viewAdmin(String adminid) {
		return sqlSession.selectOne("admin.viewAdmin", adminid);
	}

	@Override
	public boolean loginCheck(AdminDTO dto, HttpSession session) {
		String name=sqlSession.selectOne("admin.login_check", dto);
		return (name==null) ? false : true;
	}

	@Override
	public void signup(AdminDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void signout(String adminid) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modify(AdminDTO dto) {
		// TODO Auto-generated method stub

	}

}
