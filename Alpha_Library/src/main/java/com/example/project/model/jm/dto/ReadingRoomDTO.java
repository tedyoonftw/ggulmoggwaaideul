package com.example.project.model.jm.dto;

public class ReadingRoomDTO {
	
	private String userid;
	private String stime;
	private int seatno;
	private String name;
	private int seatcount;
	private int gender;
	private String etime;
	private String sadragon;
	private String prereserve;
	
	public String getPrereserve() {
		return prereserve;
	}
		
	public void setPrereserve(String prereserve) {
		this.prereserve = prereserve;
	}
	public String getSadragon() {
		return sadragon;
	}
	public void setSadragon(String sadragon) {
		this.sadragon = sadragon;
	}
	public String getEtime() {
		return etime;
	}
	public void setEtime(String etime) {
		this.etime = etime;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getSeatcount() {
		return seatcount;
	}
	public void setSeatcount(int seatcount) {
		this.seatcount = seatcount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getStime() {
		return stime;
	}
	public void setStime(String stime) {
		this.stime = stime;
	}
	public int getSeatno() {
		return seatno;
	}
	public void setSeatno(int seatno) {
		this.seatno = seatno;
	}
	
	
	@Override
	public String toString() {
		return "ReadingRoomDTO [userid=" + userid + ", stime=" + stime + ", seatno=" + seatno + ", name=" + name
				+ ", seatcount=" + seatcount + ", gender=" + gender + ", etime=" + etime + ", sadragon=" + sadragon
				+ ", prereserve=" + prereserve + "]";
	}
	
	

}
