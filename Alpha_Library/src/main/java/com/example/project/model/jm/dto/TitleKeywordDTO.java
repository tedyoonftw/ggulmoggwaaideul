package com.example.project.model.jm.dto;

import java.util.Date;

public class TitleKeywordDTO {
	
	String userid;
	String book_code;
	Date viewdate;
	String title;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "TitleKeywordDTO [userid=" + userid + ", book_code=" + book_code + ", viewdate=" + viewdate + ", title="
				+ title + "]";
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getBook_code() {
		return book_code;
	}
	public void setBook_code(String book_code) {
		this.book_code = book_code;
	}
	public Date getViewdate() {
		return viewdate;
	}
	public void setViewdate(Date viewdate) {
		this.viewdate = viewdate;
	}
	

}
