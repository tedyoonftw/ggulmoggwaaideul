package com.example.project.model.jm.dao;

import java.util.Date;
import java.util.List;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.kdk.book.dto.BookDTO;

public interface RentDAO {
	public void halfnab(int rent_code, Date return_date);

	public int newRentCount(String userid);

	public void smoke(int rentno, Date return_date);

	void rent(RentDTO dto);

	void updatebyrent(String book_code);

	RentDTO getrentinfo(int rent_code);

	void updateByReturnbook(String book_code);

	BookDTO bookinfo(String book_code);

	String rentinfouserid(String userid, String book_code);

	public List<RentDTO> RentinfoListByUserid(String userid);

	public void updaterentfordelay();

	public int delaycount(String userid);

	public MypageDTOj getinfo(int rentno);

	List<RentDTO> FindInfoListByUserid(String userid);

	public void updatePrePanelty(int rent_code);

	void updateDelayGigan(int pre_rent_code, int rent_code);

	public void startPanelty(int rent_code);

	public void updateDelayDate(int rent_code);

	public List<RentDTO> checkDelay(String userid);

	public void delayhalfnab(int rent_code, Date return_date);

	public void updatePaneltyday();

	public String getdelaygigan(String userid);

}
