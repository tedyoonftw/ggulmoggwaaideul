package com.example.project.model.hjh.mypage.dao;

import java.util.Date;
import java.util.List;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;

public interface MypageDAOj {
	public List<MypageDTOj> nowrent(String userid);
	public List<MypageDTOj> beforerent(String userid);
	public List<MypageDTOj> bookcart(String userid);
	public MypageDTOj getinfo(int rentno);
	
	public List<MypageDTOj> wishlist(String userid);
	public List<MypageDTOj> admin_wishlist(String adminid);
	//hopebook
	public void hopebook(String hpdate, String userid, String title,
			String author,String publisher,String isbn,String pubyear,
			String content);
	public MypageDTOj detail(int hpnum);
	public void wishupdate(int hpnum,String title, String author, String publisher, 
			String isbn, String pubyear, String content);
	public void wishdelete(int hpnum);
	public void proceed(int hpnum,String userid);
	
	
	
	
	
	

}
