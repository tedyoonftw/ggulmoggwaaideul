package com.example.project.model.jm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.jm.dto.BestTitleDTO;
import com.example.project.model.jm.dto.KeywordDTO;
import com.example.project.model.jm.dto.TitleKeywordDTO;

@Repository
public class SearchDAOImpl {

	@Inject
	SqlSession sqlsession;

	public List<KeywordDTO> searchList(String userid) {

		return sqlsession.selectList("keyword.searchList", userid);
	}

	// 이거추가 0309 김정명
	public void insertKey(String userid, String keyword) {
		Map<String, Object> map = new HashMap<>();
		map.put("userid", userid);
		map.put("keyword", keyword);
		sqlsession.insert("keyword.insertkeyword", map);
	}

	// 이거 추가 0310 정명
	public void insertTitleKeyword(String book_code, String userid) {
		Map<String, Object> map = new HashMap<>();
		map.put("userid", userid);
		map.put("book_code", book_code);
		sqlsession.insert("keyword.inserttitlekeyword", map);
	}

	public List<TitleKeywordDTO> getTitleKeyword(String userid) {
		return sqlsession.selectList("keyword.gettitlekeyword", userid);
	}

	public void insertkeyword(String keyword) {
		sqlsession.insert("keyword.insertkeyword", keyword);
	}

	public String findkeyword(String keyword) {

		return sqlsession.selectOne("keyword.findkeyword", keyword);

	}

	public List<KeywordDTO> getIngiKeyword() {
		return sqlsession.selectList("keyword.ingikeyword");
	}

	public void viewnoplus(String keyword) {

		sqlsession.update("keyword.viewnoplus", keyword);
	}

	public void newkeyword(String keyword) {
		sqlsession.insert("keyword.newkeyword", keyword);
	}

	public List<BestTitleDTO> getBestTile4() {
		return sqlsession.selectList("keyword.bestbook4");
	}
	public List<BestTitleDTO> getBestTile8() {
		return sqlsession.selectList("keyword.bestbook8");
	}

	public String findBestTitle(String book_code) {
		return sqlsession.selectOne("keyword.findbookcode",book_code);
	}

	public void newBestTitle(String book_code) {
		sqlsession.insert("keyword.newbestbook",book_code);
	}

	public void rentnoPlus(String book_code) {
		sqlsession.update("keyword.rentnoplus",book_code);
	}
}
