package com.example.project.model.hjh.book.dto;

import java.util.Date;

public class BookDTOj {
	private String appendixYn;	//부록유무 Y,N
	private String author;			//저작자
	private String book_code;		//책 레코드키
	private String book_no;		//청구기호
	private String controlNo;		//제어번호
	private String isbn;				//낱권ISBN
	private String libName;			//소장도서관(이름)
	private String manageCode;	//소장도서관(관리구분)
	private String pubYear;			//발행년도
	private String publisher;		//발행자
	private String regNo;			//등록번호
	private String rnum;				//결과순번
	private String shelfLocName;	//소장자료설명
	private String speciesKey;		//레코드키
	private String title;				//표제
	private String description;		//책소개
	private String imgsrc;			//책이미지
	private String group_code;		
	
	private int book_tf;				//도서상태(대출가능여부 가능1, 불가능0)
	private Date regdate;
	private String witchimg;
	
	private String group_name;
	//getter, setter, toString
	
	
	
	public String getAppendixYn() {
		return appendixYn;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getWitchimg() {
		return witchimg;
	}
	public void setWitchimg(String witchimg) {
		this.witchimg = witchimg;
	}
	public int getBook_tf() {
		return book_tf;
	}
	public void setBook_tf(int book_tf) {
		this.book_tf = book_tf;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public String getGroup_code() {
		return group_code;
	}
	public void setGroup_code(String group_code) {
		this.group_code = group_code;
	}
	public void setAppendixYn(String appendixYn) {
		this.appendixYn = appendixYn;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBook_code() {
		return book_code;
	}
	public void setBook_code(String book_code) {
		this.book_code = book_code;
	}
	public String getBook_no() {
		return book_no;
	}
	public void setBook_no(String book_no) {
		this.book_no = book_no;
	}
	public String getControlNo() {
		return controlNo;
	}
	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getLibName() {
		return libName;
	}
	public void setLibName(String libName) {
		this.libName = libName;
	}
	public String getManageCode() {
		return manageCode;
	}
	public void setManageCode(String manageCode) {
		this.manageCode = manageCode;
	}
	public String getPubYear() {
		return pubYear;
	}
	public void setPubYear(String pubYear) {
		this.pubYear = pubYear;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getRnum() {
		return rnum;
	}
	public void setRnum(String rnum) {
		this.rnum = rnum;
	}
	public String getShelfLocName() {
		return shelfLocName;
	}
	public void setShelfLocName(String shelfLocName) {
		this.shelfLocName = shelfLocName;
	}
	public String getSpeciesKey() {
		return speciesKey;
	}
	public void setSpeciesKey(String speciesKey) {
		this.speciesKey = speciesKey;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	@Override
	public String toString() {
		return "BookDTOj [appendixYn=" + appendixYn + ", author=" + author + ", book_code=" + book_code + ", book_no="
				+ book_no + ", controlNo=" + controlNo + ", isbn=" + isbn + ", libName=" + libName + ", manageCode="
				+ manageCode + ", pubYear=" + pubYear + ", publisher=" + publisher + ", regNo=" + regNo + ", rnum="
				+ rnum + ", shelfLocName=" + shelfLocName + ", speciesKey=" + speciesKey + ", title=" + title
				+ ", description=" + description + ", imgsrc=" + imgsrc + ", group_code=" + group_code + ", book_tf="
				+ book_tf + ", regdate=" + regdate + ", witchimg=" + witchimg + ", group_name=" + group_name + "]";
	}
	
	
	

	
	
	
	
	

}
