package com.example.project.model.pu.login.dao;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.pu.login.dto.UserDTO;

@Repository
public class UserDAOImpl implements UserDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public UserDTO viewUser(String userid) {
		return sqlSession.selectOne("user.viewUser", userid);
	}

	@Override
	public boolean loginCheck(UserDTO dto) {
		String name=sqlSession.selectOne("user.login_check", dto);
		return (name==null) ? false : true;
	}

	@Override
	public void signup(UserDTO dto) {
		System.out.println(dto);
		sqlSession.insert("user.insert",dto);
	}

	@Override
	public void signout(String userid) {
	}

	@Override
	public void modify(UserDTO dto) {
	}

	@Override
	public String check_id(String userid) {
		return sqlSession.selectOne("user.check_id", userid);
	}

}
