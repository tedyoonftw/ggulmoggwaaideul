package com.example.project.model.pu.login.dao;

import com.example.project.model.pu.login.dto.UserDTO;

public interface UserDAO {

	public UserDTO viewUser(String userid);
	public boolean loginCheck(UserDTO dto);
	public String check_id(String userid);
	void signup(UserDTO dto);
	void signout(String userid);
	void modify(UserDTO dto);
}
