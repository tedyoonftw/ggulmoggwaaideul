package com.example.project.model.kdk.book.dto;

public class CartDTO {
	private int cart_id;
	private String userid;
	private String title;			//표제
	private String author;		//저작자
	private String publisher;	//발행자
	private String book_no;	//청구기호
	private String book_code;		//책 레코드키
	
	private int book_tf;			//도서상태(대출가능여부 가능1, 불가능0)

	//getter, setter, toString
	
	public int getCart_id() {
		return cart_id;
	}

	public void setCart_id(int cart_id) {
		this.cart_id = cart_id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getBook_no() {
		return book_no;
	}

	public void setBook_no(String book_no) {
		this.book_no = book_no;
	}

	public String getBook_code() {
		return book_code;
	}

	public void setBook_code(String book_code) {
		this.book_code = book_code;
	}

	public int getBook_tf() {
		return book_tf;
	}

	public void setBook_tf(int book_tf) {
		this.book_tf = book_tf;
	}

	@Override
	public String toString() {
		return "CartDTO [cart_id=" + cart_id + ", userid=" + userid + ", title=" + title + ", author=" + author
				+ ", publisher=" + publisher + ", book_no=" + book_no + ", book_code=" + book_code + ", book_tf="
				+ book_tf + "]";
	}
	

	
	
}
