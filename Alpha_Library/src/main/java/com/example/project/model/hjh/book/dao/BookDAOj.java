package com.example.project.model.hjh.book.dao;

import java.util.List;

import com.example.project.model.hjh.book.dto.BookDTOj;

public interface BookDAOj {
	public List<BookDTOj> searchList(String search_option, String keyword,int start,int end);
	public List<BookDTOj> search2List(BookDTOj dtoJ,String order_option,
			String date1, String date2,int start,int end);
	public List<BookDTOj> search3List(String group_code, 
			String period, String realrealperiod,int start,int end);
	public int count(String search_option, String keyword);
	public int count2(BookDTOj dtoJ,String order_option,
			String date1, String date2);
	public int count3(String group_code, 
			String period, String realrealperiod);
	public BookDTOj bookinfo(String book_code);
}
