package com.example.project.model.ym.dao;

import java.util.List;

import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.model.ym.dto.AdminBookDTO;

public interface AdminBookDAO {
BookDTO detailBook(String book_code);
void updateBook(BookDTO dto);
void deleteBook(String book_code);
void insertBook(BookDTO dto);
String fileInfo(String book_code);

public List<AdminBookDTO> listBook(String search_option, String keyword, int start, int end) throws Exception;
public List<AdminBookDTO> listAll() throws Exception;
//레코드 갯수 계산 
public int countArticle(String search_option, String keyword) throws Exception;

}
