package com.example.project.model.ysr.culture.dto;

import java.util.Arrays;

public class CultureDTO {
	
	private int cno;
	private String title;
	private String target;
	private String adminid;
	private String adminname;
	private String userid;
	private String username;
	private String content;
	private String duedate;
	private String sdate;
	private String place;
	private String fee;
	private String appstate;
	private String status;
	private int cnt;
	private int max;
	private String url;
	private String fileName;
	private String[] files;
	
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getAdminid() {
		return adminid;
	}
	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}
	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public String getAppstate() {
		return appstate;
	}
	public void setAppstate(String appstate) {
		this.appstate = appstate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] files) {
		this.files = files;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public String toString() {
		return "CultureDTO [cno=" + cno + ", title=" + title + ", target=" + target + ", adminid=" + adminid
				+ ", adminname=" + adminname + ", userid=" + userid + ", username=" + username + ", content=" + content
				+ ", duedate=" + duedate + ", sdate=" + sdate + ", place=" + place + ", fee=" + fee + ", appstate="
				+ appstate + ", status=" + status + ", cnt=" + cnt + ", max=" + max + ", url=" + url + ", fileName="
				+ fileName + ", files=" + Arrays.toString(files) + "]";
	}
}
