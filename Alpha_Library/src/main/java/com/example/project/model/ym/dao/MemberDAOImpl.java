package com.example.project.model.ym.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ym.dto.MemberDTO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<MemberDTO> list(String search_option, String keyword, int start, int end) {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("membership.list",map);
	}

	@Override
	public MemberDTO view(String userid) throws Exception {
		return sqlSession.selectOne("membership.view",userid);
	}

	@Override
	public void delete(String userid) throws Exception {
		sqlSession.delete("membership.delete",userid);
	}

	@Override
	public void update(MemberDTO dto) throws Exception {
		sqlSession.update("membership.update",dto);
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		Map<String,Object> map = new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("membership.countArticle",map);
	}

	@Override
	public List<MemberDTO> listAll() {
		return sqlSession.selectList("membership.listAll");
	}

}
