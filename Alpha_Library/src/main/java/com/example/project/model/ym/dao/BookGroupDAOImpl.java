package com.example.project.model.ym.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ym.dto.BookGroupDTO;
import com.example.project.model.ym.dto.MemberDTO;

@Repository
public class BookGroupDAOImpl implements BookGroupDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public List<BookGroupDTO> list() {
		return sqlSession.selectList("bookgroup.list");
	}

	@Override
	public List<MemberDTO> list2() {
		return sqlSession.selectList("bookgroup.list2");
	}

	@Override
	public List<BookGroupDTO> list3() {
		return sqlSession.selectList("bookgroup.list3");
	}
	
	

}
