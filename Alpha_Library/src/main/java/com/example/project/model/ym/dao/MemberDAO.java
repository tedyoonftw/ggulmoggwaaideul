package com.example.project.model.ym.dao;

import java.util.List;

import com.example.project.model.ym.dto.MemberDTO;

public interface MemberDAO {
	public List<MemberDTO> list(String search_option, String keyword, int start, int end);
	public List<MemberDTO> listAll();
	public MemberDTO view(String userid) throws Exception;
	public void delete(String userid) throws Exception;
	public void update(MemberDTO dto) throws Exception;
	public int countArticle(String search_option, String keyword) throws Exception;
}
