package com.example.project.model.hjh.mypage.dto;

import java.util.Date;

public class MypageDTOj {
	private int rent_code;
	private String userid;
	private String username;
	private String book_code;
	private String group_code;
	private String title;
	private String author;
	private String publisher;
	private Date rent_date;
	private Date return_date;
	private int book_tf;
	private String book_no;
	private int cart_id;
	//hopebook
	private int hpnum;
	private Date hpdate;
	private String isbn;
	private String content;
	private String pubyear;
	private int wstatus;
	private String halfnab;
	private String kiteche; //연체
	String delay_date;
	String delay_gigan;
	String penalty;
	
	
	public String getDelay_date() {
		return delay_date;
	}
	public void setDelay_date(String delay_date) {
		this.delay_date = delay_date;
	}
	public String getDelay_gigan() {
		return delay_gigan;
	}
	public void setDelay_gigan(String delay_gigan) {
		this.delay_gigan = delay_gigan;
	}
	public String getPenalty() {
		return penalty;
	}
	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}
	public String getKiteche() {
		return kiteche;
	}
	public void setKiteche(String kiteche) {
		this.kiteche = kiteche;
	}
	public String getHalfnab() {
		return halfnab;
	}
	public void setHalfnab(String halfnab) {
		this.halfnab = halfnab;
	}
	public int getHpnum() {
		return hpnum;
	}
	public void setHpnum(int hpnum) {
		this.hpnum = hpnum;
	}
	public Date getHpdate() {
		return hpdate;
	}
	public void setHpdate(Date hpdate) {
		this.hpdate = hpdate;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPubyear() {
		return pubyear;
	}
	public void setPubyear(String pubyear) {
		this.pubyear = pubyear;
	}
	public int getWstatus() {
		return wstatus;
	}
	public void setWstatus(int wstatus) {
		this.wstatus = wstatus;
	}
	public int getCart_id() {
		return cart_id;
	}
	public void setCart_id(int cart_id) {
		this.cart_id = cart_id;
	}
	public String getBook_no() {
		return book_no;
	}
	public void setBook_no(String book_no) {
		this.book_no = book_no;
	}
	public int getBook_tf() {
		return book_tf;
	}
	public void setBook_tf(int book_tf) {
		this.book_tf = book_tf;
	}
	public int getRent_code() {
		return rent_code;
	}
	public void setRent_code(int rent_code) {
		this.rent_code = rent_code;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getBook_code() {
		return book_code;
	}
	public void setBook_code(String book_code) {
		this.book_code = book_code;
	}
	public String getGroup_code() {
		return group_code;
	}
	public void setGroup_code(String group_code) {
		this.group_code = group_code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public Date getRent_date() {
		return rent_date;
	}
	public void setRent_date(Date rent_date) {
		this.rent_date = rent_date;
	}
	public Date getReturn_date() {
		return return_date;
	}
	public void setReturn_date(Date return_date) {
		this.return_date = return_date;
	}
	
	@Override
	public String toString() {
		return "MypageDTOj [rent_code=" + rent_code + ", userid=" + userid + ", username=" + username + ", book_code="
				+ book_code + ", group_code=" + group_code + ", title=" + title + ", author=" + author + ", publisher="
				+ publisher + ", rent_date=" + rent_date + ", return_date=" + return_date + ", book_tf=" + book_tf
				+ ", book_no=" + book_no + ", cart_id=" + cart_id + ", hpnum=" + hpnum + ", hpdate=" + hpdate
				+ ", isbn=" + isbn + ", content=" + content + ", pubyear=" + pubyear + ", wstatus=" + wstatus
				+ ", halfnab=" + halfnab + ", kiteche=" + kiteche + "]";
	}
	
	
	
	
	
	
	
	
	
}
