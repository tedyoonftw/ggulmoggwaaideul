package com.example.project.model.ysr.qna.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ysr.qna.dto.QnADTO;

@Repository
public class QnADAOImpl implements QnADAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<QnADTO> list(String search_option, String keyword, int start, int end) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("qna.list",map);
	}

	@Override
	public void create(QnADTO dto) throws Exception {
		sqlSession.insert("qna.insert1",dto);
	}
	
	@Override
	public void create2(QnADTO dto) throws Exception {
		sqlSession.insert("qna.insert2",dto);
	}

	@Override
	public void update(QnADTO dto) throws Exception {
		sqlSession.update("qna.update",dto);
	}

	@Override
	public void delete(int qno) throws Exception {
		sqlSession.delete("qna.delete",qno);
	}

	@Override
	public QnADTO read(int qno) throws Exception {
		return sqlSession.selectOne("qna.read", qno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("qna.countArticle",map);
	}

	@Override
	public void increaseViewcnt(int qno) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFile(String fullName) {
		sqlSession.insert("qna.addFile",fullName);
	}

	@Override
	public List<String> getFile(int qno) {
		return sqlSession.selectList("qna.getFile",qno);
	}

	@Override
	public void updateFile(String fullName, int qno) {
		Map<String,Object> map=new HashMap<>();
		map.put("fullName", fullName);
		map.put("qno", qno);
		sqlSession.insert("qna.updateFile",map);
	}

	@Override
	public void deleteFile(int qno) {
		System.out.println("qno:"+qno);
		sqlSession.delete("qna.deleteFile",qno);
	}

	@Override
	public void updateStatus(int qno) throws Exception {
		sqlSession.update("qna.updateStatus",qno);
	}

}
