package com.example.project.model.ysr.culture.dao;

import java.util.List;

import com.example.project.model.ysr.culture.dto.ApplicantDTO;

public interface ApplicantDAO {
	
	public void insert(ApplicantDTO dto) throws Exception;
	public List<ApplicantDTO> list() throws Exception;
	public void delete(int ano) throws Exception;
	public String check(String userid,int cno) throws Exception;
}
