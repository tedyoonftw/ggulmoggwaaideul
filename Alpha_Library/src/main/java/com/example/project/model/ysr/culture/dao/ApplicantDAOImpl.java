package com.example.project.model.ysr.culture.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.ysr.culture.dto.ApplicantDTO;

@Repository
public class ApplicantDAOImpl implements ApplicantDAO {
	
	@Inject
	SqlSession sqlSession;
	
	@Override
	public void insert(ApplicantDTO dto) throws Exception {
		sqlSession.insert("applicant.insert",dto);
	}

	@Override
	public List<ApplicantDTO> list() throws Exception {
		return sqlSession.selectList("applicant.list");
	}

	@Override
	public void delete(int ano) throws Exception {
		sqlSession.update("applicant.delete",ano);
	}

	@Override
	public String check(String userid,int cno) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("userid", userid);
		map.put("cno", cno);
		return sqlSession.selectOne("applicant.check",map);
	}

}
