package com.example.project.model.gm.dao;

import java.util.HashMap;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class EmailDAOImpl implements EmailDAO {
	@Inject
	SqlSession sqlSession;
	
	@Override
	public String check(String email1, String email2) {
		HashMap<String,String> map=new HashMap<>();
		map.put("email1", email1);
		map.put("email2", email2);
		return sqlSession.selectOne("email.check",map);
	}

	@Override
	public void insertMail(String email) {
		sqlSession.insert("email.insertMail",email);
	}

	@Override
	public void updateMail(String email) {
		sqlSession.update("email.updateMail",email);
	}
	
	@Override
	public void updateMail2(String email) {
		sqlSession.update("email.updateMail2",email);
	}

	@Override
	public int mailCheck(String email) {
		return sqlSession.selectOne("email.mailCheck",email);
	}
	
	@Override
	public int mailCheck2(String email) {
		return sqlSession.selectOne("email.mailCheck2",email);
	}

}
