package com.example.project.model.pu.admin.dto;

import java.util.Date;

public class AdminDTO {

	private String adminid;
	private String adminpw;
	private String adminname;
	private String mail;
	private Date regi_date;

	public AdminDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getAdminid() {
		return adminid;
	}

	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}

	public String getAdminpw() {
		return adminpw;
	}

	public void setAdminpw(String adminpw) {
		this.adminpw = adminpw;
	}

	public String getAdminname() {
		return adminname;
	}

	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getRegi_date() {
		return regi_date;
	}

	public void setRegi_date(Date regi_date) {
		this.regi_date = regi_date;
	}

	@Override
	public String toString() {
		return "AdminDTO [adminid=" + adminid + ", adminpw=" + adminpw + ", adminname=" + adminname + ", mail=" + mail
				+ ", regi_date=" + regi_date + "]";
	}

}