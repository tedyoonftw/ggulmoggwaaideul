package com.example.project.model.ym.dao;

import java.util.List;

import com.example.project.model.ym.dto.BookGroupDTO;
import com.example.project.model.ym.dto.MemberDTO;

public interface BookGroupDAO {
	List<BookGroupDTO> list ();
	List<MemberDTO> list2 ();
	List<BookGroupDTO> list3 ();
}
