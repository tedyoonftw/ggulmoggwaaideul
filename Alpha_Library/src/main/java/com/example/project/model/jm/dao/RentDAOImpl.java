package com.example.project.model.jm.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.kdk.book.dto.BookDTO;

@Repository
public class RentDAOImpl implements RentDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public BookDTO bookinfo(String book_code) {
		return sqlSession.selectOne("rent.bookinfo", book_code);
	}

	@Override
	public void rent(RentDTO dto) {
		sqlSession.insert("rent.insertrent", dto);
	}

	@Override
	public void updatebyrent(String book_code) {
		sqlSession.update("rent.updatebyrent", book_code);
	}

	@Override
	public RentDTO getrentinfo(int rent_code) {
		return sqlSession.selectOne("rent.rentinfo", rent_code);
	}

	@Override
	public void updateByReturnbook(String book_code) {
		sqlSession.update("rent.updatebyreturnbook", book_code);
	}
	// 대출정보를 확인함
	@Override
	public String rentinfouserid(String userid, String book_code) {
		Map<String, Object> map = new HashMap<>();
		map.put("userid", userid);
		map.put("book_code", book_code);
		return sqlSession.selectOne("rent.rentinfobybookcode", map);
	}

	@Override 
	public MypageDTOj getinfo(int rentno) { 
		return sqlSession.selectOne("rent.getinfo", rentno); 
	}

	@Override
	public void smoke(int rentno, Date return_date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> map = new HashMap<>();
		map.put("rentno", rentno);
		map.put("return_date", sdf.format(return_date.getTime() + 1000 * 60 * 60 * 24 * 7)); // 랜트관련(반납,연기)
		sqlSession.update("rent.smoke", map);

	}

	@Override
	public void halfnab(int rent_code, Date return_date) {
		Map<String, Object> map = new HashMap<>();
		map.put("rent_code", rent_code);
		map.put("return_date", return_date); //
		sqlSession.update("rent.halfnab", map); //
	}

	@Override
	public int newRentCount(String userid) {
		return sqlSession.selectOne("rent.nowrentcount", userid);
	}

	@Override
	public List<RentDTO> RentinfoListByUserid(String userid) {
		return sqlSession.selectList("rent.rentinfolistbyuserid", userid);
	}

	@Override
	public void updaterentfordelay() {
		 sqlSession.update("rent.updatedelay");
	}

	@Override // 연체중인 도서가 몇권인지를 카운팅해봄 
	public int delaycount(String userid) {
		return sqlSession.selectOne("rent.delaycount", userid);
	}

	@Override // userid 를 통해서 패널티중인 해당관리코드에 해당 하는 대출정보를 가져옴  
	public List<RentDTO> FindInfoListByUserid(String userid) {

		return sqlSession.selectList("rent.rentinfolistbyuserid",userid);
	}
	
	

	
	@Override //기존에 있던 패널티 여부를 'N' 으로 바꿈
	public void updatePrePanelty(int rent_code) {
		sqlSession.update("rent.updateprepanelty",rent_code);
	}
	
	
	
	@Override //연체와 동시에 패널티도 시작됨
	public void startPanelty(int rent_code) {
		sqlSession.update("rent.startpanelty",rent_code);
	}

	@Override
	public void updateDelayGigan(int pre_rent_code, int rent_code) { //기존의 패널티에서 연장이 되는 개념
		Map<String,Object> map = new HashMap<>();
		map.put("pre_rent_code", pre_rent_code);
		map.put("rent_code", rent_code);
		sqlSession.update("rent.updatepanelty",map);
		
	}

	@Override
	public void updateDelayDate(int rent_code) {
		sqlSession.update("rent.startpanelty",rent_code);
	}

	@Override
	public List<RentDTO> checkDelay(String userid) {
		return sqlSession.selectList("rent.checkdelay",userid);
	}

	@Override
	public void delayhalfnab(int rent_code, Date return_date) {
		Map<String, Object> map = new HashMap<>();
		map.put("rent_code", rent_code);
		map.put("return_date", return_date); 
		sqlSession.update("rent.delayhalfnab", map); 
	}

	@Override
	public void updatePaneltyday() {
		sqlSession.update("rent.endpanelty");
	}

	@Override
	public String getdelaygigan(String userid) {
		return sqlSession.selectOne("rent.getdelaygigan",userid);
	}
	
	
	

}
