package com.example.project.model.jm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.jm.dto.ReadingRoomDTO;
import com.example.project.model.jm.dto.urmDTO;
import com.example.project.model.pu.login.dto.UserDTO;


@Repository
public class ReadingRoomDAOImpl implements ReadingRoomDAO {
	
	@Inject
	SqlSession sqlsession;
	
	
	
	@Override
	public List<ReadingRoomDTO> logoutcheck() {
		return sqlsession.selectList("readingroom.logoutcheck");
	}



	@Override
	public void afterreserve(int seatno) {
		sqlsession.update("readingroom.afterprereserve",seatno);
	}



	@Override
	public void prereserve(int seatno, String userid) {
		Map<String,Object> map = new HashMap<>();
		map.put("seatno", seatno);
		map.put("userid",userid);
		sqlsession.update("readingroom.beforeprereserve",map);
		
	}



	@Override
	public void reserveinfo(urmDTO dto) {
		sqlsession.insert("readingroom.reservationinfo",dto);
	}

	

	@Override
	public void updatereservationinfo(urmDTO dto) {
		System.out.println("리딩룸 dao"+dto);
		sqlsession.update("readingroom.updatereservationinfo",dto);
	
	}

	@Override
	public String seatedmemberlist(String userid) {
		
		return sqlsession.selectOne("readingroom.viewseatedmember",userid);
	}

	@Override
	public ReadingRoomDTO view(int seatno) {
		return sqlsession.selectOne("readingroom.seatview",seatno);
	}

	@Override
	public UserDTO memberinfo(String userid) {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("readingroom.seatinfo" , userid);
	}


	@Override
	public int countseat() {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("readingroom.seatcount");
	}

	@Override
	public List<ReadingRoomDTO> list() {
		return sqlsession.selectList("readingroom.seatlist");
	}

	@Override
	public int reservation(ReadingRoomDTO dto) {
		return sqlsession.update("readingroom.reservation",dto);
	}

	@Override
	public void logout(int seatno) {
		sqlsession.update("readingroom.logout",seatno);
	}

	@Override
	public List<String> checkprereserve() {
		return sqlsession.selectList("readingroom.checkprereserve");
	}

	@Override
	public void viewOtherPage(String checkOtherPage) {
		sqlsession.update("readingroom.otherseatview",checkOtherPage);
	}
	
	
	
	
}
