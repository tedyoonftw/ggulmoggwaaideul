package com.example.project.model.jm.dto;

import java.util.Date;

public class ScheduleDTO {
	  private int schedule_id;  
      private Date schedule_date_time; 
      private String schedule_subject;
      private String schedule_content;
      private String schedule_date;
      private int curDay;
      private int curMonth;
      private int curYear;
      private int event;
      
      
      
	public int getEvent() {
		return event;
	}
	public void setEvent(int event) {
		this.event = event;
	}
	public int getCurDay() {
		return curDay;
	}
	public void setCurDay(int curDay) {
		this.curDay = curDay;
	}
	public int getCurMonth() {
		return curMonth;
	}
	public void setCurMonth(int curMonth) {
		this.curMonth = curMonth;
	}
	public int getCurYear() {
		return curYear;
	}
	public void setCurYear(int curYear) {
		this.curYear = curYear;
	}
	public int getSchedule_id() {
		return schedule_id;
	}
	public void setSchedule_id(int schedule_id) {
		this.schedule_id = schedule_id;
	}
	public Date getSchedule_date_time() {
		return schedule_date_time;
	}
	public void setSchedule_date_time(Date schedule_date_time) {
		this.schedule_date_time = schedule_date_time;
	}
	public String getSchedule_subject() {
		return schedule_subject;
	}
	public void setSchedule_subject(String schedule_subject) {
		this.schedule_subject = schedule_subject;
	}
	public String getSchedule_content() {
		return schedule_content;
	}
	public void setSchedule_content(String schedule_content) {
		this.schedule_content = schedule_content;
	}
	
	@Override
	public String toString() {
		return "ScheduleDTO [schedule_id=" + schedule_id + ", schedule_date_time=" + schedule_date_time
				+ ", schedule_subject=" + schedule_subject + ", schedule_content=" + schedule_content
				+ ", schedule_date=" + schedule_date + ", curDay=" + curDay + ", curMonth=" + curMonth + ", curYear="
				+ curYear + ", event=" + event + "]";
	}
	public String getSchedule_date() {
		return schedule_date;
	}
	public void setSchedule_date(String schedule_date) {
		this.schedule_date = schedule_date;
	}
      
	
}
