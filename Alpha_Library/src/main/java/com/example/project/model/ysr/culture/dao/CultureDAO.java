package com.example.project.model.ysr.culture.dao;

import java.util.List;

import com.example.project.model.ysr.culture.dto.CultureDTO;

public interface CultureDAO {
	
	public List<CultureDTO> list(String search_option,String keyword
		,int start, int end) throws Exception;
	public CultureDTO view(int cno,String username) throws Exception;
	public void insert(CultureDTO dto) throws Exception;
	public void update(CultureDTO dto) throws Exception;
	public void delete(int cno) throws Exception;
	
	public int countArticle(String search_option,String keyword);
	
	public void addFile(String fileName);
	public List<String> getFile(int cno);
	public void updateFile(String fullName, int cno);
	public void deleteFile(String fullName);
	public int insertUrl(CultureDTO dto);
}
