package com.example.project.model.jm.dto;

import java.util.Date;

public class KeywordDTO {
	String userid;
	String keyword;
	Date serachdate;
	String title;
	int viewno;
    
	public int getViewno() {
		return viewno;
	}
	public void setViewno(int viewno) {
		this.viewno = viewno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "KeywordDTO [userid=" + userid + ", keyword=" + keyword + ", serachdate=" + serachdate + ", title="
				+ title + ", viewno=" + viewno + "]";
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Date getSerachdate() {
		return serachdate;
	}
	public void setSerachdate(Date serachdate) {
		this.serachdate = serachdate;
	}
	

}
