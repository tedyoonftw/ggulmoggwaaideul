package com.example.project.model.pu.mypage.dao;

import com.example.project.model.pu.login.dto.UserDTO;

public interface MypageDAO {

	UserDTO userinfo(String userid);
	UserDTO passwd(String userid);
	public String id_find(UserDTO dto2);
	public String pw_find(UserDTO dto2);
	UserDTO bye(String userid);
	void cancel(String userid);
	void pw_modify(UserDTO dto);
	void info_modify(UserDTO dto);
	public void memo1(String userid);
	public void applicant(String userid);
	public void qna(String userid);
	public void readingroom(String userid);
	public void urm(String userid);
	public void hopebook(String userid);
	public void cart(String userid);
}