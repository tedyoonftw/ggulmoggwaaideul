package com.example.project.model.hjh.mypage.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;

@Repository
public class MypageDAOjImpl implements MypageDAOj {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<MypageDTOj> nowrent(String userid) {
		return sqlSession.selectList("mypage.nowrent", userid);
	}

	@Override
	public List<MypageDTOj> beforerent(String userid) {
		return sqlSession.selectList("mypage.beforerent", userid);
	}

	@Override
	public List<MypageDTOj> bookcart(String userid) {
		return sqlSession.selectList("mypage.bookcart", userid);
	}

	@Override
	public List<MypageDTOj> wishlist(String userid) {
		return sqlSession.selectList("mypage.wish" ,userid);
	}
	
	@Override
	public List<MypageDTOj> admin_wishlist(String adminid) {
		return sqlSession.selectList("mypage.admin_wish" ,adminid);
	}
	
	@Override
	public void hopebook(String hpdate, String userid, String title,
			String author,String publisher,String isbn,String pubyear,
			String content) {
		Map<String, Object> map=new HashMap<>();
		map.put("hpdate", hpdate);
		map.put("userid", userid);
		map.put("title", title);
		map.put("author", author);
		map.put("publisher", publisher);
		map.put("isbn", isbn);
		map.put("pubyear", pubyear);
		map.put("content", content);
		sqlSession.insert("mypage.add", map);
	}

	@Override
	public MypageDTOj detail(int hpnum) {
		return sqlSession.selectOne("mypage.detail", hpnum);
	}

	@Override
	public void wishupdate(int hpnum,String title, String author, String publisher, 
			String isbn, String pubyear, String content) {
		Map<String, Object> map=new HashMap<>();
		map.put("hpnum", hpnum);
		map.put("title", title);
		map.put("author", author);
		map.put("publisher", publisher);
		map.put("isbn", isbn);
		map.put("pubyear", pubyear);
		map.put("content", content);
		sqlSession.update("mypage.update", map);
	}

	@Override
	public void wishdelete(int hpnum) {
		sqlSession.delete("mypage.delete", hpnum);
	}

	@Override
	public void proceed(int hpnum,String userid) {
		Map<String,Object> map = new HashMap<>();
		map.put("hpnum", hpnum);
		map.put("userid", userid);
		sqlSession.update("mypage.proceed", map);
	}

	@Override 
	public MypageDTOj getinfo(int rentno) { 
		return sqlSession.selectOne("mypage.getinfo", rentno); 
	}

}
