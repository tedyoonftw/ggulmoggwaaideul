package com.example.project.model.pu.admin.dao;

import javax.servlet.http.HttpSession;

import com.example.project.model.pu.admin.dto.AdminDTO;

public interface AdminDAO {

	public AdminDTO viewAdmin(String adminid);
	public boolean loginCheck(AdminDTO dto, HttpSession session);
	void signup(AdminDTO dto);
	void signout(String adminid);
	void modify(AdminDTO dto);
}
