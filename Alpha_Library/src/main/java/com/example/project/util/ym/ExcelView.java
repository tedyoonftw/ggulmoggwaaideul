package com.example.project.util.ym;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.example.project.model.ym.dto.AdminBookDTO;

public class ExcelView extends AbstractExcelPOIView {
	 
  @SuppressWarnings("unchecked")
  @Override
  protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

          //Object로 넘어온 값을 각 Model에 맞게 형변환 해준다.
          List<AdminBookDTO> listBooks = (List<AdminBookDTO>) model.get("excelList");

          //Sheet 생성
          Sheet sheet = workbook.createSheet();
          Row row = null;
          int rowCount = 0;
          int cellCount = 0;

          // 제목 Cell 생성
          row = sheet.createRow(rowCount++);

          row.createCell(cellCount++).setCellValue("도서명");
          row.createCell(cellCount++).setCellValue("대분류");
          row.createCell(cellCount++).setCellValue("저자");
          row.createCell(cellCount++).setCellValue("출판사");
          row.createCell(cellCount++).setCellValue("대출일자");
          row.createCell(cellCount++).setCellValue("반납");
          row.createCell(cellCount++).setCellValue("대출자");

          // 데이터 Cell 생성
          for (AdminBookDTO book : listBooks) {
              row = sheet.createRow(rowCount++);
              cellCount = 0;
              row.createCell(cellCount++).setCellValue(book.getTitle()); //데이터를 가져와 입력
              row.createCell(cellCount++).setCellValue(book.getGroup_name());
              row.createCell(cellCount++).setCellValue(book.getAuthor());
              row.createCell(cellCount++).setCellValue(book.getPublisher());
              row.createCell(cellCount++).setCellValue(book.getRent_date());
              if (book.getHalfnab().equals("Y")) {
              	row.createCell(cellCount++).setCellValue("반납완료");
              
              }else {
              	row.createCell(cellCount++).setCellValue("대출 중");
              }
              
              row.createCell(cellCount++).setCellValue(book.getUsername());

          }


  }

  @Override
  protected Workbook createWorkbook() {
      return new XSSFWorkbook();
  }

}