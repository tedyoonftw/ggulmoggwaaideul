package com.example.project.service.ysr.qna;

import java.util.List;

import com.example.project.model.ysr.qna.dto.QnADTO;

public interface QnAService {
	
	public List<QnADTO> list(String search_option,
		String keyword,int start,int end) throws Exception;
	public void create(QnADTO dto) throws Exception;
	public void create2(QnADTO dto) throws Exception;
	public void update(QnADTO dto) throws Exception;
	public void delete(int qno) throws Exception;
	
	public QnADTO read(int qno) throws Exception;
	public int countArticle(String search_option,String keyword);
	public void increaseViewcnt(int qno) throws Exception;
	public List<String> getFile(int qno);
}
