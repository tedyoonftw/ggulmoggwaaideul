package com.example.project.service.hjh.mypage;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.hjh.mypage.dao.MypageDAOj;
import com.example.project.model.hjh.mypage.dto.MypageDTOj;
import com.example.project.model.jm.dto.RentDTO;

@Service
public class MypageServicejImpl implements MypageServicej {

	@Inject
	MypageDAOj mypageDao;
	
	@Override
	public List<MypageDTOj> nowrent(String userid) {
		return mypageDao.nowrent(userid);
	}

	@Override
	public List<MypageDTOj> beforerent(String userid) {
		return mypageDao.beforerent(userid);
	}

	@Override
	public List<MypageDTOj> bookcart(String userid) {
		return mypageDao.bookcart(userid);
	}

	@Override
	public MypageDTOj getinfo(int rentno) {
		return mypageDao.getinfo(rentno);
	}

	

	@Override
	public List<MypageDTOj> admin_wishlist(String adminid) {
		return mypageDao.admin_wishlist(adminid);
	}
	
	@Override
	public List<MypageDTOj> wishlist(String userid) {
		return mypageDao.wishlist(userid);
	}

	@Override
	public void hopebook(String hpdate, String userid, String title,
			String author,String publisher,String isbn,String pubyear,
			String content) {
		mypageDao.hopebook(hpdate,userid,title,author,publisher,
				 isbn,pubyear,content);
	}
	@Override
	public MypageDTOj detail(int hpnum) {
		return mypageDao.detail(hpnum);
	}
	
	@Override
	public void wishupdate(int hpnum,String title, String author, String publisher, String isbn, String pubyear,
			String content) {
		mypageDao.wishupdate(hpnum,title, author, publisher, isbn, pubyear, content);
	}
	
	@Override
	public void wishdelete(int hpnum) {
		mypageDao.wishdelete(hpnum);
	}
	
	@Override
	public void proceed(int hpnum,String userid) {
		mypageDao.proceed(hpnum,userid);
	}
	
	
	
}
