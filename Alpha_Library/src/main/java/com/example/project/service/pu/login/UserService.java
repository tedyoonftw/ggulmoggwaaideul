package com.example.project.service.pu.login;

import javax.servlet.http.HttpSession;

import com.example.project.model.pu.login.dto.UserDTO;

public interface UserService {
	
	public UserDTO viewUser(String userid);
	public boolean loginCheck(UserDTO dto, HttpSession session);
	public void logout(HttpSession session);
	public String check_id(String userid);
	void signup(UserDTO dto);
	void signout(String userid);
	void modify(UserDTO dto);

}
