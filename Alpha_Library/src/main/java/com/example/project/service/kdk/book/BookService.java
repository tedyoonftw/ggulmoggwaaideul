package com.example.project.service.kdk.book;

import java.util.List;

import com.example.project.model.kdk.book.dto.BookDTO;


public interface BookService {
	BookDTO detailBook(String book_code);
	void updateBook(BookDTO dto);
	void deleteBook(String book_code);
	void insertBook(BookDTO dto);
	String fileInfo(String book_code);
	
	public List<BookDTO> listBook(int start, int end) throws Exception;;
	//레코드 갯수 계산 
	public int countArticle() throws Exception;
	

	
}
