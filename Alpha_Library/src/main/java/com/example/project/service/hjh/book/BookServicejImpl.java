package com.example.project.service.hjh.book;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.hjh.book.dao.BookDAOj;
import com.example.project.model.hjh.book.dto.BookDTOj;

@Service
public class BookServicejImpl implements BookServicej {

	@Inject
	BookDAOj bookDaoj;
	
	@Override
	public List<BookDTOj> searchList(String search_option, String keyword,int start,int end) {
		return bookDaoj.searchList(search_option, keyword,start,end);
	}

	@Override
	public List<BookDTOj> search2List(BookDTOj dtoj, String order_option,
			String date1, String date2,int start,int end) {
		System.out.println("여기까진 와야지"+dtoj.getTitle());
		return bookDaoj.search2List(dtoj, order_option, date1, date2,start,end);
	}

	@Override
	public List<BookDTOj> search3List(String group_code, 
			String period, String realrealperiod,int start,int end) {
		return bookDaoj.search3List(group_code,
				period, realrealperiod,start,end);
	}

	@Override
	public int count(String search_option, String keyword) {
		return bookDaoj.count(search_option, keyword);
	}

	@Override
	public int count2(BookDTOj dtoJ, String order_option, String date1, String date2) {
		return bookDaoj.count2(dtoJ, order_option, date1, date2);
	}

	@Override
	public int count3(String group_code, String period, String realrealperiod) {
		return bookDaoj.count3(group_code, period, realrealperiod);
	}

	@Override
	public BookDTOj bookinfo(String book_code) {
		return bookDaoj.bookinfo(book_code);
	}

}
