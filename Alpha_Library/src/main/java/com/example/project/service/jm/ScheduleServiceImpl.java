package com.example.project.service.jm;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.jm.dao.ScheduleDAO;
import com.example.project.model.jm.dto.ScheduleDTO;
@Service
public class ScheduleServiceImpl implements ScheduleService {
	@Inject
	ScheduleDAO ScheduleDAO;
	
	@Override
	public List<ScheduleDTO> month_query(Date firstDayOfMonth, Date firstDayOfNextMonth) {
		return ScheduleDAO.month_query(firstDayOfMonth,firstDayOfNextMonth);
	
	}

	@Override
	public ScheduleDTO ShowSchedule(int schedule_id) {
		return ScheduleDAO.ShowSchedule(schedule_id);
	}

	@Override
	public void delete(int schedule_id) {
		ScheduleDAO.DeleteSchedule(schedule_id);
	}
	
	@Override
	public void update(ScheduleDTO dto) {
		String curDay = String.valueOf(dto.getCurDay());
		String curYear = String.valueOf(dto.getCurYear());
		String curMonth = String.valueOf(dto.getCurMonth());
		Date schedule_date_time = java.sql.Date.valueOf(curYear+"-"+curMonth+"-"+curDay);
		dto.setSchedule_date_time(schedule_date_time);
		ScheduleDAO.update(dto);
		
	}

	@Override
	public void insert(ScheduleDTO dto) {
		String curDay = String.valueOf(dto.getCurDay());
		String curYear = String.valueOf(dto.getCurYear());
		String curMonth = String.valueOf(dto.getCurMonth());
		Date schedule_date_time = java.sql.Date.valueOf(curYear+"-"+curMonth+"-"+curDay);
		dto.setSchedule_date_time(schedule_date_time);
		ScheduleDAO.insert(dto);
	}
	
}
