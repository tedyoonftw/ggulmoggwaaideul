package com.example.project.service.jm;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.jm.dao.SearchDAOImpl;
import com.example.project.model.jm.dto.BestTitleDTO;
import com.example.project.model.jm.dto.KeywordDTO;
import com.example.project.model.jm.dto.TitleKeywordDTO;
@Service
public class SearchKeywordImpl implements SearchKeyword {

	@Inject
	SearchDAOImpl searchDao;
	
	
	
	@Override
	public List<BestTitleDTO> getBestTile4() {
		return searchDao.getBestTile4();
	}

	
	@Override
	public List<BestTitleDTO> getBestTile8() {
		return searchDao.getBestTile8();
	}


	@Override
	public String findBestTitle(String book_code) {
		return searchDao.findBestTitle(book_code);
	}


	@Override
	public void newBestTitle(String book_code) {
		searchDao.newBestTitle(book_code);
	}


	@Override
	public void rentnoPlus(String book_code) {
		searchDao.rentnoPlus(book_code);
	}


	//바뀐부분 0310 정명
	@Override
	public void insertTitleKeyword(String book_code, String userid) {
	searchDao.insertTitleKeyword(book_code, userid);
	}
	 
	
	@Override
	public List<TitleKeywordDTO> getTitleKeyword(String userid) {
		return searchDao.getTitleKeyword(userid);
	}

	// 바뀐부분 0309 정명
	@Override
	public void insertKeyword(String userid, String keyword) {
		searchDao.insertKey(userid,keyword);
		
	}
	
	@Override
	public List<KeywordDTO> searchList(String userid) {
		return searchDao.searchList(userid);
	}


	@Override
	public List<KeywordDTO> getIngiKeyword() {
		return searchDao.getIngiKeyword();
	}


	@Override
	public String findkeyword(String keyword) {
		return searchDao.findkeyword(keyword);
	}


	@Override
	public void newkeyword(String keyword) {
		searchDao.newkeyword(keyword);
	}


	@Override
	public void viewnoplus(String keyword) {
		searchDao.viewnoplus(keyword);
	}
	
	
	
}
