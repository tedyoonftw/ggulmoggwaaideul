package com.example.project.service.ym;

import java.util.List;

import com.example.project.model.ym.dto.MemberDTO;

public interface MemberService {
public List<MemberDTO> list(String search_option, String keyword, int start, int end);
public MemberDTO view(String userid) throws Exception;
public void delete(String userid) throws Exception;
public void update(MemberDTO dto) throws Exception;
public int countArticle(String search_option, String keyword) throws Exception;
public List<MemberDTO> listAll();
}
