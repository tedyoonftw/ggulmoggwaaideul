package com.example.project.service.ysr.qna;

import java.util.List;

import com.example.project.model.ysr.qna.dto.ReplyDTO;

public interface ReplyService {
	
	public List<ReplyDTO> list(int qno);
	public int count(int qno);
	public void create(ReplyDTO dto,int qno) throws Exception;
}
