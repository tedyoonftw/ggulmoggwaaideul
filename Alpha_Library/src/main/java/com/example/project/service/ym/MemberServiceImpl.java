package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dao.MemberDAO;
import com.example.project.model.ym.dto.MemberDTO;

@Service
public class MemberServiceImpl implements MemberService{

	@Inject
	MemberDAO dao;
	
	@Override
	public List<MemberDTO> list(String search_option, String keyword, int start, int end) {
		return dao.list(search_option,keyword,start,end);
	}

	@Override
	public MemberDTO view(String userid) throws Exception {
		return dao.view(userid);
	}

	@Override
	public void delete(String userid) throws Exception {
		dao.delete(userid);
	}

	@Override
	public void update(MemberDTO dto) throws Exception {
		dao.update(dto);
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		return dao.countArticle(search_option,keyword);
	}

	@Override
	public List<MemberDTO> listAll() {
		return dao.listAll();
	}

}
