package com.example.project.service.pu.login;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.example.project.model.pu.login.dao.UserDAO;
import com.example.project.model.pu.login.dto.UserDTO;

@Service
public class UserServiceImpl implements UserService {

	@Inject
	UserDAO userDao;
	
	@Override
	public UserDTO viewUser(String userid) {
		return userDao.viewUser(userid);
	}

	@Override
	public boolean loginCheck(UserDTO dto, HttpSession session) {
		boolean result=userDao.loginCheck(dto);
		if(result) {
			UserDTO dto2=viewUser(dto.getUserid());
			session.setAttribute("userid", dto.getUserid());
			session.setAttribute("name", dto2.getUsername());
		}
		return result;
	}

	@Override
	public void logout(HttpSession session) {
		session.invalidate();
	}

	@Override
	public void signup(UserDTO dto) {
		userDao.signup(dto);
	}

	@Override
	public void signout(String userid) {
	}

	@Override
	public void modify(UserDTO dto) {
	}

	@Override
	public String check_id(String userid) {
		return userDao.check_id(userid);
	}

}
