package com.example.project.service.ysr.notice;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.ysr.notice.dao.NoticeDAO;
import com.example.project.model.ysr.notice.dto.NoticeDTO;

@Service
public class NoticeServiceImpl implements NoticeService {
	
	@Inject
	NoticeDAO noticeDao;
	
	@Override
	public List<NoticeDTO> list(String search_option, String keyword, int start, int end) throws Exception {
		return noticeDao.list(search_option, keyword, start, end);
	}
	
	@Transactional
	@Override
	public void create(NoticeDTO dto) throws Exception {
		noticeDao.create(dto);
		/*String[] files=dto.getFiles();
		if(files==null) return;
		for(String name : files) {
			noticeDao.addFile(name);
		}*/
	}
	
	@Transactional
	@Override
	public void update(NoticeDTO dto) throws Exception {
		noticeDao.update(dto);
		String[] files=dto.getFiles();
		if(files==null) return;
		for(String name : files) {
			noticeDao.updateFile(name, dto.getNno());
		}
	}
	
	@Override
	public void delete(String fullName,int nno) throws Exception {
		/*System.out.println("출력"+fullName+nno);
		noticeDao.deleteFile(fullName);*/
		noticeDao.delete(nno);
	}

	@Override
	public NoticeDTO read(int nno) throws Exception {
		return noticeDao.read(nno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		return noticeDao.countArticle(search_option, keyword);
	}

	@Override
	public void increaseViewcnt(int nno, HttpSession session) throws Exception {
		long update_time=0;
		if(session.getAttribute("update_time_"+nno)!=null) {
			//최근에 조회수를 올린 시간
			update_time=(long)session.getAttribute("update_time_"+nno);
		}
		long current_time=System.currentTimeMillis();
		//일정 시간이 경과한 후 조회수 증가 처리
		if(current_time - update_time > 5*1000) {
			//조회수 증가 처리 
			noticeDao.increaseViewcnt(nno);
			//조회수를 올린 시간 저장
			session.setAttribute("update_time_"+nno, current_time);
		}
	}

	@Override
	public void addFile(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getFile(int nno) {
		return noticeDao.getFile(nno);
	}

	@Override
	public void updateFile(String fullName, int nno) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteFile(String fullName) {
		// TODO Auto-generated method stub

	}

}
