package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.kdk.book.dto.BookDTO;
import com.example.project.model.ym.dao.AdminBookDAO;
import com.example.project.model.ym.dto.AdminBookDTO;

@Service
public class AdminBookServiceImpl implements AdminBookService {

	@Inject
	AdminBookDAO dao;
	
	@Override
	public List<AdminBookDTO> listBook(String search_option, String keyword, int start, int end) throws Exception {
		return dao.listBook(search_option,keyword,start, end);
	}

	@Override
	public BookDTO detailBook(String book_code) {
		return dao.detailBook(book_code);
	}

	@Override
	public void updateBook(BookDTO dto) {
		dao.updateBook(dto);
	}

	@Override
	public void deleteBook(String book_code) {
		dao.deleteBook(book_code);
	}

	@Override
	public void insertBook(BookDTO dto) {
		dao.insertBook(dto);
	}


	@Override
	public String fileInfo(String book_code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		return dao.countArticle(search_option,keyword);
	}

	@Override
	public List<AdminBookDTO> listAll() throws Exception {
		return dao.listAll();
	}


	

}
