package com.example.project.service.ysr.culture;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.ysr.culture.dao.ApplicantDAO;
import com.example.project.model.ysr.culture.dto.ApplicantDTO;

@Service
public class ApplicantServiceImpl implements ApplicantService {
	
	@Inject
	ApplicantDAO Dao;
	
	@Override
	public void insert(ApplicantDTO dto) throws Exception {
		Dao.insert(dto);
	}

	@Override
	public List<ApplicantDTO> list() throws Exception {
		return Dao.list();
	}
	
	@Override
	public void delete(int ano) throws Exception {
		Dao.delete(ano);
	}

	@Override
	public String check(String userid,int cno) throws Exception {
		return Dao.check(userid,cno);
	}

}
