package com.example.project.service.kdk.book;

import java.util.List;

import com.example.project.model.kdk.book.dto.CartDTO;

public interface CartService {
	
	public void delete(int cart_id);
	public void deleteAll(String userid);
	public void update(int cart_id);
	public void insert(CartDTO dto);
	public List<CartDTO> listCart(
			String search_option, String keyword,int start, int end, String userid) 
																				throws Exception;
	public int countArticle(
			String search_option, String keyword, String userid) throws Exception;
	
	public int countCart(String book_code, String userid);
	
}
