package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dao.AdminApplicantDAO;
import com.example.project.model.ym.dto.AdminApplicantDTO;
import com.example.project.model.ym.dto.AdminBookDTO;

@Service
public class AdminApplicantServiceImpl implements AdminApplicantService {

	@Inject
	AdminApplicantDAO dao;

	@Override
	public List<AdminApplicantDTO> listApplicant(String search_option, String keyword, int start, int end, int cno)
			throws Exception {
		// TODO Auto-generated method stub
		return dao.listApplicant(search_option,keyword,start, end, cno);
	}

	@Override
	public int countArticle(String search_option, String keyword, int cno) throws Exception {
		// TODO Auto-generated method stub
		return dao.countArticle(search_option,keyword,cno);
	}
	


	

}
