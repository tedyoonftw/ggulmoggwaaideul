package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dao.BookGroupDAO;
import com.example.project.model.ym.dto.BookGroupDTO;
import com.example.project.model.ym.dto.MemberDTO;

@Service
public class BookGroupServiceImpl implements BookGroupService{

	@Inject
	BookGroupDAO dao;
	
	@Override
	public List<BookGroupDTO> list() {
		return dao.list();
	}

	@Override
	public List<MemberDTO> list2() {
		return dao.list2();
	}

	@Override
	public List<BookGroupDTO> list3() {
		return dao.list3();
	}

}
