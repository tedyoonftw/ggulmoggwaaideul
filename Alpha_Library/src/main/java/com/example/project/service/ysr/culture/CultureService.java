package com.example.project.service.ysr.culture;

import java.util.List;

import com.example.project.model.ysr.culture.dto.CultureDTO;

public interface CultureService {
	
	public List<CultureDTO> list( String search_option,String keyword
			,int start, int end) throws Exception;
		public CultureDTO view(int cno,String username) throws Exception;
		public void insert(CultureDTO dto) throws Exception;
		public void update(CultureDTO dto) throws Exception;
		public void delete(int cno) throws Exception;
		
		public int countArticle(String search_option,String keyword);
		public int insertUrl(CultureDTO dto) throws Exception;
}
