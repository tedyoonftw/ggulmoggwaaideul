package com.example.project.service.pu.mypage;

import javax.servlet.http.HttpSession;

import com.example.project.model.pu.login.dto.UserDTO;

public interface MypageService {

	UserDTO userinfo(String userid);
	UserDTO passwd(String userid);
	UserDTO bye(String userid);
	public String id_find(UserDTO dto2);
	public String pw_find(UserDTO dto2);
	void cancel(String userid);
	void pw_modify(UserDTO dto);
	void info_modify(UserDTO dto);
	public void memo1(String userid);
	public void applicant(String userid);
	public void qna(String userid);
	public void readingroom(String userid);
	public void urm(String userid);
	public void hopebook(String userid);
	public void cart(String userid);
}
