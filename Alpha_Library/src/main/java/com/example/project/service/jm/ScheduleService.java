package com.example.project.service.jm;

import java.util.Date;
import java.util.List;

import com.example.project.model.jm.dto.ScheduleDTO;

public interface ScheduleService {
	List<ScheduleDTO> month_query(Date firstDayOfMonth,Date firstDayOfNextMonth);
	ScheduleDTO ShowSchedule(int schedule_id);
	void delete(int schedule_id );
	void update(ScheduleDTO dto);
	void insert(ScheduleDTO dto);
}
