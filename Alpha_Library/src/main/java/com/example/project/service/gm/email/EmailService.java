package com.example.project.service.gm.email;

import com.example.project.model.gm.dto.EmailDTO;

public interface EmailService {
	public void sendMail(EmailDTO dto);
	public String check(String email1,String email2);
	public void insertMail(String email);
	public void updateMail(String email);
	public int confirm(String email);
}
