package com.example.project.service.gm.email;

import javax.inject.Inject;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.gm.dao.EmailDAO;
import com.example.project.model.gm.dto.EmailDTO;

@Service //서비스 빈으로 등록
public class EmailServiceImpl implements EmailService {

	@Inject//메일 발송 객체
	JavaMailSender mailSender; //root-context.xml에 설정한 bean
	@Inject
	EmailDAO emailDao;
	
	@Override
	public void sendMail(EmailDTO dto) {
		try {
			MimeMessage msg=mailSender.createMimeMessage();
			//이메일 수신자
			msg.addRecipient(RecipientType.TO, 
					new InternetAddress(dto.getReceiveMail()));
			//이메일 발신자
			msg.addFrom(new InternetAddress[] {
new InternetAddress(dto.getSenderMail(),dto.getSenderName())
			});
			//이메일 제목
			msg.setSubject(dto.getSubject(),"utf-8");
			//이메일 본문
			msg.setText(dto.getMessage(), "utf-8", "html");
			System.out.println("이메일 보내기  msg : "+msg);
			mailSender.send(msg);//전송
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//등록된 메일 체크
	@Override
	public String check(String email1, String email2) {
		return emailDao.check(email1, email2);
	}
	//메일인증등록
	@Override
	public void insertMail(String email) {
		int count=emailDao.mailCheck2(email);
		System.out.println("카운트  : "+count);
		if (count == 0) {
			emailDao.insertMail(email);
		} else {
			emailDao.updateMail2(email);
		}
	}
	//메일인증성공
	@Override
	public void updateMail(String email) {
		emailDao.updateMail(email);
	}
	//메일인증여부확인
	@Override
	public int confirm(String email) {
		return emailDao.mailCheck(email);
	}
}






