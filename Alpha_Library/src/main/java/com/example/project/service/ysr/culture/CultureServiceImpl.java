package com.example.project.service.ysr.culture;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.ysr.culture.dao.CultureDAO;
import com.example.project.model.ysr.culture.dto.CultureDTO;

@Service
public class CultureServiceImpl implements CultureService {
	
	@Inject
	CultureDAO cultureDao;
	
	@Override
	public List<CultureDTO> list(String search_option, String keyword, int start, int end) throws Exception {
		return cultureDao.list(search_option, keyword, start, end);
	}

	@Override
	public CultureDTO view(int cno,String username) throws Exception {
		return cultureDao.view(cno,username);
	}
	
	@Override
	public void insert(CultureDTO dto) throws Exception {
		//System.out.println("야야아아아아은광사랑나라사랑"+dto.getUrl());
		cultureDao.insert(dto);
		/*String[] files=dto.getFiles(); //첨부파일 이름 배열
		if(files==null) {
			return;
			} //첨부파일이 없으면 skip
		for(String name : files) {
			cultureDao.addFile(name);
		}*/
	}

	@Override
	public void update(CultureDTO dto) throws Exception {
		cultureDao.update(dto);
	}

	@Override
	public void delete(int cno) throws Exception {
		cultureDao.delete(cno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		return cultureDao.countArticle(search_option, keyword);
	}

	@Override
	public int insertUrl(CultureDTO dto) throws Exception {
		return cultureDao.insertUrl(dto);
	}

}
