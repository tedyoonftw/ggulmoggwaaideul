package com.example.project.service.kdk.book;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.kdk.book.dao.CartDAO;
import com.example.project.model.kdk.book.dto.CartDTO;

@Service
public class CartServiceImpl implements CartService {
	
	@Inject
	CartDAO cartDao;
	
	@Override
	public List<CartDTO> listCart(
			String search_option, String keyword,int start, int end, String userid)
																throws Exception {
		return cartDao.listCart(search_option,keyword,start,end, userid); 
	}

	@Override
	public void delete(int cart_id) {
		cartDao.delete(cart_id);
	}

	@Override
	public void deleteAll(String userid) {
		cartDao.deleteAll(userid);
	}

	@Override
	public void update(int cart_id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insert(CartDTO dto) {
		cartDao.insert(dto);
	}

	@Override
	public int countArticle(String search_option, String keyword ,String userid) throws Exception {
		return cartDao.countArticle(search_option, keyword,userid);
	}

	@Override
	public int countCart(String cart_id, String userid) {
		return cartDao.countCart(cart_id, userid);
	}

}
