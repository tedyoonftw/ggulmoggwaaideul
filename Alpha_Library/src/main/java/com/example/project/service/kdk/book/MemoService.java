package com.example.project.service.kdk.book;

import java.util.List;

import com.example.project.model.kdk.book.dto.MemoDTO;

public interface MemoService {
	public List<MemoDTO> listMemo(String book_code);
	public void updateMemo(MemoDTO dto);
	public void deleteMemo(int idx);
	public void insertMemo(String userid, String memo, String book_code);
}
