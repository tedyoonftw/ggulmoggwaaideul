package com.example.project.service.jm;

import java.util.List;

import com.example.project.model.jm.dto.BestTitleDTO;
import com.example.project.model.jm.dto.KeywordDTO;
import com.example.project.model.jm.dto.TitleKeywordDTO;

public interface SearchKeyword {
	public List<KeywordDTO> searchList (String userid);

	public void insertKeyword(String userid, String keyword);

	public void insertTitleKeyword(String book_code, String userid);

	public List<TitleKeywordDTO> getTitleKeyword(String userid);
	
	public List<KeywordDTO> getIngiKeyword();
	public String findkeyword(String keyword);	
	public void newkeyword (String keyword);
	public void viewnoplus (String keyword); 
	
	public List<BestTitleDTO> getBestTile4();
	public List<BestTitleDTO> getBestTile8();
	public String findBestTitle (String book_code);
	public void newBestTitle(String book_code);
	public void rentnoPlus(String book_code);
}
