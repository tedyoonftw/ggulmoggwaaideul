package com.example.project.service.ysr.notice;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.project.model.ysr.notice.dto.NoticeDTO;

public interface NoticeService {
	
	public List<NoticeDTO> list(String search_option,
		String keyword,int start, int end) throws Exception;
	public void create(NoticeDTO dto) throws Exception;
	public void update(NoticeDTO dto) throws Exception;
	public void delete(String fullName,int nno) throws Exception;
		
	public NoticeDTO read(int nno) throws Exception;
	public int countArticle(String search_option,String keyword);
	public void increaseViewcnt(int nno, HttpSession session) throws Exception;
		
	public void addFile(String fullName);
	public List<String> getFile(int nno);
	public void updateFile(String fullName, int nno);
	public void deleteFile(String fullName);
}
