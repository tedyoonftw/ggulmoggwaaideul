package com.example.project.service.pu.admin;

import javax.servlet.http.HttpSession;

import com.example.project.model.pu.admin.dto.AdminDTO;

public interface AdminService {
	
	public AdminDTO viewAdmin(String adminid);
	public boolean loginCheck(AdminDTO dto, HttpSession session);
	public void logout(HttpSession session);
	void signup(AdminDTO dto);
	void signout(String adminid);
	void modify(AdminDTO dto);

}
