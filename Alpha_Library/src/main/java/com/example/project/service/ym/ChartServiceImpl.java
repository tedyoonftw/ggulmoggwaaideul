package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.example.project.model.ym.dto.BookGroupDTO;
import com.example.project.model.ym.dto.MemberDTO;
import com.example.project.model.ym.dto.UrmDTO;

@Service
public class ChartServiceImpl implements ChartService {
	
	@Inject
	BookGroupService bookgroupService;
	@Inject
	UrmService UrmService;
	
	@Override
	public JSONObject getChartData2() {
		
	List<BookGroupDTO> items=bookgroupService.list();
		
		JSONObject data = new JSONObject(); // 리턴할 최종 JSON 객체
		JSONObject col1 = new JSONObject(); 
		JSONObject col2 = new JSONObject();
		
		JSONArray title=new JSONArray();
		
		col1.put("labal", "대분류");
		col1.put("type", "string");
		col2.put("labal", "장서 수");
		col2.put("type", "number");
		
		// JSON 배열에 JSON 객체를 추가
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		
		JSONArray body = new JSONArray();
		for(BookGroupDTO dto : items) {
			JSONObject groupName=new JSONObject();
			groupName.put("v", dto.getGroup_name());
			JSONObject count=new JSONObject();
			count.put("v", dto.getCount());
			JSONArray row = new JSONArray();
			row.add(groupName);
			row.add(count);
			JSONObject cell=new JSONObject();
			cell.put("c",row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@Override
	public JSONObject getChartData3() {
List<MemberDTO> items=bookgroupService.list2();
		
		JSONObject data = new JSONObject(); // 리턴할 최종 JSON 객체
		JSONObject col1 = new JSONObject(); 
		JSONObject col2 = new JSONObject();
		
		JSONArray title=new JSONArray();
		
		col1.put("labal", "회원");
		col1.put("type", "string");
		col2.put("labal", "도서 수");
		col2.put("type", "number");
		
		// JSON 배열에 JSON 객체를 추가
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		
		JSONArray body = new JSONArray();
		for(MemberDTO dto : items) {
			JSONObject username=new JSONObject();
			username.put("v", dto.getUsername());
			JSONObject count=new JSONObject();
			count.put("v", dto.getCount());
			JSONArray row = new JSONArray();
			row.add(username);
			row.add(count);
			JSONObject cell=new JSONObject();
			cell.put("c",row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}
	
	@Override
	public JSONObject getChartData4() {
List<BookGroupDTO> items=bookgroupService.list3();
		
		JSONObject data = new JSONObject(); // 리턴할 최종 JSON 객체
		JSONObject col1 = new JSONObject(); 
		JSONObject col2 = new JSONObject();
		
		JSONArray title=new JSONArray();
		
		col1.put("labal", "회원");
		col1.put("type", "string");
		col2.put("labal", "도서 수");
		col2.put("type", "number");
		
		// JSON 배열에 JSON 객체를 추가
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		
		JSONArray body = new JSONArray();
		for(BookGroupDTO dto : items) {
			JSONObject groupName=new JSONObject();
			groupName.put("v", dto.getGroup_name());
			JSONObject count=new JSONObject();
			count.put("v", dto.getCount());
			JSONArray row = new JSONArray();
			row.add(groupName);
			row.add(count);
			JSONObject cell=new JSONObject();
			cell.put("c",row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}
	
	@Override
	public JSONObject getChartData5() {
List<UrmDTO> items=UrmService.list();
		
		JSONObject data = new JSONObject(); // 리턴할 최종 JSON 객체
		JSONObject col1 = new JSONObject(); 
		JSONObject col2 = new JSONObject();
		
		JSONArray title=new JSONArray();
		
		col1.put("labal", "회원");
		col1.put("type", "string");
		col2.put("labal", "이용횟수");
		col2.put("type", "number");
		
		// JSON 배열에 JSON 객체를 추가
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		
		JSONArray body = new JSONArray();
		for(UrmDTO dto : items) {
			JSONObject userid=new JSONObject();
			userid.put("v", dto.getUserid());
			JSONObject count=new JSONObject();
			count.put("v", dto.getCount());
			JSONArray row = new JSONArray();
			row.add(userid);
			row.add(count);
			JSONObject cell=new JSONObject();
			cell.put("c",row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}
}
