package com.example.project.service.ysr.qna;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.ysr.qna.dao.QnADAO;
import com.example.project.model.ysr.qna.dao.ReplyDAO;
import com.example.project.model.ysr.qna.dto.ReplyDTO;

@Service
public class ReplyServiceImpl implements ReplyService {
	
	@Inject
	ReplyDAO replyDao;
	@Inject
	QnADAO dao;
	
	@Override
	public List<ReplyDTO> list(int qno) {
		return replyDao.list(qno);
	}

	@Override
	public int count(int qno) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Transactional
	@Override
	public void create(ReplyDTO dto,int qno) throws Exception {
		replyDao.create(dto);
		dao.updateStatus(qno);
	}

}
