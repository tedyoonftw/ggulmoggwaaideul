package com.example.project.service.ysr.culture;

import java.util.List;

import com.example.project.model.ysr.culture.dto.ApplicantDTO;

public interface ApplicantService {
	
	public void insert(ApplicantDTO dto) throws Exception;
	public List<ApplicantDTO> list() throws Exception;
	public void delete(int ano) throws Exception;
	public String check(String userid,int cno) throws Exception;
}
