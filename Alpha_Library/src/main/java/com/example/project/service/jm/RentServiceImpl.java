package com.example.project.service.jm;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.jm.dao.RentDAO;
import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.kdk.book.dto.BookDTO;

@Service
public class RentServiceImpl implements RentService {

	@Inject
	RentDAO rentdao;
	
	@Override
	public String delayinfo(String userid) {
		return null;
	}
	@Override
	public BookDTO bookinfo(String book_code) {
		return rentdao.bookinfo(book_code);
	}

	@Override
	public void rent(RentDTO dto) {
		rentdao.rent(dto);
	}

	@Override
	public void updatebyrent(String book_code) {
		rentdao.updatebyrent(book_code);
	}
	
	@Override
	public RentDTO getrentinfo(int rent_code) {
		return rentdao.getrentinfo(rent_code);
	}

	@Override
	public void updateByReturnbook(String book_code) {
		rentdao.updateByReturnbook(book_code);
	}

	@Override
	public String rentinfouserid(String userid, String book_code) {
		return rentdao.rentinfouserid(userid,book_code);
	}
	@Override
	public void smoke(int rentno, Date return_date) {
		rentdao.smoke(rentno,return_date);
	}
	
	@Override
	public void halfnab(int rent_code, Date return_date) {
		rentdao.halfnab(rent_code,return_date);
	}

	@Override
	public int nowRentCount(String userid) {
		return rentdao.newRentCount(userid);
	}

	@Override
	public List<RentDTO> RentinfoListByUserid(String userid) {
		return rentdao.RentinfoListByUserid(userid);
	}

	@Override // Rent 테이블에서 현재 반납일이 지난애들을 바꿔줌
	public void updaterentfordelay() {
		rentdao.updaterentfordelay();
	}

	@Override
	public int delaycount(String userid) {
		return rentdao.delaycount(userid);
	}
	
	@Override //기존에 패널티가 없을때 패널티 여부와, 날짜를 업데이트 시킴
	public void updateDelayDate(int rent_code) {
	rentdao.updateDelayDate(rent_code);
	}
	
	@Override
	public void updateDelayGigan(int pre_rent_code, int rent_code) { //기존 코드와 현재코드를 둘다 필요로 하는 메소드
		rentdao.updateDelayGigan(pre_rent_code, rent_code);
	}
	@Override
	public List<RentDTO> FindInfoListByUserid(String userid) {
		return rentdao.FindInfoListByUserid(userid);
	}
	@Override
	public void updatePrePanelty(int rent_code) {
		rentdao.updatePrePanelty(rent_code);
	}
	@Override
	public List<RentDTO> checkdely(String userid) {
		return rentdao.checkDelay(userid);
	}
	
	@Override
	public void updateDelayHalfnab(int rent_code, Date return_date) {
		rentdao.delayhalfnab(rent_code,return_date);
		
	}
	@Override
	public void updatePaneltyDay() {
		rentdao.updatePaneltyday();
	}
	@Override
	public String getdelaygigan(String userid) {
		return rentdao.getdelaygigan(userid);
	}
	
	
	
	
	
}
