package com.example.project.service.ysr.qna;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project.model.ysr.qna.dao.QnADAO;
import com.example.project.model.ysr.qna.dto.QnADTO;

@Service
public class QnAServiceImpl implements QnAService {
	
	@Inject
	QnADAO Dao;
	
	@Override
	public List<QnADTO> list(String search_option, String keyword, int start, int end) throws Exception {
		return Dao.list(search_option, keyword, start, end);
	}
	
	@Transactional
	@Override
	public void create(QnADTO dto) throws Exception {
		Dao.create(dto);
		String[] files=dto.getFiles();
		//System.out.println("파일:"+files);
		if(files==null) return;
		for(String name : files) {
			Dao.addFile(name);
		}
	}
	
	@Transactional
	@Override
	public void create2(QnADTO dto) throws Exception {
		Dao.create2(dto);
		String[] files=dto.getFiles();
		//System.out.println("파일:"+files);
		if(files==null) return;
		for(String name : files) {
			Dao.addFile(name);
		}
	}
	
	@Transactional
	@Override
	public void update(QnADTO dto) throws Exception {
		Dao.update(dto);
		String[] files=dto.getFiles();
		if(files==null) return;
		for(String name : files) {
			Dao.updateFile(name, dto.getQno());
		}
	}
	
	@Transactional
	@Override
	public void delete(int qno) throws Exception {
		Dao.deleteFile(qno);
		Dao.delete(qno);
	}

	@Override
	public QnADTO read(int qno) throws Exception {
		return Dao.read(qno);
	}

	@Override
	public int countArticle(String search_option, String keyword) {
		return Dao.countArticle(search_option, keyword);
	}

	@Override
	public void increaseViewcnt(int qno) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getFile(int qno) {
		return Dao.getFile(qno);
	}

}
