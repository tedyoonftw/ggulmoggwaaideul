package com.example.project.service.pu.admin;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.example.project.model.pu.admin.dao.AdminDAO;
import com.example.project.model.pu.admin.dto.AdminDTO;

@Service
public class AdminServiceImpl implements AdminService {

	@Inject
	AdminDAO adminDao;
	
	@Override
	public AdminDTO viewAdmin(String adminid) {
		return adminDao.viewAdmin(adminid);
	}

	@Override
	public boolean loginCheck(AdminDTO dto, HttpSession session) {
		boolean result=adminDao.loginCheck(dto, session);
		if(result) {
			AdminDTO dto2=viewAdmin(dto.getAdminid());
			session.setAttribute("adminid", dto.getAdminid());
			session.setAttribute("name", dto2.getAdminname());
		}
		return result;
	}

	@Override
	public void logout(HttpSession session) {
		session.invalidate();
	}

	@Override
	public void signup(AdminDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void signout(String adminid) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modify(AdminDTO dto) {
		// TODO Auto-generated method stub

	}

}
