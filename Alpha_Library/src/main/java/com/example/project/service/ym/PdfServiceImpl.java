package com.example.project.service.ym;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dto.AdminBookDTO;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class PdfServiceImpl implements PdfService{

	@Inject
	AdminBookService adminbookService;
	
	@Override
	public String createPdf() {
		String result="";
		try {// com.itextpdf.text.Document
		Document document = new Document(); // pdf 문서 객체 생성
		// pdf writer 객체
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("d:/대출도서목록.pdf"));

		document.open(); // pdf 문서 열기
		BaseFont baseFont =
		BaseFont.createFont("c:/windows/fonts/malgun.ttf", BaseFont.IDENTITY_H,	BaseFont.EMBEDDED);
		Font font = new Font(baseFont, 9); // 폰트 설정
		
		PdfPTable table = new PdfPTable(7); // 7 컬럼의 테이블
		Chunk chunk = new Chunk("대출도서", font);// 출력할 내용
		Paragraph ph = new Paragraph(chunk);// 문단
		ph.setAlignment(Element.ALIGN_CENTER);// 가운데 정렬
		document.add(ph);
		document.add(Chunk.NEWLINE); // 줄바꿈 처리
		document.add(Chunk.NEWLINE);
		// document.newPage(); //페이지 나누기
		// 테이블의 타이틀 행 생성
		PdfPCell cell1 = new PdfPCell(new Phrase("도서명", font));
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell1);
		PdfPCell cell2 = new PdfPCell(new Phrase("대분류", font));
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell2);
		PdfPCell cell3 = new PdfPCell(new Phrase("저자", font));
		cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell3);
		PdfPCell cell4 = new PdfPCell(new Phrase("출판사", font));
		cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell4);
		PdfPCell cell5 = new PdfPCell(new Phrase("대출일자", font));
		cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell5);
		PdfPCell cell6 = new PdfPCell(new Phrase("대출/반납여부", font));
		cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell6);
		PdfPCell cell7 = new PdfPCell(new Phrase("대출자", font));
		cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell7);
		List<AdminBookDTO> items=adminbookService.listAll();
		for (int i = 0; i < items.size(); i++) {
		AdminBookDTO dto = items.get(i);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		PdfPCell cellTitle = new PdfPCell(new Phrase(dto.getTitle(), font));
		table.addCell(cellTitle); // 도서명
		PdfPCell cellGroupname = new PdfPCell(new Phrase(""+dto.getGroup_name(),font));
		table.addCell(cellGroupname); // 대분류
		PdfPCell cellAuthor = new PdfPCell(new Phrase("" +dto.getAuthor(), font));
		table.addCell(cellAuthor); // 저자
		PdfPCell cellPublisher = new PdfPCell(new Phrase("" +dto.getPublisher(), font));
		table.addCell(cellPublisher); //출판사
		PdfPCell cellRentdate = new PdfPCell(new Phrase("" +sdf.format(dto.getRent_date()), font));
		table.addCell(cellRentdate); //대출일자
	//	System.out.println("뭐가문제야"+dto.getHalfnab());
		if (dto.getHalfnab().equals("Y")) {
			PdfPCell cellHalfnab = new PdfPCell(new Phrase(""+"반납완료",font));
			table.addCell(cellHalfnab);
		}else {
			PdfPCell cellHalfnab = new PdfPCell(new Phrase(""+"대출 중",font));
			table.addCell(cellHalfnab);
		}
		 // 대출가능여부
		PdfPCell cellUsername = new PdfPCell(new Phrase("" +dto.getUsername(), font));
		table.addCell(cellUsername); // 대출자
		// 날짜 자료를 처리할 경우 참조 :
		// Date date = dto.getRegdate(); // java.util.Date
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// String strDate = sdf.format(date);
		}
		document.add(table);
		
		
		
		document.close(); // pdf 문서 닫기
		result="D드라이브에 대출도서목록.pdf 파일이 생성되었습니다.";
		} catch (Exception e) {
		e.printStackTrace();
		result="PDF 파일 생성 실패...";
		}
		return result;
	}


}
