package com.example.project.service.jm;

import java.util.List;

import com.example.project.model.jm.dto.ReadingRoomDTO;
import com.example.project.model.jm.dto.urmDTO;
import com.example.project.model.pu.login.dto.UserDTO;

public interface ReadingService {
	public List<ReadingRoomDTO> list();
	public String seatedmemberlist(String userid);
	public int countseat();
	public UserDTO memberinfo (String userid);
	public int reservation(ReadingRoomDTO dto);
	public ReadingRoomDTO viewseat(int seatno);
	public void logout(int seatno);
	public void reserveinfo (urmDTO dto);
	public void updatereservationinfo(urmDTO dto);
	public void prereserve(int seatno, String userid);
	public void afterreserve(int seatno);
	public List<ReadingRoomDTO> logoutcheck();
	public List<String> checkprereserve();
	public void viewOtherPage(String checkOtherPage);
}
