package com.example.project.service.pu.mypage;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.example.project.model.pu.login.dto.UserDTO;
import com.example.project.model.pu.mypage.dao.MypageDAO;

@Service
public class MypageServiceImpl implements MypageService {

	@Inject
	MypageDAO mypageDao;
	
	@Override
	public void pw_modify(UserDTO dto) {
		mypageDao.pw_modify(dto);
	}

	@Override
	public UserDTO userinfo(String userid) {
		return mypageDao.userinfo(userid);
	}

	@Override
	public void info_modify(UserDTO dto) {
		mypageDao.info_modify(dto);
	}

	@Override
	public UserDTO passwd(String userid) {
		return mypageDao.passwd(userid);
	}

	@Override
	public UserDTO bye(String userid) {
		return mypageDao.bye(userid);
	}

	@Override
	public void cancel(String userid) {
		mypageDao.cancel(userid);
	}

	@Override
	public String id_find(UserDTO dto2) {
		return mypageDao.id_find(dto2);
	}

	@Override
	public String pw_find(UserDTO dto2) {
		return mypageDao.pw_find(dto2);
	}

	@Override
	public void memo1(String userid) {
		mypageDao.memo1(userid);
		
	}

	@Override
	public void applicant(String userid) {
		mypageDao.applicant(userid);
	}

	@Override
	public void qna(String userid) {
		mypageDao.qna(userid);
	}

	@Override
	public void readingroom(String userid) {
		mypageDao.readingroom(userid);
	}

	@Override
	public void urm(String userid) {
		mypageDao.urm(userid);
	}

	@Override
	public void hopebook(String userid) {
		mypageDao.hopebook(userid);
	}

	@Override
	public void cart(String userid) {
		mypageDao.cart(userid);
	}

}
