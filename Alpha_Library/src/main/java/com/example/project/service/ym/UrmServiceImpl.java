package com.example.project.service.ym;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dao.UrmDAO;
import com.example.project.model.ym.dto.UrmDTO;

@Service
public class UrmServiceImpl implements UrmService{

	@Inject
	UrmDAO dao;
	
	@Override
	public List<UrmDTO> list() {
		return dao.list();
	}

}
