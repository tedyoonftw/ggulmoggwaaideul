package com.example.project.service.kdk.book;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.kdk.book.dao.BookDAO;
import com.example.project.model.kdk.book.dto.BookDTO;

@Service
public class BookServiceImpl implements BookService {

	@Inject
	BookDAO bookDao;
	
	@Override
	public List<BookDTO> listBook(int start, int end) throws Exception {
		return bookDao.listBook(start, end);
	}

	@Override
	public BookDTO detailBook(String book_code) {
		return bookDao.detailBook(book_code);
	}

	@Override
	public void updateBook(BookDTO dto) {
		bookDao.updateBook(dto);
	}

	@Override
	public void deleteBook(String book_code) {
		bookDao.deleteBook(book_code);
	}

	@Override
	public void insertBook(BookDTO dto) {
		bookDao.insertBook(dto);
	}


	@Override
	public String fileInfo(String book_code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countArticle() throws Exception {
		return bookDao.countArticle();
	}


	

}
