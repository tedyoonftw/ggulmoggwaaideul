package com.example.project.service.kdk.book;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.kdk.book.dao.MemoDAO;
import com.example.project.model.kdk.book.dto.MemoDTO;

@Service
public class MemoServiceImpl implements MemoService {
	
	@Inject
	MemoDAO memoDao;
	
	@Override
	public List<MemoDTO> listMemo(String book_code) {
		return memoDao.listMemo(book_code);
	}

	@Override
	public void insertMemo(String userid, String memo, String book_code) {
		memoDao.insertMemo(userid,memo,book_code);
	}

	@Override
	public void updateMemo(MemoDTO dto) {
		memoDao.updateMemo(dto);
	}

	@Override
	public void deleteMemo(int idx) {
		memoDao.deleteMemo(idx);
	}

}
