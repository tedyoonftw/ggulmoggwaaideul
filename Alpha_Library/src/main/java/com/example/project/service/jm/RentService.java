package com.example.project.service.jm;

import java.util.Date;

import java.util.List;

import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.kdk.book.dto.BookDTO;

public interface RentService {

	public String delayinfo(String userid);
	BookDTO bookinfo(String book_code);
	void rent(RentDTO dto);
	void updatebyrent(String book_code);
	RentDTO getrentinfo(int rent_code);
	void updateByReturnbook(String book_code);
	String rentinfouserid(String userid, String book_code);
	
	
	public void smoke(int rentno, Date return_date);
	public void halfnab(int rent_code, Date return_date);
	public int nowRentCount(String userid);
	public List<RentDTO> RentinfoListByUserid(String userid);
	public void updaterentfordelay();
	public int delaycount(String userid);
	public void updateDelayDate(int rent_code);
	public void updatePrePanelty(int rent_code); //기존책에 대한 패널티를 없앰. 
	List<RentDTO> FindInfoListByUserid(String userid);
	void updateDelayGigan(int pre_rent_code, int rent_code);
	public List<RentDTO> checkdely(String userid);
	public void updateDelayHalfnab(int rent_code, Date return_date);
	public void updatePaneltyDay();
	public String getdelaygigan(String userid);
	
	
}
