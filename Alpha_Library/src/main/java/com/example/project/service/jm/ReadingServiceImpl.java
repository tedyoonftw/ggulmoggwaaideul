package com.example.project.service.jm;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project.model.jm.dao.ReadingRoomDAO;
import com.example.project.model.jm.dto.ReadingRoomDTO;
import com.example.project.model.jm.dto.urmDTO;
import com.example.project.model.pu.login.dto.UserDTO;

@Service
public class ReadingServiceImpl implements ReadingService {
	@Inject
	ReadingRoomDAO readingDao;
	
	 
	
	@Override
	public List<ReadingRoomDTO> logoutcheck() {
		return readingDao.logoutcheck();
	}

	@Override
	public void afterreserve(int seatno) {
		readingDao.afterreserve(seatno);
	}

	@Override
	public void prereserve(int seatno, String userid) {
		readingDao.prereserve(seatno, userid);
	}

	@Override
	public void updatereservationinfo(urmDTO dto) {
		readingDao.updatereservationinfo(dto);
	}

	@Override
	public void reserveinfo(urmDTO dto) {
			
		readingDao.reserveinfo(dto);
	}

	@Override
	public int countseat() {
		return readingDao.countseat();
	}

	@Override
	public List<ReadingRoomDTO> list() {
		return readingDao.list();
	}

	@Override
	public UserDTO memberinfo(String userid) {
		return readingDao.memberinfo(userid);
	}

	@Override
	public int reservation(ReadingRoomDTO dto) {
	
		return readingDao.reservation(dto);
	}

	@Override
	public ReadingRoomDTO viewseat(int seatno) {
		return readingDao.view(seatno);
	}

	@Override
	public void logout(int seatno) {
		readingDao.logout(seatno);
	}

	@Override
	public String seatedmemberlist(String userid) {
		return readingDao.seatedmemberlist(userid);
	}

	@Override
	public List<String> checkprereserve() {
		return readingDao.checkprereserve();
	}

	@Override
	public void viewOtherPage(String checkOtherPage) {
		readingDao.viewOtherPage(checkOtherPage);
	}
	
}
