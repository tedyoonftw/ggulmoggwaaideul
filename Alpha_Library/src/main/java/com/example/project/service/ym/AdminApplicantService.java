package com.example.project.service.ym;

import java.util.List;

import com.example.project.model.ym.dto.AdminApplicantDTO;
import com.example.project.model.ym.dto.AdminBookDTO;


public interface AdminApplicantService {
	
	public List<AdminApplicantDTO> listApplicant(String search_option, String keyword, int start, int end, int cno) throws Exception;
	public int countArticle(String search_option, String keyword, int cno) throws Exception;
	

	
}
