package com.example.project.service.ym;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.example.project.model.ym.dao.ExcelDAO;

@Service(value = "excelService")
public class ExcelServiceImpl implements ExcelService {
 
    @Resource(name = "excelDao")
    private ExcelDAO excelDao;
 
 
    @Override
    public List<Object> getAllObjects(){
         
            return excelDao.getBooks();
        
    }
 
}
