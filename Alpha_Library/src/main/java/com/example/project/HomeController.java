package com.example.project;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.project.model.jm.dto.BestTitleDTO;
import com.example.project.model.jm.dto.RentDTO;
import com.example.project.model.ysr.culture.dto.CultureDTO;
import com.example.project.model.ysr.notice.dto.NoticeDTO;
import com.example.project.service.hjh.mypage.MypageServicej;
import com.example.project.service.jm.RentService;
import com.example.project.service.jm.ScheduleService;
import com.example.project.service.jm.SearchKeyword;
import com.example.project.service.ysr.culture.CultureService;
import com.example.project.service.ysr.notice.NoticeService;
import com.example.project.service.ysr.notice.Pager;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Inject
	ScheduleService scheduleService;
	
	@Inject
	NoticeService noticeService;
	
	@Inject 
	SearchKeyword keywordSerivce; 
	
	@Inject
	MypageServicej mypageService;
	
	@Inject
	CultureService cultureService;
	
	@Inject
	RentService rentService;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home( Model model, HttpSession session,
			@RequestParam(defaultValue="title") String search_option,
			@RequestParam(defaultValue="") String keyword,
			@RequestParam(defaultValue="1") int curPage) throws Exception{
		
		// 미니달력을 위해서 현재 날짜 정보를 가져옴
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd"); 
		Date today = calendar.getTime();
		int curYear = 	calendar.get(Calendar.YEAR);
		int curMonth = 	(calendar.get(Calendar.MONTH) + 1);
		int curDay = calendar.get(Calendar.DATE);
		
		//today 정보
		
		model.addAttribute("today",today);
		model.addAttribute("curYear", curYear);
		model.addAttribute("curMonth", curMonth);
		model.addAttribute("curDay", curDay);
		//해당월의 1일로 캘린더 설정.
		calendar.set(Calendar.DATE, 1); 
		
		Long firstDayOFMonth = calendar.getTimeInMillis();
		
		model.addAttribute("firstDayOfMonth", firstDayOFMonth);
		session.setAttribute("firstDayOfWeek", calendar.get(Calendar.DAY_OF_WEEK));
		session.setAttribute("lastDayOfMonth", 	calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		//해당월의 마지막일로 캘린더 설정.
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		session.setAttribute("lastDayOfLastWeek", calendar.get(Calendar.DAY_OF_WEEK));
		//다음달의 1일로 설정.
		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
		calendar.set(Calendar.DATE, 1);
		Long firstDayOFNextMonth = calendar.getTimeInMillis();
		String strDT2 = dayTime.format(firstDayOFNextMonth);
		Date firstDayOfNextMonth = java.sql.Date.valueOf(strDT2.substring(0, 10));
		
		model.addAttribute("firstDayOfNextMonth", firstDayOfNextMonth);
	    String strDT = dayTime.format(firstDayOFMonth); 
	    Date firstDayOfMonth = java.sql.Date.valueOf(strDT.substring(0, 10));
		model.addAttribute("month_query",scheduleService.month_query(firstDayOfMonth,firstDayOfNextMonth));
		
		
		/*행사안내*/
		int count=5;
		  
		 List<CultureDTO> list=cultureService.list(search_option, keyword, 1, 5);
		 /*게시판*/
		int count2=noticeService.countArticle(search_option, keyword);
		Pager pager2=new Pager(count2,curPage);
		int start2=pager2.getPageBegin();
		int end2=pager2.getPageEnd();
		
		List<NoticeDTO> list2=noticeService.list(search_option, keyword, start2, end2);
		
		
		// 우리도서관 대출 최고 1위 부터 8위 까지
		List<BestTitleDTO> besttop4 = keywordSerivce.getBestTile4();
		List<BestTitleDTO> besttop8 = keywordSerivce.getBestTile8();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list2", list2);
		map.put("list", list);
		map.put("count",count);
		map.put("pager",pager2);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		model.addAttribute("besttop4",besttop4);
		model.addAttribute("besttop8",besttop8);
		model.addAttribute("map",map);
		String userid = (String)session.getAttribute("userid");
		
		// 메인페이지가 뜨는 순간순간 마다, 반납일이 지난 것을 검사 패널티를 검사
		rentService.updaterentfordelay();
		rentService.updatePaneltyDay(); 
		
		if(userid != null) {
				
			List<RentDTO> checkdelay =rentService.RentinfoListByUserid(userid);
			int rentcount = rentService.nowRentCount(userid);
				model.addAttribute("rentcount", rentcount);
			
				for(RentDTO rentdto : checkdelay) {
				
				String kiteche = rentdto.getKiteche();
				
				if(kiteche.equals("Y")) { //연체가 확인됨
					
					model.addAttribute("delay","yes");
					
				}
				
			}
					
		}
		
		return "index";
	}
	
}
