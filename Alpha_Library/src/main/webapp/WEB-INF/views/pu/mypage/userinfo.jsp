<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
function sample6_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('sample6_address').value = fullAddr;

            // 커서를 상세주소 필드로 이동한다.
            document.getElementById('sample6_address2').value = "";
            document.getElementById('sample6_address2').focus();
        }
    }).open();
}

	function onlyNumber(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105)
				|| keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			return false;
	}
	function removeChar(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
	}

	$(function() {
		var gender1 = $("#gender1").val();
		if(gender1=="1"){
			document.getElementById("gender").value = "남자";
		}else{
			document.getElementById("gender").value = "여자";
		}
	});
	function modify(){
		var username = $("#username").val();
		var birthyear = $("#birthyear").val();
		var birthmonth = $("#birthmonth").val();
		var birthday = $("#birthday").val();
		var phone1 = $("#phone1").val();
		var phone2 = $("#phone2").val();
		var phone3 = $("#phone3").val();
		var zipcode = $("#zipcode").val();
		var add1 = $("#add1").val();
		var add2 = $("#add2").val();
		var gender = $("#gender").val();
		
		if(gender == "남자"){
			document.getElementById("gender").value = "1";
		}else if(gender == "여자"){
			document.getElementById("gender").value = "2";
		}else{
			alert("성별을 다시 입력해주세요.");
			return;
		}
		if (username == "") {
			alert("이름을 입력하세요");
			$("#username").focus();
			return;
		}
		if (birthyear == "") {
			alert("태어난 연도를 선택하세요");
			$("#birthyear").focus();
			return;
		}
		if (birthmonth == "") {
			alert("태어난 월을 선택하세요");
			$("#birthmonth").focus();
			return;
		}
		if (birthday == "") {
			alert("태어난 일을 선택하세요");
			$("#birthday").focus();
			return;
		}
		if (gender == "") {
			alert("성별을 입력하세요");
			$("#gender").focus();
			return;
		}
		if (phone1 == "") {
			alert("전화번호를 입력하세요");
			$("#phone1").focus();
			return;
		}
		if (phone2 == "") {
			alert("전화번호를 입력하세요");
			$("#phone2").focus();
			return;
		}
		if (phone3 == "") {
			alert("전화번호를 입력하세요");
			$("#phone3").focus();
			return;
		}
		if (zipcode == "") {
			alert("주소를 입력하세요");
			$("#zipcode").focus();
			return;
		}
		if (add1 == "") {
			alert("주소를 입력하세요");
			$("#add1").focus();
			return;
		}
		if (add2 == "") {
			alert("주소를 입력하세요");
			$("#add2").focus();
			return;
		}
		if(confirm("회원정보를 수정하시겠습니까?")){
			document.form1.action = "${path}/pu/page/update.do";
			document.form1.submit();
			alert("회원정보가 수정되었습니다.");
		}return;
	}
</script>
</head>



<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/mypage.jsp"%>


        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

	
	<div class="container">
<div class="row">
<div class="col-md-8">

<div class="container" style="">
	<div class="row">
	
	<h2>회원정보수정</h2>
	
	<hr>
	
	<div class="well"
		style="width: 750px;">
		
	 <form name="form1" id="form1" class="form-inline" role="form">
	 
	   <%-- <input type="hidden" name="userid" value="${dto.userid}">
	<div class="form-group" style="margin-left: 30px;">
	  <img <c:if test="${dto.gender==1}">src="${path}/images/index/nam.jpg"</c:if>
	  	   <c:if test="${dto.gender==2}">src="${path}/images/index/mi.jpg"</c:if>
	  class="form-control" style="width: 150px;  height:120px; margin-left: 20px;">
   </div> 
   
	
	<br>
	<br>
   --%>
	 
	 
	 	 <div class="form-group" style="margin-left: 30px;">
   		   <label for="name">아&nbsp;이&nbsp;디&nbsp;</label>
			  <input type="text" id="userid" name="userid" value="${dto.userid}" readonly value="${dto.userid}" class="form-control" style="width: 100px; margin-left: 20px;">
  		 </div>
		
			<%-- <div class="form-group" style="margin-left: 50px;">
    		  <label>비밀번호</label>
    		  <input type="password" name="passwd" value="${dto.passwd}" class="form-control" style="width: 100px; margin-left: 20px;">
  		  </div> --%>
		
		<br>
    	<br>
    	
		  <div class="form-group" style="margin-left: 30px;">
     		 <label>&nbsp;성  &nbsp;&nbsp;&nbsp;명&nbsp;&nbsp;</label>
     		 <input type="text" id="username" name="username" value="${dto.username}" class="form-control" style="width: 100px; margin-left: 20px;">
   		 </div>
		
		
	<%-- <input type="text" id="birthyear" name="birthyear" value="${dto.username}">
<input type="text" id="birthmonth" name="birthmonth" value="${dto.username}">
<input type="text" id="birthday" name="birthday" value="${dto.username}"> --%>
		 <div class="form-group" style="margin-left: 50px;">
     		 <label>생년월일</label>
     		 
      		<select name ="birthyear" id="birthyear"  class="form-control" style="margin-left: 20px;">
      			<option value="">년도</option>
			<c:forEach var="row" begin="1920" end="2018">
				<option value="${row}" 
				<c:out value="${dto.birthyear == row ? 'selected' :'' }" />>${row}년</option>
			</c:forEach>
			</select>
   		 </div>
		
		
		<div class="form-group" style="margin-left: 10px;">
    		  <label> / </label>
    		  
   		   <select  id="birthmonth" name ="birthmonth" class="form-control" style="margin-left: 10px;">
     		   <option value="" >월</option>
			<c:forEach var="row" begin="1" end="12">
				<option value="${row}"
				<c:out value="${dto.birthmonth == row ? 'selected' :'' }" />>${row}월</option>
			</c:forEach>
			</select>
   		 </div>
		
		 <div class="form-group" style="margin-left: 10px;">
      		<label> / </label>
      		
     		 <select id="birthday" name ="birthday" class="form-control" style="margin-left: 10px;">
     		  <option value="">일</option>
			<c:forEach var="row" begin="1" end="31">
				<option value="${row}"
				<c:out value="${dto.birthday == row ? 'selected' :'' }" />>${row}일</option>
			</c:forEach>
			</select>
   		 </div>
    
    	<br>
    	<br>
    
    	<div class="form-group" style="margin-left: 30px;">
    		  <label>&nbsp;성 &nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;  </label>
				<input type="hidden" id="gender1" name="gender1" value="${dto.gender}" >
				<input type="text" id="gender" name="gender"  class="form-control" style="width: 100px; margin-left: 20px;">
   		 </div>	
    
    
	<%-- 	<label>성별</label>
		<input type="hidden" id="gender1" name="gender1" value="${dto.gender}">
		<input type="text" id="gender" name="gender">
		<br> --%>
		
		<div class="form-group" style="margin-left: 50px;">
     		 <label>연&nbsp;락&nbsp;처&nbsp;</label>
     		 <select name="phone1" id="phone1" class="form-control" style="margin-left: 20px;">
     		   	<option value="">전화번호</option>
					<option value="010"
					<c:out value="${dto.phone1 == '010' ? 'selected' :'' }" />>010</option>
					<option value="011"
					<c:out value="${dto.phone1 == '011' ? 'selected' :'' }" />>011</option>
					<option value="016"
					<c:out value="${dto.phone1 == '016' ? 'selected' :'' }" />>016</option>
					<option value="019"
					<c:out value="${dto.phone1 == '019' ? 'selected' :'' }" />>019</option>
   		   </select>
   		 </div>
		
		<div class="form-group" style="margin-left: 10px;">
  		    <label> - </label>
  		    
  		    <input type="text" id="phone2" name="phone2" value="${dto.phone2}" class="form-control" 
  		    style="width: 70px; margin-left: 10px; 'ime-mode: disabled;'"
  		    onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)'
			onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			maxlength="4">
   		 </div>
		
			<div class="form-group" style="margin-left: 10px;">
     			 <label>-</label>
     			 
     			 <input type="text" id="phone3" name="phone3" value="${dto.phone3}" class="form-control" 
     			 style="width: 70px; margin-left: 10px; 'ime-mode: disabled;'"
     			 onkeydown='return onlyNumber(event)'
				 onkeyup='removeChar(event)'
				 onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			     maxlength="4">
   			 </div>	
   			 
			<br>
    		<br>
    		
    		<div class="form-group" style="margin-left: 30px;">
    		  <label>이 메 일 &nbsp;</label>
     		 <input type="text" id="email1" name="email1" readonly value="${dto.email1}@${dto.email2}" class="form-control" style="width: 150px; margin-left: 20px;">
  		   </div>
    		
    		<%-- <div class="form-group" style="margin-left: 10px;">
     		 <label> @ </label>
    		  <input name="email2" value="${dto.email2}" readonly class="form-control" style="width: 150px; margin-left: 10px;">
   			 </div>
    		 --%>
  		  <br>
  		  <br>
    	
    			<div class="form-group" style="margin-left: 30px;">
     			 <label>우편번호</label>
   				   <input type="text" id="sample6_postcode" name="zipcode" value="${dto.zipcode}"  readonly class="form-control" style="width: 200px; margin-left: 20px;">
    			
    			<button type="button" onclick="sample6_execDaumPostcode()"
    			  class="btn btn-default"  style="width: 130px; background-color: #428bca; color: white;">우편번호 찾기</button>
  			  </div>
    			
    			
    		<br>
   			<br>
   			
   			<div class="form-group" style="margin-left: 30px;">
    			  <label>&nbsp;주  &nbsp;&nbsp;&nbsp;소&nbsp;&nbsp;  </label>
   			   <input type="text" id="sample6_address" name="add1" value="${dto.add1}" class="form-control" style="width: 250px; margin-left: 20px;">
   			 </div>
   
			<div class="form-group" style="margin-left: 0;">
      			<label></label>
   			   <input type="text" id="sample6_address2" name="add2" value="${dto.add2}" class="form-control" style="width: 200px; margin-left: 10px;">
   			 </div>
		
			<br>
   			<br>
		
		  <div align="center">
  			 <button type="button"  onclick="modify()" class="btn btn-default" 
 			  style="width: 100px; background-color: #428bca; color: white;">수정</button>
   			</div>
		
	</form>
	</div>
</div>
</div>





</div>
</div>
</div>

	
	
	
	<%-- 
	
	
	<h2>회원정보수정</h2>
	
	<form id="form1" name="form1" method="post">
	
		<label>아이디</label>
		<input type="text" id="userid" name="userid" value="${dto.userid}" readonly>
		<br>
		
		<label>성명</label>
		<input type="text" id="username" name="username" value="${dto.username}">
		<br>
		
		<label>생년월일</label>
		
	<input type="text" id="birthyear" name="birthyear" value="${dto.username}">
<input type="text" id="birthmonth" name="birthmonth" value="${dto.username}">
<input type="text" id="birthday" name="birthday" value="${dto.username}">

		<select id="birthyear" name="birthyear">
			<option value="">년도</option>
			<c:forEach var="row" begin="1920" end="2018">
				<option value="${row}" 
				<c:out value="${dto.birthyear == row ? 'selected' :'' }" />>${row}년</option>
			</c:forEach>
		</select>
		
		<select id="birthmonth" name="birthmonth">
			<option value="" >월</option>
			<c:forEach var="row" begin="1" end="12">
				<option value="${row}"
				<c:out value="${dto.birthmonth == row ? 'selected' :'' }" />>${row}월</option>
			</c:forEach>
		</select>
		
		<select id="birthday" name="birthday">
			<option value="">일</option>
			<c:forEach var="row" begin="1" end="31">
				<option value="${row}"
				<c:out value="${dto.birthday == row ? 'selected' :'' }" />>${row}일</option>
			</c:forEach>
		</select>
		<br>
		
		<label>성별</label>
		<input type="hidden" id="gender1" name="gender1" value="${dto.gender}">
		<input type="text" id="gender" name="gender">
		<br>
		
		<label>연락처</label>
		<select id="phone1" name="phone1">
			<option value="">전화번호</option>
			<option value="010"
			<c:out value="${dto.phone1 == '010' ? 'selected' :'' }" />>010</option>
			<option value="011"
			<c:out value="${dto.phone1 == '011' ? 'selected' :'' }" />>011</option>
			<option value="016"
			<c:out value="${dto.phone1 == '016' ? 'selected' :'' }" />>016</option>
			<option value="019"
			<c:out value="${dto.phone1 == '019' ? 'selected' :'' }" />>019</option>
		</select>
		
		<input type="text" id="phone2" name="phone2" value="${dto.phone2}"
			onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)'
			style='ime-mode: disabled;'
			onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			maxlength="4">
			
		<input type="text" id="phone3" name="phone3"
			value="${dto.phone3}" onkeydown='return onlyNumber(event)'
			onkeyup='removeChar(event)' style='ime-mode: disabled;'
			onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			maxlength="4">
			<br>
			
		<label>이메일</label>
		<input type="text" id="email1" name="email1" readonly value="${dto.email1}@${dto.email2}" />
		<br>
		
		<label>우편번호</label>
		<input type="text" id="sample6_postcode" name="zipcode"
			value="${dto.zipcode}" readonly>
			
		<input type="button" onclick="sample6_execDaumPostcode()" value="우편번호 찾기">
		<br>
		
		<label>주소</label>
		<input type="text" id="sample6_address" name="add1" value="${dto.add1}">
		<br>
		
		<label>상세주소</label>
		<input type="text" id="sample6_address2" name="add2" value="${dto.add2}">
		<br>
		
		<input type="button" value="회원정보수정" onclick="modify()">
	</form>
	
	
	 --%>
	
	
	

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


<%@ include file="../../include/footer.jsp"%>

</body>
</html>