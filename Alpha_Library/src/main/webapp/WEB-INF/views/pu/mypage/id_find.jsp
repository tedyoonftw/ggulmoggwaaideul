<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<script>
$(function(){
	$("#email3").change(function() {
		var email = $("#email3").val();
		$("#email2").val(email);
	});
	
	$("#num").keyup(function(){
		var num=$("#num").val();
		var result ="";
		var emailnum = $("#number").val();
		
		if (num == emailnum ){
			result = "일치합니다"
		} else {
			result = "일치하지않습니다."
		}
		$("#result").html(result);	
	});
	
	$("#btnin").click(function(){
		var email1=$("#email1").val();
		var email2=$("#email2").val();
		var username = $("#username").val();
		
		if(username = ""){
			alert("성명을 입력해주세요.");
			$("#username").focus(); //입력 포커스 이동
			return; //함수 종료
		}
		
		if(email1 == ""){
			alert("이메일을 입력해주세요.");
			$("#email1").focus(); //입력 포커스 이동
			return; //함수 종료
		} 
		if(email2 ==""){
			alert("이메일을 입력해주세요.");
			$("#email2").focus();
			return;
		}
		document.form1.submit();
	});
	
	 $("#find").click(function(){
		var num=$("#num").val();
		var emailnum = $("#number").val();
		var userid = $("#userid").val();
		
		if(num != emailnum){
			alert("인증번호를 확인하세요.");
			$("#num").focus(); //입력 포커스 이동
			return; //함수 종료
		}
		
		location.href="${path}/pu/page/id_find.do?userid="+userid;
	});
});
	
</script>
<body style="
    height: 200px; margin-left: 40px; margin-top: 40px;">
<h2>아이디 찾기(${number})</h2>
<form name="form1" method="post" action="${path}/project/email/id_send.do">

<br>
<input type="hidden" name="userid" id="userid" value="${userid}">
성명 : <input type="text" name="username" id="username" value="${username }">
<br>
이메일 : <input type="text" id="email1" name="email1" value="${email1}" />
@<input type="text" id="email2" name="email2" value="${email2 }" />
<select id="email3" >
	<option value="">직접입력</option>
	<option value="daum.net"
		<c:out value="${email2 == 'daum.net' ? 'selected' :'' }" />>daum.net</option>
	<option value="gmail.com"
		<c:out value="${email2 == 'gmail.com' ? 'selected' :'' }" />>gmail.com</option>
	<option value="naver.com"
		<c:out value="${email2 == 'naver.com' ? 'selected' :'' }" />>naver.com</option>
	<option value="nate.com"
		<c:out value="${email2 == 'nate.com' ? 'selected' :'' }" />>nate.com</option>
</select>
<input type="button" id="btnin" value="인증하기" >
</form>
<c:if test="${message == '이메일이 발송되었습니다.'}">
	<br>
	<input id="num" name="receiveMail" placeholder="인증번호"
	style="text-align:center;
    width: 310px;
    height: 20px;"><br>
</c:if>
<span style="color:red; font-size: 14px;">${message}</span><br>
<input type="hidden" id="number" value="${number}">
<div id="result" style="color: red;"></div><br>
<input type="button" id="find" value="확인" 
style="background-color:#3c3cf2;width: 296px; 
margin-left: 10px; height: 30px;">
</body>

<script type="text/javascript">
function OpenWin(URL, width, height) {
	var str, width, height;
	str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
	str = str + "width=" + width;
	str = str + ",height=" + height + "',top=50,left=50";
	window.open(URL, 'id', str);
}
</script>
</html>