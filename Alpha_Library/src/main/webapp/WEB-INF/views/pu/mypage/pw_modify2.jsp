<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<script>
$(function() {
	$("input[id=newpw2]").keyup(function() {
		var newpw = document.getElementById("newpw").value;
		var newpw2 = document.getElementById("newpw2").value;
		if (newpw == newpw2) {
			red = "";
			$("#red").html(red);
			blue = "비밀번호가 일치합니다.";
			$("#blue").html(blue);
		} else if (newpw != newpw2) {
			blue = "";
			$("#blue").html(blue);
			red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
			$("#red").html(red);
		}
	});
	$("input[id=newpw]").keyup(function() {
		var newpw = document.getElementById("newpw").value;
		var newpw2 = document.getElementById("newpw2").value;
		if (newpw == newpw2) {
			red = "";
			$("#red").html(red);
			blue = "비밀번호가 일치합니다.";
			$("#blue").html(blue);
		} else if (newpw != newpw2) {
			blue = "";
			$("#blue").html(blue);
			red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
			$("#red").html(red);
		}
	});
});

function pw_modify() {
	var nowpw=$("#nowpw").val();
	var newpw=$("#newpw").val();
	var newpw2=$("#newpw2").val();
	var passwd= $("#passwd").val();
	console.log(passwd);
	if (nowpw == "") {
		alert("현재 비밀번호를 입력해주세요.");
		$("#nowpw").focus();
		return;
	}
	if (newpw == "") {
		alert("새로운 비밀번호를 입력해주세요.");
		$("#newpw").focus();
		return;
	}
	if (newpw2 == "") {
		alert("새로운 비밀번호를 입력해주세요.");
		$("#newpw2").focus();
		return;
	}
	
	if (nowpw != passwd){
		alert("현재 비밀번호가 맞지 않습니다. 다시 확인해주세요.");
		$("#nowpw").focus();
		return;
	}
	
	var pw_passed = true;
	var id = $("#userid").val();
	pw_passed = true;
	var pattern1 = /[0-9]/;
	var pattern2 = /[a-zA-Z]/;
	var pattern3 = /[~!@\#$%<>^&*]/;
	var pw_msg = "";
	if (newpw != newpw2) {
		alert("비밀번호가 일치하지 않습니다.");
		$("#newpw").focus();
		return false;
	}
	if (!pattern1.test(newpw) || !pattern2.test(newpw)
			|| !pattern3.test(newpw) || newpw.length<8||newpw.length>21) {
		alert("비밀번호는 영문,숫자,특수기호를 포함하여 8자리 이상, 20자리 이하로 구성해주세요.");
		$("#newpw").focus();
		return false;
	}
	if (newpw.indexOf(id) > -1) {
		alert("비밀번호는 ID를 포함할 수 없습니다.");
		$("#newpw").focus();
		return false;
	}

	var SamePass_0 = 0;
	var SamePass_1 = 0;
	var SamePass_2 = 0;
	for (var i = 0; i < newpw.length; i++) {
		var chr_pass_0;
		var chr_pass_1;
		var chr_pass_2;
		if (i >= 2) {
			chr_pass_0 = newpw.charCodeAt(i - 2);
			chr_pass_1 = newpw.charCodeAt(i - 1);
			chr_pass_2 = newpw.charCodeAt(i);
			if ((chr_pass_0 == chr_pass_1) && (chr_pass_1 == chr_pass_2)) {
				SamePass_0++;
			} else {
				SamePass_0 = 0;
			}
			if (chr_pass_0 - chr_pass_1 == 1
					&& chr_pass_1 - chr_pass_2 == 1) {
				SamePass_1++;
			} else {
				SamePass_1 = 0;
			}
			if (chr_pass_0 - chr_pass_1 == -1
					&& chr_pass_1 - chr_pass_2 == -1) {
				SamePass_2++;
			} else {
				SamePass_2 = 0;
			}
		}

		if (SamePass_0 > 0) {
			alert("동일문자를 3자 이상 연속 입력할 수 없습니다.");
			$("#newpw").focus();
			pw_passed = false;
		}

		if (SamePass_1 > 0 || SamePass_2 > 0) {
			alert("영문, 숫자는 3자 이상 연속 입력할 수 없습니다.");
			$("#newpw").focus();
			pw_passed = false;
		}

		if (!pw_passed) {
			$("#newpw").focus();
			return false;
			break;
		}
	}
	if(confirm("비밀번호를 수정하시겠습니까?")){
		true;
		document.form1.action = "${path}/pu/page/passwd.do";
		document.form1.submit();
		alert("비밀번호가 변경되었습니다.");
	}return;
}
</script>
</head>


<style>
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>


<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;"><span style="font-size: 20px; ">
                <br>나만의 도서관</span></a></li>
                <li><a href="${path}/hjh/mypage/nowrent.do"> 대출현황관리</a></li>
                <li><a href="${path}/hjh/mypage/beforerent.do"> 대출이력조회</a></li>
                <li><a href="${path}/hjh/book/cart/list.do"> 관심도서목록</a></li>
                <li><a href="${path}/hjh/mypage/wishlist.do">희망도서신청목록</a></li>
                <li><a href="${path}/pu/page/pw.do">비밀번호변경</a></li>
                <li><a href="${path}/pu/page/list.do">회원정보수정</a></li>
                <li><a href="${path}/pu/page/bye.do">회원탈퇴</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>






<h2>비밀번호 변경</h2>
<form id="form1" name="form1" method="post">
<input type="hidden" id="userid" name="userid" value="${dto.userid }">
<input type="hidden" id="passwd" value="${dto.passwd }">
<label>현재 비밀번호</label>
<input type="password" id="nowpw" name="nowpw">
<br>
<label>새 비밀번호</label>
<input type="password" id="newpw" name="passwd">
<br>
<label>새 비밀번호 확인</label>
<input type="password" id="newpw2" name="newpw2">
<br>
<div>비밀번호는 영문,숫자,특수기호를 포함하여 8자리 이상, 20자리 이하로 구성해주세요.</div>
<div id="red" style="color: red;"></div>
<div id="blue" style="color: blue;"></div>
<br>
<input type="button" onclick="pw_modify()" value="비밀번호 변경">
</form>








<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>





</body>
</html>