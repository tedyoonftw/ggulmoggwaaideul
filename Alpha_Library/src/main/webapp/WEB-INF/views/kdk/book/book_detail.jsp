<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
$(function(){
	$("#btnList").click(function(){
		//목록으로 이동
		location.href="${path}/kdk/book/book/list.do";
	
	}); 

});

$(function(){
	$("#btnUpdate").click(function(){
		document.form2.action="${path}/kdk/book/book/update.do";
		document.form2.submit();
	});
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")){
		document.form2.action="${path}/kdk/book/book/delete.do";
		document.form2.submit();
		}
	});
});

function MemoDelete (idx,book_code) {
	location.href="${path}/kdk/book/memo/westpyeongsakjae.do?idx="+idx+"&book_code="+book_code;
}

function OpenWin(URL, width, height) {
	var str, width, height;
	str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
	str = str + "width=" + width;
	str = str + ",height=" + height + "',top=50,left=50";
	window.open(URL, 'remoteSchedule', str);
}

function rent(str) {
	OpenWin("${path}/book/rent/rentinfo/"+str, 480, 360);
}



		/* alert("장바구니에 있는데 왜 또 관심갖음? 미쳤음?");
 */



function witch(str){
	OpenWin("${path}/hjh/book/bookwitch/"+str, 500, 800);
}
</script>


<!-- 책테이블 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}


/* 테이블 */
.art-title{color:#231f20; font-size:20px; font-weight:700;}
.artist-data{width:100%; padding-bottom: 25px;}
.artst-pic{width:33%;position: relative;}
.artst-pic span a{color: #fff; font-size: 16px; display: none;}
.artst-pic span.artst-like{position: absolute; left: 11%; bottom: 10px;}
.artst-pic span.artst-share{position: absolute; left:46%; bottom: 10px;}
.artst-pic span.artst-plus{position: absolute; right: 9%; bottom: 10px;}
.artst-prfle{width:63%;}
.artst-prfle span.artst-sub{font-size:15px; color:#bbb; float:left; width:100%; font-weight:normal; padding:5px 0;}
.artst-prfle span.artst-sub span.byname{font-weight:700; color:#aaa;}
.artst-prfle span.artst-sub span.daysago{float:right; font-size:12px;}
.counter-tab{float: left; width: 100%; padding-top: 45px;}
.counter-tab div{float: left; width: 33%; color: #aaa; font-size: 12px;}
.bot-links{float: left; width: 100%; padding-top: 10px;}
.bot-links a{display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;}
span.play-icon{position: absolute; left: 31%; top: 32%; display: none;}
.artst-pic:hover img.play-icon, .artst-pic:hover span a{display: block; } 




/* 댓글달기 */
.message-item {
margin-bottom: 25px;
margin-left: 40px;
position: relative;
}
.message-item .message-inner {
background: #fff;
border: 1px solid #ddd;
border-radius: 3px;
padding: 10px;
position: relative;
}
.message-item .message-inner:before {
border-right: 10px solid #ddd;
border-style: solid;
border-width: 10px;
color: rgba(0,0,0,0);
content: "";
display: block;
height: 0;
position: absolute;
left: -20px;
top: 6px;
width: 0;
}
.message-item .message-inner:after {
border-right: 10px solid #fff;
border-style: solid;
border-width: 10px;
color: rgba(0,0,0,0);
content: "";
display: block;
height: 0;
position: absolute;
left: -18px;
top: 6px;
width: 0;
}
.message-item:before {
background: #fff;
border-radius: 2px;
bottom: -30px;
box-shadow: 0 0 3px rgba(0,0,0,0.2);
content: "";
height: 100%;
left: -30px;
position: absolute;
width: 3px;
}
.message-item:after {
background: #fff;
border: 2px solid #ccc;
border-radius: 50%;
box-shadow: 0 0 5px rgba(0,0,0,0.1);
content: "";
height: 15px;
left: -36px;
position: absolute;
top: 10px;
width: 15px;
}
.clearfix:before, .clearfix:after {
content: " ";
display: table;
}
.message-item .message-head {
border-bottom: 1px solid #eee;
margin-bottom: 8px;
padding-bottom: 8px;
}
.message-item .message-head .avatar {
margin-right: 20px;
}
.message-item .message-head .user-detail {
overflow: hidden;
}
.message-item .message-head .user-detail h5 {
font-size: 16px;
font-weight: bold;
margin: 0;
}
.message-item .message-head .post-meta {
float: left;
padding: 0 15px 0 0;
}
.message-item .message-head .post-meta >div {
color: #333;
font-weight: bold;
text-align: right;
}
.post-meta > div {
color: #777;
font-size: 12px;
line-height: 22px;
}
.message-item .message-head .post-meta >div {
color: #333;
font-weight: bold;
text-align: right;
}
.post-meta > div {
color: #777;
font-size: 12px;
line-height: 22px;
}
/* img {
 min-height: 40px;
 max-height: 40px;
} */








</style>

</head>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 800px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 30px;">
                <span style="font-size: 20px; ">
                <br> 자료 검색</span></a></li>                
                <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
  


<div class="container">
<div class="row">
<div class="col-md-7">


<section>
<h2>도서상세정보</h2>
<hr>
                	<div class="artist-data pull-left">
                    	<div class="artst-pic pull-left">
                    		<span class="artst-like"><a href="#"><i class="glyphicon glyphicon-heart-empty"></i></a></span>
                    		<span class="artst-share"><a href="#"><i class="glyphicon glyphicon-share"></i></a></span>
                    		<span class="artst-plus"><a href="#"><i class="glyphicon glyphicon-plus-sign"></i></a></span>
                    		<a href="#">
                    			<img src="${path}/images/${dto.imgsrc}" alt=".." class="img-responsive" />
                    		</a>
                    		
                    	</div>
                    	
                    	
                    	
                        <div class="artst-prfle pull-right">
                        	<div class="art-title" style="width: 500px;">
                            	<img src="${path}/images/book.jpg" alt=""> &nbsp;&nbsp;
       	<a href ="${path}/kdk/book/book/detail?book_code=${dto.book_code}">
		${dto.title}</a>
                                <span class="artst-sub"><span class="byname">
       	저자 : ${dto.author}<br>
출판사 :  ${dto.publisher}<br>
발행년도 : ${dto.pubYear}<br>
ISBN :  ${dto.isbn}<br>
청구기호 :  ${dto.book_no}<br>
소장자료설명(책위치)  : ${dto.shelfLocName}<br><br>
<i class="icon-user"></i>&nbsp;&nbsp;<a href="#">자료상태 : <c:if test="${dto.book_tf == '0'}">
<label style="color: red;">대출중</label></c:if>
<c:if test="${dto.book_tf == '1'}"><label style="color: blue;">대출가능</label></c:if></a>
&nbsp;&nbsp;<a href="${path}/kdk/book/cart/insert.do?book_code=${dto.book_code}">
<i class="fa fa-book"></i>관심도서담기</a>&nbsp;&nbsp;

<c:choose>
<c:when test = "${checkrent == dto.book_code }">
<i class="fa fa-calendar"></i><span style="color: black;">대출중</span>
</c:when>

<c:when test = "${sessionScope.userid != null }">
<a href="javascript:rent('${dto.book_code}')">
<i class="fa fa-calendar"></i>도서대출</a>
</c:when>
<c:otherwise>
<i class="fa fa-calendar"></i>도서대출
</c:otherwise>
</c:choose>
<br><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="javascript:witch('${dto.book_code}')"><i class="fa fa-book"></i>자료위치보기</a>
&nbsp;&nbsp;<a href="${path}/hjh/book/list.do"><i class="fa fa-list-alt"></i>도서목록</a>
                                
                                
                                
                                </span></span>  <!-- <span class="daysago">5 Days Ago</span> -->
                            </div>
                        </div>
                        
                        
                        
                    </div>
</section>
                    <!-- 아래부분 -->
<section>                  
                  <h2>소장정보</h2>	
<hr>



<table>
	<tr>
		<th style="padding-right: 50px;">소장자료설명(책위치)</th>
		<th style="padding-right: 50px;">청구기호</th>
		<th style="padding-right: 50px;">등록번호</th>
		<th style="padding-right: 50px;">도서상태</th>
	</tr>
	
	<tr>
		<td style="padding-right: 50px;">${dto.shelfLocName}</td>
		<td style="padding-right: 50px;">${dto.book_no}</td>
		<td style="padding-right: 50px;">${dto.regNo}</td>
		<td style="padding-right: 50px;"><c:if test="${dto.book_tf == '0'}"><label style="color: red;">대출중</label></c:if>
			   <c:if test="${dto.book_tf == '1'}"><label style="color: blue;">대출가능</label></c:if></td>
	</tr>
</table>	
</section>
<hr>



<h2>책소개</h2>
<div class="col-md-12">
<hr>
                	<div class="artist-data pull-left">
                    	<div class="artst-pic pull-left">
                    		<span class="artst-like"><a href="#"><i class="glyphicon glyphicon-heart-empty"></i></a></span>
                    		<span class="artst-share"><a href="#"><i class="glyphicon glyphicon-share"></i></a></span>
                    		<span class="artst-plus"><a href="#"><i class="glyphicon glyphicon-plus-sign"></i></a></span>
                    		<a href="#">
                    			<img src="${path}/images/${dto.imgsrc}" alt=".." class="img-responsive" />
                    		</a>
                    		
                    	</div>
                    	
                        <div class="artst-prfle pull-right">
                        	<div style="width: 500px;">
       <span>${dto.description}</span>  <!-- <span class="daysago">5 Days Ago</span> -->
                            </div>
<hr>
                        </div>
                        
                        
                    </div>
              </div>
              
              
              

<hr>
	






<%-- <table>
	<tr>
		<th>번호</th>
		<th>아이디</th>
		<th>내용</th>
		<th>등록일자</th>
		<!-- <th>도서 레코드</th> -->
		<th>기능</th>
	</tr>
 <c:forEach var="row" items="${list}">
	
	
	<tr>
		<td>${row.idx}</td>
		<td>${row.userid}</td>
		<td>${row.memo}</td>
		<td><fmt:formatDate value="${row.post_date}" pattern="yyyy-MM-dd" /></td>
		<td>${row.book_code}</td>
		
		
		<c:if test="${sessionScope.userid == row.userid }">
		<td><button type="button" id="MemoDelete" onclick="javascript:MemoDelete('${row.idx}','${row.book_code}')">삭제</button></td>
		</c:if>
		<!-- <td><form name="form3"  method="post"><button type="button" id="btnUpdate">수정</button><button type="button" id="btnDelete" >삭제</button></form></td> -->
	</tr>
</c:forEach>
	


</table> --%>


<div class="container" style="
    width: 630px;">
	<div class="row">
		<h2>한줄평</h2>
	</div>
	 <c:forEach var="row" items="${list}">
    <div class="qa-message-list" id="wallmessages">
    				<div class="message-item" id="m16">
						<div class="message-inner">
							<div class="message-head clearfix">
								<div class="avatar pull-left"><a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko"><img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"
								style="min-height: 40px; max-height: 40px;"></a></div>
								<div class="user-detail">
									<h5 class="handle"><span style="margin-right: 390px;">${row.userid}</span>
									<c:if test="${sessionScope.userid == row.userid || sessionScope.adminid == 'admin'}">
									<a href="#" class="btn btn-default"
									 id="MemoDelete" onclick="javascript:MemoDelete('${row.idx}','${row.book_code}')"
									><span style="color: red;" class="glyphicon glyphicon-trash"></span></a>
									</c:if>
									</h5>
									<div class="post-meta">
										<div class="asker-meta">
											<span class="qa-message-what"></span>
											<span class="qa-message-when">
												<span class="qa-message-when-data"><fmt:formatDate value="${row.post_date}" pattern="yyyy-MM-dd" /></span>
											</span>
											<!-- <span class="qa-message-who">
												<span class="qa-message-who-pad">by </span>
												<span class="qa-message-who-data"><a href="./index.php?qa=user&qa_1=Oleg+Kolesnichenko">Oleg Kolesnichenko</a></span>
											</span> -->
										</div>
									</div>
								</div>
							</div>
							<div class="qa-message-content">
								${row.memo}<br>
							</div>
					</div></div>				
				</div>
				</c:forEach>
			</div>

<div align="center" style="margin-left: 50px;">
<form method="post" action="${path}/kdk/book/memo/insert.do"> 
	<input type="hidden" value="${userid}" name="userid" id="userid">
	<input type="hidden" value="${dto.book_code}" name="book_code" id="book_code">
	<c:if test = "${sessionScope.userid != null }">
	<input  name="memo" id="memo" placeholder=" 책에 대한 한줄평을 적어주세요!"
	style="width: 500px; height: 30px;">
	<input type="submit" value="확인" style="height: 30px;">
	</c:if>
</form>
</div>

</div>
</div>
</div>



<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>