<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file = "../../include/header.jsp" %>
<script>
	function list(page) {
		location.href = "${path}/ym/adminapplicant/list.do?curPage=" + page;
	}
</script>
</head>
<body>
<%@ include file = "../../include/menu.jsp" %>
<h2>문화프로그램 신청자 리스트</h2>
${map.count}명
<form name="form1" method="post" action="${path}/ym/adminapplicant/list.do">
	<select name="search_option">
		
		<option value="userid" <c:if test="${map.search_option == 'userid'}">selected</c:if>>ID</option>
		<option value="username"<c:if test="${map.search_option == 'username'}">selected</c:if>>이름</option>
		<option value="all" <c:if test="${map.search_option == 'all'}">selected</c:if>>ID/이름</option>
	
	</select>
	<input name="keyword" value="${map.keyword }">
	<input type="submit" value="조회">
	</form>
<table border="1">
<tr>
<th>신청번호</th>
<th>신청강좌</th>
<th>신청자명</th>
<th>신청자ID</th>
</tr>
<c:forEach var="dto" items="${map.list}">
<tr>
<td>${dto.ano}</td>
<td>${dto.title}</td>
<td>${dto.username}</td>
<td>${dto.userid}</td>
</tr>
</c:forEach>
<tr>
			<td colspan="4" align="center">
			<c:if test="${map.pager.curBlock>1}">
			<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curPage>1}">
			<a href="#" onclick="list('${map.pager.prevPage}')">[이전]</a>
			</c:if>
			<c:forEach var="num" begin="${map.pager.blockBegin}"
					end="${map.pager.blockEnd}">
					<c:choose>
						<c:when test="${num==map.pager.curPage }">
						<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
							<span style="color: red;">${num}</span>
						</c:when>
						<c:otherwise>
							<a href="#" onclick="list('${num}')">${num}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			<c:if test="${map.pager.curBlock<map.pager.totBlock}">
			<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage<map.pager.totPage}">
			<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>	
			</td>
		</tr>
</table>
</body>
</html>