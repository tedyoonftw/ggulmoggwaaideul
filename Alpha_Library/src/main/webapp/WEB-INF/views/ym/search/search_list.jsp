<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>

<script>
function list(page){
	 location.href=
	 "${path}/hjh/book/list.do?curPage="+page
	 +"&search_option=${map.search_option}"
	 +"&keyword=${map.keyword}";
}

/* 추가 */
function OpenWin(URL, width, height) {
	var str, width, height;
	str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
	str = str + "width=" + width;
	str = str + ",height=" + height + "',top=50,left=50";
	window.open(URL, 'remoteSchedule', str);
}

function rent(str) {
	OpenWin("${path}/hjh/book/book/detail/" + str, 480, 360);
}

</script>

</head>
<!-- 추가부분 -->
<!-- 
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
 --><script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<!-- 책테이블 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}


/* 테이블 */
.art-title{color:#231f20; font-size:20px; font-weight:700;}
.artist-data{width:100%; padding-bottom: 25px;}
.artst-pic{width:33%;position: relative;}
.artst-pic span a{color: #fff; font-size: 16px; display: none;}
.artst-pic span.artst-like{position: absolute; left: 11%; bottom: 10px;}
.artst-pic span.artst-share{position: absolute; left:46%; bottom: 10px;}
.artst-pic span.artst-plus{position: absolute; right: 9%; bottom: 10px;}
.artst-prfle{width:63%;}
.artst-prfle span.artst-sub{font-size:15px; color:#bbb; float:left; width:100%; font-weight:normal; padding:5px 0;}
.artst-prfle span.artst-sub span.byname{font-weight:700; color:#aaa;}
.artst-prfle span.artst-sub span.daysago{float:right; font-size:12px;}
.counter-tab{float: left; width: 100%; padding-top: 45px;}
.counter-tab div{float: left; width: 33%; color: #aaa; font-size: 12px;}
.bot-links{float: left; width: 100%; padding-top: 10px;}
.bot-links a{display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;}
span.play-icon{position: absolute; left: 31%; top: 32%; display: none;}
.artst-pic:hover img.play-icon, .artst-pic:hover span a{display: block; } 

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 700px;">
                <li class="active"><a href="#"><i class="fa fa-home fa-fw"></i> 자료 검색</a></li>
                <li><a href="${path}/hjh/book/search.do"><i class="fa fa-search"></i> 간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do"><i class="fa fa-search-plus"></i> 상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do"><i class="glyphicon glyphicon-book"></i> 신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do"><i class="fa fa-book"></i> 도서 리스트</a></li>
            	<li><br></li>
            	<li><br></li>
            	<li><br></li>
            	<c:if test = "${sessionScope.userid != null }">
 				<li><span style="font-size: 24px; color: blue;"><b>최근</b></span><span style="font-size: 20px;"><!-- <i class="fa fa-keyboard-o"></i> --> 검색어</span> </li>
                <c:forEach var="keyworddto" items="${keywords}">
                <li><i class="fa fa-check-circle-o"></i> ${keyworddto.keyword}</li>
                </c:forEach>
            	<li><br></li>
            	</c:if>
            	<li><br></li>
            	<li><br></li>
                <li><span style="font-size: 24px; color: blue;"><b>인기</b></span><span style="font-size: 20px;"><!-- <i class="fa fa-list-ol"></i> --> 검색어</span></li>
                <c:forEach var = "ingikeywords" items = "${ingikeywords}">
                <c:if test="${ingikeywords.keyword != '' }">                
                <li><i class="fa fa-check-circle-o"></i>${ingikeywords.keyword}</li>
                </c:if>
                </c:forEach>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
  


<div class="container">
<div class="row">
<div class="col-md-8">
	
	<h2>통합검색결과</h2>
                
<h4>
<span style="color: red"><i class="fa fa-asterisk"></i></span>
<c:if test="${map.count1} != null "> 요청하신'<span style="color: red;">${map.keyword}</span>' 에 대한</c:if> 소장자료 검색결과  <span style="color: red; ">${map.count1}${map.count2}${map.count3}</span>권 </h4> 


<form class="form"
                    name="form1" method="post" 
	action="${path}/hjh/book/list.do">
                        <div class="form-group">
                            <div class="input-group">
                            
                                <span class="input-group-addon" id="month-addon">자료검색 </span>
                                <select name="search_option" class="form-control" aria-describedby="month-addon">
                                    <option value="all"
		<c:out value="${map.search_option=='all'?'selected':''}" /> >전체 </option>
	<option value="title" 
		<c:out value="${map.search_option=='title'?'selected':''}" /> >제목 </option>
	<option value="author"
		<c:out value="${map.search_option=='author'?'selected':''}" /> >저자 </option>
	<option value="publisher"
		<c:out value="${map.search_option=='publisher'?'selected':''}" /> >출판사 </option>                          
                                </select>
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                                <input name="keyword" value="${map.keyword}" 
                                class="form-control" aria-describedby="month-addon">
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                               	<input type="submit" value="검색" name="control-month" style="background-color: #428bca; color: white;"
                               	class="form-control" aria-describedby="month-addon">
                           
                            </div>
                        </div>
                    </form>
                
                <!-- 			최근본자료	 		-->
                
<div class="container">
<div class="row">
<div class="col-md-12"
style="padding-left: 0px; padding-bottom: 15px;">
                <span style=" color: red;">최근 본 자료 : &nbsp;</span>
				<c:forEach var="titleKeyword" items="${titleKeyword}">
      			<c:set var="TitleKeyword" value="${titleKeyword.title}" />
       			<a href="${path}/hjh/book/book/detail/${titleKeyword.book_code}">
       			<span style="margin-right: 20px;"><i class="fa fa-tags"></i>
       			${fn:substring(TitleKeyword, 0, 7)}...</span>
       			</a>
      			</c:forEach>
</div>                
</div>                
</div>                
                
                





                
<div class="container" style="margin-left: 20px;">
<div class="row">
<div class="col-md-4" 
style="padding-left: 0px;">                
                
                
                <c:forEach var="row" items="${map.list}">
                	<div class="artist-data pull-left" >
                    	<div class="artst-pic pull-left">
                    		<span class="artst-like"><a href="#"><i class="glyphicon glyphicon-heart-empty"></i></a></span>
                    		<span class="artst-share"><a href="#"><i class="glyphicon glyphicon-share"></i></a></span>
                    		<span class="artst-plus"><a href="#"><i class="glyphicon glyphicon-plus-sign"></i></a></span>
                    		<a href="#">
                    			<img src="${path}/images/${row.imgsrc}" alt="" class="img-responsive" />
                    		</a>
                    		
                    	</div>
                    	
                    	
                    	
                        <div class="artst-prfle pull-right" >
                        <div class="art-title" style="width: 800px;">
                        <img src="${path}/images/book.jpg" alt=""> &nbsp;&nbsp;
                        
                        
       	<a href ="${path}/hjh/book/book/detail/${row.book_code}">
		${row.title}</a>
                               <span class="artst-sub"style="margin-left: 70px;"><span class="byname" style="font-size: 16px">
       	저자 : ${row.author}<br>
출판사 :  ${row.publisher}<br>
발행년도 : ${row.pubYear}<br>
ISBN :  ${row.isbn}<br>
청구기호 :  ${row.book_no}<br>
소장자료설명(책위치)  : ${row.shelfLocName}<br><br>
<i class="icon-user"></i><a href="#">자료상태 : <c:if test="${row.book_tf == '0'}">
<label style="color: red;">대출불가</label></c:if>
<c:if test="${row.book_tf == '1'}"><label style="color: blue;">대출가능</label></c:if></a>
&nbsp;&nbsp;<a href="${path}/hjh/book/cart/insert.do?book_code=${row.book_code}">
<i class="fa fa-book"></i>관심도서담기</a>
&nbsp;&nbsp;<a href="#">
<i class="fa fa-calendar"></i>도서예약</a>
                                </span></span>  <!-- <span class="daysago">5 Days Ago</span> -->
  
                    </div>
                    </div>
                    </div>
<hr>
                </c:forEach>


</div>
</div>
</div>
                
                
                
                <!-- 페이지 네비게이션 출력 -->
            <div align="center">
            <p style="font-size: 20px;">
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('1')">[처음]&nbsp;</a>
			</c:if>
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('${map.pager.prevPage}')">
				[이전]</a>
			</c:if>
			<c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
					<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
						<span style="color:red;">&nbsp;${num}&nbsp;</span>
					</c:when>
					<c:otherwise>
						<a href="#" onclick="list('${num}')">&nbsp;${num}&nbsp;</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${map.pager.curBlock < map.pager.totBlock}">
				<a href="#" 
				onclick="list('${map.pager.nextPage}')">[다음]&nbsp;</a>
			</c:if>
			<c:if test="${map.pager.curPage < map.pager.totPage}">
				<a href="#" 
				onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>
			</p>
		</div>
              





</div>
</div>
</div>
<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>



<%@ include file="../../include/footer.jsp"%>
</body>

</html>