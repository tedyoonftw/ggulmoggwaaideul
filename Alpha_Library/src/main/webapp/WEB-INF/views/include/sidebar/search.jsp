<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
uri="http://java.sun.com/jsp/jstl/core" %>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
.fa-fw {width: 2em;}
</style>

<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 800px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 30px;">
                <span style="font-size: 20px; ">
                <br> 자료 검색</span></a></li>                
                <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
            	<li><br></li>
            	<li><br></li>
            	<c:if test = "${sessionScope.userid != null }">
 				<li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>최근</b></span><span style="font-size: 20px;"> 검색어</span> </li>
                <c:forEach var="keyworddto" items="${keywords}">
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i> ${keyworddto.keyword}</li>
                </c:forEach>
            	<li><br></li>
            	</c:if>
            	<li><br></li>
                <li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>인기</b></span><span style="font-size: 20px;"> 검색어</span></li>
                <c:forEach var = "ingikeywords" items = "${ingikeywords}">
                <c:if test="${ingikeywords.keyword != '' }">                
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i>${ingikeywords.keyword}</li>
                </c:if>
                </c:forEach>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

          
          