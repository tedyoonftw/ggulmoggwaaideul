<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
uri="http://java.sun.com/jsp/jstl/core" %>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
.fa-fw {width: 2em;}
</style>

<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
       <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;"><span style="font-size: 20px; ">
                <br>나만의 도서관</span></a></li>
                <li><a href="${path}/hjh/mypage/nowrent.do"> 대출현황관리</a></li>
                <li><a href="${path}/hjh/mypage/beforerent.do"> 대출이력조회</a></li>
                <li><a href="${path}/kdk/book/cart/list.do"> 관심도서목록</a></li>
                <li><a href="${path}/hjh/mypage/wishlist.do">희망도서 신청목록</a></li>
                <li><a href="${path}/applicant/list.do">문화 신청목록</a></li>
                <li><a href="${path}/pu/page/pw.do">비밀번호변경</a></li>
                <li><a href="${path}/pu/page/list.do">회원정보수정</a></li>
                <li><a href="${path}/pu/page/bye.do">회원탈퇴</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

          
          