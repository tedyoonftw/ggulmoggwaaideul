<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 세션을 사용하지 않는 옵션 -->
<!-- 헤더 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<style>


/* 헤더 */

body{
    padding:0;
}

#login-dp{
    min-width: 250px;
    padding: 14px 14px 0;
    overflow:hidden;
    background-color:rgba(255,255,255,.8);
}
#login-dp .help-block{
    font-size:12px    
}
#login-dp .bottom{
    background-color:rgba(255,255,255,.8);
    border-top:1px solid #ddd;
    clear:both;
    padding:14px;
}
#login-dp .social-buttons{
    margin:12px 0    
}
#login-dp .social-buttons a{
    width: 49%;
}
#login-dp .form-group {
    margin-bottom: 10px;
}
.btn-fb{
    color: #fff;
    background-color:#3b5998;
}
.btn-fb:hover{
    color: #fff;
    background-color:#496ebc 
}
.btn-tw{
    color: #fff;
    background-color:#55acee;
}
.btn-tw:hover{
    color: #fff;
    background-color:#59b5fa;
}
@media(max-width:768px){
    #login-dp{
        background-color: inherit;
        color: #fff;
    }
    #login-dp .bottom{
        background-color: inherit;
        border-top:0 none;
    }
}

a:link {color: black;}
a:visited {color: black;}
a:hover {color: blue; text-decoration: none;}
a:active {color: black; text-decoration: none;}
</style>

<div id="wrapper" style="margin-left: 150px; margin-right: 150px;">
	<div style="
    margin-top: 20px; height: 80px;">	
	<a href="${path}/"><img src="${path}/images/logo.png" 
	style="margin-bottom: 30px;" alt="${path}/"></a>
	<a href="${path}/"><span style="font-size: 40px; color: blue;"><b>미래</b></span> <span style="font-size: 30px; color: black;"><b>도서관</b></span></a>
	
	<!-- <form class="navbar-form navbar-right" role="search"
	style="margin-top: 50px;">
        <div class="form-group" >
          <input type="text" class="form-control" 
          style="width: 500px;" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">찾기</button>
      </form> -->
	
	</div>
</div>
<br>
<!-- 헤더 -->
<nav class="navbar navbar-default navbar-inverse" role="navigation" style="margin-left: 150px; margin-right: 150px;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Clean</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">도서관 소개</a></li> -->
        
        
        	<!-- 드랍메뉴 -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;">도서관 소개</span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${path}/project/gm/insa.do">인사말</a></li>
            <li class="divider"></li>
            <li><a href="${path}/chart/book_list.do">자료현황</a></li>
            <li class="divider"></li>
            <li><a href="${path}/project/info/visit.do">오시는 길</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;">도서관 이용 </span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${path}/project/schedule/bigcalview.do">휴관일/행사</a></li>
            <li class="divider"></li>
            <li><a href="${path}/project/email/agree.do">회원가입</a></li>
            <li class="divider"></li>
            <li><a href="${path}/project/readingroom/list.do">열람실 이용</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;">자료검색</span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
            <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
            <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
            <li><a href="${path}/hjh/mypage/form.do">희망도서신청</a></li>
            <li class="divider"></li>
            <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;">정보마당</span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${path}/notice/list.do">공지사항</a></li>
            <li><a href="${path}/qna/list.do">묻고 답하기</a></li>
            <li><a href="${path}/culture/list.do">문화프로그램</a></li>
          </ul>
        </li>
      <c:if test="${sessionScope.adminid == 'admin' }">
                <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;">관리자</span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${path}/ym/membership/list.do">회원정보관리</a></li>
            <li><a href="${path}/project/admin_readingroom/bigcalview.do">일정관리</a></li>
            <li><a href="${path}/project/admin_readingroom/admin_list.do">열람실관리</a></li>
            <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
            <li class="divider"></li>
            <li><a href="${path}/kdk/book/book/book_insert.do">도서등록</a></li>
            <li><a href="${path}/ym/adminbook/list.do">대출도서목록</a></li>
            <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
            <li><a href="${path}/ym/pdf/pdf.do">대출도서PDF생성</a></li>
            <li><a href="${path}/ym/pdf/downloadPDF">대출도서PDF보기</a></li>
            <li><a href="${path}/excelDownload">대출도서excel</a></li>
            <li class="divider"></li>
            <li><a href="${path}/chart/book_list2.do">사용자별 대출 통계현황</a></li>
            <li><a href="${path}/chart/book_list3.do">대분류별 대출 통계현황</a></li>
            <li><a href="${path}/chart/urm_list.do">사용자별 열람실 이용현황</a></li>
          </ul>
        </li>
       </c:if>
      
      </ul>
      
      
      
      <form class="navbar-form navbar-left" role="search"
      method="post" action="${path}/hjh/book/list.do">
        <div class="form-group">
          <input type="text" name="keyword" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">도서 빠른검색</button>
      </form>
      
      
      <!-- 로그인 -->
      <ul class="nav navbar-nav navbar-right">
      <c:if test="${sessionScope.userid == null && sessionScope.adminid == null}">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;"><b>user</b></span></a>
			<ul id="login-dp" class="dropdown-menu">
				<li>
					 <div class="row">
							<div class="col-md-12">
								
								 <form class="form" role="form" method="post" action="${path}/pu/login/user/login_check.do" accept-charset="UTF-8" id="login-nav">
										<div class="form-group">
											 <label class="sr-only" for="exampleInputEmail2">Email address</label>
											 <input type="text" class="form-control" id="id" 
											 name="userid" placeholder="아이디" required>
										</div>
										<div class="form-group">
											 <label class="sr-only" for="exampleInputPassword2">Password</label>
											 <input type="password" class="form-control" id="pw" placeholder="비밀번호" 
											 name="passwd" required>
                                             <div class="help-block text-right"><a href="#">계정을 잊으셨나요?</a></div>
										</div>
										<div class="form-group">
											 <button type="submit" class="btn btn-primary btn-block">로그인</button>
										</div>
										<div class="checkbox">
											 <label>
											 <input type="checkbox"> 로그인 상태 유지
											 </label>
										</div>
								 </form>
							</div>
							<div class="bottom text-center">
								회원이 아니십니까?<a href="${path}/project/email/agree.do"><b> 가입하기</b></a>
							</div>
					 </div>
				</li>
				
				
			</ul>
			
        </li>
        </c:if>
        
        
        
        <!-- <li>
          <a href="#" class="dropdown-toggle" ><span style="color: white;"><b>|</b></span></a>
        </li> -->
        
        
        <!-- 관리자용 -->
         <c:if test="${sessionScope.userid == null && sessionScope.adminid == null}">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;"><b>admin</b></span></a>
			<ul id="login-dp" class="dropdown-menu">
				<li>
					 <div class="row">
							<div class="col-md-12">
								
								 <form class="form" role="form" method="post" action="${path}/pu/login/admin/login_check.do" accept-charset="UTF-8" id="login-nav">
										<div class="form-group">
											 <input type="text" class="form-control" 
											 name="userid" placeholder="아이디" required>
										</div>
										<div class="form-group">
											 <input type="password" class="form-control" placeholder="비밀번호" 
											 name="passwd" required>
										</div>
										<div class="form-group">
											 <button type="submit" class="btn btn-primary btn-block">로그인</button>
										</div>
										<div class="checkbox">
											 <label>
											 <input type="checkbox"> 로그인 상태 유지
											 </label>
										</div>
								 </form>
							</div>
							
					 </div>
				</li>
			</ul>
			
        </li>
        </c:if>
        
        
        
        <!-- 로그아웃 -->
        <c:if test="${sessionScope.userid != null && sessionScope.adminid == null}">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;"><b>마이페이지</b></span></a>
			<ul id="login-dp" class="dropdown-menu">
				<li>
					 <div class="row">
						<div class="col-md-12">
							<form class="form" role="form" method="post" action="${path}/pu/login/user/logout.do" accept-charset="UTF-8" id="login-nav">
								<div class="form-group">
								<label class="sr-only" for="exampleInputPassword2">Password</label>
								<span style="font-size: 17px;"> <b>${sessionScope.name} 님</b> </span> &nbsp;<a href="${path}/pu/page/list.do"> 내정보</a>
                                	<div class="help-block text-right">대출중 
                                		<a href="#" style="border-right-width: 10px;margin-right: 85px;">${rentcount}권</a>
                                		<button type="submit">로그아웃</button>
                                    </div>
								</div>
								</form>
							<div class="bottom text-center" style="padding-top: 10px;padding-bottom: 10px;padding-left: 5px;padding-right: 5px;">
								<a href="${path}/hjh/mypage/nowrent.do">대출현황/이력 &nbsp;</a> | <a href="${path}/kdk/book/cart/list.do"> &nbsp;관심도서조회</a>
							</div>
						</div>
					 </div>
				</li>
			</ul>
        </li>
       </c:if>
       
<!-- 관리자 로그아웃 -->
<c:if test="${sessionScope.adminid != null && sessionScope.userid == null}">
<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="color: white;"><b>마이페이지</b></span></a>
			<ul id="login-dp" class="dropdown-menu">
				<li>
					 <div class="row">
						<div class="col-md-12">
							<form class="form" role="form" method="post" action="${path}/pu/login/user/logout.do" accept-charset="UTF-8" id="login-nav">
								<div class="form-group">
								<label class="sr-only" for="exampleInputPassword2">Password</label>
								<span style="font-size: 17px;"> <b>${sessionScope.name} 님</b> </span>
                                	<div class="help-block text-right">
                                		<a href="#" style="border-right-width: 10px;margin-right: 85px;"></a>
                                		<button type="submit">로그아웃</button>
                                    </div>
								</div>
								</form>
							<div class="bottom text-center" style="padding-top: 10px;padding-bottom: 10px;padding-left: 5px;padding-right: 5px;">
								<a href="${path}/ym/membership/list.do">관리자 페이지로 바로 가기</a>
							</div>
						</div>
					 </div>
				</li>
			</ul>
        </li>
</c:if>      
       
       
       
        <!-- 로그인 끝 -->
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div style="margin-left: 150px; margin-right: 150px;"> 
<hr style="border-top-width: 2px;">