<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>인사말</title>
<%@ include file="../../include/header.jsp" %>
</head>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/cowdog.jsp"%>

<div class="container" style="height: 120%; margin-bottom: 50px;">
<div class="row">
<div class="col-md-8">
 
<h2>"미래 도서관에 오신것을 환영합니다."</h2>
<br>
미래 도서관은 미래능력개발교육원 여러분들의 지적,문화적 수준 향상과 독서문화 인프라 구축을 위해<br>
2018년 3월 16일 금요일 개관하였습니다.<br><br>

미래 도서관은 미래능력개발교육원과 함께하는 도서관, 공감하는 도서관, 소통하는 도서관을 만들어 나가고자 합니다.<br>

또한 미래 도서관은 지식정보화시대의 최신정보전달과 학생들의 독서진흥을 위해<br>
미래도서관을 주민 모두가 쉽게 이용할 수 있도록 열람실, 자료실 등을 <br>
최적의 정보공간으로 운영하고 있습니다.<br><br>

미래 능력개발교육원의 핵심 정보센터로서 모든 종류의 지식과 정보를 학생들이 <br>
쉽게 이용할 수 있도록 하고,<br>
학생문화역량 강화, 취미활동 지원을 위해 노력하겠습니다.<br><br>

또한 독서활동, 문화활동, 평생학습의 기회를 제공하여 지식의 
향상과 문화적 발전에 기여하기 위해 최선을 다하겠습니다.<br><br>
<br><br>
<span style="font-size: 18px;">미래재단 이사장  허준희</span><br><br>
<img alt="" src="${path}/images/junhee.png" style="width: 160px; height: 200px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img alt="" src="${path}/images/esajang.jpg" style="width: 360px; height: 100px;">


</div>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>