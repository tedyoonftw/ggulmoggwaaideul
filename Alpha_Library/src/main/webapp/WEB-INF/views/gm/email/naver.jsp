<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<script>
	$(function(){
		$("#btnSignup").click(function(){
			location.href="${path}/project/email/confirm.do";
		});
	});
</script>
<body>
<%@ include file="../../include/menu.jsp" %>
<%@ include file="../../include/menu1.jsp" %>
<%@ include file="../../include/sidebar/twodragon.jsp" %>
<input type="hidden" id="receiveMail" value="${receiveMail}">
<div align="center">
<span style="font-size: 38px; color: black;">인증되었습니다.</span>
</div>
<%@ include file="../../include/footer.jsp" %>
</body>
</html>