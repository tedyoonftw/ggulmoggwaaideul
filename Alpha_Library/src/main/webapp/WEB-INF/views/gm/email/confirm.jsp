
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<script>
	$(function(){
		$("#btnSignup").click(function(){
			location.href="${path}/pu/login/user/signup.do";
		});
		
		$("#btnConfirm").click(function(){
			var receiveMail= $("#receiveMail").val();
			location.href="${path}/project/email/mailCheck.do?receiveMail="+receiveMail;
		});
	});
</script>
<body>
<%@ include file="../../include/menu.jsp" %>
<%@ include file="../../include/menu1.jsp" %>
<%@ include file="../../include/sidebar/twodragon.jsp" %>
<input type="hidden" id="receiveMail" value="${receiveMail}">
<c:if test="${confirm == 'N' }">
<script>
	alert("이메일 인증을 해주세요");
</script>
<div align="center">
<span style="font-size: 38px; color: black;">승인중입니다.</span>
<div align="right">
<button type="button" id="btnConfirm">인증확인</button>
</div>
</div>
</c:if>
<c:if test="${confirm == 'Y' }">
<script>
	alert("이메일 인증 승인 완료");
</script>
<div align="center">
<span style="font-size: 38px; color: green;">이메일 본인 인증 승인완료</span>
</div>
<div align="right">
<button type="button" id="btnSignup">회원가입하러가기</button>
</div>
</c:if>
<%@ include file="../../include/footer.jsp" %>
</body>
</html>