<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<script>
	$(function(){
		$("#btnConfirm").click(function(){
			var receiveMail= $("#receiveMail").val();
			location.href="${path}/project/email/mailCheck.do?receiveMail="+receiveMail;
		});
	});
</script>
<body>
<%@ include file="../../include/menu.jsp" %>
<%@ include file="../../include/menu1.jsp" %>
<%@ include file="../../include/sidebar/twodragon.jsp" %>

<input type="hidden" id="receiveMail" value="${receiveMail}">
<div align="center">
<span style="font-size: 38px; color: black;">이메일 인증을 해주세요.</span>
</div>
<div align="right">
<button type="button" id="btnConfirm">인증확인</button>
</div>
<%@ include file="../../include/footer.jsp" %>
</body>
</html>