<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>열람실좌석현</title>

<link href="${path}/include/css/readingroom/seat.css" rel="stylesheet">
<script>
	function OpenWin(URL, width, height) {
		var str, width, height;
		str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
		str = str + "width=" + width;
		str = str + ",height=" + height + "',top=50,left=50";
		window.open(URL, 'remoteSchedule', str);
	}

	function view(str) {
		OpenWin("${path}/project/admin_readingroom/admin_sadragoninfo.do/" + str,
				480, 360);
	}

	function setClock() {
		location.href = "${path}/project/admin_readingroom/admin_list.do";
	}
	setInterval("setClock()", 30000);
</script>


</head>



<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 40px;">
                <span style="font-size: 20px; ">
                <br> 관리자</span></a></li>
                <li><a href="${path}/ym/membership/list.do"> 회원정보관리</a></li>
                <li><a href="${path}/project/admin_readingroom/bigcalview.do"> 일정관리</a></li>
                <li><a href="${path}/project/admin_readingroom/admin_list.do"> 열람실관리</a></li>
                <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
                <li><a href="${path}/hjh/book/book/book_insert.do">도서등록</a></li>
                <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
                <li><a href="${path}/chart/book_list2.do">도서차트1</a></li>
                <li><a href="${path}/chart/book_list3.do">도서차트2</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>


	<br>
	<table width="800px">
		<tr>
			<td align="center">
				<h2 style="padding-right: 80px;">좌석 현황</h2>
			</td>
		</tr>
		<tr>
		<td>
			<span style="color: red"><i class="fa fa-asterisk"></i></span><span style="font-size: 16px;">이페이지는 1분 마다 갱신됩니다</span>
		</td>
		</tr>
		<tr>
			<td align="right"><span style="font-size: 16px;">남은좌석: </span><span style="color: red; font-size: 18px;"> ${count}</span></td>
		</tr>
	</table>
	<form>
		<table id="seatTable">
			<tr>
				<td class="exit" colspan="7" align="center"><img
					src="${path}/images/readingroom/exit.png"></td>
			</tr>
			<tr>

				<c:forEach var="dto" items="${list}" begin="0" end="2">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<td rowspan="7" class="path"></td>


				<c:forEach var="dto" items="${list}" begin="12" end="14">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="dto" items="${list}" begin="3" end="5">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="15" end="17">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>

			<tr>
				<td colspan="7" class="path"></td>
			</tr>

			<tr>
				<c:forEach var="dto" items="${list}" begin="6" end="8">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="18" end="20">
				<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && dto.userid == sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="dto" items="${list}" begin="9" end="11">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="21" end="23">
					<td class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
					<c:if
							test="${dto.userid != null && sessionScope.adminid == 'admin' }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>

		</table>
	</form>



<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>





</body>
</html>