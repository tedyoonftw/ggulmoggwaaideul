<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>자료 현황</title>
<%@ include file = "../include/header.jsp" %>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
	google.load('visualization', '1', {
		'packages' : [ 'corechart' ]
	});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		var jsonData = $.ajax({
			url : "${path}/chart/book_group_list.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		var data = new google.visualization.DataTable(jsonData);
		var chart = new google.visualization.BarChart(document.getElementById("chart_div"));
		chart.draw(data, {
			title : "도서 통계",
			width : 600,
			height : 440
		});
	}
</script>
</head>


<body>

<%@ include file="../include/menu.jsp"%>
<%@ include file="../include/menu1.jsp"%>
<%@ include file="../include/sidebar/cowdog.jsp"%>






<div class="container">
<div class="row">
<div class="col-md-5">



	<div id="chart_div"></div>
	<div style="margin-left: 100px;">
	<button id="btn" type="button" onclick="drawChart()">refresh</button>
	</div>



</div>
</div>
</div>



<%@ include file="../include/footer.jsp"%>

</body>
</html>