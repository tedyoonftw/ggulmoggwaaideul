<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<body>
<%@ include file="../../include/menu.jsp" %>
<%@ include file="../include/mypagemenu.jsp" %>
<h2>관심도서목록</h2>
<table border="1" width="700">
	<tr>
		<th>번호</th>
		<th>도서명</th>
		<th>저자</th>
		<th>출판사</th>
		<th>청구기호</th>
		<th>자료상태</th>
	</tr>
<c:forEach var="row" items="${clist}">
	<tr>
		<td>${row.cart_id}</td>
		<td>${row.title}</td>
		<td>${row.author}</td>
		<td>${row.publisher}</td>               
		<td>${row.book_no}</td>
		<td>
		<c:choose>
			<c:when test="${row.book_tf == 0}">
				<span style="color:red;">대출중(대출불가)</span>
			</c:when>
			<c:otherwise>
				<span style="color:green;">대출가능</span>
			</c:otherwise>
		</c:choose>
		</td>
</c:forEach>

</table>

</body>
</html>