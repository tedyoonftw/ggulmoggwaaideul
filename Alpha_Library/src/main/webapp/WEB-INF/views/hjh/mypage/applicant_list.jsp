<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>미래 도서관</title>
<%@ include file="../../include/header.jsp"%>
<script>
$(function(){
	
	/* $("#btnDelete").click(function(){
		if(confirm("취소하시겠습니까?")){
			var ano=("#ano").val();
			document.form1.action="${path}/applicant/delete.do?ano"+ano;
			document.form1.submit();
		}
	}); */
});
function delete1(ano) {
	if(confirm("취소하시겠습니까?")){
		location.href="${path}/applicant/delete.do?ano="+ano;
	}
}
</script>
</head>
<body>
	<%@ include file="../../include/menu.jsp"%>
	<%@ include file="../../include/menu1.jsp"%>
	<%@ include file="../../include/sidebar/mypage.jsp"%>
	
	<h2>문화프로그램 신청목록</h2>
	
	
	
<form name="form1" id="form1" method="post">
	<table border="1" width="600px">
		<tr>
			<th>번호</th>
			<th>제목</th>
			<th>신청인</th>
			<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
		<c:forEach var="row" items="${list}">
			<tr>
				<td>${row.ano}</td>
				<td>${row.title}</td>
				<td>${row.username}</td>
				<input type="hidden" name="ano"  value="${row.ano}">
				<td><a href="#" onclick="delete1('${row.ano}')">신청취소</a></td>
				<%-- onclick="window.location.href='${path}/applicant/delete.do?ano=${row.ano}';" --%>
			</tr>
		</c:forEach>
	</table>
</form>	
	
	<%@ include file="../../include/footer.jsp"%>
</body>
</html>