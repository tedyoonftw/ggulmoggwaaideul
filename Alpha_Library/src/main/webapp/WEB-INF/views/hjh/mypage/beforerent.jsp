<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/mypage.jsp"%>




<h2>대출이력조회</h2>
<table border="1" width="700">
	<tr>
		<th>번호</th>
		<th>도서명</th>
		<th>저자</th>
		<th>대출일</th>
		<th>반납일</th>
		<th>연체 여부 </th>
		<th>대출불가 기간 </th>
	</tr>
<c:forEach var="row" items="${blist}">
	<tr>
		<td>${row.rent_code}</td>
		<td>${row.title}</td>
		<td>${row.author}</td>               
		<td><fmt:formatDate value="${row.rent_date}"
			pattern="yyyy-MM-dd" /></td>
		<td>
		<c:choose>
		
		<c:when test="${ row.penalty == 'N'}">
		<fmt:formatDate value="${row.return_date}"
			pattern="yyyy-MM-dd" />
		</c:when>
		
		<c:otherwise>
			${row.delay_gigan }
		</c:otherwise>
			
		</c:choose>
		</td>
		<td><c:if test="${row.penalty == 'Y'}">
		<span style="color: red;">연체</span>
		</c:if></td>
		<td>${row.delay_gigan}</td>
</c:forEach>

</table>












<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>

<%@ include file="../../include/footer.jsp"%>

</body>
</html>