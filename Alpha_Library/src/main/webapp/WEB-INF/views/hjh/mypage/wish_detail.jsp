<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnUpdate").click(function(){
		var title=$("#title").val();
		var author=$("#author").val();
		var publisher=$("#publisher").val();
		var isbn=$("#isbn").val();
		if(title == "null"){
			alert("희망도서명을 입력하세요.")
			return;
		}
		if(author == "null"){
			alert("저자를 입력하세요.")
			return;
		}
		if(publisher == "null"){
			alert("출판사를 입력하세요.")
			return;
		}
		if(isbn == "null"){
			alert("isbn을 입력하세요.")
			return;
		}
		document.form1.action="${path}/hjh/mypage/wishupdate.do";
		document.form1.submit();
	});
});

$(function(){
	$("#btnReturn").click(function(){
		location.href="${path}/hjh/mypage/wishlist.do";
	});
});

$(function(){
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")){
			document.form1.action
				="${path}/hjh/mypage/wishdelete.do";
			document.form1.submit();
		}
	});
});
</script>
</head>
<body>
<%@ include file="../../include/menu.jsp" %>
<h2>희망도서신청내역</h2>

*필수입력
<form name="form1" method="post">
<table border="1" width="70%">
	<tr>
		<td>신청일자</td>
		<td><fmt:formatDate value="${dto.hpdate}"
			pattern="yyyy-MM-dd" /></td>
	</tr>
	<tr>
		<td>신청자*</td>
		<td><input name="userid" value="${dto.userid}" 
				readonly="readonly"></td>
	</tr>
	<tr>
		<td>희망도서명*</td>
		<td><input id="title" name="title"
			value="${dto.title}"></td>
		
	</tr>
	<tr>
		<td>저자*</td>
		<td><input id="author" name="author"
			value="${dto.author}"></td>
	</tr>
	<tr>
		<td>출판사*</td>
		<td><input id="publisher" name="publisher"
			value="${dto.publisher}"></td>
	</tr>
	<tr>
		<td>ISBN*</td>
		<td><input id="isbn" name="isbn"
			value="${dto.isbn}"></td>
	</tr>
	<tr>
		<td>출판년</td>
		<td><input name="pubyear"
			value="${dto.pubyear}"></td>
	</tr>
	<tr>	
		<td>추천의견(최대100자)</td>
		<td><textarea id="content" name="content"
			row="10" cols="80">${dto.content}</textarea></td>
</table>
<input type="hidden" name="hpnum" value="${dto.hpnum}">
<input type="button" id="btnUpdate" value="수정">
<input type="button" id="btnDelete" value="삭제">
<input type="button" id="btnReturn" value="확인">
</form>
</body>
</html>