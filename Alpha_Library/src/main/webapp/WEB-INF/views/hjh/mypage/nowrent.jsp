<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>

<script >


function smoke(rentno){
	
	if(confirm("대출을 연장하시겠습니까?")){
		location.href = "${path}/book/rent/smoke.do/"+rentno;			
		}
	
}
function deletere(idx){
	if(confirm("반납 하시겠습니까?")){
		location.href = "${path}/book/rent/halfnab.do/"+idx;			
		}
	
}

</script>
</head>


<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/mypage.jsp"%>


        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>




<h2>대출현황관리</h2> 
<span style="color: red; font-size: 20px;">${rentcount}권 대출중</span>
<br>

<table border="1" width="700">
	<tr>
		<th>도서명</th>
		<th>저자</th>
		<th>대출일</th>
		<th>반납예정일</th>
		<th>상태</th>
		<th>선택</th>
	</tr>
<c:forEach var="row" items="${nowlist}">
	<tr>
		<td>${row.title}</td>
		<td>${row.author}</td>               
		<td><fmt:formatDate value="${row.rent_date}"
			pattern="yyyy-MM-dd" /></td>
		<td><fmt:formatDate value="${row.return_date}"
			pattern="yyyy-MM-dd" /></td>
		<td>
			<c:if test="${row.book_tf == 0}">
				대출중
			</c:if>
		</td>
		<td><input type="button" name="post" id="btnPost" 
			value="반납연기" onclick="smoke('${row.rent_code}')">
				<input type="button" name="post" id="btnPost" 
			value="반납" onclick="deletere('${row.rent_code}')">
		</td>
</c:forEach>

</table>











<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>