<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/mypage.jsp"%>



        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>


<h2>희망도서신청목록</h2>
<table border="1" width="700">
	<tr>
		<th>번호</th>
		<th>신청일자</th>
		<th>신청도서명</th>
		<th>처리상태</th>
	</tr>
<c:forEach var="row" items="${wlist}">
	<tr>
		<td>${row.hpnum}</td>
		<td><fmt:formatDate value="${row.hpdate}"
			pattern="yyyy-MM-dd" /></td>
		<td><a href="${path}/hjh/mypage/detail/${row.hpnum}">
				${row.title}</a></td>               
		<td>
		<c:choose>
			<c:when test="${row.wstatus == 0}">
				<span style="color:red;">처리중</span>
			</c:when>
			<c:otherwise>
				<span style="color:green;">처리완료</span>
			</c:otherwise>
		</c:choose>
		</td>
</c:forEach>

</table>












<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


<%@ include file="../../include/footer.jsp"%>

</body>
</html>