<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 800px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 30px;">
                <span style="font-size: 20px; ">
                <br> 자료 검색</span></a></li>                
                <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
            	<li><br></li>
            	<li><br></li>
            	<c:if test = "${sessionScope.userid != null }">
 				<li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>최근</b></span><span style="font-size: 20px;"> 검색어</span> </li>
                <c:forEach var="keyworddto" items="${keywords}">
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i> ${keyworddto.keyword}</li>
                </c:forEach>
            	<li><br></li>
            	</c:if>
            	<li><br></li>
                <li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>인기</b></span><span style="font-size: 20px;"> 검색어</span></li>
                <c:forEach var = "ingikeywords" items = "${ingikeywords}">
                <c:if test="${ingikeywords.keyword != '' }">                
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i>${ingikeywords.keyword}</li>
                </c:if>
                </c:forEach>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
  
                
         

<div class="container">
<div class="row">
<div class="col-md-8">





<h2> 간략 검색</h2>
<hr>
<div class="container">
<div class="row">
<div class="col-md-8" style="padding-left: 0px;">
                    
                    
                    <form class="form"
                    name="form1" method="post" 
	action="${path}/hjh/book/list.do">
                        <div class="form-group">
                            <div class="input-group">
                            
                                <span class="input-group-addon">자료검색 </span>
                                <select name="search_option" class="form-control" aria-describedby="month-addon">
                                    <option value="all">전체</option>                            
                                    <option value="title">제목</option>                            
                                    <option value="author">저자</option>
									<option value="publisher">출판사</option>                            
                                </select>
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon"></span>
                                <input name="keyword" class="form-control" aria-describedby="month-addon">
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon"></span>
                               	<input type="submit" value="검색" name="control-month" style="background-color: #428bca; color: white;"
                               	class="form-control" aria-describedby="month-addon">
                           
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
            </div>
        </div>	
        
<div class="container">
<div class="row">
<div class="col-md-12"
style="padding-left: 0px; padding-bottom: 15px;">
        		<c:if test = "${sessionScope.userid != null }">
				<span style=" color: red;">최근 본 자료 : &nbsp;</span>
				<c:forEach var="titleKeyword" items="${titleKeyword}">
      			<c:set var="TitleKeyword" value="${titleKeyword.title}" />
       			<a href="${path}/kdk/book/book/detail/${titleKeyword.book_code}">
       			<span style="margin-right: 20px;"><i class="fa fa-tags"></i>
       			${fn:substring(TitleKeyword, 0, 7)}...</span>
       			</a>
      			</c:forEach>
				</c:if>
</div>                
</div>                
</div>     












</div>
</div>
</div>
<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>