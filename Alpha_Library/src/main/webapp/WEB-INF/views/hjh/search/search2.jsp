<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnInitialize").click(function(){
		$("#title").val("");
		$("#isbn").val("");
		$("#author").val("");
		$("#publisher").val("");
		$("#date1").val("");
		$("#date2").val("");
		$("#order_option").val("title");
	});
	$("#btnSearch").click(function(){
		var date1=$("#date1").val();
		var date2=$("#date2").val();
		if(date1 != ""){
			if(date2 == ""){
				alert("발행년도를 입력하세요.");
				return;
			}
		}
		document.form1.action="${path}/hjh/book/list2.do";
		document.form1.submit();
	});
});
</script>
</head>
<!-- 추가부분 -->
<!-- 
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
 -->
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<!-- 책테이블 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}


/* 테이블 */
.art-title{color:#231f20; font-size:20px; font-weight:700;}
.artist-data{width:100%; padding-bottom: 25px;}
.artst-pic{width:33%;position: relative;}
.artst-pic span a{color: #fff; font-size: 16px; display: none;}
.artst-pic span.artst-like{position: absolute; left: 11%; bottom: 10px;}
.artst-pic span.artst-share{position: absolute; left:46%; bottom: 10px;}
.artst-pic span.artst-plus{position: absolute; right: 9%; bottom: 10px;}
.artst-prfle{width:63%;}
.artst-prfle span.artst-sub{font-size:15px; color:#bbb; float:left; width:100%; font-weight:normal; padding:5px 0;}
.artst-prfle span.artst-sub span.byname{font-weight:700; color:#aaa;}
.artst-prfle span.artst-sub span.daysago{float:right; font-size:12px;}
.counter-tab{float: left; width: 100%; padding-top: 45px;}
.counter-tab div{float: left; width: 33%; color: #aaa; font-size: 12px;}
.bot-links{float: left; width: 100%; padding-top: 10px;}
.bot-links a{display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;}
span.play-icon{position: absolute; left: 31%; top: 32%; display: none;}
.artst-pic:hover img.play-icon, .artst-pic:hover span a{display: block; } 

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 800px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 30px;">
                <span style="font-size: 20px; ">
                <br> 자료 검색</span></a></li>                
                <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
            	<li><br></li>
            	<li><br></li>
            	<c:if test = "${sessionScope.userid != null }">
 				<li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>최근</b></span><span style="font-size: 20px;"> 검색어</span> </li>
                <c:forEach var="keyworddto" items="${keywords}">
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i> ${keyworddto.keyword}</li>
                </c:forEach>
            	<li><br></li>
            	</c:if>
            	<li><br></li>
                <li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>인기</b></span><span style="font-size: 20px;"> 검색어</span></li>
                <c:forEach var = "ingikeywords" items = "${ingikeywords}">
                <c:if test="${ingikeywords.keyword != '' }">                
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i>${ingikeywords.keyword}</li>
                </c:if>
                </c:forEach>
            </ul>
        </div>




      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
  



<form name="form1" method="post">
<table border="1">
	<tr>
		<td>서명</td>
		<td><input name="title" id="title"></td>
		<td>ISBN</td>
		<td><input name="isbn" id="isbn"></td>
	</tr>
	<tr>
		<td>저자</td>
		<td><input name="author" id="author"></td>
		<td>출판사</td>
		<td><input name="publisher" id="publisher"></td>
	</tr>
	<tr>
		<td>정렬조건</td>
		<td><select name="order_option" id="order_option">
				<option value="title">제목</option>
				<option value="author">저자</option>
				<option value="publisher">출판사</option>
				<option value="pubyear">출판연도</option>
			</select> </td>
		<td>발행연도</td>
		<td><input name="date1" size="5" id="date1"> 부터 
			<input name="date2" size="5" id="date2"> 까지 </td>
</table>
<input type="button" id="btnSearch" value="상세검색">
<input type="button" id="btnInitialize" value="초기화">
	
</form>










<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>



<%@ include file="../../include/footer.jsp"%>








</body>
</html>