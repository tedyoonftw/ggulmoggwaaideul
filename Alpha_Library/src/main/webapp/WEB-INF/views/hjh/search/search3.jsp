<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnSearch").click(function(){
		var prd=$("#period").val();
		if( prd == "null"){
			alert("기간을 선택하세요.");
			return;
		}
		document.form1.action="${path}/hjh/book/list3.do";
		document.form1.submit();
		}
	);
});
</script>
</head>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 800px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 30px;">
                <span style="font-size: 20px; ">
                <br> 자료 검색</span></a></li>                
                <li><a href="${path}/hjh/book/search.do">간략검색</a></li>
                <li><a href="${path}/hjh/book/search2.do">상세검색</a></li>
                <li><a href="${path}/hjh/book/search3.do">신착자료검색</a></li>
                <li><a href="${path}/hjh/book/list.do">도서 리스트</a></li>
            	<li><br></li>
            	<li><br></li>
            	<c:if test = "${sessionScope.userid != null }">
 				<li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>최근</b></span><span style="font-size: 20px;"> 검색어</span> </li>
                <c:forEach var="keyworddto" items="${keywords}">
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i> ${keyworddto.keyword}</li>
                </c:forEach>
            	<li><br></li>
            	</c:if>
            	<li><br></li>
                <li><span style="font-size: 24px; color: blue; border-bottom: solid;"><b>인기</b></span><span style="font-size: 20px;"> 검색어</span></li>
                <c:forEach var = "ingikeywords" items = "${ingikeywords}">
                <c:if test="${ingikeywords.keyword != '' }">                
                <li style="margin-left: 5px; margin-top: 5px;"><i class="fa fa-check-circle-o"></i>${ingikeywords.keyword}</li>
                </c:if>
                </c:forEach>
            </ul>
        </div>




      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 



<form name="form1" method="post">
<select>
	<option selected>미래도서관</option>
</select>

<select name="shleflocname">
	<option selected>자료실</option>
	<option value="all">전체</option>
</select>

<select name="group_code">
	<option value="all">전체분류</option>
	<option value="000">기타</option>
	<option value="100">종교</option>
	<option value="200">사회과학</option>
	<option value="300">자연과학</option>
	<option value="400">기술과학</option>
	<option value="500">예술</option>
	<option value="600">언어</option>
	<option value="700">문학</option>
	<option value="800">역사</option>
	<option value="900">철학</option>
</select>

<select name="period" id="period">
	<option value="null" selected>기간선택</option>
	<option value="1week">1주 이내</option>
	<option value="2week">2주 이내</option>
	<option value="3week">3주 이내</option>
	<option value="1month">30일 이내</option>
</select>

<input type="button" id="btnSearch" value="검색">	
</form>






<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>