<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${path}/include/js/datepicker/datepicker-ko.js"></script>
<script src="${path}/include/js/datepicker/date.js"></script>

</head>
<body>
<h2>도서위치정보</h2>
<form name="form1" id="form1" method="post">
<h4>${bookdto.title}</h4>
<table border="1" width="70%">
	<tr>
		<td>ISBN</td>
		<td>${bookdto.isbn}</td>
	</tr>
	<tr>
		<td>저자</td>
		<td>${bookdto.author}</td>
	</tr>
	<tr>
		<td>출판사</td>
		<td>${bookdto.publisher}</td>
	</tr>
</table>
<h4>도서 분류</h4>
<table border="1" width="70%">
	<tr>
		<td>총류 : ${bookdto.group_name}</td>
	</tr>
	<tr>
		<td>서가번호 : ${bookdto.book_no}</td>
	</tr>
</table>

<h4>도서 위치 (■ : 색칠된 부분)</h4>
<div style="width: 600px">
<img src="${path}/images/${bookdto.witchimg}" />
</div>
</form>
</body>
</html>