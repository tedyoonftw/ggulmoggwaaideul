<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>정보마당:묻고답하기</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnWrite").click(function(){
		location.href="${path}/qna/write.do";
	});
	
});
function list(page) {
	location.href="${path}/qna/list.do?curPage="+page;
}
</script>
</head>


<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<script src="${path}/include/js/jquery-3.2.1.min.js"></script>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 



<%-- <c:if test="${sessionScope.adminid=='admin' }">
<%@ include file="../../include/admin_menu.jsp" %>

</c:if>
<c:if test="${sessionScope.userid!=null }">
<%@ include file="../../include/menu.jsp" %>

</c:if>
<c:if test="${sessionScope.userid==null }">
<%@ include file="../../include/menu.jsp" %>

</c:if> --%>





<!-- 테이블 -->
<div class="container" style="width: 900px;height: 0px;">

	<div class="row">
        <div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
        <div class="table-responsive">
        
        
	<h2>질문 게시판</h2>
<hr>


       <form class="form" name="form1" method="post" 
       																action="${path}/qna/list.do">
       <div class="form-group">
          <div class="input-group">

		<span class="input-group-addon" id="month-addon">자료검색 </span>
		
         <select name="search_option" class="form-control" aria-describedby="month-addon">
        		                            
    		<option value="title"
				<c:if test="${map.search_option == 'title'}">selected</c:if>>제목</option>
			<option value="content"
				<c:if test="${map.search_option == 'content'}">selected</c:if>>내용</option>
			<option value="all"
				<c:if test="${map.search_option == 'all' }">selected</c:if>>제목+내용</option>                    
        </select>


	<span style="background-color: white; border-top-color: white; border-bottom-color: white;
       padding-right: 0; padding-left: 0;" 
       class="input-group-addon" id="month-addon"></span>
    <input name="keyword" value="${map.keyword}" class="form-control" aria-describedby="month-addon">
	
	<span style="background-color: white; border-top-color: white; border-bottom-color: white;
     padding-right: 0; padding-left: 0;" 
     class="input-group-addon" id="month-addon"></span>
    <input type="submit" value="검색" name="control-month" style="background-color: #428bca; color: white;"
     class="form-control" aria-describedby="month-addon">
	
		</div>
	  </div>
    </form>
	
	
	
	

























              <table id="mytable" class="table table-bordred table-striped" >
              <thead>
                   <th style="padding-right: 0px;padding-left: 10px;">번호</th>
                   <th style="padding-right: 0px;padding-left: 10px;">제목</th>
                   <th style="padding-right: 0px;padding-left: 10px;">작성자</th>
                   <th style="padding-right: 0px;padding-left: 10px;">등록일</th>
                   <th style="padding-right: 0px;padding-left: 10px;">처리상태</th>
              </thead>
 <c:forEach var="row" items="${map.list}"> 
<tbody>
<tr>
    <td style="padding-right: 0px;padding-left: 10px;">${row.qno}</td>
    <td style="padding-right: 0px;padding-left: 10px;"><a href="${path}/qna/view.do?qno=${row.qno}">${row.title}
			</a></td>
    <td style="padding-right: 0px;padding-left: 10px;">${row.username}${row.adminname}</td>
	<td style="padding-right: 0px;padding-left: 10px;"><fmt:formatDate value="${row.regdate}"
			pattern="yyyy-MM-dd" /></td>
			<c:if test="${row.check1 == 'Y' }">
	<td style="padding-right: 0px;padding-left: 10px;">답변대기</td>
			</c:if>
			<c:if test="${row.check1 == 'N' }">
			<c:if test="${row.cnt > 0}">
	<td style="padding-right: 0px;padding-left: 10px;">답변완료</td>
			</c:if>
		</c:if>
</tr> 
    </tbody>
</c:forEach>
	
			</table>
			
			
			
			<div align="right" style="margin-top: 30px;">
   				<button type="button" id="btnWrite" class="btn btn-default" 
   				style="width: 100px; background-color: #428bca; color: white;">글쓰기</button>
			</div>
			
			
<div align="center">
<ul class="pagination pull-center">
<%-- <c:if test="${map.pager.curPage > 1}">
<li><a href="#" onclick="list('1')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
</c:if> --%>
<%-- <c:if test="${map.pager.curBlock > 1}"> --%>
  <li><a href="#" onclick="list('${map.pager.prevPage}')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
<%-- </c:if>  --%>
  <c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
  						<li class="active"><a href="#" onclick="list('${num}')">${num}</a></li>
 					 </c:when>
  					<c:otherwise>
 						 <li><a href="#" onclick="list('${num}')">${num}</a></li>
  					</c:otherwise>
 				</c:choose>
 </c:forEach>
  <!-- <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li> -->
<%-- <c:if test="${map.pager.curBlock < map.pager.totBlock}"> --%>
  <li><a href="#" onclick="list('${map.pager.nextPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
<%-- </c:if> --%>
<%-- <c:if test="${map.pager.curPage < map.pager.totPage}">
<li><a href="#" onclick="list('${map.pager.totPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></a></li>
</c:if> --%>
</ul>
</div>
<!-- 				페이지 나누기 끝 				-->

</div>
</div>
</div>
<!-- 			테이블 끝		 		-->  














<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>
</div>

<%@ include file="../../include/footer.jsp"%>

</body>
</html>