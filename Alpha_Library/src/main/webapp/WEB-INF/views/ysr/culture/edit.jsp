<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<!-- include summernote css/js -->
<link href="${path}/summernote/summernote.css" rel="stylesheet">
<script src="${path}/summernote/summernote.js"></script>
<script>
$(function(){
	$("#content").summernote({
		height: 300,
	    weight: 1200
	});
	$("#btnList").click(function(){
		location.href="${path}/culture/list.do";
	});
	$("#btnUpdate").click(function(){
		document.form1.action="${path}/culture/update.do";
		document.form1.submit();
	});
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")) {
			document.form1.action="${path}/culture/delete.do";
			document.form1.submit();
		}
	});
});
</script>
</head>
<style type="text/css">
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>
</head>
<body>
<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>

      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

<c:if test="${sessionScope.userid!=null  }">
<%@ include file="../../include/menu.jsp" %>
</c:if>
<c:if test="${sessionScope.userid==null && sessionScope.adminid==null}">
<%@ include file="../../include/menu.jsp" %>
</c:if>

<h2>문화프로그램 정보 수정</h2>

<!--					 테이블 					-->
<div class="container" style="width: 900px;height: 0px;">
<div class="row">
<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
<div class="table-responsive">



<form name="form1" method="post">
<table width="800px">
	<tr>
		<td>강좌명</td>
		<td colspan="3"><input name="title" id="title"
			value="${dto.title}"></td>
	</tr>
	<tr>
		<td>마감기간</td>
		<td>
			<%-- <input name="duedate" id="duedate" 
			value="<fmt:formatDate pattern="yyyy-MM-dd" 
			value="${dto.duedate}"/>"> --%>
			<input name="duedate" id="duedate" value="${dto.duedate}">
		</td>
		<td>정원</td>
		<td><input name="max" id="max"
			value="${dto.max}"></td>
	</tr>
	<tr>
		<td>강좌일시</td>
		<td><%-- <input name="sdate" id="sdate" 
			value="<fmt:formatDate pattern="yyyy-MM-dd" 
			value="${dto.sdate}"/>"> --%>
			<input name ="sdate" id="sdate" value="${dto.sdate }">
		</td>
		<td>참가비</td>
		<td><input name="fee" id="fee" value="${dto.fee}"></td>
	</tr>
	<tr>
		<td>대상</td>
		<td><input name="target" id="target" value="${dto.target}"></td>
		<td>장소</td>
		<td><input name="place" id="place" value="${dto.place}"></td>
	</tr>
	<tr>
		<td colspan="4"><textarea id="content" name="content"
				rows="20" cols="80">${dto.content}</textarea>
		</td>
	</tr>
</table>
<div style="width:700px; text-align:center;">
		<input type="hidden" name="cno" value="${dto.cno}">
		<c:if test="${sessionScope.adminid == 'admin'}">
			<button type="button" id="btnUpdate">수정</button>
			<button type="button" id="btnDelete">삭제</button>
		</c:if>
		<button type="button" id="btnList">목록</button>
	</div>
</form>



	  
</div>
</div>
</div>
</div>
<!-- 					테이블 끝		 				-->  


	
<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>

	


</body>
</html>