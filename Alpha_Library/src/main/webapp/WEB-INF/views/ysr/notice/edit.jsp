<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script src="${path}/include/js/common.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<!-- include summernote css/js -->
<link href="${path}/summernote/summernote.css" rel="stylesheet">
<script src="${path}/summernote/summernote.js"></script>
<script>
$(function(){
	$("#content").summernote({
		height: 300,
	    weight: 800
	});
	$("#btnList").click(function(){
		location.href="${path}/notice/list.do";
	});
	$("#btnUpdate").click(function(){
		var str="";
		$("uplodedList .file").each(function(i){
			str+=
				"<input type='hidden' name='files["+i+"]' value='"
				+$(this).val()+"'>";
		});
		$("#form1").append(str);
		document.form1.action="${path}/notice/update.do";
		document.form1.submit();
	});
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")) {
			document.form1.action="${path}/notice/delete.do";
			document.form1.submit();
		}
	});
	
	listFile();
	
	$("#uploadedList").on("click",".file_del",function(e){
		var that=$(this);
		$.ajax({
			type: "post",
			url: "${path}/upload/deleteFile",
			data: {fileName: $(this).attr("data-src")},
			dataType: "text",
			success: function(result){
				if(result=="deleted"){
					that.parent("div").remove();
				}
			}
			
		});
	});
});
function listFile(){
	$.ajax({
		type: "post",
		url: "${path}/notice/getFile/${dto.nno}",
		success: function(list){
			$(list).each(function(){
				var fileInfo=getFileInfo(this);
				var html="<div><a href='"+fileInfo.getLink+"'>"
					+fileInfo.fileName+"</a>&nbsp;&nbsp;";
				<c:if test="${sessionScope.adinid == dto.adminid}">
				html+="<a href='#' class='file_del' data-src='"
					+this+"'>[삭제]</a></div>";
				</c:if>
				$("#uploadedList").append(html);
			});
		}
	});
}
</script>

<!-- <style>
.fileDrop {
	width: 600px;
	height: 100px;
	border: 1px dotted black;
	background-color: white;
}
</style> -->

<style type="text/css">

/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>


</head>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

<div class="container">
<div class="row">
 <div class="col-md-4">

<!----------------------------------------		사이드 오른쪽		------------------------------------------>

<div class="container">
<div class="row">
	
<c:if test="${sessionScope.userid!=null  }">
<%@ include file="../../include/menu.jsp" %>
</c:if>
<c:if test="${sessionScope.userid==null && sessionScope.adminid==null}">
<%@ include file="../../include/menu.jsp" %>
</c:if>

<h2>공지사항</h2>
<hr>
<div class="well" style="width: 750px;">

<form name="form1"  id="form1" class="form-inline" role="form" method="post" >

	<div class="form-group"> 
		<label>제&nbsp;&nbsp;목&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</label></div> 
		<input name="title" id="title" size="20"
				value="${dto.title}"><br>
	<div class="form-group" style="margin-top:5px;"> 
		<label>작성자&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label></div> 
		<input name="adminid" id="adminid" size="20"
				value="${dto.adminid}" readonly style="margin-top:5px;"><br>
	<div class="form-group" style="margin-top:5px;"> 
		<label>조회수 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${dto.viewcnt}</label></div> <br>
	
	<div style="width:700px; margin-top:5px;" >
		<label>내&nbsp;&nbsp;용</label>  
		<textarea id="content" name="content"
				rows="20" cols="80" style="margin-top:5px;">${dto.content}</textarea>
	</div>
	<!-- <div> 첨부파일을 등록하세요
		<div class="fileDrop"></div>
		<div id="uploadedList"></div>
	</div> -->
	
	<div  class="form-group" style="width:700px; text-align:center; margin-bottom: 10px;">
		<input type="hidden" name="nno" value="${dto.nno}">
			<div align="center" style="margin-top: 10px;">
			
		<c:if test="${sessionScope.adminid == 'admin'}">
   					<button type="button" id="btnUpdate" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">수정</button>
   					<button type="button" id="btnDelete" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">삭제</button>
		</c:if>
					<button type="button" id="btnList" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">목록</button>
		</div>
	</div>
</form>


</div>
</div>
</div>
<!----------------------------------------		오른쪽 영역 끝		------------------------------------------>
</div>
</div>
</div>



<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>

</body>
</html>