<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- 세션을 사용하지 않는 옵션 -->
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en" style="width: 100%;">

<head>
<meta charset="utf-8">
<title>미래 도서관</title>

<%@ include file="include/header.jsp"%>

</head>


<!-- 사진 밑바닥 -->
 <style> 
        @import url("include/gm/style2.css") 
 </style> 


<style>
/* /* 추가 */
 {width: 100%;
 position: relative; display: inline-block;}
 */

/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

/* 영역나누기 */
@media ( min-width: 768px;) {
    .grid-divider {
        position: relative;
        padding: 0;
    }
    .grid-divider>[class*='col-'] {
        position: static;
    }
    .grid-divider>[class*='col-']:nth-child(n+2):before {
        content: "";
        border-left: 1px solid #DDD;
        position: absolute;
        top: 0;
        bottom: 0;
    }
    .col-padding {
        padding: 0 15px;
    }
}

/* 사진부분 */
.carousel {
    position: relative;
}

.controllers {
    position: absolute;
    top: 50px;
}

.carousel-caption {
left: 20%;
right: 20%;
padding-bottom: 30px;
}

.carousel-control.left, 
.carousel-control.right {
    background-image: none;
}

.carousel-caption {
position: relative;
left: 0;
right: 0;
bottom: 0;
z-index: 10;
padding-top: 0;
padding-bottom: 0;
color: #000;
text-align: center;
text-shadow: none;
text-shadow: 0 1px 2px rgba(0,0,0,.6);
}






/* 탭 */
.nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 15px 0; }
.tab-content{padding:20px}

.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
</style>





<!-- 하단부 사진 부분 -->
<script type="text/javascript">
//Carousel Auto-Cycle
$(document).ready(function() {
  $('.carousel').carousel({
    interval: 6000
  })
});
</script>

<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<!-- 사진 --> 
 <!-- style="margin-left: 150px; margin-right: 150px;" -->
<body>


<!-- 이거 추가 0314 -->
<c:if test ="${delay == 'yes'}">
<script>
window.open("${path}/book/rent/loadgo.do","알림","width=300,height=400,status=no,toolbar=no,resizable=no,location=no");
</script>
</c:if>


<%@ include file="include/menu.jsp"%>
<%@ include file="include/menu1.jsp"%>

<!-- 		사진 부분 		-->
			<!-- start slider -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="width: 100%;">

  <!-- Wrapper for slides -->
  <div 
  style="margin-bottom: 40px; "class="carousel-inner">
    <div class="item active">
      <div class="col-sm-12">
        <img class="img-responsive" src="${path}/images/index/1.jpg" alt="..."
        style="width: 100%;">
      </div>
      <div class="col-sm-11">
        <div class="carousel-caption">
            <!-- <h2>Slide 1</h2> -->
        </div>
      </div>
    </div>
    
    <div class="item">
      <div class="col-sm-12">
        <img class="img-responsive" src="${path}/images/index/S__5390340.jpg" alt="..."
        style="width: 100%;">
      </div>
      <div class="col-sm-12">
        <div class="carousel-caption">
            <!-- <h2>Slide 2</h2> -->
        </div>
      </div>
    </div>
    
    <div class="item">
      <div class="col-sm-12">
        <a href="${path}/culture/view.do?cno=1"> <img class="img-responsive" src="${path}/images/index/문화-사서와 함께 하는 독서교실(배너용).jpg" 
        alt="..."
        style="width: 100%;"></a>
      </div>
      <div class="col-sm-13">
        <div class="carousel-caption">
            <!-- <h2>Slide 2</h2> -->
        </div>
      </div>
    </div>
    
    <div class="item">
      <div class="col-sm-12">
       <a href="${path}/culture/view.do?cno=3"> <img class="img-responsive" src="${path}/images/index/문화-안전한 생활을 위한 응급처치교육(배너용).jpg" 
        alt="..."
        style="width: 100%;"></a>
      </div>
      <div class="col-sm-14">
        <div class="carousel-caption">
            <!-- <h2>Slide 2</h2> -->
        </div>
      </div>
    </div>
    
    <div class="item">
      <div class="col-sm-12">
        <a href="${path}/culture/view.do?cno=2"><img class="img-responsive" src="${path}/images/index/문화-엄마톡 아이톡(배너용).jpg" 
        alt="..."
        style="width: 100%;"></a>
      </div>
      <div class="col-sm-15">
        <div class="carousel-caption">
            <!-- <h2>Slide 2</h2> -->
        </div>
      </div>
    </div>
    
  </div>
</div>			



<!-- 			사진 부분 끝 			-->









<!-- 		영역 나누기 			-->
<!-- <div class="container"> -->


<!-- 		영역 1 			-->
<div class="row grid-divider">
 <div class="col-sm-2">
      <div class="col-padding">
    
    
 
<div class="container">
<div class="row">
		                   <div class="col-md-8" style="margin-top: 25px; margin-left: 10px;">
                           <div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">공지사항</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">행사안내</a></li>
                                   </ul>

                                    <!-- Tab panes -->
                                   <div class="tab-content"
                                   style="height: 220px;padding-bottom: 0px;padding-right: 0px;padding-left: 20px;padding-top: 0px;">
                                        <div role="tabpanel" class="tab-pane active" id="home">



        <c:forEach var="row" items="${map.list2}" begin="1" end="5">
		<a href="${path}/notice/view.do?nno=${row.nno}" style="margin-right: 60px;">
			<span style="font-size: 16px;">${row.title}</span></a><br><br>
			</c:forEach>






										</div>
                                        <div role="tabpanel" class="tab-pane" id="profile">
                                        
                                         <c:forEach var="row" items="${map.list}" begin="1" end="5">
											<a href="${path}/culture/view.do?cno=${row.cno}" style="margin-right: 180px;">
											<span style="font-size: 16px;">${row.title}</span></a> <BR><br>
											</c:forEach>
                                        
                                        
                                        
                                        
                                        </div>
                                   </div>
							</div>
                         </div>
</div>
</div> 
    
    
    
    
    
</div>
</div>
    
<div class="col-sm-4">
<div class="col-padding">
       
      
      
</div>
</div>
    

    
    
    
<!-- 		영역 2 			-->    
    <div class="col-sm-6">
    <div class="col-padding" align="right">
      
 <!-- 달력 -->
			<%-- base table --%>
			<TABLE cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" >
				<TR>
					<TD colspan="2" align="right" width="365">
						${curYear} 년 &nbsp;&nbsp; ${curMonth} 월 
						<a href="#">
						<span style="font-size: 20px; margin-right: 70px; margin-left: 50px;">
						+</span></a>
					</TD>
				</TR>
				<TR height="3">
					<TD colspan="2"></TD>
				</TR>
				<TR>
					<TD align="center" colspan="3" valign="top">
						<%-- body table --%>
						<TABLE border="0" cellspacing="0" cellpadding="0">
							<TR>
								<TD valign="top" style="border: #666666 1px solid; padding: 5px"
									align="center">
									<%-- month outline table --%>
									<TABLE border="0" cellspacing="0" cellpadding="0">
										<TR height="30">
											<TD align=center><FONT color=red>일</FONT></TD>
											<TD align=center>월</TD>
											<TD align=center>화</TD>
											<TD align=center>수</TD>
											<TD align=center>목</TD>
											<TD align=center>금</TD>
											<TD align=center>토</TD>
										</TR>
										<TR>
											<TD colspan=7 bgcolor=#888888 height=1></TD>
										</TR>
										<TR>
											<TD colspan=7 bgcolor=#ffffff height=5></TD>
										</TR>
										<TR>
											<TD colspan=7>
												<%-- month content table --%>
												<TABLE border='0' cellspacing='1' cellpadding='0'
													bgcolor=#dddddd>
													<TR>
														<c:if test="${firstDayOfWeek != '1'}">
															<%-- 해당 월의 가장 첫째줄에 있는 공백부분을 셈해서 처리한다.--%>
															<c:forEach var="i" begin="1" end="${firstDayOfWeek-1}">
																<TD  class="uline" valign="top"
																	align="right" style="padding: 5"></TD>
															</c:forEach>
														</c:if>

														<%-- 이 달의 끝날까지 메모의 제목과 날짜(숫자)를 출력한다 --%>
														<c:set var="dbIndex" value="0" />
														<c:forEach var="currentDay" begin="1" end="${lastDayOfMonth}">
															<TD bgcolor="#ffffff" style="paddi ng: 5">
																<TABLE cellpadding="0" cellsping="0" border="0" width="35">
																	<TR>
																		<TD height="10" width="35" class="uline" valign="top"
																			align="right">
																								
												                <c:choose>
																<c:when test="${((currentDay-(8-firstDayOfWeek)) % 7) == 1}">
																						<!-- 일요일 -->
																						<FONT color="red"> <c:out value="${currentDay}" />
																						</FONT>
																					</c:when>
																					<c:otherwise>
																						<c:out value="${currentDay}" />
																					</c:otherwise>
																				</c:choose>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="12" width="35" valign="top">
																			<TABLE>
																			 	<c:forEach var="dayIndex" items="${month_query}">
																					
																					<c:if test="${currentDay == dayIndex.schedule_date}">
																						<TR>
																						<c:choose>
																							<c:when test="${dayIndex.event == 2}">
																							<TD bordercolor="red">
																							<img alt="휴" src="${path}/images/schedule/hue.jpg">
																							</TD>	
														
																							</c:when>
																							
																							<c:when test="${dayIndex.event == 3}">
																							<TD bordercolor="auqa" >
																							<img alt="행" src="${path}/images/schedule/hang.jpg">
																							</TD>
																							</c:when>																							
																		
																							</c:choose>
																						</TR>
																					</c:if>
																					
																				</c:forEach>
																			
																			
																			</TABLE>
																		</TD> 
																	</TR>

																</TABLE>
															</TD>
															<%-- 만약 한주의 마지막날(토요일)이고 이 달의 마지막 날이 아니라면 다음 줄로 넘어간다. --%>
															<c:if test="${((currentDay-(8-firstDayOfWeek)) % 7) == 0}">
															<TR>
															</TR>
															</c:if>
														</c:forEach>
														<%-- 해당 월의 가장 마지막 줄에 있는 공백부분을 셈해서 처리한다.--%>
														<c:if test="${lastDayOfLastWeek != '7'}">
															<c:forEach var="i" begin="1" end="${7-lastDayOfLastWeek}">
																<TD width=20 height=30 class=uline valign=top
																	align=right style='padding: 5'></TD>
															</c:forEach>
														</c:if>
													</TR>
												</TABLE> <%-- end month content table --%>
											</TD>
										</TR>
									</TABLE> <%-- end month outline table --%>
								</TD>
							</TR>
						</TABLE> <%-- end body table --%>
					</TD>
				</TR>
				<TR height=10>
					<TD></TD>
				</TR>
				<TR>
					<TD align=right></TD>
				</TR>
			</table>
			<div align="center" style="margin-left: 250px;">
			<span style="color: blue; font-size: 18px;"><i class="fa fa-circle"></i></span>
			<span style="font-size: 18px;">행사일</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span style="color: red; font-size: 18px;"><i class="fa fa-circle"></i></span>
			<span style="font-size: 18px;">휴관일</span>
			</div>		
		</div>
	</div>
      
      
      
     
    </div>
    


<!-- 		영역  나누기 끝			-->






<!-- 			하단부			 이달의 --> 
<div class="container" style="width: 100%;">
<div class="col-xs-12">

    <div class="page-header">
        <h3>우리도서관 Best8</h3>
        <p>-대출을 많이 신청한 책입니다-</p>
    </div>
        
    <div class="carousel slide" id="myCarousel">
        <div class="carousel-inner" style="right: 20px;">
            <div class="item active">
                    <ul class="thumbnails">
                <c:forEach var="best4" items ="${besttop4}" > 
                 		<li class="col-sm-3">
    						<div class="fff">
								<div class="thumbnail">
									<a href="${path}/hjh/book/book/detail?book_code=${best4.book_code}"><img src="${path}/images/${best4.imgsrc}" alt="이미지는 아직이다"></a>
								</div>
								<div class="caption">
									<h4>${best4.title }</h4>
									<p>${best4.author }</p>
									<a class="btn btn-mini" href="${path}/hjh/book/book/detail?book_code=${best4.book_code}">» 자세히 보기 </a>
								</div>
                            </div>
                        </li>
                 </c:forEach> 
                    </ul>
              </div>
             <!-- /Slide1 --> 
            <div class="item">
                    <ul class="thumbnails">
                    <c:forEach var="best8" items="${besttop8}">
                        <li class="col-sm-3">
							<div class="fff">
								<div class="thumbnail">
									<a href="${path}/hjh/book/book/detail?book_code=${best8.book_code}"><img src="${path}/images/${best8.imgsrc}" alt="으엉2번슬라이더인데.."></a>
								</div>
								<div class="caption">
									<h4>${best8.title}</h4>
									<p>${best8.author}</p>
									<a class="btn btn-mini" href="${path}/hjh/book/book/detail?book_code=${best8.book_code}">» 자세히보기</a>
								</div>
                            </div>
                        </li>
                    </c:forEach>
                    </ul>
              </div><!-- /Slide2 --> 
            
        </div>
        
       
	   <nav>
			<ul class="control-box pager">
				<li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
				<li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></a></li>
			</ul>
		</nav>
                              
    </div>
        
</div>

</div>


<%@ include file="include/footer.jsp"%>
</body>
</html>







