<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>

$(function(){
	msg();
	//모두 선택,해제 체크박스
	 $("#selectAll").click(function() {
		 //체크 상태이면
		 if ($("#selectAll").prop("checked")) {
			 //모두 선택
			 $("input[name=select]").prop("checked", true);
	 } else { //해제 상태이면
			 //모두 해제
			 $("input[name=select]").prop("checked", false);
		  }
	 });
	
	
	$("#btnList").click(function(){
		//도서목록으로 이동
		location.href="${path}/kdk/book/list.do";
	});
	
	$("#btnCartList").click(function(){
		//관심도서목록으로 이동
		location.href="${path}/kdk/bookcart/list.do";
	});
	
	$("#btnDeleteAll").click(function(){
		if(confirm("관심도서 목록을 비우시겠습니까?")){
			location.href="${path}/kdk/bookcart/deleteAll.do";
		}
	});
	
	
	$("#btnDelete").click(function(){
		if(!$('#select').is(":checked")){
			alert("삭제 할 관심도서를 체크해주세요!");
		    return;
		}
		document.form2.action = "${path}/kdk/bookcart/delete.do";
		document.form2.submit();
	}); 
	
	
	$("#search").click(function(){
		document.form1.action = "${path}/kdk/bookcart/list.do";
		document.form1.submit();
	});



});

function list(page){
	document.form2.submit();
	location.href="${path}/kdk/bookcart/list.do?curPage="+page;
}

function msg(){
	msg = ${msg};
	if (msg != null){
		alert(msg);
	}
}

</script>
</head>


<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/mypage.jsp"%>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>


<div class="container">
<div class="row">
<div class="col-md-8">


<h2> 관심도서목록</h2>
<hr>
<div class="container">
<div class="row">
<div class="col-md-5" style="padding-left: 0px;">
                    
     				<form class="form"
                    name="form1" id="form1" method="post" 
	action="${path}/kdk/booklist.do">
                        <div class="form-group">
                            <div class="input-group">
                            
                                <span class="input-group-addon" id="month-addon">자료검색 </span>
                                <select name="search_option" class="form-control" aria-describedby="month-addon">
        <option value="title" <c:if test="${map.search_option == 'title'}">selected</c:if>> 제목</option>
		<option value="author" <c:if test="${map.search_option == 'author'}">selected</c:if>>저자</option>
		<option value="publisher" <c:if test="${map.search_option == 'publisher'}">selected</c:if>>출판사</option>
		<option value="all" <c:if test="${map.search_option == 'all'}">selected</c:if>>전체</option>                         
                                </select>
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                                
                                <input name="keyword" <c:if test="${map.keyword != null}"> value="${map.keyword}"</c:if>
                                class="form-control" aria-describedby="month-addon">
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                               	<input type="button" id="search" value="검색" name="control-month" style="background-color: #428bca; color: white;"
                               	class="form-control" aria-describedby="month-addon">
                           
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>








<!-- 테이블 -->
<div class="container" style="width: 900px;height: 0px;">
	<div class="row">
        
        <div class="col-md-11" style="padding-left: 0px;padding-right: 0px;">
        <div class="table-responsive">
        
        
 <c:choose>
	<c:when test="${map.count == 0}">
		관심도서 목록이 비었습니다.
	</c:when>
	
	<c:otherwise>
     <form name="form2">    
              <table id="mytable" class="table table-bordred table-striped" >
              <thead>
                   <th style="padding-right: 0px;padding-left: 10px;"><input type="checkbox" id="selectAll" /></th>
                   <th style="padding-right: 0px;padding-left: 10px;">번호</th>
                   <th style="padding-right: 0px;padding-left: 10px;">자료명</th>
                   <th style="padding-right: 0px;padding-left: 10px;">저자</th>
                   <th style="padding-right: 0px;padding-left: 10px;">발행자</th>
                   <th style="padding-right: 0px;padding-left: 10px;">청구기호</th>
                   <th style="padding-right: 0px;padding-left: 10px;">자료상태</th>
              </thead>
                  
<c:forEach var="row" items="${map.list}"> 
                   
                   
<tbody>
    
<tr>
  <td style="padding-right: 0px;padding-left: 10px;"><input type="checkbox" name="select" id="select" value="${row.cart_id}" ></td>
  <td style="padding-right: 0px;padding-left: 10px;">${row.cart_id}</td>
  <td style="padding-right: 0px;padding-left: 10px;"><a href ="${path}/kdk/book/book/detail/${row.book_code}">${row.title}</a></td>
  <td style="padding-right: 0px;padding-left: 10px;">${row.author}</td>
  <td style="padding-right: 0px;padding-left: 10px;">${row.publisher}</td>
  <td style="padding-right: 0px;padding-left: 10px;">${row.book_no}</td>
  <td style="padding-right: 0px;padding-left: 10px;">
  <c:if test="${row.book_tf == '0'}"><label style="color: red;">대출중</label></c:if>
  <c:if test="${row.book_tf == '1'}"><label style="color: blue;">대출가능</label></c:if></td>
</tr> 
    </tbody>
</c:forEach>


			</table>


<!-- 			페이지 나누기			 -->
	<div class="clearfix">
		<button type="button" id="btnDeleteAll">전체삭제</button>
		<button type="button" id="btnDelete">선택삭제</button>
		<button type="button" id="btnList">도서목록</button>
		<button type="button" id="btnCartList">관심도서목록</button>
	</div>


<div align="center">
<ul class="pagination pull-right">
  <li><a href="#" onclick="list('${map.pager.prevPage}')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
  <c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
  						<li class="active"><a href="#" onclick="list('${num}')">${num}</a></li>
 					 </c:when>
  					<c:otherwise>
 						 <li><a href="#" onclick="list('${num}')">${num}</a></li>
  					</c:otherwise>
 				</c:choose>
 </c:forEach>
  <li><a href="#" onclick="list('${map.pager.nextPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
</ul>
</div>
<!-- 				페이지 나누기 끝 				-->

</form>
	</c:otherwise>
</c:choose>
</div>
</div>
</div>
</div>
<!-- 			테이블 끝		 		-->  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 </div>
 </div>
 </div>
  
  
	

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>
