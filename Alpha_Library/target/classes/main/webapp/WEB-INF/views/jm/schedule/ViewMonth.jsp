<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- 세션을 사용하지 않는 옵션 -->
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title>공지사항</title>

<%@ include file="../../include/header.jsp"%>


</head>


<STYLE>
BODY {
	FONT-SIZE: 9pt;
	FONT-FAMILY: tahoma
}

TABLE {
	FONT-SIZE: 9pt;
	COLOR: black;
	FONT-FAMILY: tahoma
}

A {
	COLOR: #999999;
	TEXT-DECORATION: none
}

/* A .she{
	COLOR: #36253b;
	TEXT-DECORATION: none
} */

A:hover {
	COLOR: red;
	TEXT-DECORATION: none
}

TD.main {
	FONT-WEIGHT: bold;
	TEXT-ALIGN: right
}

TD.uline {
	FONT-SIZE: 7pt;
	COLOR: #999999;
	BACKGROUND-COLOR: #ffffff
}

TD.r_uline {
	FONT-SIZE: 7pt;
	COLOR: #999999;
	BACKGROUND-COLOR: #f4f4f4
}

INPUT {
	MARGIN: -5px
}
</STYLE>


<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/twodragon.jsp"%>



   <h2 style="margin-left: 250px;">휴관일/행사</h2>
        <hr style="border-top-width: 2px; margin-right: 125px; margin-bottom: 0px; width: 900px;">     
	<div style="margin: 0 auto">
		<FORM name="theForm">
			<%-- base table --%>
			<TABLE cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff"
				width="620" height="665">
				<TR>
					<TD colspan="2" align="center" width="365">
					<A href="${path}/project/schedule/bigcalminusview.do?type=MONTH&curYear=<c:out value="${curYear}"/>&curMonth=<c:out value="${curMonth-1}"/>&curDay=<c:out value="${curDay}"/>">◀</a>
					<span style="font-size: 17px;">	${curYear} 년 &nbsp;&nbsp; ${curMonth} 월  </span>
						<A href="${path}/project/schedule/bigcalplusview.do?type=MONTH&curYear=<c:out value="${curYear}"/>&curMonth=<c:out value="${curMonth+1}"/>&curDay=<c:out value="${curDay}"/>">▶</a>
					</TD>
				</TR>
				<TR height="3">
					<TD colspan="2"></TD>
				</TR>
				<TR>
					<TD align="center" colspan="3" valign="top">
						<%-- body table --%>
						<TABLE border="1" cellspacing="0" cellpadding="0">
							<TR>
								<TD valign="top" style="border: #666666 1px solid; padding: 5px"
									align="center">
									<%-- month outline table --%>
									<TABLE border="1" cellspacing="0" cellpadding="0">
										<TR height="30">
											<TD align=center><FONT color=red>일요일</FONT></TD>
											<TD align=center>월요일</TD>
											<TD align=center>화요일</TD>
											<TD align=center>수요일</TD>
											<TD align=center>목요일</TD>
											<TD align=center>금요일</TD>
											<TD align=center>토요일</TD>
										</TR>
										<TR>
											<TD colspan=7 bgcolor=#888888 height=1></TD>
										</TR>
										<TR>
											<TD colspan=7 bgcolor=#ffffff height=5></TD>
										</TR>
										<TR>
											<TD colspan=7>
												<%-- month content table --%>
												<TABLE border="1" cellspacing='0' cellpadding='0'
													bgcolor=#dddddd>
													<TR>
														<c:if test="${firstDayOfWeek != '1'}">
															<%-- 해당 월의 가장 첫째줄에 있는 공백부분을 셈해서 처리한다.--%>
															<c:forEach var="i" begin="1" end="${firstDayOfWeek-1}">
																<TD width="70" height="78" class="uline" valign="top"
																	align="right" style="padding: 5; font-size: 16;" ></TD>
															</c:forEach>
														</c:if>

														<%-- 이 달의 끝날까지 메모의 제목과 날짜(숫자)를 출력한다 --%>
														<c:set var="dbIndex" value="0" />
														<c:forEach var="currentDay" begin="1" end="${lastDayOfMonth}">
															<TD bgcolor="#ffffff" style="paddi ng: 5">
																<TABLE cellpadding="0" cellsping="0" border="0" width="120">
																	<TR>
																		<TD height="10" width="70" class="uline" valign="top"
																			align="right">
																								
												                <c:choose>
																<c:when test="${((currentDay-(8-firstDayOfWeek)) % 7) == 1}">
																						<!-- 일요일 -->
																						<FONT color="red" size="5px;"> <c:out value="${currentDay}" />
																						</FONT>
																					</c:when>
																					<c:otherwise>
																					<FONT size="5px;">	<c:out value="${currentDay}" /></FONT>
																					</c:otherwise>
																				</c:choose>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="68" width="70" valign="top">
																			<TABLE>
																	
																			 	
																			 	<c:forEach var="dayIndex" items="${month_query}">
																					
																					<c:if test="${currentDay == dayIndex.schedule_date}">
																						<TR>
																						<c:choose>
																							<c:when test="${dayIndex.event == 2}">
																							<TD style="background-color: pink;">
																							<font color="black">
																									<span style="color: black; font-weight: bold; font-size: 17px;">${dayIndex.schedule_subject}</span> 
																							</font>
																							</TD>	
														
																							</c:when>
																							
																							<c:when test="${dayIndex.event == 3}">
																							<TD style="background-color: olive;">
																							<font color="white">
																							
																							<span style="color: white; font-weight: bold; font-size: 17px;">${dayIndex.schedule_subject}</span> 
																							</font>
																							</TD>	
														
																							</c:when>
																							
																								<c:otherwise>
																									<TD><font color="black">
																								<span style="color: black; font-weight: bold; font-size: 17px;">${dayIndex.schedule_subject} </span></font> </TD>
																								</c:otherwise>
																							</c:choose>
																						</TR>
																					</c:if>
																					
																				</c:forEach>
																			
																			
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</TD>
															<%-- 만약 한주의 마지막날(토요일)이고 이 달의 마지막 날이 아니라면 다음 줄로 넘어간다. --%>
															<c:if test="${((currentDay-(8-firstDayOfWeek)) % 7) == 0}">
													<TR>
													</TR>
														</c:if>
														</c:forEach>

														<%-- 해당 월의 가장 마지막 줄에 있는 공백부분을 셈해서 처리한다.--%>
														<c:if test="${lastDayOfLastWeek != '7'}">
															<c:forEach var="i" begin="1" end="${7-lastDayOfLastWeek}">
																<TD width=70 height=78 class=uline valign=top
																	align=right style='padding: 5'></TD>
															</c:forEach>
														</c:if>
													</TR>
												</TABLE> <%-- end month content table --%>
											</TD>
										</TR>
									</TABLE> <%-- end month outline table --%>
								</TD>
							</TR>
						</TABLE> <%-- end body table --%>
					</TD>
				</TR>
				<TR height=10>
					<TD></TD>
				</TR>
				<TR>
					<TD align=right></TD>
				</TR>
			</table>
			<%-- end base table --%>
		</FORM>
	</div> 
 
 
 
 
 
 
 
 
 
 
 
 





<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


<%@ include file="../../include/footer.jsp"%>

</body>
<SCRIPT type="text/javascript">
	function view(str) {
		OpenWin("${path}/project/schedule/viewschedule.do?type=SELECT&schedule_id=" + str, 480, 360);
	}

	function dWrite(y, m, d) {
		OpenWin("${path}/project/schedule/insertScheduleByIndex.do?curYear=" + y + "&curMonth=" + m + "&curDay=" + d,
				470, 320);
	}
	function OpenWin(URL, width, height) {
		var str, width, height;
		str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
		str = str + "width=" + width;
		str = str + ",height=" + height + "',top=50,left=50";
		window.open(URL, 'remoteSchedule', str);
	}
</SCRIPT>
</html>
