<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>대출정보확인</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="${path}/include/js/datepicker/datepicker-ko.js"></script>
<script src="${path}/include/js/datepicker/date.js"></script>



<script type="text/javascript">
	
	$(function() {
		$("#esc").click(function(){
			
			
			window.close();
			
			
		});
		
		
		$("#rent_date").datepicker({
			minDate: -3, 
	        maxDate: +14,
	        onSelect: function(dateText) {  
				var myDate = new Date(dateText);
				var dayOfMonth = myDate.getDate();
				myDate.setDate(dayOfMonth +1);
				var return_date = new Date(myDate).toString('yyyy-MM-dd');
				$("#return_date").val(return_date);
				}
			
		});
		
		
		$("#rent").click(function() {
			if (confirm("대출정보를확인하셨습니까?")) {

				var userid = $("#userid").val();

				var book_code = $("#book_code").val();
		
				var rent_date = $("#rent_date").val();
				
				var return_date = $("#return_date").val();
				
				var group_code = $("#group_code").val();
					
				var param = {
					"userid" : userid,
					"book_code" : book_code,
					"rent_date" : rent_date,
					"return_date": return_date,
					"group_code": group_code				
				}; //JSON
				
				console.log(param);
				
				$.ajax({
					type : "post",
					url : "${path}/book/rent/rent.do",
					data : param,
					success : function(result) {
						alert(result);
						opener.location.href="${path}/hjh/book/book/detail?book_code="+book_code;
						window.close();
					}
				});
			
			}
		
		
		});

	});

</script>
</head>
<body>
<br>
<div align="center">
	<h3>대출정보</h3>
	<form name="Form1" id="Form1" method="post"
	style="margin-left: 40px;">
		<table style="width: 400px; height: 200px;">
			<tr>
				<th>ID</th>
				<td><input name="userid" id="userid" value="${userid}" 
				style="height: 25px; width: 250px;" readonly="readonly"></td>
			</tr>
			<tr>
				<th>도서</th>
				<td><input style="height: 25px; width: 250px;" readonly="readonly" value="${bookdto.title}(${bookdto.group_code})">
			</td></tr>
			
			<tr>
				<th>저자</th>
				<td><input name="author" value="${bookdto.author}" 
				style="height: 25px; width: 250px;" readonly="readonly"></td>
			</tr>
			<tr>	
				<th>대출일</th>
				<td><input id="rent_date" name="rent_date" 
				style="height: 25px; width: 250px;" placeholder="날짜를 선택해주세요(클릭하세요)"></td>
			</tr>
			<tr>
				<th>반납일</th>
				<td><input id="return_date" name="return_date"
				style="height: 25px; width: 250px;"></td>
			</tr>
		</table>
		
		
			<div align="center"><input type="button" id="rent" value="대출">
								<input type="button" id="esc" value="취소">
			</div>
		<input type="hidden" value="${bookdto.book_code}" name="book_code" id="book_code">
		<input type="hidden" name="group_code" id="group_code" value="${bookdto.group_code}">
	</form>
</div>	
</body>
</html>