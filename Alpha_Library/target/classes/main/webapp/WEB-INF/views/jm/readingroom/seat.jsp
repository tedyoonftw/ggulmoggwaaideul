<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- 세션을 사용하지 않는 옵션 -->
<%@ page session="true"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<title>열람실좌석현황</title>

<%@ include file="../../include/header.jsp"%>

<link href="${path}/include/css/readingroom/seat.css" rel="stylesheet">

<script>
	function OpenWin(URL, width, height) {
		var str, width, height;
		str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
		str = str + "width=" + width;
		str = str + ",height=" + height + "',top=50,left=50";
		window.open(URL, 'remoteSchedule', str);
	}

	function reserve(str) {
		OpenWin("${path}/project/readingroom/reservation.do/" + str, 480, 360);
	}
	function view(str) {
		OpenWin("${path}/project/readingroom/sadragoninfo.do/" + str, 480, 360);
	}
	
	function setClock() {
		location.href = "${path}/project/readingroom/list.do";
		}
		setInterval("setClock()", 30000);
</script>




</head>

<body>



<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/twodragon.jsp"%>




      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>


 
 <br>
	<table width="800px">
		<tr>
			<td align="center">
				<h2 style="padding-right: 80px;">좌석 현황</h2>
			</td>
		</tr>
		<tr>
		<td>
			<span style="color: red"><i class="fa fa-asterisk"></i></span><span style="font-size: 16px;">이페이지는 1분 마다 갱신됩니다</span>
		</td>
		</tr>
		<tr>
			<td align="right"><span style="font-size: 16px;">남은좌석: </span><span style="color: red; font-size: 18px;"> ${count}</span></td>
		</tr>
	</table>
	<form>
		<table id="seatTable">
			<tr>
				<td class="exit" colspan="7" align="center"><img src="${path}/images/readingroom/exit.png"></td>
			</tr>
			<tr>
			
				<c:forEach var="dto" items="${list}" begin="0" end="2">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;
"class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>

				<td rowspan="7" 
				style="width: 100px;
	height: 100px;
	visibility: hidden;" class="path"></td>


				<c:forEach var="dto" items="${list}" begin="12" end="14">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>


						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="dto" items="${list}" begin="3" end="5">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;"
    class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="15" end="17">
					<td style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>

						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>

			<tr>
				<td colspan="7" 
				style="width: 100px;
	height: 100px;
	visibility: hidden;"
				class="path"></td>
			</tr>

			<tr>
				<c:forEach var="dto" items="${list}" begin="6" end="8">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="18" end="20">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="dto" items="${list}" begin="9" end="11">
					<td style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>

						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
				<c:forEach var="dto" items="${list}" begin="21" end="23">
					<td 
					style="
    width: 100px;
    height: 100px;
    text-align: center;" class="seat" id="seat${dto.seatno}"
						background=<c:choose>
				<c:when test="${dto.gender == 1 }">
				"${path}/images/readingroom/namja.png"
				</c:when>
				<c:when test="${dto.gender == 2 }">
				"${path}/images/readingroom/yeoja.png"
				</c:when>
				<c:otherwise>
				"${path}/images/readingroom/nomal.png"
				</c:otherwise>
				</c:choose>>${dto.seatno}<br>
						<c:if
							test="${dto.userid == null && sessionScope.userid != null && search == null  }">
							<a href="javascript:reserve('${dto.seatno}')"> <font
								color="white"> 사용등록 </font>
							</a>
						</c:if> <c:if
							test="${dto.userid != null && dto.userid == sessionScope.userid }">
							<a href="javascript:view('${dto.seatno}')"> <font
								color="white"> 사용정보 </font>
							</a>
						</c:if>
				</c:forEach>
			</tr>

		</table>
	</form>
 
 
 
 
 
 





<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
<%@ include file="../../include/footer.jsp"%>

</body>
</html>
