<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>일정관리</title>
<script src="${path}/include/js/jquery-3.2.1.min.js"></script>
<STYLE>
body {
	font-size: 9pt;
	font-family: tahoma
}

table {
	font-size: 9pt;
	font-family: tahoma;
	color: black
}

A {
	color: black;
	text-decoration: none
}

A:hover {
	color: red;
	text-decoration: none;
}

td.main {
	text-align: right;
	font-weight: bold;
}

td.sub {
	padding-left: 10
}

.text_gray {
	Font-Family: "Arial", "굴림체";
	Font-Size: 10pt;
	Background-Color: #FFFFFF;
	Color: #000000;
	border: 1px #818181 solid
}

.text_purple {
	border: solid 1;
	background-color: white;
	border-color: 160F25;
	font-size: 8pt;
	font-family: Arial;
	vertical-align: top;
	border-left-color: #ffffff;
	border-right-color: #ffffff;
	border-top-color: #74A6AB;
	border-bottom-color: #74A6AB;
	height: 18px;
}

.textarea {
	font-size: 10pt;
	background-color: white;
	border-width: 1;
	border-style: solid;
	color: #45250C;
	border-color: #777777;
	scrollbar-face-color: white;
	scrollbar-shadow-color: #777777;
	scrollbar-highlight-color: white;
	scrollbar-3dlight-color: #777777;
	scrollbar-darkshadow-color: white;
	scrollbar-arrow-color: #777777;
	scrollbar-track-color: white;
}
</STYLE>
<script>
	$(function() {
		
		$("#btnDelete").click(function() {

			if (window.confirm('정말로 삭제 하시겠습니까?')) {
				document.theForm.action = "${path}/project/schedule/delete.do";
				document.theForm.submit();
				opener.location.href = "${path}/project/admin_readingroom/bigcalview.do";
			}
		});

		$("#btnUpdate").click(function() {

			if (checkAction()) {
				document.theForm.action = "${path}/project/schedule/update.do";
				document.theForm.submit();
			}
			opener.location.href = "${path}/project/admin_readingroom/bigcalview.do";
			window.close();

		});

		$("#btnSave").click(function() {

			if (checkAction()) {
				document.theForm.action = "${path}/project/schedule/insert.do";
				document.theForm.submit();
			}
			opener.location.href = "${path}/project/admin_readingroom/bigcalview.do";
			window.close();
		});

	});

	function checkBlankMessage(elem, msg) {
		var v = elem.value;
		var l = elem.value.length;

		if (v == '') {
			alert(msg);
			elem.focus();
			return false;
		}

		return true;
	}

	function checkAction() {
		return (checkBlankMessage(document.theForm.curYear, '년도를 입력 하십시오')
				&& checkBlankMessage(document.theForm.curMonth, '달을 입력 하십시오')
				&& checkBlankMessage(document.theForm.curDay, '일자를 입력 하십시오')
				&& checkBlankMessage(document.theForm.schedule_subject,
						'제목을 입력 하십시오') && checkBlankMessage(
				document.theForm.schedule_content, '내용을 입력 하십시오'));
	}
</script>
</head>
<body>
	<FORM method="post" name="theForm" id="theForm" style="margin: 0">
		<TABLE cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff"
			width="100%" height="100%">
			<TR>
				<TD align="center">
					<TABLE cellpadding="0" cellspacing="1" border="0" bgcolor="#ffffff"
						width="440">
						<TR height="10">
							<TD></TD>
						</TR>
						<TR>
							<TD align=center bgcolor="#ffffff">
								<TABLE cellpadding="5" cellspacing="1" border="0"
									bgcolor="#666666">
									<TR bgcolor="#ffffff">
										<TD class="main">일자</TD>
	
										<TD><INPUT type="text" name="curYear" id="curYear"
											class="text_gray" size="4" maxlength="4"
											placeholder="ex)2018" 
											<c:choose>
											<c:when test="${type == 'view' }">
											value="${curYear}"
											</c:when>
											<c:otherwise>
											value="${dto.curYear}"
											</c:otherwise>
											</c:choose>
											>년
											
											<INPUT
											type="text" name="curMonth" id="curMonth" class="text_gray"
											size="2" maxlength="2" placeholder="ex)01"
											
											<c:choose>
											<c:when test="${type == 'view' }">
											value="${curMonth}"
											</c:when>
											<c:otherwise>
											value="${dto.curMonth}"
											</c:otherwise>
											</c:choose>
									
											>월 
											<INPUT type="text"
											placeholder="ex)01" id="curDay" name="curDay"
											class="text_gray" size="2" maxlength="2"
											<c:choose>
											<c:when test="${type == 'view' }">
											value="${curDay}"
											</c:when>
											<c:otherwise>
											value="${dto.curDay}"
											</c:otherwise>
											</c:choose>
											>일</TD>
									</TR>
									
									<TR bgcolor="#ffffff">
										<TD>
										<select id="event" name="event">
											<option value="1" <c:if test="${dto.event == 1}"> selected="selected" </c:if> > 일반 </option>
											<option value="2" <c:if test="${dto.event == 2}"> selected="selected" </c:if>> 공휴일 </option>
											<option value="3"<c:if test="${dto.event == 3}"> selected="selected" </c:if>> 이벤트 </option>
										</select>
										</TD>
										<TD width="380">제목<INPUT type="text"
											name="schedule_subject" class="text_gray" size="30"
											maxlength="50"
											value='<c:out value="${dto.schedule_subject}"/>'></TD>
									</TR>
									<TR bgcolor="#ffffff">
										<TD class="main">내용</TD>
										<TD><TEXTAREA NAME="schedule_content" class="textarea"
												ROWS="12" COLS="52" wrap="soft"><c:out
													value="${dto.schedule_content}" /></TEXTAREA></TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
						<TR>
							<TD colspan="2" height="5"></TD>
						</TR>
						<TR>
							<TD align="center">
								<c:if test="${type == 'INIT' || type == 'view'}">
									<button id="btnSave">저장</button>
								</c:if> <c:if test="${type == 'SELECT'}">
									<button id="btnDelete">삭제</button>
									<button id="btnUpdate">수정</button>
								</c:if>

								<button id="btnClose" onclick="window.close()">닫기</button>
						</TR>
						<TR>
							<TD colspan=2 height=5></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		<INPUT type="hidden" name="type"> <INPUT type="hidden"
			name="schedule_id" value="${dto.schedule_id}">
		<!-- 	<INPUT type="hidden" name="schedule_date_time"> -->
	</FORM>
</body>
</html>