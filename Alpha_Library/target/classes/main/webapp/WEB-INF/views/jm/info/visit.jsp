<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../include/header.jsp"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>도서관소개</title>

<script type="text/javascript"
	src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=ThkUGeejzfWgG9i9R8de&submodules=geocoder"></script>
<script type="text/javascript">
$("#findroad").click(function(){
	window.open("http://map.daum.net/link/to/서울시 성동구 무학로2길 54,37.5621197,127.0329923",600,800);
});
</script>
</head>

<script type="text/javascript">
function findgil(url){
	window.open(url,"길찾기");
}

</script>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<%@ include file="../../include/sidebar/cowdog.jsp"%>



<div class="container">
	<span style="font-size: 25px; margin-right: 480px;">찾아가는길</span>
	<A href="javascript:findgil('http://map.daum.net/link/to/서울시 성동구 무학로2길 54,37.5621197,127.0329923')">
	<span style="color: blue; font-size: 18px;">길찾기(Daum지도사용)</span></A>
	<!-- <input type="button" value="길찾기" id="findroad"> -->
	<hr style="width: 70%; margin-right: 200px; border-top-width: 2px;">
	<div id="map" style="width: 70%; height: 300px;"></div>
	

	<script>
		var map = new naver.maps.Map('map');
		var myaddress = '서울시 성동구 무학로2길 54';// 도로명 주소나 지번 주소만 가능 (건물명 불가!!!!)
		naver.maps.Service
				.geocode(
						{
							address : myaddress
						},
						function(status, response) {
							if (status !== naver.maps.Service.Status.OK) {
								return alert(myaddress
										+ '의 검색 결과가 없거나 기타 네트워크 에러');
							}
							var result = response.result;
							// 검색 결과 갯수: result.total
							// 첫번째 결과 결과 주소: result.items[0].address
							// 첫번째 검색 결과 좌표: result.items[0].point.y, result.items[0].point.x
							var myaddr = new naver.maps.Point(
									result.items[0].point.x,
									result.items[0].point.y);
							map.setCenter(myaddr); // 검색된 좌표로 지도 이동
							// 마커 표시
							var marker = new naver.maps.Marker({
								position : myaddr,
								map : map
							});
							naver.maps.Event.addListener(marker, "click",
									function(e) {
										if (infowindow.getMap()) {
											infowindow.close();
										} else {
											infowindow.open(map, marker);
										}
									});
							// 마크 클릭시 인포윈도우 오픈
							var infowindow = new naver.maps.InfoWindow(
									{
										content : '<h4> [어여가대도서관] </h4><img src="${path}/images/info.png"></a>'
									});
						});
		
	</script>
<hr style="width: 65%;margin-right: 200px; border-top-width: 2px;">
<p>
※지하철<br>
&nbsp;- 2호선,5호선,중앙선 왕십리역 1번 출구(도보 1분이하)<br>
※버스<br>
&nbsp;- 간선 버스 : 263, 302 코스모스역에서 하차 후 도보 5분 이하<br>
&nbsp;- 지선 버스 : 2014. 2220, 2222 성동구청역에서 하차 후 도보 5분 이하<br>
&nbsp;- 마을 버스 : 왕십리역에서 하차 후 도보 3분 이하<br>
※주차<br>
&nbsp;- 도서관 이용자 : 1시간 무료 주차, 추가적으로 5분에 150원 (1층 안내데스크에서 이용확인도장)<br>
&nbsp;- 장애인주차장 : 시네센터(영화관) 2층 갤러리 앞<br>
&nbsp;- 영화(예매)표 소지자는 3시간 무료 주차(시네센터 매표실에서 주차확인도장)
							</p>
</div>











<%@ include file="../../include/footer.jsp"%>
</body>
</html>