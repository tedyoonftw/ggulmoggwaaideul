<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../../include/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	
	$("#btnUpdate").click(function(){
		if(confirm("이용하시겠습니까?")){
		document.form1.action = "${path}/project/readingroom/reserve.do";
		document.form1.submit();		
		}
		/* opener.location.href = "${path}/project/readingroom/list.do"; 
 		window.close();   */

	});
	
	$("#btnOut").click(function(){
		if(confirm("사용을 종료하시겠습니까?")){
		var seatno = $("#seatno").val();
		document.form1.action = "${path}/project/readingroom/logout.do/"+seatno;
		document.form1.submit();			
		}
/* 		opener.location.href = "${path}/project/readingroom/list.do"; 
 		window.close(); */

	});
	
	$("#btnCancel").click(function(){
		var seatno = $("#seatno").val();
		location.href="${path}/project/readingroom/close.do/"+seatno;
	
		
	});

});



</script>
<!-- 추가부분 -->
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<!-- 책테이블 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


</head>
<body style="margin-top: 50px;">



<div class="container">
<div class="row">
<div class="col-md-6">


<h3>열람실 이용 확인</h3>
<table class="table table-bordred table-striped">

<tr>
<th>좌석번호</th>
<td>${dto.seatno}</td>
</tr>

<tr>
<th>사용자</th>
<td>${dto.username}(${dto.userid})
</tr>

<tr>
<th>이용시간 </th>
<td> ${dto.starttime} ~ ${dto.endtime}</td>
</tr>
</table>
<div align="center">
<c:choose>
<c:when test ="${dto.sadragon == 'N' }">
<input type="button" id = "btnUpdate" value="사용등록" 
class="btn btn-default" style="width: 100px; background-color: #428bca; color: white;">
</c:when>
<c:otherwise>
<input type="button" id = "btnOut" value="사용종료" 
class="btn btn-default" style="width: 100px; background-color: #428bca; color: white;">
</c:otherwise>
</c:choose>

<input type="button" id = "btnCancel" value="닫기" 
class="btn btn-default" style="width: 100px; background-color: #428bca; color: white;">
</div>
<form method="post" name="form1">
<input type="hidden" name = "userid" value="${dto.userid}">
<input type="hidden" name = "seatno" id="seatno" value="${dto.seatno}">
<input type="hidden" name = "gender" value="${dto.gender}">
<input type="hidden" name = "stime" value="${dto.starttime}">
<input type="hidden" name = "etime" value="${dto.endtime}">
</form>




</div>
</div>
</div>
</body>

</html>