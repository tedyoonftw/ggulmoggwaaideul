<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<script>
	function byebye() {
		if (!$("#check").is(":checked")) {
			alert("탈퇴 동의 여부를 체크해주세요. 동의시에만 탈퇴가 가능합니다.");
			return;
		}
		if ($("#check").is(":checked")) {
			var id = $("#userid").val();
			var passwdcheck = $("#passwdcheck").val();
			var passwd = $("#passwd").val();
			var rentcount = $("#rentcount").val();
			if (passwd == "") {
				alert("비밀번호를 입력해주세요.");
				$("#passwd").focus();
				return;
			}

			if (passwdcheck != passwd) {
				alert("비밀번호가 맞지 않습니다. 다시 확인해주세요.");
				$("#passwd").focus();
				return;
			}

			if (rentcount > 0) {
				alert("대출하신 책을 반납 후 탈퇴신청하시기 바랍니다.");
				return;
			}

			if (confirm("탈퇴시 도서관이용이 불가합니다. 탈퇴하시겠습니까?")) {
				document.form1.action = "${path}/pu/page/cancel.do";
				document.form1.submit();
				alert("탈퇴되었습니다. 이용해주셔서 감사합니다.");
			}

		}
	}
</script>
</head>



<body>

	<%@ include file="../../include/menu.jsp"%>
	<%@ include file="../../include/menu1.jsp"%>
	<%@ include file="../../include/sidebar/mypage.jsp"%>



	<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>






	<h2>회원탈퇴</h2>
	<form id="form1" name="form1" method="post">
		<input type="hidden" id="userid" name="userid" value="${dto.userid }">
		<input type="hidden" id="passwdcheck" value="${dto.passwd }">
		<label>비밀번호</label> <input type="password" id="passwd" name="passwd">
		<br> <label><input type="checkbox" id="check"> 탈퇴
			동의 여부~잘가~가지마~행복해~떠나지마~</label> <br> <br> <input type="button"
			onclick="byebye()" value="회원탈퇴"> <input type="hidden"
			id="rentcount" name="rentcount" value="${rentcount }">
	</form>




	<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>


	<%@ include file="../../include/footer.jsp"%>

</body>
</html>