<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<style type="text/css">
@import
	url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");

.login-block {
	background: #3f51b5; /* fallback for old browsers */
	background: -webkit-linear-gradient(to bottom, #FFB88C, #3f51b5);
	/* Chrome 10-25, Safari 5.1-6 */
	background: linear-gradient(to bottom, rgba(242, 237, 233, 0.26),
		rgba(186, 196, 208, 0.44));
	/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	float: left;
	width: 100%;
	padding: 50px 0;
}

.banner-sec {
	background: url(https://static.pexels.com/photos/33972/pexels-photo.jpg)
		no-repeat left bottom;
	background-size: cover;
	min-height: 500px;
	border-radius: 0 10px 10px 0;
	padding: 0;
}

.container {
	background: #fff;
	border-radius: 10px;
	box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1);
}

.carousel-inner {
	border-radius: 0 10px 10px 0;
}

.carousel-caption {
	text-align: left;
	left: 5%;
}

.login-sec {
	padding: 50px 30px;
	position: relative;
}

.login-sec .copy-text {
	position: absolute;
	width: 80%;
	bottom: 20px;
	font-size: 13px;
	text-align: center;
}

.login-sec .copy-text i {
	color: #FEB58A;
}

.login-sec .copy-text a {
	color: #E36262;
}

.login-sec h2 {
	margin-bottom: 30px;
	font-weight: 800;
	font-size: 30px;
	color: #3f51b5;
}

.login-sec h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #68A4C4;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
	margin-left: auto;
	margin-right: auto
}

.btn-login {
	background: #3f51b5;
	color: #fff;
	font-weight: 600;
}

.banner-text {
	width: 70%;
	position: absolute;
	bottom: 40px;
	padding-left: 20px;
}

.banner-text h2 {
	color: #fff;
	font-weight: 600;
}

.banner-text h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #FFF;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
}

.banner-text p {
	color: #fff;
}
</style>
<script>
	$(function() {
		function saveCookie(id) {
			if (id != "") {
				setCookie("userid", id, 7);
			} else {
				setCookie("userid", id, -1);
			}
		}
		function setCookie(name, value, days) {
			var today = new Date();
			today.setDate(today.getDate() + days);
			document.cookie = name + "=" + value + ";path=/project;expires="
					+ today.toGMTString() + ";";
		}
		function getCookie(cname) {
			var cookie = document.cookie + ";";
			var idx = cookie.indexOf(cname, 0);
			var val = "";
			if (idx != -1) {
				console.log(idx + "," + cookie.length);
				cookie = cookie.substring(idx, cookie.length);
				begin = cookie.indexOf("=", 0) + 1;
				end = cookie.indexOf(";", begin);
				val = cookie.substring(begin, end);
			}
			return val;
		}
		var cookie_userid = getCookie("userid");
		if (cookie_userid != "") {
			$("#userid").val(cookie_userid);
			$("#chkSave").attr("checked", true);
		}
		
		$("#btnLogin").click(function() {
			if ($("#chkAdmin").is(":checked")) {
					if ($("#chkSave").is(":checked")) {
						saveCookie($("#userid").val());
					} else {
						saveCookie("");
					} 
					
					var adminid = $("#userid").val();
					var adminpw = $("#passwd").val();
					if (adminid == "") {
						alert("관리자 아이디를 입력하세요.");
						$("#userid").focus();
						return;
					}
					if (adminpw == "") {
						alert("비밀번호를 입력하세요.");
						$("#passwd").focus();	
						return;
					}
					document.form1.action = "${path}/pu/login/admin/login_check.do";
					document.form1.submit();
			}
			if (!$("#chkAdmin").is(":checked")) {
				if ($("#chkSave").is(":checked")) {
					saveCookie($("#userid").val());
				} else {
					saveCookie("");
				} 
			
				var userid = $("#userid").val();
				var passwd = $("#passwd").val();
				if (userid == "") {
					alert("아이디를 입력하세요.");
					$("#userid").focus();
					return;
				}
				if (passwd == "") {
					alert("비밀번호를 입력하세요.");
					$("#passwd").focus();
					return;
				}
				document.form1.action = "${path}/pu/login/user/login_check.do";
				document.form1.submit();
			}
		});
		
		$("#chkSave").click(function() {
			if ($("#chkSave").is(":checked")) {
				if (!confirm("아이디를 저장하시겠습니까?")) {
					$("#chkSave").prop("checked", false);
				}
			}
		});
	});
function find(){
	OpenWin("${path}/pu/page/choose.do",400, 300);
}
</script>
</head>
<body>
	<%@ include file="../../include/menu.jsp"%>

	<link
		href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		rel="stylesheet" id="bootstrap-css">
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->

	<section class="login-block">
		<div class="container">
			<div class="row">
				<div class="col-md-4 login-sec">
					<h2 class="text-center">Login Now</h2>
					<form class="login-form" name="form1" method="post">
						<div class="form-group">
							<label for="exampleInputEmail1" class="text-uppercase">아이디</label>
							<input type="text" class="form-control" id="userid" name="userid"
								placeholder="ID">

						</div>
						<div class="form-group">
							<label for="exampleInputPassword1" class="text-uppercase">비밀번호</label>
							<input type="password" class="form-control" id="passwd"
								name="passwd" placeholder="PW">
						</div>

						<div class="form-check">
							<label class="form-check-label"> <input type="checkbox"
								class="form-check-input" id="chkAdmin"> 
								<small>관리자 로그인</small></label>
								<br>
							<label class="form-check-label"> <input type="checkbox"
								class="form-check-input" id="chkSave"> 
								<small>아이디 저장</small></label>
							<input type="button" id="btnLogin"
								class="btn btn-login float-right" value="Login">
						</div>

					</form>
					<c:if test="${check == 'Y'}">
						<script>
							alert("등록된 이메일입니다.")
						</script>
					</c:if>


					<c:if test="${param.message == 'nologin' }">
						<script>
							alert("로그인 하신 후 사용하세요.");
						</script>
					</c:if>
					<c:if test="${message == 'error' }">
						<script>
							alert("아이디 또는 비밀번호가 일치하지 않습니다.");
						</script>
					</c:if>
					<c:if test="${message == 'logout' }">
						<script>
							alert("로그아웃 처리되었습니다.");
						</script>
					</c:if>

					<div>
						<a href="${path}/pu/login/user/signup.do" style="color: #3f51b5;">회원가입</a>
						<br>
					</div>

					<br> <br>
					<div class="copy-text">
						아이디/비밀번호를 잃어버리셨나요?<i style="color: #68A4C4;" class="fa fa-heart"></i><br>
						<a onclick="find()" href="#" style="color: #3f51b5;">아이디/비밀번호 찾기</a>
						<br>
					</div>
				</div>
				
				<div class="col-md-8 banner-sec">
					<div id="carouselExampleIndicators" class="carousel slide"
						data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0"
								class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<img class="d-block img-fluid"
									src="https://static.pexels.com/photos/33972/pexels-photo.jpg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>This is Heaven</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut labore et dolore
											magna aliqua. Ut enim ad minim veniam, quis nostrud
											exercitation</p>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid"
									src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>This is Heaven</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut labore et dolore
											magna aliqua. Ut enim ad minim veniam, quis nostrud
											exercitation</p>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid"
									src="https://static.pexels.com/photos/33972/pexels-photo.jpg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>This is Heaven</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											elit, sed do eiusmod tempor incididunt ut labore et dolore
											magna aliqua. Ut enim ad minim veniam, quis nostrud
											exercitation</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
</body>
<script type="text/javascript">
function OpenWin(URL, width, height) {
	var str, width, height;
	str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
	str = str + "width=" + width;
	str = str + ",height=" + height + "',top=50,left=50";
	window.open(URL, 'id', str);
}
</script>
</html>