<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
function sample6_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('sample6_address').value = fullAddr;

            // 커서를 상세주소 필드로 이동한다.
            document.getElementById('sample6_address2').value = "";
            document.getElementById('sample6_address2').focus();
        }
    }).open();
}

	function onlyNumber(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105)
				|| keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			return false;
	}
	function removeChar(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
	}

	$(function() {
		var gender1 = $("#gender1").val();
		if(gender1=="1"){
			document.getElementById("gender").value = "남자";
		}else{
			document.getElementById("gender").value = "여자";
		}
	});
	function modify(){
		var username = $("#username").val();
		var birthyear = $("#birthyear").val();
		var birthmonth = $("#birthmonth").val();
		var birthday = $("#birthday").val();
		var phone1 = $("#phone1").val();
		var phone2 = $("#phone2").val();
		var phone3 = $("#phone3").val();
		var zipcode = $("#zipcode").val();
		var add1 = $("#add1").val();
		var add2 = $("#add2").val();
		var gender = $("#gender").val();
		
		if(gender == "남자"){
			document.getElementById("gender").value = "1";
		}else if(gender == "여자"){
			document.getElementById("gender").value = "2";
		}else{
			alert("성별을 다시 입력해주세요.");
			return;
		}
		if (username == "") {
			alert("이름을 입력하세요");
			$("#username").focus();
			return;
		}
		if (birthyear == "") {
			alert("태어난 연도를 선택하세요");
			$("#birthyear").focus();
			return;
		}
		if (birthmonth == "") {
			alert("태어난 월을 선택하세요");
			$("#birthmonth").focus();
			return;
		}
		if (birthday == "") {
			alert("태어난 일을 선택하세요");
			$("#birthday").focus();
			return;
		}
		if (gender == "") {
			alert("성별을 입력하세요");
			$("#gender").focus();
			return;
		}
		if (phone1 == "") {
			alert("전화번호를 입력하세요");
			$("#phone1").focus();
			return;
		}
		if (phone2 == "") {
			alert("전화번호를 입력하세요");
			$("#phone2").focus();
			return;
		}
		if (phone3 == "") {
			alert("전화번호를 입력하세요");
			$("#phone3").focus();
			return;
		}
		if (zipcode == "") {
			alert("주소를 입력하세요");
			$("#zipcode").focus();
			return;
		}
		if (add1 == "") {
			alert("주소를 입력하세요");
			$("#add1").focus();
			return;
		}
		if (add2 == "") {
			alert("주소를 입력하세요");
			$("#add2").focus();
			return;
		}
		if(confirm("회원정보를 수정하시겠습니까?")){
			document.form1.action = "${path}/pu/page/update.do";
			document.form1.submit();
			alert("회원정보가 수정되었습니다.");
		}return;
	}
</script>
</head>

<style>
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>


<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;"><span style="font-size: 20px; ">
                <br>나만의 도서관</span></a></li>
                <li><a href="${path}/hjh/mypage/nowrent.do"> 대출현황관리</a></li>
                <li><a href="${path}/hjh/mypage/beforerent.do"> 대출이력조회</a></li>
                <li><a href="${path}/hjh/book/cart/list.do"> 관심도서목록</a></li>
                <li><a href="${path}/hjh/mypage/wishlist.do">희망도서신청목록</a></li>
                <li><a href="${path}/pu/page/pw.do">비밀번호변경</a></li>
                <li><a href="${path}/pu/page/list.do">회원정보수정</a></li>
                <li><a href="${path}/pu/page/bye.do">회원탈퇴</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

	
	
	
	
	
	<h2>회원정보수정</h2>
	
	<form id="form1" name="form1" method="post">
	
		<label>아이디</label>
		<input type="text" id="userid" name="userid" value="${dto.userid}" readonly>
		<br>
		
		<label>성명</label>
		<input type="text" id="username" name="username" value="${dto.username}">
		<br>
		
		<label>생년월일</label>
		
	<%-- <input type="text" id="birthyear" name="birthyear" value="${dto.username}">
<input type="text" id="birthmonth" name="birthmonth" value="${dto.username}">
<input type="text" id="birthday" name="birthday" value="${dto.username}"> --%>

		<select id="birthyear" name="birthyear">
			<option value="">년도</option>
			<c:forEach var="row" begin="1920" end="2018">
				<option value="${row}" 
				<c:out value="${dto.birthyear == row ? 'selected' :'' }" />>${row}년</option>
			</c:forEach>
		</select>
		
		<select id="birthmonth" name="birthmonth">
			<option value="" >월</option>
			<c:forEach var="row" begin="1" end="12">
				<option value="${row}"
				<c:out value="${dto.birthmonth == row ? 'selected' :'' }" />>${row}월</option>
			</c:forEach>
		</select>
		
		<select id="birthday" name="birthday">
			<option value="">일</option>
			<c:forEach var="row" begin="1" end="31">
				<option value="${row}"
				<c:out value="${dto.birthday == row ? 'selected' :'' }" />>${row}일</option>
			</c:forEach>
		</select>
		<br>
		
		<label>성별</label>
		<input type="hidden" id="gender1" name="gender1" value="${dto.gender}">
		<input type="text" id="gender" name="gender">
		<br>
		
		<label>연락처</label>
		<select id="phone1" name="phone1">
			<option value="">전화번호</option>
			<option value="010"
			<c:out value="${dto.phone1 == '010' ? 'selected' :'' }" />>010</option>
			<option value="011"
			<c:out value="${dto.phone1 == '011' ? 'selected' :'' }" />>011</option>
			<option value="016"
			<c:out value="${dto.phone1 == '016' ? 'selected' :'' }" />>016</option>
			<option value="019"
			<c:out value="${dto.phone1 == '019' ? 'selected' :'' }" />>019</option>
		</select>
		
		<input type="text" id="phone2" name="phone2" value="${dto.phone2}"
			onkeydown='return onlyNumber(event)' onkeyup='removeChar(event)'
			style='ime-mode: disabled;'
			onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			maxlength="4">
			
		<input type="text" id="phone3" name="phone3"
			value="${dto.phone3}" onkeydown='return onlyNumber(event)'
			onkeyup='removeChar(event)' style='ime-mode: disabled;'
			onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
			maxlength="4">
			<br>
			
		<label>이메일</label>
		<input type="text" id="email1" name="email1" readonly value="${dto.email1}@${dto.email2}" />
		<br>
		
		<label>우편번호</label>
		<input type="text" id="sample6_postcode" name="zipcode"
			value="${dto.zipcode}" readonly>
			
		<input type="button" onclick="sample6_execDaumPostcode()" value="우편번호 찾기">
		<br>
		
		<label>주소</label>
		<input type="text" id="sample6_address" name="add1" value="${dto.add1}">
		<br>
		
		<label>상세주소</label>
		<input type="text" id="sample6_address2" name="add2" value="${dto.add2}">
		<br>
		
		<input type="button" value="회원정보수정" onclick="modify()">
	</form>
	
	
	
	
	
	

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>

</body>
</html>