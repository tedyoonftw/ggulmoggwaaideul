<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<script>
$(function() {
	$("input[id=newpw2]").keyup(function() {
		var newpw = document.getElementById("newpw").value;
		var newpw2 = document.getElementById("newpw2").value;
		if (newpw == newpw2) {
			red = "";
			$("#red").html(red);
			blue = "비밀번호가 일치합니다.";
			$("#blue").html(blue);
		} else if (newpw != newpw2) {
			blue = "";
			$("#blue").html(blue);
			red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
			$("#red").html(red);
		}
	});
	$("input[id=newpw]").keyup(function() {
		var newpw = document.getElementById("newpw").value;
		var newpw2 = document.getElementById("newpw2").value;
		if (newpw == newpw2) {
			red = "";
			$("#red").html(red);
			blue = "비밀번호가 일치합니다.";
			$("#blue").html(blue);
		} else if (newpw != newpw2) {
			blue = "";
			$("#blue").html(blue);
			red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
			$("#red").html(red);
		}
	});
});

function pw_re() {
	var newpw=$("#newpw").val();
	var newpw2=$("#newpw2").val();
	
	if (newpw == "") {
		alert("새로운 비밀번호를 입력해주세요.");
		$("#newpw").focus();
		return;
	}
	if (newpw2 == "") {
		alert("새로운 비밀번호를 입력해주세요.");
		$("#newpw2").focus();
		return;
	}
	
	var pw_passed = true;
	var id = $("#userid").val();
	pw_passed = true;
	var pattern1 = /[0-9]/;
	var pattern2 = /[a-zA-Z]/;
	var pattern3 = /[~!@\#$%<>^&*]/;
	var pw_msg = "";
	if (newpw != newpw2) {
		alert("비밀번호가 일치하지 않습니다.");
		$("#newpw").focus();
		return false;
	}
	if (!pattern1.test(newpw) || !pattern2.test(newpw)
			|| !pattern3.test(newpw) || newpw.length<8||newpw.length>21) {
		alert("비밀번호는 영문,숫자,특수기호를 포함하여 8자리 이상, 20자리 이하로 구성해주세요.");
		$("#newpw").focus();
		return false;
	}
	if (newpw.indexOf(id) > -1) {
		alert("비밀번호는 ID를 포함할 수 없습니다.");
		$("#newpw").focus();
		return false;
	}

	var SamePass_0 = 0;
	var SamePass_1 = 0;
	var SamePass_2 = 0;
	for (var i = 0; i < newpw.length; i++) {
		var chr_pass_0;
		var chr_pass_1;
		var chr_pass_2;
		if (i >= 2) {
			chr_pass_0 = newpw.charCodeAt(i - 2);
			chr_pass_1 = newpw.charCodeAt(i - 1);
			chr_pass_2 = newpw.charCodeAt(i);
			if ((chr_pass_0 == chr_pass_1) && (chr_pass_1 == chr_pass_2)) {
				SamePass_0++;
			} else {
				SamePass_0 = 0;
			}
			if (chr_pass_0 - chr_pass_1 == 1
					&& chr_pass_1 - chr_pass_2 == 1) {
				SamePass_1++;
			} else {
				SamePass_1 = 0;
			}
			if (chr_pass_0 - chr_pass_1 == -1
					&& chr_pass_1 - chr_pass_2 == -1) {
				SamePass_2++;
			} else {
				SamePass_2 = 0;
			}
		}

		if (SamePass_0 > 0) {
			alert("동일문자를 3자 이상 연속 입력할 수 없습니다.");
			$("#newpw").focus();
			pw_passed = false;
		}

		if (SamePass_1 > 0 || SamePass_2 > 0) {
			alert("영문, 숫자는 3자 이상 연속 입력할 수 없습니다.");
			$("#newpw").focus();
			pw_passed = false;
		}

		if (!pw_passed) {
			$("#newpw").focus();
			return false;
			break;
		}
	}true;
	document.form1.action = "${path}/pu/page/pw_re.do";
	document.form1.submit();
	alert("새로운 비밀번호가 발급되었습니다.");
}
</script>
</head>
<body>
<%@ include file="../../include/menu.jsp"%>
<h2>비밀번호 재발급</h2>
<form id="form1" name="form1" method="post">
<input type="hidden" id="userid" name="userid" value="${userid }">
<label>새 비밀번호</label>
<input type="password" id="newpw" name="passwd">
<br>
<label>새 비밀번호 확인</label>
<input type="password" id="newpw2" name="newpw2">
<br>
<div>비밀번호는 영문,숫자,특수기호를 포함하여 8자리 이상, 20자리 이하로 구성해주세요.</div>
<div id="red" style="color: red;"></div>
<div id="blue" style="color: blue;"></div>
<br>
<input type="button" onclick="pw_re()" value="비밀번호 재발급">
</form>
</body>
</html>