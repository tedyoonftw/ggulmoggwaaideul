<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp"%>
<style type="text/css">
@import
	url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css")
	;

.login-block {
	background: #3F51B5; /* fallback for old browsers */
	background: -webkit-linear-gradient(to bottom, #FFB88C, #DE6262);
	/* Chrome 10-25, Safari 5.1-6 */
	background: linear-gradient(to bottom, rgba(242, 237, 233, 0.26),
		rgba(186, 196, 208, 0.44));
	/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	float: left;
	width: 100%;
	padding: 50px 0;
}

.banner-sec {
	background: url(https://static.pexels.com/photos/33972/pexels-photo.jpg)
		no-repeat left bottom;
	background-size: cover;
	min-height: 500px;
	border-radius: 0 10px 10px 0;
	padding: 0;
}

.container {
	background: #fff;
	border-radius: 10px;
	box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1);
}

.carousel-inner {
	border-radius: 0 10px 10px 0;
}

.carousel-caption {
	text-align: left;
	left: 5%;
}

.login-sec {
	padding: 50px 150px;
	position: relative;
}

.login-sec .copy-text {
	position: absolute;
	width: 80%;
	bottom: 20px;
	font-size: 13px;
	text-align: center;
}

.login-sec .copy-text i {
	color: #FEB58A;
}

.login-sec .copy-text a {
	color: #E36262;
}

.login-sec h2 {
	margin-bottom: 30px;
	font-weight: 800;
	font-size: 30px;
	color: #3F51B5;
}

.login-sec h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #68A4C4;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
	margin-left: auto;
	margin-right: auto
}

.btn-login {
	background: #3F51B5;
	color: #fff;
	font-weight: 600;
}

.banner-text {
	width: 70%;
	position: absolute;
	bottom: 40px;
	padding-left: 20px;
}

.banner-text h2 {
	color: #fff;
	font-weight: 600;
}

.banner-text h2:after {
	content: " ";
	width: 100px;
	height: 5px;
	background: #FFF;
	display: block;
	margin-top: 20px;
	border-radius: 3px;
}

.banner-text p {
	color: #fff;
}
</style>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>

function sample6_execDaumPostcode() {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = ''; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                fullAddr = data.roadAddress;

            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                fullAddr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
            if(data.userSelectedType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
            document.getElementById('sample6_address').value = fullAddr;

            // 커서를 상세주소 필드로 이동한다.
            document.getElementById('sample6_address2').focus();
        }
    }).open();
}

	function onlyNumber(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105)
				|| keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			return false;
	}
	function removeChar(event) {
		event = event || window.event;
		var keyID = (event.which) ? event.which : event.keyCode;
		if (keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39)
			return;
		else
			event.target.value = event.target.value.replace(/[^0-9]/g, "");
	}

	$(function() {
		$("#email3").change(function() {
			var email = $("#email3").val();
			$("#email2").val(email);
		});
	});

	function confirm() {
		var userid = $("#userid").val();
		var pattern1 = /[0-9]/;
		var pattern2 = /[a-zA-Z]/;
		if (userid == "") {
			alert("아이디를 입력하세요");
			$("#userid").focus();
			return;
		} else if (!pattern1.test(userid) || !pattern2.test(userid)
				|| userid.length > 17) {
			alert("아이디는 영문,숫자를 포함하여 16자리 이하로 구성해주세요.");
			$("#userid").focus();
			return;
		} else {
			document.form1.action = "${path}/pu/login/user/check_id.do";
			document.form1.submit();
		}
	}

	$(function() {
		$("#gender1").click(function() {
			var number = $("#gender1").val();
			$("#choose").val("1");
			console.log($("#choose").val());
		});

		$("input[name=userid]").keyup(function(event) {
			if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
				var inputVal = $(this).val();
				$(this).val(inputVal.replace(/[^a-z0-9]/gi, ''));
			}
		});

		$("input[name=email1]").keyup(function(event) {
			if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
				var inputVal = $(this).val();
				$(this).val(inputVal.replace(/[^a-z0-9]/gi, ''));
			}
		});
		$("input[name=email2]").keyup(function(event) {
			if (!(event.keyCode >= 37 && event.keyCode <= 40)) {
				var inputVal = $(this).val();
				$(this).val(inputVal.replace(/[^a-z0-9]/gi, ''));
			}
		});
	});

	function gender2() {
		$("#choose").val("2");
		var choose = $("#choose");
		console.log(choose);
	}

	$(function() {
		$("input[id=passwd2]").keyup(function() {
			var passwd = document.getElementById("passwd").value;
			var passwd2 = document.getElementById("passwd2").value;
			if (passwd == passwd2) {
				red = "";
				$("#red").html(red);
				blue = "비밀번호가 일치합니다.";
				$("#blue").html(blue);
			} else if (passwd != passwd2) {
				blue = "";
				$("#blue").html(blue);
				red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
				$("#red").html(red);
			}
		});
		$("input[id=passwd]").keyup(function() {
			var passwd = document.getElementById("passwd").value;
			var passwd2 = document.getElementById("passwd2").value;
			if (passwd == passwd2) {
				red = "";
				$("#red").html(red);
				blue = "비밀번호가 일치합니다.";
				$("#blue").html(blue);
			} else if (passwd != passwd2) {
				blue = "";
				$("#blue").html(blue);
				red = "비밀번호가 일치하지 않습니다. 다시 확인해주세요.";
				$("#red").html(red);
			}
		});
	});

	function signup() {
		var userid = $("#userid").val();
		var check = $("#check").val();
		var username = $("#username").val();
		var birthyear = $("#birthyear").val();
		var birthmonth = $("#birthmonth").val();
		var birthday = $("#birthday").val();
		var choose = $("#choose").val();
		var phone1 = $("#phone1").val();
		var phone2 = $("#phone2").val();
		var phone3 = $("#phone3").val();
		var email1 = $("#email1").val();
		var email2 = $("#email2").val();
		var zipcode = $("#zipcode").val();
		var add1 = $("#add1").val();
		var add2 = $("#add2").val();
		var passwd = document.getElementById("passwd").value;
		var passwd2 = document.getElementById("passwd2").value;

		if (userid == "") {
			alert("아이디를 입력하세요");
			$("#userid").focus();
			return;
		}

		if (check == "") {
			alert("아이디 중복여부를 확인해주세요");
			$("#userid").focus();
			return;
		}

		if (passwd == "") {
			alert("비밀번호를 입력하세요");
			$("#passwd").focus();
			return;
		}
		if (passwd2 == "") {
			alert("비밀번호를 입력하세요.");
			$("#passwd2").focus();
			return;
		}
		if (username == "") {
			alert("이름을 입력하세요");
			$("#username").focus();
			return;
		}
		if (birthyear == "") {
			alert("태어난 연도를 선택하세요");
			$("#birthyear").focus();
			return;
		}
		if (birthmonth == "") {
			alert("태어난 월을 선택하세요");
			$("#birthmonth").focus();
			return;
		}
		if (birthday == "") {
			alert("태어난 일을 선택하세요");
			$("#birthday").focus();
			return;
		}
		if (choose == "") {
			alert("성별을 선택하세요");
			$("#gender1").focus();
			return;
		}
		if (phone1 == "") {
			alert("전화번호를 입력하세요");
			$("#phone1").focus();
			return;
		}
		if (phone2 == "") {
			alert("전화번호를 입력하세요");
			$("#phone2").focus();
			return;
		}
		if (phone3 == "") {
			alert("전화번호를 입력하세요");
			$("#phone3").focus();
			return;
		}
		if (email1 == "") {
			alert("이메일을 입력하세요");
			$("#email1").focus();
			return;
		}
		if (email2 == "") {
			alert("이메일을 입력하세요");
			$("#email2").focus();
			return;
		}
		if (zipcode == "") {
			alert("주소를 입력하세요");
			$("#zipcode").focus();
			return;
		}
		if (add1 == "") {
			alert("주소를 입력하세요");
			$("#add1").focus();
			return;
		}
		if (add2 == "") {
			alert("주소를 입력하세요");
			$("#add2").focus();
			return;
		}

		var pw_passed = true;
		var id = document.getElementById("userid").value;
		pw_passed = true;
		var pattern1 = /[0-9]/;
		var pattern2 = /[a-zA-Z]/;
		var pattern3 = /[~!@\#$%<>^&*]/;
		var pw_msg = "";
		if (passwd != passwd2) {
			alert("비밀번호가 일치하지 않습니다.");
			$("#passwd").focus();
			return false;
		}
		if (!pattern1.test(passwd) || !pattern2.test(passwd)
				|| !pattern3.test(passwd) || passwd.length<8||passwd.length>21) {
			alert("비밀번호는 영문,숫자,특수기호를 포함하여 8자리 이상, 20자리 이하로 구성해주세요.");
			$("#passwd").focus();
			return false;
		}
		if (passwd.indexOf(id) > -1) {
			alert("비밀번호는 ID를 포함할 수 없습니다.");
			$("#passwd").focus();
			return false;
		}

		var SamePass_0 = 0;
		var SamePass_1 = 0;
		var SamePass_2 = 0;
		for (var i = 0; i < passwd.length; i++) {
			var chr_pass_0;
			var chr_pass_1;
			var chr_pass_2;
			if (i >= 2) {
				chr_pass_0 = passwd.charCodeAt(i - 2);
				chr_pass_1 = passwd.charCodeAt(i - 1);
				chr_pass_2 = passwd.charCodeAt(i);
				if ((chr_pass_0 == chr_pass_1) && (chr_pass_1 == chr_pass_2)) {
					SamePass_0++;
				} else {
					SamePass_0 = 0;
				}
				if (chr_pass_0 - chr_pass_1 == 1
						&& chr_pass_1 - chr_pass_2 == 1) {
					SamePass_1++;
				} else {
					SamePass_1 = 0;
				}
				if (chr_pass_0 - chr_pass_1 == -1
						&& chr_pass_1 - chr_pass_2 == -1) {
					SamePass_2++;
				} else {
					SamePass_2 = 0;
				}
			}

			if (SamePass_0 > 0) {
				alert("동일문자를 3자 이상 연속 입력할 수 없습니다.");
				$("#passwd").focus();
				pw_passed = false;
			}

			if (SamePass_1 > 0 || SamePass_2 > 0) {
				alert("영문, 숫자는 3자 이상 연속 입력할 수 없습니다.");
				$("#passwd").focus();
				pw_passed = false;
			}

			if (!pw_passed) {
				$("#passwd").focus();
				return false;
				break;
			}
		}
		true;
		document.form1.action = "${path}/pu/login/user/insert.do";
		document.form1.submit();
		alert("회원가입이 완료되었습니다.");
	}
</script>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
</head>
<body>
	<%@ include file="../../include/menu.jsp"%>


	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->

	<section class="login-block">
		<div class="container">
			<div class="row">
				<div class="login-sec">
					<h2 class="text-center">회원가입</h2>

					<form class="login-form" id="form1" name="form1" method="post">
						<div class="row">
							<div class="col-xs-6 col-md-6">
								<input type="text" name="userid" id="userid"
									value="${dto.userid}" class="form-control input-lg"
									placeholder="아이디" maxlength="16" /> <br>
							</div>
							<div class="col-xs-6 col-md-6">
								<input type="button" id="btnConfirm" onclick="confirm()"
									class="btn btn-login float-right" value="중복확인">
								<c:if test="${message == null }">
									<div style="color: black;">영문,숫자 포함 16자리 이하만 가능합니다.</div>
								</c:if>
								<c:if test="${message == 'use' }">
									<div style="color: blue;">사용가능한 아이디입니다.</div>
								</c:if>
								<c:if test="${message == 'overlap' }">
									<div style="color: red;">중복된 아이디 입니다.</div>
								</c:if>
								<br>
							</div>

							<input type="hidden" id="check" name="check" value="${message}">
							<br>

							<div class="col-xs-6 col-md-6">
								<input type="password" name="passwd" id="passwd"
									value="${dto.passwd}" class="form-control input-lg"
									placeholder="비밀번호" maxlength="20" />
							</div>
							<div class="col-xs-6 col-md-6">
								<input type="password" name="passwd2" id="passwd2"
									value="${dto.passwd}" class="form-control input-lg"
									placeholder="비밀번호 확인" />
							</div>
						</div>

						<div id="red" style="color: red;"></div>
						<div id="blue" style="color: blue;"></div>
						<br> <input type="text" name="username" id="username"
							value="${dto.username}" class="form-control input-lg"
							placeholder="성명" /> <br>

						<div class="row">
							<div class="col-xs-4 col-md-4">
								<select id="birthyear" name="birthyear"
									class="form-control input-lg">
									<option value="">년도</option>
									<c:forEach var="row" begin="1920" end="2018">
										<option value="${row}" 
										<c:out value="${dto.birthyear == row ? 'selected' :'' }" />>${row}년</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-xs-4 col-md-4">
								<select id="birthmonth" name="birthmonth"
									class="form-control input-lg">
									<option value="" >월</option>
										<c:forEach var="row" begin="1" end="12">
											<option value="${row}"
											<c:out value="${dto.birthmonth == row ? 'selected' :'' }" />>${row}월</option>
										</c:forEach>
								</select>
							</div>
							<div class="col-xs-4 col-md-4">
								<select id="birthday" name="birthday"
									class="form-control input-lg">
									<option value="">일</option>
										<c:forEach var="row" begin="1" end="31">
											<option value="${row}"
											<c:out value="${dto.birthday == row ? 'selected' :'' }" />>${row}일</option>
										</c:forEach>
								</select> <br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-6 col-md-6">
								<input type="button" id="gender1"
									class="btn btn-lg btn-primary btn-block signup-btn" value="남성"
									style="border-color: #3F51B5; background-color: #fff; font-size: 18px; color: black;" />
							</div>
							<div class="col-xs-6 col-md-6">
								<input type="button" id="gender"
									class="btn btn-lg btn-primary btn-block signup-btn" value="여성"
									style="border-color: #3F51B5; background-color: #fff; font-size: 18px; color: black;"
									onclick="gender2()" />
							</div>
						</div>

						<input type="hidden" id="choose" name="gender" value="${dto.gender}"> <br>
						<div class="row">
						
							<div class="col-xs-4 col-md-4">
								<select id="phone1" name="phone1" class="form-control input-lg">
									<option value="">전화번호</option>
									<option value="010"
										<c:out value="${dto.phone1 == '010' ? 'selected' :'' }" />>010</option>
									<option value="011"
										<c:out value="${dto.phone1 == '011' ? 'selected' :'' }" />>011</option>
									<option value="016"
										<c:out value="${dto.phone1 == '016' ? 'selected' :'' }" />>016</option>
									<option value="019"
										<c:out value="${dto.phone1 == '019' ? 'selected' :'' }" />>019</option>
								</select>
							</div>
							<div class="col-xs-4 col-md-4">
								<input type="text" id="phone2" name="phone2"
									value="${dto.phone2}" class="form-control input-lg"
									placeholder="" onkeydown='return onlyNumber(event)'
									onkeyup='removeChar(event)' style='ime-mode: disabled;'
									onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
									maxlength="4">
							</div>
							<div class="col-xs-4 col-md-4">
								<input type="text" id="phone3" name="phone3"
									value="${dto.phone3}" class="form-control input-lg"
									onkeydown='return onlyNumber(event)'
									onkeyup='removeChar(event)' style='ime-mode: disabled;'
									onKeypress="if (!(event.keyCode > 47 && event.keyCode < 58)) event.returnValue = false;"
									maxlength="4"><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-4 col-md-4">
								<input type="text" id="email1" name="email1"
									value="${dto.email1}" class="form-control input-lg"
									placeholder="이메일" />
							</div>

							<div class="col-xs-4 col-md-4">
								<input type="text" id="email2" name="email2"
									class="form-control input-lg" value="${dto.email2 }"
									placeholder="" />
							</div>
							<div class="col-xs-4 col-md-4">
								<select id="email3" class="form-control input-lg">
									<option value="">직접입력</option>
									<option value="daum.net"
									<c:out value="${dto.email2 == 'daum.net' ? 'selected' :'' }" />>daum.net</option>
									<option value="gmail.com"
									<c:out value="${dto.email2 == 'gmail.com' ? 'selected' :'' }" />>gmail.com</option>
									<option value="naver.com"
									<c:out value="${dto.email2 == 'naver.com' ? 'selected' :'' }" />>naver.com</option>
									<option value="nate.com"
									<c:out value="${dto.email2 == 'nate.com' ? 'selected' :'' }" />>nate.com</option>
								</select> <br>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-6 col-md-6">
								<input type="text" id="sample6_postcode" name="zipcode"
									value="${dto.zipcode}" readonly class="form-control input-lg"
									placeholder="우편번호">
							</div>
							<div class="col-xs-6 col-md-6">
								<input type="button" class="btn btn-login float-right"
									onclick="sample6_execDaumPostcode()" value="우편번호 찾기">
							</div>
						</div>
						<br> <input type="text" id="sample6_address" name="add1"
							value="${dto.add1}" class="form-control input-lg"
							placeholder="주소"> <input type="text"
							id="sample6_address2" name="add2" value="${dto.add2}"
							class="form-control input-lg" placeholder="상세주소"> <br>
							
						<input style="border-color: #3F51B5; background-color: #3F51B5"
							class="btn btn-lg btn-primary btn-block signup-btn" type="button"
							value="가입하기" onclick="javascript:signup()"> <br>
					</form>


				</div>
			</div>
		</div>
	</section>
</body>
</html>