<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
</script>
</head>


<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 40px;">
                <span style="font-size: 20px; ">
                <br> 관리자</span></a></li>
                <li><a href="${path}/ym/membership/list.do"> 회원정보관리</a></li>
                <li><a href="${path}/project/admin_readingroom/bigcalview.do"> 일정관리</a></li>
                <li><a href="${path}/project/admin_readingroom/admin_list.do"> 열람실관리</a></li>
                <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
                <li><a href="${path}/hjh/book/book/book_insert.do">도서등록</a></li>
                <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
                <li><a href="${path}/chart/book_list2.do">도서차트1</a></li>
                <li><a href="${path}/chart/book_list3.do">도서차트2</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>




<h2>희망도서신청목록</h2>
<table border="1" width="700">
	<tr>
		<th>번호</th>
		<th>아이디</th>
		<th>신청일자</th>
		<th>신청도서명</th>
		<th>처리상태</th>
		<th>처리</th>
	</tr>
<c:forEach var="row" items="${wlist}">
	<tr>
		<td>${row.hpnum}</td>
		<td>${row.userid}</td>
		<td><fmt:formatDate value="${row.hpdate}"
			pattern="yyyy-MM-dd" /></td>
		<td><a href="${path}/hjh/mypage/detail/${row.hpnum}">
				${row.title}</a></td>               
		<td>
		<c:choose>
			<c:when test="${row.wstatus == 0}">
				<span style="color:red;">처리중</span>
			</c:when>
			<c:otherwise>
				<span style="color:green;">처리완료</span>
			</c:otherwise>
		</c:choose>
		</td>
		<td><input type="button" value="처리" onclick="location.href='${path}/hjh/mypage/proceed.do?hpnum=${row.hpnum}&userid=${row.userid}'" /></td>
</c:forEach>

</table>

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>