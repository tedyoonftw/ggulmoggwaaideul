<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnWrtie").click(function(){
		var title=$("#title").val();
		var author=$("#author").val();
		var publisher=$("#publisher").val();
		var isbn=$("#isbn").val();
		if(title == ""){
			alert("희망도서명을 입력하세요.")
			$("#title").focus();
			return;
		}
		if(author == ""){
			alert("저자를 입력하세요.")
			$("#author").focus();
			return;
		}
		if(publisher == ""){
			alert("출판사를 입력하세요.")
			$("#publisher").focus();
			return;
		}
		if(isbn == ""){
			alert("isbn을 입력하세요.")
			$("#isbn").focus();
			return;
		}
		document.form1.action="${path}/hjh/mypage/insert.do";
		document.form1.submit();
	});
});
</script>
</head>

<style>
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>


<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 		사이드바		 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
       <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;"><span style="font-size: 20px; ">
                <br>나만의 도서관</span></a></li>
                <li><a href="${path}/hjh/mypage/nowrent.do"> 대출현황관리</a></li>
                <li><a href="${path}/hjh/mypage/beforerent.do"> 대출이력조회</a></li>
                <li><a href="${path}/hjh/book/cart/list.do"> 관심도서목록</a></li>
                <li><a href="${path}/hjh/mypage/wishlist.do">희망도서신청목록</a></li>
                <li><a href="${path}/pu/page/pw.do">비밀번호변경</a></li>
                <li><a href="${path}/pu/page/list.do">회원정보수정</a></li>
                <li><a href="${path}/pu/page/bye.do">회원탈퇴</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>




<h2>희망도서신청 (신청하기)</h2>

*필수입력
<form name="form1" method="post">
<table border="1" width="70%">
	<tr>
		<td>신청일자</td>
		<td><input id="hpdate" name="hpdate"
			 value="${hpdate}" /></td>
	</tr>
	<tr>
		<td>신청자*</td>
		<td><input id="userid" name="userid"
			value="${sessionScope.userid}" /></td>
	</tr>
	<tr>
		<td>희망도서명*</td>
		<td><input id="title" name="title"></td>
		
	</tr>
	<tr>
		<td>저자*</td>
		<td><input id="author" name="author"></td>
	</tr>
	<tr>
		<td>출판사*</td>
		<td><input id="publisher" name="publisher"></td>
	</tr>
	<tr>
		<td>ISBN*</td>
		<td><input id="isbn" name="isbn"></td>
	</tr>
	<tr>
		<td>출판년</td>
		<td><input name="pubyear"></td>
	</tr>
	<tr>	
		<td>추천의견(최대100자)</td>
		<td><textarea id="content" name="content"
			row="10" cols="80"></textarea></td>
</table>
<input type="button" id="btnWrtie" value="신청하기">
<input type="button" id="btnReturn" value="취소">
</form>


<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>