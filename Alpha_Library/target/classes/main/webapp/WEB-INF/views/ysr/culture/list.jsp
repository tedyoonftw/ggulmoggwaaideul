<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	var cno=$("#cno").val();
	var title=$("#title").val();
	$("#btnWrite").click(function(){
		location.href="${path}/culture/write.do";
	});
	$("#btnApply").click(function(){
		location.href="${path}/applicant/write.do?title="+title+"&cno="+cno;
	});
});
function list(page) {
	location.href="${path}/culture/list.do?curPage="+page;
}

function letsgo(title,cno){
	
	location.href="${path}/applicant/write.do?title="+title+"&cno="+cno;
}
</script>
</head>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 





<h2>문화프로그램</h2>
<!--					 테이블 					-->
<div class="container" style="width: 900px; height: 0px;">
<div class="row">
<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
<div class="table-responsive">


<table class="table table-bordred table-striped" >
	<thead>
		<th>번호</th>
		<th>강좌명</th>
		<th>대상</th>
		<th>마감기간</th>
		<th>신청현황</th>
		<th>상태</th>
	</thead>
<c:forEach var="row" items="${map.list}">
<tbody>
	<tr>
		<td>${row.cno}</td>
		<td><a href="${path}/culture/view.do?cno=${row.cno}">
			${row.title}</a></td>
		<td>${row.target}</td>
		<td><%-- <fmt:formatDate value="${row.duedate}" 
			pattern="yyyy-MM-dd"/> --%>
			${row.duedate }
		</td>
		<td>
		<c:if test="${sessionScope.adminid=='admin'}">
		<a href="${path}/ym/adminapplicant/list.do?cno=${row.cno}">${row.cnt} / ${row.max}</a>
		</c:if>
		<c:if test="${sessionScope.userid!=null}">
		${row.cnt} / ${row.max}
		</c:if>
		</td>
		<td>
			<c:if test="${row.cnt < row.max}">
				<c:if test="${sessionScope.userid == null}">모집중</c:if>
				<c:if test="${sessionScope.userid != null }">
					<a href="javascript:letsgo('${row.title}','${row.cno}')">신청</a>
				</c:if>
			</c:if>
			<c:if test="${row.cnt >= row.max}">마감</c:if>
		</td>
	</tr>
	<input type="hidden" value="${row.cno}" id="cno" name="cno">
	<input type="hidden" value="${row.title}" id="title" name="title">
	<input type="hidden" value="${sessionScope.userid}" id="userid" name="userid">
</tbody>
</c:forEach>
<tr>
		<td colspan="6" align="center">
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('${map.pager.prevPage}')">[이전]</a>
			</c:if>
			<c:forEach var="num" begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
						<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
						<span style="color:red;">${num}</span>
					</c:when>
					<c:otherwise>
						<a href="#" onclick="list('${num}')">${num}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${map.pager.curBlock < map.pager.totBlock}">
				<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage < map.pager.totPage}">
				<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>
		</td>
	</tr>
</table>
<form name="form1" method="post"
	action="${path}/culture/list.do">
	<select name="search_option">
		<option value="title"
			<c:if test="${map.search_option == 'title'}">selected</c:if>
		>강좌명</option>
		<option value="target"
			<c:if test="${map.search_option == 'target'}">selected</c:if>
		>대상</option>
		<option value="all"
			<c:if test="${map.search_option == 'all' }">selected</c:if>
		>전체</option>
	</select>
	<input name="keyword" value="${map.keyword}">
	<input type="submit" value="검색">
	<c:if test="${sessionScope.adminid != dto.adminid}">
	<button type="button" id="btnWrite" >글쓰기</button>
	</c:if>
</form>


<c:if test="${message == 'error' }">
		<script>
			alert("이미 신청하신 강좌입니다.");
		</script>
</c:if>



   
</div>
</div>
</div>
</div>
<!-- 					테이블 끝		 				-->  



<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>