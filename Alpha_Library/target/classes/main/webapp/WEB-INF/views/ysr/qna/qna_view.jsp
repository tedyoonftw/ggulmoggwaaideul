<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script src="${path}/include/js/common.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<!-- include summernote css/js -->
<link href="${path}/summernote/summernote.css" rel="stylesheet">
<script src="${path}/summernote/summernote.js"></script>
<script>
$(function(){
	
	listReply2();
	
	$("#content").summernote({
		height: 300,
	    weight: 1200,
	    toolbar: "hide"
	});
	$("#btnReply").click(function(){
		var replytext=$("#replytext").val();
		var qno="${dto.qno}";
		var param="replytext="+replytext+"&qno="+qno;
		$.ajax({
			type: "post",
			url: "${path}/reply/insert.do",
			data: param,
			success: function(){
				alert("댓글이 등록되었습니다.");
				listReply2();
			}
		});
	});
	
	$("#btnList").click(function(){
		location.href="${path}/qna/list.do";
	});
	$("#btnUpdate").click(function(){
		var str="";
		$("#uploadedList .file").each(function(i){
			str+=
				"<input type='hidden' name='files["+i+"]' value='"
				+$(this).val()+"'>";
		});
		$("#form1").append(str);
		document.form1.action="${path}/qna/update.do";
		document.form1.submit();
	});
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")){
			document.form1.action="${path}/qna/delete.do";
			document.form1.submit();
		}
	});
	
	listAttach();
	
	$("#uploadedList").on("click",".file_del",function(e){
		var that=$(this);
		$.ajax({
			type: "post",
			url: "${path}/upload/deleteFile",
			data: {fileName: $(this).attr("data-src")},
			dataType: "text",
			success: function(result){
				if(result=="deleted"){
					that.parent("div").remove();
				}
			}
		});
	});
	
	$("#btnSave").click(function(){
		var str="";
		$("#uploadedList .file").each(function(i){
			str +=
			"<input type='hidden' name='files["+i+"]' value='"
			+$(this).val()+"'>";
		});
		$("#form1").append(str);
		document.form1.submit();
	});
	
	$(".fileDrop").on("dragenter dragover",function(e){
		e.preventDefault();
	});
	
	$(".fileDrop").on("drop",function(e){
		
		e.preventDefault();
		var files=e.originalEvent.dataTransfer.files;
		var file=files[0];
		var formData=new FormData();
		formData.append("file",file);
		
		$.ajax({
			url: "${path}/upload/uploadAjax",
			data: formData,
			dataType: "text",
			processData: false,
			contentType: false,
			type: "post",
			success: function(data){
				var fileInfo=getFileInfo(data);
				var html="<a href='"+fileInfo.getLink+"'>"+
					fileInfo.fileName+"</a><br>";
				html += "<input type='hidden' class='file' value='"
					+fileInfo.fullName+"'>";
				$("#uploadedList").append(html);
			}
		});
	});
});

function changeDate(date){
	date=new Date(parseInt(date));
	year=date.getFullYear();
	month=date.getMonth();
	day=date.getDate();
	strDate=
		year+"-"+month+"-"+day;
	return strDate;
}

function listReply2(){
	$.ajax({
		type: "get",
		contentType: "application/json",
		url: "${path}/reply/list_json.do?qno=${dto.qno}",
		success: function(result){
			var output="<table>";
			for(var i in result){
				var repl=result[i].replytext;
				repl=repl.replace(/\n/gi,"<br>");
				repl=repl.replace(/  /gi,"&nbsp;&nbsp;");
				repl=repl.replace(/</gi,"&lt;");
				repl=repl.replace(/>/gi,"&gt;");
				
				output += "<tr><td>"+result[i].name;
				date=changeDate(result[i].regdate);
				output += "("+date+")";
				output += "<br>"+repl+"</td></tr>";
			}
			output += "</table>";
			$("#listReply").html(output);
		}
	});
}

function listAttach() {
	$.ajax({
		type: "post",
		url: "${path}/qna/getAttach/${dto.qno}",
		success: function(list){
			$(list).each(function(){
				var fileInfo=getFileInfo(this);
				var html="<div><a href='"+fileInfo.getLink+"'>"
					+fileInfo.fileName+"</a>&nbsp;&nbsp;";
				<c:if test="${sessionScope.userid == 'userid'}">
				html+="<a href='#' class='file_del' data-src='"
					+this+"'>[삭제]</a></div>";
				</c:if>
				<c:if test="${sessionScope.adminid == 'adminid'}">
				html+="<a href='#' class='file_del' data-src='"
					+this+"'>[삭제]</a></div>";
				</c:if>
				$("#uploadedList").append(html);
			});
		}
	});
}
</script>

<style>
.fileDrop {
	width: 600px;
	height: 100px;
	border: 1px dotted black;
	background-color: white;
}
</style>
</head>



<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
 <!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

<%-- <c:if test="${sessionScope.adminid=='admin' }">
<%@ include file="../../include/admin_menu.jsp" %>

</c:if>
<c:if test="${sessionScope.userid!=null }">
<%@ include file="../../include/menu.jsp" %>

</c:if>
 --%>

<div class="container">
    <div class="row">
    
    <div class="col-md-8">
    
		<div class="container">
			<div class="row">
             
			<h2>질문 게시판</h2>
			<hr>
				<div class="well" style="width: 750px;">
				
				<form name="form1"  id="form1" class="form-inline" role="form" method="post" >

		
	<div class="form-group" >
     		 <label>제&nbsp;&nbsp;목&nbsp;</label>
      		<input name="title" id ="title" value="${dto.title}" class="form-control" style="width: 200px; margin-left: 30px;">
    </div>
		
		<br>
		
	<div class="form-group" style="margin-top: 10px;" >
     		 <label>작&nbsp;성&nbsp;자</label>
      		<input name="name" id ="name" value="${dto.adminname}${dto.username}" readonly class="form-control" style="width: 200px; margin-left: 20px;">
    </div>	
	
	
		
	<div class="form-group" style="width:700px; margin-top: 10px;" >
		<label>내&nbsp;용&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
		
		
		<textarea rows="20" cols="30"
			id="content" name="content">${dto.content}</textarea>
	</div>
	
	<div class="form-group" style ="margin-top: 10px;">
     		 <label>첨부파일을 등록하세요</label>
			<div class="fileDrop" style="width:700px;"></div>
			<div id="uploadedList"></div>
    </div>
	
	
	
	
		<div  class="form-group" style="width:700px; text-align:center; margin-bottom: 10px;">
		<input type="hidden" name="qno" value="${dto.qno}">
		
		
		
			<%-- <c:if test="${sessionScope.userid == dto.userid && sessionScope.adminid != 'admin'}">
   		<button type="button" id="btnUpdate">수정</button>
   <button type="button" id="btnDelete">삭제</button>
  </c:if>
  <c:if test="${sessionScope.adminid == 'admin' && sessionScope.userid != dto.userid}">
   <button type="button" id="btnUpdate">수정</button>
   <button type="button" id="btnDelete">삭제</button>
  </c:if>		 --%>
		
		
		<div align="center" style="margin-top: 10px;">
				<c:if test="${sessionScope.userid == dto.userid && sessionScope.adminid == null}">	
   					<button type="button" id="btnUpdate" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">수정</button>
					<button type="button" id="btnDelete" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">삭제</button>
			</c:if>
			<c:if test="${sessionScope.adminid == 'admin' && sessionScope.userid == null}">
   					<button type="button" id="btnUpdate" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">수정</button>
					<button type="button" id="btnDelete" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">삭제</button>
			</c:if>
			
   					<button type="button" id="btnList" class="btn btn-default" 
   					style="width: 50px; background-color: #428bca; color: white;">목록</button>
			
		</div>
		
	</div>
	
	
		<div class="form-group" style="width:700px; text-align:center;" >
     		 
     		 <c:if test="${sessionScope.adminid == 'admin'}">
				<textarea rows="5" cols="80" id="replytext"
					placeholder="댓글을 작성하세요" style="width:700px;"></textarea>
				
				<div align="center" style="margin-top:10px;">
   				<button type="button" id="btnReply" class="btn btn-default" 
   				style="width: 80px; background-color: #428bca; color: white;">댓글쓰기</button>
				</div>
				
			</c:if>
   	 	</div>

<div id="listReply"></div>


</form>

</div>
</div>
</div>


<!----------------------------------------		오른쪽 영역 끝		------------------------------------------>
</div>
</div>
</div>

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>

<%@ include file="../../include/footer.jsp"%>
</body>
</html>