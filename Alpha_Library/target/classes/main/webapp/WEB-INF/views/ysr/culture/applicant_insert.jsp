<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnWrite").click(function(){
		document.form1.action="${path}/applicant/insert.do";
		document.form1.submit();
	});
	$("#btnList").click(function(){
		location.href="${path}/culture/list.do";
	});
});
</script>
</head>
<style>
/* 사이드바 */ 
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 
 
<div class="container">
<div class="row">
<div class="col-md-8">
 
 
 
 

<h1>문화프로그램 신청</h1> <hr>
<form name="form1" id="form1" method="post">
	<input type="hidden" value="${dto.cno}" name="cno">
	<input type="hidden" value="${dto.userid}" name="userid">
	<div>강좌명 : ${dto.title}</div><br>
	<div>신청자 이름 : &nbsp;<input name="username" id="username" value="${dto.username}" readonly></div><br>
	<div class="row">
		<div class="col-xs-4 col-md-4">
			<label style="font-size: small;">성별 &nbsp;:&nbsp;</label>
			<label class="radio-inline" style="font-size: small;"> 
				<input type="radio" id="gender" name="gender" value="0" /> 남성
			</label>
			<label class="radio-inline" style="font-size: small;"> 
				<input type="radio" id="gender" name="gender" value="1" /> 여성
			</label>
		</div>
	</div><br>
	
	<div class="row">
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<select id="birthyear" name="birthyear"
				class="form-control input-lg" style="font-size: small;">
				<c:forEach var="row" begin="1920" end="2018">
					<option value="${row}">${row}년</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<select id="birthmonth" name="birthmonth"
				class="form-control input-lg" style="font-size: small;">
				<c:forEach var="row" begin="1" end="12">
					<option value="${row}">${row}월</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<select id="birthday" name="birthday"
				class="form-control input-lg" style="font-size: small;">
				<c:forEach var="row" begin="1" end="31">
					<option value="${row}">${row}일</option>
				</c:forEach>
			</select> <br>
		</div>
	</div>
	
	<%-- <div class="row">
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<select id="birthyear" name="birthyear"
				class="form-control input-lg">
				<c:forEach var="row" begin="1920" end="2018">
					<option value="${row}">${row}년</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<select id="birthmonth" name="birthmonth"
				class="form-control input-lg">
				<c:forEach var="row" begin="1" end="12">
					<option value="${row}">${row}월</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<select id="birthday" name="birthday"
				class="form-control input-lg">
				<c:forEach var="row" begin="1" end="31">
					<option value="${row}">${row}일</option>
				</c:forEach>
			</select> <br>
		</div>
	</div> --%>
	
	<div class="row">
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<select id="phone1" name="phone1" class="form-control input-lg" style="font-size: small;">
				<option value="010">010</option>
				<option value="011">011</option>
				<option value="016">016</option>
				<option value="019">019</option>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<input type="text" id="phone2" name="phone2" value=""
				class="form-control input-lg" placeholder="PHONE NUMBER" style="font-size: small;" />
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 20%; height: 33%;">
			<input type="text" id="phone3" name="phone3" value=""
				class="form-control input-lg" placeholder="" /> <br>
		</div>
	</div><br>
	
	<!-- <div class="row">
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<select id="phone1" name="phone1" class="form-control input-lg">
				<option value="010">010</option>
				<option value="011">011</option>
				<option value="016">016</option>
				<option value="019">019</option>
			</select>
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<input type="text" id="phone2" name="phone2" value=""
				class="form-control input-lg" placeholder="PHONE NUMBER" />
		</div>
		<div class="col-xs-4 col-md-4" style="float: left; width: 33%;">
			<input type="text" id="phone3" name="phone3" value=""
				class="form-control input-lg" placeholder="" /> <br>
		</div>
	</div> -->
	<div>
		<button type="button" id="btnWrite" >신청하기</button>
		<button type="button" id="btnList" >취소</button>
	</div>
</form>

	
<c:if test="${param.message == 'error' }">
		<script>
			alert("이미 신청하신 강좌입니다.");
		</script>
</c:if>



</div>
</div>
</div>



<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>