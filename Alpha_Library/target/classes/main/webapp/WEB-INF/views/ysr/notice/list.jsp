<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>정보마당:공지사항</title>
<%@ include file="../../include/header.jsp" %>
<script>
$(function(){
	$("#btnWrite").click(function(){
		location.href="${path}/notice/write.do";
	});
});
function list(page) {
	location.href="${path}/notice/list.do?curPage="+page;
}
</script>

</head>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

/* 찾기버튼 */
#custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 





<span style="font-size: 30px;">공지사항</span>
        <c:if test="${sessionScope.adminid != dto.adminid}">
		<span>
			<button type="button" id="btnWrite" >글쓰기</button>
		</span> 
		</c:if>
<hr>

<!--					 테이블 					-->
<div class="container" style="width: 900px;height: 0px;">
<div class="row">
<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
<div class="table-responsive">
	
              <table class="table table-bordred table-striped" >
              <thead>
                   <th style="padding-right: 0px;padding-left: 10px;">번호</th>
                   <th style="padding-right: 0px;padding-left: 10px;">제목</th>
                   <th style="padding-right: 0px;padding-left: 10px;">작성자</th>
                   <th style="padding-right: 0px;padding-left: 10px;">등록일</th>
                   <th style="padding-right: 0px;padding-left: 10px;">조회수</th>
              </thead>
 <c:forEach var="row" items="${map.list}"> 
<tbody>
<tr>
    <td style="padding-right: 0px;padding-left: 10px;">${row.nno}</td>
    <td style="padding-right: 0px;padding-left: 10px;"><a href="${path}/notice/view.do?nno=${row.nno}">
			${row.title}</a></td>
    <td style="padding-right: 0px;padding-left: 10px;">${row.adminname}</td>
	<td style="padding-right: 0px;padding-left: 10px;"><fmt:formatDate value="${row.regdate}"
			pattern="yyyy-MM-dd"/></td>
	<td style="padding-right: 0px;padding-left: 10px;">${row.viewcnt}</td>
</tr> 
    </tbody>
</c:forEach>
			</table>
<div align="center">
<ul class="pagination pull-center">
<%-- <c:if test="${map.pager.curPage > 1}">
<li><a href="#" onclick="list('1')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
</c:if> --%>
<%-- <c:if test="${map.pager.curBlock > 1}"> --%>
  <li><a href="#" onclick="list('${map.pager.prevPage}')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
<%-- </c:if>  --%>
  <c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
  						<li class="active"><a href="#" onclick="list('${num}')">${num}</a></li>
 					 </c:when>
  					<c:otherwise>
 						 <li><a href="#" onclick="list('${num}')">${num}</a></li>
  					</c:otherwise>
 				</c:choose>
 </c:forEach>
  <!-- <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li> -->
<%-- <c:if test="${map.pager.curBlock < map.pager.totBlock}"> --%>
  <li><a href="#" onclick="list('${map.pager.nextPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
<%-- </c:if> --%>
<%-- <c:if test="${map.pager.curPage < map.pager.totPage}">
<li><a href="#" onclick="list('${map.pager.totPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></a></li>
</c:if> --%>
</ul>
</div>
<!-- 				페이지 나누기 끝 				-->

<form name="form1" method="post"
	action="${path}/notice/list.do">
<div align="center">


<div class="col-md-2 col-md-offset-3">
            <select id="mySelect" class="form-control">
                <option value="title"
			<c:if test="${map.search_option == 'title'}">selected</c:if>
		>제목</option>
		<option value="content"
			<c:if test="${map.search_option == 'content'}">selected</c:if>
		>내용</option>
		<option value="all"
			<c:if test="${map.search_option == 'all' }">selected</c:if>
		>전체</option>
            </select>
        </div>

		<div class="col-md-5" style="right: 25px;">
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" name="keyword" value="${map.keyword}"
                    class="form-control input-lg" placeholder="검색어를 입력해주세요." 
                    style="padding-top: 6px; padding-bottom: 6px; height: 32px;"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        
 </div>       
 </form>
 
 
   
</div>
</div>
</div>
</div>
<!-- 					테이블 끝		 				-->  











<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>
</body>
</html>