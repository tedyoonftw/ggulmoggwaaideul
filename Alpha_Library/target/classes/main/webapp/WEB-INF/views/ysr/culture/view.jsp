<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css"
	rel="stylesheet">
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<!-- include summernote css/js -->
<link href="${path}/summernote/summernote.css" rel="stylesheet">
<script src="${path}/summernote/summernote.js"></script>
<script>
$(function(){
	$("#content").summernote({
		height: 300,
	    weight: 1200,
	    toolbar: "hide"
	});
	$("#btnList").click(function(){
		location.href="${path}/culture/list.do";
	});
	$("#btnUpdate").click(function(){
		location.href="${path}/culture/edit/${dto.cno}";
	});
	/* 이부분 */
	$("#btnApplicant").click(function(){
		document.form1.submit();
	});
});
</script>
<style type="text/css">
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}
</style>
</head>
<body>
<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>
<!-- 사이드바 -->
<div class="container">
<div class="row" style="height: 110%;">
<div class="col-md-2" style="height: 110%; margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 35px;">
                <span style="font-size: 20px; ">
                <br> 정보마당</span></a></li>
                <li><a href="${path}/notice/list.do"> 공지사항</a></li>
                <li><a href="${path}/qna/list.do"> 묻고 답하기</a></li>
                <li><a href="${path}/culture/list.do"> 문화 프로그램</a></li>
            </ul>
        </div>


      
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
 
<!--					 테이블 					-->

<h2>문화프로그램</h2>

<div class="container" style="width: 900px;height: 0px;">
<div class="row">
<div class="col-md-10" style="padding-left: 0px;padding-right: 0px;">
<div class="table-responsive">


<%-- 
<c:if test="${sessionScope.userid!=null  }">
<%@ include file="../../include/menu.jsp" %>
</c:if>
<c:if test="${sessionScope.userid==null && sessionScope.adminid==null}">
<%@ include file="../../include/menu.jsp" %>
</c:if>
 --%>
<form id="form1" name="form1" method="post" action="${path}/applicant/write.do">
<table class="table table-bordred">
	<input type="hidden" value="${sessionScope.username}">
	<tr>
		<th>강좌명</th>
		<td colspan="3">${dto.title}</td>
	</tr>
	<tr>
		<th>마감기간</th>
		<td><%-- <fmt:formatDate pattern="yyyy-MM-dd" value="${dto.duedate}"/> --%>
		${dto.duedate}
		</td>
		
		<th>신청현황</th>
		<td>${dto.cnt} / ${dto.max}</td>
	</tr>
	<tr>
		<th>강좌일시</th>
		<td><%-- <fmt:formatDate pattern="yyyy-MM-dd" value="${dto.sdate}"/> --%>
			${dto.sdate }
		</td>
		<th>참가비</th>
		<td>${dto.fee}</td>
	</tr>
	<tr>
		<th>대상</th>
		<td>${dto.target}</td>
		<th>장소</th>
		<td>${dto.place}</td>
	</tr>
	<tr>
		<td colspan="4"><textarea id="content" name="content"
				rows="20" cols="80">${dto.content}</textarea>
		</td>
	</tr>
</table>
<div style="width:700px; text-align:center;">
		<input type="hidden" name="cno" value="${dto.cno}">
		<input type="hidden" name="title" value="${dto.title}">
		<c:if test="${sessionScope.adminid == 'admin'}">
			<button type="button" id="btnUpdate">수정</button>
		</c:if>
		<c:if test="${sessionScope.userid != null}">
			<c:if test="${dto.cnt < dto.max}">
			<button type="button" id="btnApplicant">신청하기</button>
			</c:if>
		</c:if>
		<input type="button" id="btnList" value="목록">
	</div>
</form>




   
</div>
</div>
</div>
</div>
<!-- 					테이블 끝		 				-->  




<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>



</body>
</html>