<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file = "../../include/header.jsp" %>
<script>
$(function(){
	$("#btnDelete").click(function(){
		if(confirm("삭제하시겠습니까?")){
			document.form1.action="${path}/ym/membership/delete.do";
			document.form1.submit();
		}
	});
	$("#btnUpdate").click(function(){
		document.form1.action="${path}/ym/membership/update.do";
		document.form1.submit();
	});
});
</script>
</head>




<!-- 추가부분 -->
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<!-- 책테이블 -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


<style>
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}


/* 테이블 */
.art-title{color:#231f20; font-size:20px; font-weight:700;}
.artist-data{width:100%; padding-bottom: 25px;}
.artst-pic{width:33%;position: relative;}
.artst-pic span a{color: #fff; font-size: 16px; display: none;}
.artst-pic span.artst-like{position: absolute; left: 11%; bottom: 10px;}
.artst-pic span.artst-share{position: absolute; left:46%; bottom: 10px;}
.artst-pic span.artst-plus{position: absolute; right: 9%; bottom: 10px;}
.artst-prfle{width:63%;}
.artst-prfle span.artst-sub{font-size:15px; color:#bbb; float:left; width:100%; font-weight:normal; padding:5px 0;}
.artst-prfle span.artst-sub span.byname{font-weight:700; color:#aaa;}
.artst-prfle span.artst-sub span.daysago{float:right; font-size:12px;}
.counter-tab{float: left; width: 100%; padding-top: 45px;}
.counter-tab div{float: left; width: 33%; color: #aaa; font-size: 12px;}
.bot-links{float: left; width: 100%; padding-top: 10px;}
.bot-links a{display: inline-block; padding: 5px; background: #ccc; font-size: 12px; margin-bottom: 5px; color: #9c9c9c; text-decoration:none;}
span.play-icon{position: absolute; left: 31%; top: 32%; display: none;}
.artst-pic:hover img.play-icon, .artst-pic:hover span a{display: block; } 

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 40px;">
                <span style="font-size: 20px; ">
                <br> 관리자</span></a></li>
                <li><a href="${path}/ym/membership/list.do"> 회원정보관리</a></li>
                <li><a href="${path}/project/admin_readingroom/bigcalview.do"> 일정관리</a></li>
                <li><a href="${path}/project/admin_readingroom/admin_list.do"> 열람실관리</a></li>
                <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
                <li><a href="${path}/hjh/book/book/book_insert.do">도서등록</a></li>
                <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
                <li><a href="${path}/chart/book_list2.do">도서차트1</a></li>
                <li><a href="${path}/chart/book_list3.do">도서차트2</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>



<div class="container">
<div class="row">
<div class="col-md-8">





<%-- 
<form name="form1" id="form1" method="post">
<input type="hidden" name="userid" value="${dto.userid}">
<table border="1">
<tr>
<td>아이디</td>
<td>${dto.userid}</td>
<td>비밀번호</td>
<td><input type="password" name="passwd" value="${dto.passwd}"></td>
</tr>

<tr>
<td>이름</td>
<td><input name="username" value="${dto.username}"></td>
<td>생년월일</td>
<td>
<select name ="birthyear">
<c:forEach var="i" begin = "1900" end = "2010" step="1">
<option value="${i}">${i}</option>
</c:forEach>
<option value="${dto.birthyear}" selected>${dto.birthyear}</option>
</select>
/
<select name ="birthmonth">
<c:forEach var="i" begin = "1" end = "12" step="1">
<option value="${i}">${i}</option>
</c:forEach>
<option value="${dto.birthmonth}" selected>${dto.birthmonth}</option>
</select>
/
<select name ="birthday">
<c:forEach var="i" begin = "1" end = "31" step="1">
<option value="${i}">${i}</option>
</c:forEach>
<option value="${dto.birthday}" selected>${dto.birthday}</option>
</select></td>
</tr>

<tr>
<td>성별</td><td><select name="gender">
<option value="1" <c:if test="${dto.gender==1}">selected</c:if>>남</option>
<option value="2" <c:if test="${dto.gender==2}">selected</c:if>>여</option>
</select></td>
<td>연락처</td>
<td><select name="phone1">
<option value="010">010</option>
<option value="011">011</option>
<option value="016">016</option>
<option value="017">017</option>
<option value="018">018</option>
<option value="019">019</option>
<option value="${dto.phone1}" selected>${dto.phone1}</option>
</select>-<input name="phone2" value="${dto.phone2}">-<input name="phone3" value="${dto.phone3}"></td>
</tr>

<tr>
<td>이메일</td>
<td colspan="3"><input name="email1" value="${dto.email1}">@<input name="email2" value="${dto.email2}"></td>
</tr>

<tr>
<td>우편번호</td>
<td colspan="3"><input name="zipcode" value="${dto.zipcode}"></td>
</tr>
<tr>
<td>주소</td>
<td colspan="3"><input name="add1" value="${dto.add1}">&nbsp;<input name="add2" value="${dto.add2}"></td>
</tr>
</table>
<input type="button" id="btnUpdate" value="수정">
<input type="button" id="btnDelete" value="삭제">
</form> --%>





<div class="container" style="">
	<div class="row">
		<h2>회원정보 수정</h2>
		<div class="well"
		style="width: 680px;">
		
		
		
		    <form name="form1" class="form-inline" role="form">
		    <input type="hidden" name="userid" value="${dto.userid}">
	<div class="form-group" style="margin-left: 30px;">
	  <img <c:if test="${dto.gender==1}">src="${path}/images/index/nam.jpg"</c:if>
	  	   <c:if test="${dto.gender==2}">src="${path}/images/index/mi.jpg"</c:if>
	  class="form-control" style="width: 150px;  height:120px; margin-left: 20px;">
   </div>
	
	<br>
	<br>
	
	
	
		    
   <div class="form-group" style="margin-left: 30px;">
      <label for="name">아&nbsp;이&nbsp;디&nbsp;</label>
	  <input value="${dto.userid}" class="form-control" style="width: 100px; margin-left: 20px;">
   </div>
   
   <div class="form-group" style="margin-left: 50px;">
      <label>비밀번호</label>
      <input type="password" name="passwd" value="${dto.passwd}" class="form-control" style="width: 100px; margin-left: 20px;">
    </div>
   
    <br>
    <br>
    
    <div class="form-group" style="margin-left: 30px;">
      <label>&nbsp;이  &nbsp;&nbsp;&nbsp;름&nbsp;&nbsp;</label>
      <input name="username" value="${dto.username}" class="form-control" style="width: 100px; margin-left: 20px;">
    </div>
    
    
    
    
    
    
    
    
    <div class="form-group" style="margin-left: 50px;">
      <label>생년원일</label>
      <select name ="birthyear"  class="form-control" style="margin-left: 20px;">
        <c:forEach var="i" begin = "1900" end = "2010" step="1">
		<option value="${i}">${i}</option>
		</c:forEach>
		<option value="${dto.birthyear}" selected>${dto.birthyear}</option>
      </select>
    </div>
    
    <div class="form-group" style="margin-left: 10px;">
      <label> / </label>
      <select name ="birthmonth" class="form-control" style="margin-left: 10px;">
        <c:forEach var="i" begin = "1" end = "12" step="1">
		<option value="${i}">${i}</option>
		</c:forEach>
		<option value="${dto.birthmonth}" selected>${dto.birthmonth}</option>
     </select>
    </div>
	
	<div class="form-group" style="margin-left: 10px;">
      <label> / </label>
      <select name ="birthday" class="form-control" style="margin-left: 10px;">
        <c:forEach var="i" begin = "1" end = "31" step="1">
		<option value="${i}">${i}</option>
		</c:forEach>
		<option value="${dto.birthday}" selected>${dto.birthday}</option>
     </select>
    </div>
    
    <br>
    <br>
    
    <div class="form-group" style="margin-left: 30px;">
      <label>&nbsp;성 &nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;  </label>
      <select name="gender" class="form-control" style="margin-left: 20px;">
        <option value="1" <c:if test="${dto.gender==1}">selected</c:if>>남</option>
		<option value="2" <c:if test="${dto.gender==2}">selected</c:if>>여</option>lue="${dto.birthmonth}" selected>${dto.birthmonth}</option>
     </select>
    </div>
    
    <div class="form-group" style="margin-left: 90px;">
      <label>연&nbsp;락&nbsp;처&nbsp;</label>
      <select name="phone1" class="form-control" style="margin-left: 20px;">
        <option value="010">010</option>
		<option value="011">011</option>
		<option value="016">016</option>
		<option value="017">017</option>
		<option value="018">018</option>
		<option value="019">019</option>
		<option value="${dto.phone1}" selected>${dto.phone1}</option>
      </select>
    </div>
    
    <div class="form-group" style="margin-left: 10px;">
      <label> - </label>
      <input name="phone2" value="${dto.phone2}" class="form-control" style="width: 70px; margin-left: 10px;">
    </div>
    
    <div class="form-group" style="margin-left: 10px;">
      <label>-</label>
      <input name="phone3" value="${dto.phone3}" class="form-control" style="width: 70px; margin-left: 10px;">
    </div>
    
    <br>
    <br>
    
    <div class="form-group" style="margin-left: 30px;">
      <label>이 메 일 &nbsp;</label>
      <input name="email1" value="${dto.email1}" class="form-control" style="width: 150px; margin-left: 20px;">
    </div>
    <div class="form-group" style="margin-left: 10px;">
      <label> @ </label>
      <input name="email2" value="${dto.email2}" class="form-control" style="width: 150px; margin-left: 10px;">
    </div>
    
    <br>
    <br>
    
    <div class="form-group" style="margin-left: 30px;">
      <label>우편번호</label>
      <input name="zipcode" value="${dto.zipcode}" class="form-control" style="width: 200px; margin-left: 20px;">
    </div>
    
    <br>
    <br>

	<div class="form-group" style="margin-left: 30px;">
      <label>&nbsp;주  &nbsp;&nbsp;&nbsp;소&nbsp;&nbsp;  </label>
      <input name="add1" value="${dto.add1}" class="form-control" style="width: 250px; margin-left: 20px;">
    </div>
    
    <div class="form-group" style="margin-left: 0;">
      <label></label>
      <input name="add2" value="${dto.add2}" class="form-control" style="width: 200px; margin-left: 10px;">
    </div>
    
    <br>
    <br>
    <div align="center">
   <button type="button" id="btnUpdate" class="btn btn-default" 
   style="width: 100px; background-color: #428bca; color: white;">수정</button>
   &nbsp;&nbsp;&nbsp;
   <button type="button" id="btnDelete" class="btn btn-default" 
   style="width: 100px; background-color: #428bca; color: white;">삭제</button>
	</div>


</form>

</div>
</div>
</div>





</div>
</div>
</div>










<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>