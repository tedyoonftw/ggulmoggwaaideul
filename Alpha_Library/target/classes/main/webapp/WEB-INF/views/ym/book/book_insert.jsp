<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file = "../../include/header.jsp" %>
<script>

	$(function() {
		$("#btnGo").click(function() {

			var book_code = $("#book_code").val();
			if(book_code==""){
				alert("도서 코드를 입력해 주십시오.");
				$("#book_code").focus();
				return;
			}

			if (confirm("등록하시겠습니까?")) {

				document.form1.action = "${path}/ym/book/list.do";
				document.form1.submit();
			}
		});
	});
</script>
</head>






<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>

<script src="${path}/include/js/jquery-3.2.1.min.js"></script>
<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 40px;">
                <span style="font-size: 20px; ">
                <br> 관리자</span></a></li>
                <li><a href="${path}/ym/membership/list.do"> 회원정보관리</a></li>
                <li><a href="${path}/project/admin_readingroom/bigcalview.do"> 일정관리</a></li>
                <li><a href="${path}/project/admin_readingroom/admin_list.do"> 열람실관리</a></li>
                <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
                <li><a href="${path}/hjh/book/book/book_insert.do">도서등록</a></li>
                <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
                <li><a href="${path}/chart/book_list2.do">도서차트1</a></li>
                <li><a href="${path}/chart/book_list3.do">도서차트2</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>
<div class="container">
<div class="row">

<div class="col-md-8">
<div class="container">
<div class="row">

		<h2>도서 등록</h2>
		<div class="well"
		style="width: 750px;">
		
		<form name="form1" class="form-inline" role="form" method="post" >
		    
		<div class="form-group" style="margin-left: 30px; ">
    		  <label>부&nbsp;&nbsp;록&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
   			  <select name="AppendixYn" class="form-control" style="margin-left: 20px;">
    		  <option value="Y" selected>있음</option>
			  <option value="N">없음</option>
			  </select>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		 </div>

		<div class="form-group" style="margin-left: 50px;">
     		 <label>표&nbsp;&nbsp;제&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
      		<input name="title" class="form-control" style="width: 250px; margin-left: 20px;">
    	</div>

		 <div class="form-group" style="margin-left: 30px; margin-top: 15px;" >
     		 <label>저&nbsp;&nbsp;자&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  		    <input name="author" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>   
		    
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
     		 <label>책&nbsp;코&nbsp;드&nbsp;<span style="color:red;">(*) 필수 입력</span></label>
	 		 <input name="book_code" id="book_code" class="form-control" style="width: 180px; margin-left: 20px;">
  		 </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>청구기호&nbsp;</label>
    		  <input name="book_no" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>
  		  
  		   <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>제어번호&nbsp;&nbsp;&nbsp;&nbsp;</label>
    		  <input name="controlNo" class="form-control" style="width: 240px; margin-left: 20px;">
  		  </div>
  		  
		    <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>낱권ISBN</label>
    		  <input name="isbn" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>
		  
		   <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>소장도서관(이름)</label>
    		  <input name="libName" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>소장도서관(관리구분)</label>
    		  <input name="manageCode" class="form-control" style="width: 125px; margin-left: 20px;">
  		  </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>발행년도&nbsp;&nbsp;&nbsp;&nbsp;</label>
    		  <input name="pubYear" class="form-control" style="width: 237px; margin-left: 20px;">
  		  </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
     		 <label>발&nbsp;행&nbsp;자&nbsp;&nbsp;&nbsp;</label>
	 		 <input name="publisher" class="form-control" style="width: 200px; margin-left: 20px;">
  		 </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>등록번호&nbsp;&nbsp;&nbsp;</label>
    		  <input name="regNo" class="form-control" style="width: 240px; margin-left: 20px;">
  		  </div>

			<div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>결과순번&nbsp;</label>
    		  <input name="rnum" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>

		<div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>소장자료설명</label>
    		  <input name="shelfLocName" class="form-control" style="width: 240px; margin-left: 5px;">
  		  </div>

		<div class="form-group" style="margin-left: 30px; margin-top: 15px;">
    		  <label>레코드키&nbsp;</label>
    		  <input name="speciesKey" class="form-control" style="width: 200px; margin-left: 20px;">
  		  </div>

		   <div class="form-group" style="margin-left: 30px; margin-top: 15px;">
     		 <label>책&nbsp;소&nbsp;개&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
     		 <textarea name="description"></textarea>
  		 </div>
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px; ">
     		 <label>대&nbsp;분&nbsp;류&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
     		 <select name="group_code" class="form-control" >
     		 	<option value="000" selected>기타</option>
				<option value="100">종교</option>
				<option value="200">사회과학</option>
				<option value="300">자연과학</option>
				<option value="400">기술과학</option>
				<option value="500">예술</option>
				<option value="600">언어</option>
				<option value="700">문학</option>
				<option value="800">역사</option>
				<option value="900">철학</option>
     		 </select>
		  </div>
		  
	<!-- 	  	<div class="form-group" style="margin-left: 30px; ">
    		  <label>부&nbsp;&nbsp;록&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
   			  <select name="AppendixYn" class="form-control" style="margin-left: 20px;">
    		  <option value="Y" selected>있음</option>
			  <option value="N">없음</option>
			  </select> -->
		  
		  
		  <div class="form-group" style="margin-left: 30px; margin-top: 15px; ">
     		<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     					도서상태&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
		  	<select name="book_tf" class="form-control">
		  		<option value="1" selected>있음</option>
				<option value="0">없음</option>
		  	</select>
		  </div>
		  	
			<div align="center" style="margin-top: 30px;">
   				<button type="button" id="btnGo" class="btn btn-default" 
   				style="width: 100px; background-color: #428bca; color: white;">등록</button>
			</div>
			
		</form>
</div>
</div>
</div>


		
</div>
</div>
</div>		
		

<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>