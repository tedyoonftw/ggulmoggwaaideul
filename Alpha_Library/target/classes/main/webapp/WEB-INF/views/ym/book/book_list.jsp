<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file = "../../include/header.jsp" %>

<script>
	function list(page) {
		location.href = "${path}/ym/adminbook/list.do?curPage=" + page;
	}
</script>
</head>
<body>
<%@ include file = "../../include/menu.jsp" %>
<h2>대출도서 리스트</h2>
${map.count}권
<form name="form1" method="post" action="${path}/ym/adminbook/list.do">
	<select name="search_option">
		
		<option value="author" <c:if test="${map.search_option == 'author'}">selected</c:if>>저자명</option>
		<option value="title"<c:if test="${map.search_option == 'title'}">selected</c:if>>도서명</option>
		<option value="all" <c:if test="${map.search_option == 'all'}">selected</c:if>>저자명/도서명</option>
	
	</select>
	<input name="keyword" value="${map.keyword }">
	<input type="submit" value="조회">
	</form>
<table border="1">
<tr>
<th>도서명</th>
<th>대분류</th>
<th>저자</th>
<th>출판사</th>
<th>대출일</th>
<th>대출/반납여부</th>
<th>대출자</th>
</tr>
<c:forEach var="dto" items="${map.list}">
<tr>
<td>${dto.title}</td>
<td>${dto.group_name}</td>
<td>${dto.author}</td>
<td>${dto.publisher}</td>
<td><fmt:formatDate value="${dto.rent_date}" pattern="yyyy-MM-dd" /></td>
<td>		
	<c:choose>
		<c:when test="${dto.halfnab=='N'}">대출 중</c:when>
		<c:when test="${dto.halfnab=='Y'}">반납완료</c:when>
	</c:choose>
</td>
<td>${dto.username }</td>
</tr>
</c:forEach>
<tr>
			<td colspan="7" align="center">
			<c:if test="${map.pager.curBlock>1}">
			<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curPage>1}">
			<a href="#" onclick="list('${map.pager.prevPage}')">[이전]</a>
			</c:if>
			<c:forEach var="num" begin="${map.pager.blockBegin}"
					end="${map.pager.blockEnd}">
					<c:choose>
						<c:when test="${num==map.pager.curPage }">
						<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
							<span style="color: red;">${num}</span>
						</c:when>
						<c:otherwise>
							<a href="#" onclick="list('${num}')">${num}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			<c:if test="${map.pager.curBlock<map.pager.totBlock}">
			<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage<map.pager.totPage}">
			<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>	
			</td>
		</tr>
</table>
</body>
</html>