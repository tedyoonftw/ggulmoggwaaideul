<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file = "../../include/header.jsp" %>
<script>

	function list(page) {
		location.href = "${path}/ym/membership/list.do?curPage=" + page;
	}
</script>
</head>

<style>
/* 사이드바 */
@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
body{margin-top:20px;}
.fa-fw {width: 2em;}

</style>

<body>

<%@ include file="../../include/menu.jsp"%>
<%@ include file="../../include/menu1.jsp"%>



<!-- 사이드바 -->
<div class="container">
    <div class="row" style="
    height: 110%;">
        <div class="col-md-2" style="
        height: 110%;
        margin-right: 50px;">
            <ul class="nav nav-pills nav-stacked" style="height: 600px;">
                <li class="active"><a href="#"
                style="height: 105px;  width: 150px; padding-left: 40px;">
                <span style="font-size: 20px; ">
                <br> 관리자</span></a></li>
                <li><a href="${path}/ym/membership/list.do"> 회원정보관리</a></li>
                <li><a href="${path}/project/admin_readingroom/bigcalview.do"> 일정관리</a></li>
                <li><a href="${path}/project/admin_readingroom/admin_list.do"> 열람실관리</a></li>
                <li><a href="${path}/culture/list.do">문화프로그램관리</a></li>
                <li><a href="${path}/hjh/book/book/book_insert.do">도서등록</a></li>
                <li><a href="${path}/hjh/mypage/admin_wishlist.do">희망도서 리스트</a></li>
                <li><a href="${path}/chart/book_list2.do">도서차트1</a></li>
                <li><a href="${path}/chart/book_list3.do">도서차트2</a></li>
            </ul>
        </div>

        
<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>



<div class="container">
<div class="row">
<div class="col-md-8">


<h2> 회원관리</h2>
<hr>
<div class="container">
<div class="row">
<div class="col-md-5" style="padding-left: 0px;">
                    
     				<form class="form"
                    name="form1" method="post" action="${path}/ym/membership/list.do">
                        <div class="form-group">
                            <div class="input-group">
                            
                                <span class="input-group-addon" id="month-addon">회원검색 </span>
                                <select name="search_option" class="form-control" aria-describedby="month-addon">
        
        <option value="username"
		<c:if test="${map.search_option == 'username'}">selected</c:if>>이름</option>
		<option value="userid"<c:if test="${map.search_option == 'userid'}">selected</c:if>>ID</option>
		<option value="all" <c:if test="${map.search_option == 'all'}">selected</c:if>>이름/ID</option>
	
                                </select>
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                                
                                <input name="keyword" value="${map.keyword}"
                                class="form-control" aria-describedby="month-addon">
                                
                                <span style="background-color: white; border-top-color: white; border-bottom-color: white;
                                padding-right: 0; padding-left: 0;" 
                                class="input-group-addon" id="month-addon"></span>
                               	<input type="submit" value="조회" name="control-month" style="background-color: #428bca; color: white;"
                               	class="form-control" aria-describedby="month-addon">
                           
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>


<%-- 
<h2>회원관리</h2>
<!-- 검색 폼 -->
	<form name="form1" method="post" action="${path}/ym/membership/list.do">
	<select name="search_option">
		
		<option value="username"
		<c:if test="${map.search_option == 'username'}">selected</c:if>
		>이름</option>
		<option value="userid"<c:if test="${map.search_option == 'userid'}">selected</c:if>>ID</option>
		<option value="all" <c:if test="${map.search_option == 'all'}">selected</c:if>>이름/ID</option>
	
	</select>
	<input name="keyword" value="${map.keyword }">
	<input type="submit" value="조회">
	</form>
 --%>	
	
	
	<!-- 검색 폼 끝 -->
<%-- 	<br> <span style="color: red; font-size: 20px;">${map.count}</span> 명의 회원이 있습니다.
<table border="1">
<tr>
<th>아이디</th>
<th>이름</th>
<th>생년월일</th>
<th>성별</th>
<th>연락처</th>
<th>이메일</th>
<th>주소</th>
<th>가입일</th>
</tr>
<c:forEach var="row" items="${map.list}">
<tr>
<td><a href="${path}/ym/membership/view.do?userid=${row.userid}"> ${row.userid} </a></td>
<td>${row.username}</td>
<td>${row.birthyear}/${row.birthmonth}/${row.birthday}</td>
<td>
<c:choose>
<c:when test="${row.gender==1}">남</c:when>
<c:when test="${row.gender==2}">여</c:when>
</c:choose>
</td>
<td>${row.phone1}-${row.phone2}-${row.phone3}</td>
<td>${row.email1}@${row.email2}</td>
<td>${row.add1}&nbsp;${row.add2}</td>
<td><fmt:formatDate value="${row.join_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
</tr>
</c:forEach>
<!-- 페이지 네비게이션 출력 -->
		<tr>
			<td colspan="8" align="center">
			<c:if test="${map.pager.curBlock>1}">
			<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curPage>1}">
			<a href="#" onclick="list('${map.pager.prevPage}')">[이전]</a>
			</c:if>
			<c:forEach var="num" begin="${map.pager.blockBegin}"
					end="${map.pager.blockEnd}">
					<c:choose>
						<c:when test="${num==map.pager.curPage }">
						<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
							<span style="color: red;">${num}</span>
						</c:when>
						<c:otherwise>
							<a href="#" onclick="list('${num}')">${num}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			<c:if test="${map.pager.curBlock<map.pager.totBlock}">
			<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage<map.pager.totPage}">
			<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>	
			</td>
		</tr>
</table>
 --%>




<!-- 			테이블				 -->
<div class="container">
<div class="row">
        
<div class="col-md-8" style="padding-left: 0px;padding-right: 0px;">
<div class="table-responsive">
<!-- 			테이블				 -->


              <table id="mytable" class="table table-bordred table-striped" >
              <thead>
                   <th>&nbsp;</th>
                   <th>아이디</th>
                   <th>이름</th>
                   <th>생년월일</th>
                   <th>성별</th>
                   <th>연락처</th>
                   <th>이메일</th>
                   <!-- <th>주소</th> -->
                   <th>가입일</th>
              </thead>
                  
<c:forEach var="row" items="${map.list}"> 
                   
            
    <tbody>
    
<tr>
	<td>&nbsp;</td>
    <td><a href="${path}/ym/membership/view.do?userid=${row.userid}"><span style="color: blue;">${row.userid}</span></a></td>
	<td>${row.username}</td>
	<td>${row.birthyear}/${row.birthmonth}/${row.birthday}</td>
	<td>
		<c:choose>
		<c:when test="${row.gender==1}">남</c:when>
		<c:when test="${row.gender==2}">여</c:when>
		</c:choose>
	</td>
	<td>${row.phone1}-${row.phone2}-${row.phone3}</td>
	<td>${row.email1}@${row.email2}</td>
	<%-- <td>${row.add1}&nbsp;${row.add2}</td> --%>
	<td><fmt:formatDate value="${row.join_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
</tr> 
    </tbody>
</c:forEach>
			</table>


<!-- 			페이지 나누기			 -->
	<div class="clearfix">
		
	</div>


<div align="center">
<ul class="pagination pull-right">
  <li><a href="#" onclick="list('${map.pager.prevPage}')"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
  <c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
  						<li class="active"><a href="#" onclick="list('${num}')">${num}</a></li>
 					 </c:when>
  					<c:otherwise>
 						 <li><a href="#" onclick="list('${num}')">${num}</a></li>
  					</c:otherwise>
 				</c:choose>
 </c:forEach>
  <li><a href="#" onclick="list('${map.pager.nextPage}')"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
</ul>
</div>
<!-- 				페이지 나누기 끝 				-->





<!-- 			테이블				 -->
</div>
</div>
</div>
</div>


<!-- 			테이블				 -->





  
  
  
 </div>
 </div>
 </div>
  
<!----------------------------------------		콘텐트  영역	 끝	------------------------------------------>
</div>
</div>


<%@ include file="../../include/footer.jsp"%>


</body>
</html>