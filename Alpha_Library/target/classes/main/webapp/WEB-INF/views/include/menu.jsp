<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- views/include/menu.jsp -->
<%@ taglib prefix="c" 
uri="http://java.sun.com/jsp/jstl/core" %>
<style>
/* html, body{ height:100%; margin:0px; } */


html, body{ height:100%; width: 100%;}



a:link {color: black;}
a:visited {color: black;}
a:hover {color: blue; text-decoration: none;}
a:active {color: black; text-decoration: none;}
</style>
<div style="text-align:right; background-color: #428bca; height: 22px;">
	<div style="margin-right: 150px;">
		<a href="${path}/"><span style="color: white;">홈 |</span></a>
	<c:choose>
		<c:when test="${sessionScope.userid == null && sessionScope.adminid == null}">
			<a href="${path}/pu/login/user/login.do"><span style="color: white;">로그인 |</span></a>
		</c:when>
		<c:when test="${sessionScope.userid != null && sessionScope.adminid == null}">
		<a href="${path}/hjh/mypage/nowrent.do">마이페이지</a> |
			<a href="${path}/pu/login/user/logout.do"><span style="color: white;">${sessionScope.name} 님(로그아웃) |</span></a>
		</c:when>
	</c:choose>
	<c:choose>
		<%-- <c:when test="${sessionScope.adminid == null && sessionScope.userid == null}">
			<a href="${path}/pu/login/admin/login.do"><span style="color: white;">  관리자 로그인</span></a>
		</c:when> --%>
		<c:when test="${sessionScope.adminid != null && sessionScope.userid == null}">
			<span style="color: white;">${sessionScope.name}님이 로그인중입니다.</span>
			<a href="${path}/pu/login/admin/logout.do"><span style="color: white;"> | 관리자 로그아웃 | </span></a>
		</c:when>
	</c:choose>
	<c:if test="${sessionScope.userid == null}">
			<a href="${path}/project/email/agree.do"><span style="color: white;">  회원가입 |</span></a>
	</c:if>
			<a href="#"><span style="color: white;">사이트맵</span></a>
</div>
</div>













