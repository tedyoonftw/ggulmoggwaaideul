<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../../include/header.jsp" %>
</head>
<script>
$(function(){
	
	$("#btnin").click(function(){
		var receiveMail=$("#receiveMail").val();
		if(receiveMail == ""){
			alert("이메일을 입력해주세요.");
			$("#receiveMail").focus(); //입력 포커스 이동
			return; //함수 종료
		} else {
			document.form1.submit();
		}
	});
	
	 $("#join").click(function(){
		var receiveMail=$("#receiveMail").val();
		if(receiveMail == ""){
			alert("이메일을 입력해주세요.");
			$("#receiveMail").focus(); //입력 포커스 이동
				return; //함수 종료
		} else {	
		opener.location.href = "${path}/project/email/sendMail.do?receiveMail="+receiveMail;
		window.close();
		}
	});
});

</script>
<body style="height: 200px; margin-left: 40px; margin-top: 40px;">
<h2>이메일 본인인증</h2>
<form name="form1" method="post" action="${path}/project/email/send.do">
<br>
이메일 : <input name="receiveMail" id="receiveMail" value="${receiveMail}">
<input type="button" id="btnin" value="인증하기" >
</form>
<span style="color:red; font-size: 14px;">${message}</span><br>
<input type="hidden" id="number" value="${number}">
<div id="result" style="color: red;"></div><br>
<input type="button" id="join" value="확인" 
style="background-color:#3c3cf2;width: 296px; 
margin-left: 10px; height: 30px;">
</body>
</html>













