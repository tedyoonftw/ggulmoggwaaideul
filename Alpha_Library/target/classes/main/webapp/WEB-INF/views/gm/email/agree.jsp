<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>로그인</title>

<%@ include file="../../include/header.jsp"%>

<link href="${path}/include/gm/agree.css" rel="stylesheet" >

<script>
$(function(){
	$("#btnLogin").click(function(){
		var select1=$("#select1");
		var select2=$("#select2");
		
		if (!select1.is(":checked")){
			alert("이용약관에 대한 동의 후 인증해 주세요.")
			return;
		}
		
		if (!select2.is(":checked")){
			alert("개인정보 수집에 대한 동의 후 인증해 주세요.")
			return;
		}
		OpenWin("${path}/project/email/write.do",400, 300);
	});
});
</script>
</head>

<body>
	<%@ include file="../../include/menu.jsp"%>
	<%@ include file="../../include/menu1.jsp"%>
	<%@ include file="../../include/sidebar/twodragon.jsp"%>
	
	

<!----------------------------------------		콘텐트  영역	 시작		------------------------------------------>

	

				<div class="col-md-8 login-sec" style="padding-top: 0px;">
					<h1 style="color: blue;"><b>미래 도서관</b></h1><br>
<div >					
						<div class="form-group">
						<div align="left">
						<h5>이용약관 동의(필수) <input type="checkbox" name="select" id="select1" value="check1"></h5>
						</div>
							<textarea rows="5" cols="100" name="memo" id="memo">
제 1 조 (목적)
이 약관은 네이버 주식회사 ("회사" 또는 "네이버")가 제공하는 네이버 및 네이버 관련 제반 서비스의 이용과 관련하여 회사와 회원과의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정함을 목적으로 합니다.

제 2 조 (정의)
이 약관에서 사용하는 용어의 정의는 다음과 같습니다.

①"서비스"라 함은 구현되는 단말기(PC, TV, 휴대형단말기 등의 각종 유무선 장치를 포함)와 상관없이 "회원"이 이용할 수 있는 네이버 및 네이버 관련 제반 서비스를 의미합니다.
②"회원"이라 함은 회사의 "서비스"에 접속하여 이 약관에 따라 "회사"와 이용계약을 체결하고 "회사"가 제공하는 "서비스"를 이용하는 고객을 말합니다.
③"아이디(ID)"라 함은 "회원"의 식별과 "서비스" 이용을 위하여 "회원"이 정하고 "회사"가 승인하는 문자와 숫자의 조합을 의미합니다.
④"비밀번호"라 함은 "회원"이 부여 받은 "아이디와 일치되는 "회원"임을 확인하고 비밀보호를 위해 "회원" 자신이 정한 문자 또는 숫자의 조합을 의미합니다.
⑤"유료서비스"라 함은 "회사"가 유료로 제공하는 각종 온라인디지털콘텐츠(각종 정보콘텐츠, VOD, 아이템 기타 유료콘텐츠를 포함) 및 제반 서비스를 의미합니다.
⑥"포인트"라 함은 서비스의 효율적 이용을 위해 회사가 임의로 책정 또는 지급, 조정할 수 있는 재산적 가치가 없는 "서비스" 상의 가상 데이터를 의미합니다.
⑦"게시물"이라 함은 "회원"이 "서비스"를 이용함에 있어 "서비스상"에 게시한 부호ㆍ문자ㆍ음성ㆍ음향ㆍ화상ㆍ동영상 등의 정보 형태의 글, 사진, 동영상 및 각종 파일과 링크 등을 의미합니다.
</textarea><br>

						</div>
						
						<div class="form-group">
						<div align="left">
						<h5>개인정보 수집에 대한 안내(필수) <input type="checkbox" name="select" id="select2" value="check2"></h5>
						</div>
							<textarea rows="5" cols="100" name="memo" id="memo">
정보통신망법 규정에 따라 네이버에 회원가입 신청하시는 분께 수집하는 개인정보의 항목, 개인정보의 수집 및 이용목적, 개인정보의 보유 및 이용기간을 안내 드리오니 자세히 읽은 후 동의하여 주시기 바랍니다.
1. 수집하는 개인정보
이용자는 회원가입을 하지 않아도 정보 검색, 뉴스 보기 등 대부분의 네이버 서비스를 회원과 동일하게 이용할 수 있습니다. 이용자가 메일, 캘린더, 카페, 블로그 등과 같이 개인화 혹은 회원제 서비스를 이용하기 위해 회원가입을 할 경우, 네이버는 서비스 이용을 위해 필요한 최소한의 개인정보를 수집합니다.
회원가입 시점에 네이버가 이용자로부터 수집하는 개인정보는 아래와 같습니다.

</textarea><br><br>
						</div>
						
						<div align="center">
						<div style="border:0.5px solid; height: 140px; width: 600px; border-color: gray;
						padding-top: 5px;"
						align="center">
							<span style="font-size: 18px;">이메일 인증</span><br>
							<span style="font-size: 14px;">가입자의 본인 명의의 이메일로 본인여부를 확인합니다.<br>
아래 버튼을 눌러 인증을 진행하시기 바랍니다.</span>
							<input type="button" id="btnLogin" width="300px;"
							style="margin-top: 10px;height: 34px;width: 380px;"
								class="btn btn-login float-center" value="인증">
						</div>
						</div>


				</div>
</div>				


<%@ include file="../../include/footer.jsp"%>
</body>
<!-- 윈도우 창 띄우기 --> 
<script type="text/javascript">
/* function login() {
	OpenWin("${path}/project/email/write.do",400, 300);
} */

function OpenWin(URL, width, height) {
	var str, width, height;
	str = "'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,";
	str = str + "width=" + width;
	str = str + ",height=" + height + "',top=50,left=50";
	window.open(URL, 'emailconfirm', str);
}
</script>
</html>